#!/bin/bash

#@
#@ USAGE  : ts_detrend.sh -f <tserie> -o <output_type> <options>
#@
#@ TASK   : Detrend time serie
#@
#@ OPTIONS: -o <output_type>  - solution | residual
#@          -v                - verbose mode
#@          -h                - help
#@
#@ NOTE   : - <tserie> could be multiple filenames separated by spaces 
#@            and use double quotes "tserie_1 tserie_2 ... tserie_n"
#@          - The input time serie(s) have to have 2, 4 or 7 columns
#@
#@ OUTPUT :
#@          - <tserie>-detrended.txt
#@            this is the detrended output file
#@
#@          - ts_detrend.log
#@            this logfile contains how many record was removed from <tserie>
#@
#* by     : marcell dot ferenc dot uni at gmail dot com

## test GMT installation
type gmtmath &> /dev/null
if [ $? -ne 0 ]; then echo "GMT is needed for $0 script"; exit 2; fi

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir .gmt* gmt.conf gmt.history 2> /dev/null
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
. $HOME/bin/func/general.func
. $HOME/bin/func/ts.func

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## default variable
log=$HOME/log/$scriptname.log
v=no
LC_NUMERIC=C ## for decimal separator in floating numbers
#w=0.6
w=0

mkdir -p $HOME/log

## list of options the program will accept
optstring=f:o:dvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  d) set -x ;;
  f) files=( $OPTARG ) ;;
  o) output=$OPTARG ;;
  d) debug=yes; set -x ;;
  v) v=yes ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## test files
if [ -z "$files" ]; then
 color_txt bold red "-f [file(s)] option is required"; exit 2
else
 for file in ${files[@]}; do
  if [ -s $file ]; then
   info=( $( istab $file ) )
   if [ "${info[0]}" == "is_data_table" ]; then
    case "${info[1]}" in
     2|4|7) is_item+=( "$file" ); ts_convert_date.sh -o iso $file
            [ "$v" == "yes" ] && color_txt bold green "$file - exist" ;;
         *) no_item+=( "$file" )
            [ "$v" == "yes" ] && color_txt bold red "$file - format error" ;; 
    esac
   else
    no_item+=( "$file" )
    [ "$v" == "yes" ] && color_txt bold red "$file - not a data table"; fi
  else
   no_item+=( "$file" )
   [ "$v" == "yes" ] && color_txt bold red "$file - does not exist or empty"; fi
  if [ -z "$is_item" ]; then
  color_txt bold red "not accepted input: ${no_item[@]}"; exit 2; fi
 done  
fi

case $output in
 solution|residual) ;;
 *) color_txt bold red "${0##*/}: error: missing option: -o <output_type>"; exit 2 ;;
esac
 
## remove logfile if exists due to previous run
/bin/rm -f $log 2> /dev/null

## gmt settings
gmt_vers=$( sample1d &> $tmp.gmt; awk 'NR==1 { print substr($2,1,1) }' $tmp.gmt )
case $gmt_vers in
 4) gmtset INPUT_CLOCK_FORMAT=hh:mm:ss; gmtset INPUT_DATE_FORMAT=yyyy-mm-dd ;;
 5) gmtset FORMAT_CLOCK_IN=hh:mm:ss; gmtset FORMAT_DATE_IN=yyyy-mm-dd;;
esac

## loop - existing input
for file in ${is_item[@]}
do

 case $output in
  solution) fout=${file//-solution.txt/}-solution.txt; cflag="m" ;;
  residual) fout=${file//-residual.txt/}-residual.txt; cflag="r" ;;
 esac

 ## info of output data
 info_i=( $( istab $file ) ); if [ $? -eq 2 ]; then continue; fi
 min_data=$( gmtmath -Q ${info_i[3]} 0.8 MUL FLOOR = )

 [ "$v" == "yes" ] && color_txt bold blue "$file - ${info_i[2]}"
 
 case ${info_i[2]} in
  2) cols=( 2 ) ;;
  4) cols=( 2 3 4 ) ;;
  7) cols=( 2 4 6 ) ;;
 esac

 comp=( "" east north up )

 ncol=0
 for col in ${cols[@]}; do
  ncol=$(( $ncol + 1 ))
  awk -v c=$col '{ print $1,$c }' $file \
  | trend1d -Fx${cflag}w -N2r -f0T --TIME_UNIT=h -V > $tmp.d.$ncol 2>> $tmp.log
 done
 
 #awk '/(Polynomial)/ { printf "%20.13E\n",$6 }' $tmp.log
 
 case ${info_i[2]} in
  2) awk -v w=$w '$3>=w { printf "%19.19s %20.13E\n",$1,$2 }' $tmp.d.1 > $fout ;;
  4) paste $tmp.d.1 \
  <( awk '{ print $2,$3 }' $tmp.d.2 ) \
  <( awk '{ print $2,$3  }' $tmp.d.3 ) \
  | awk -v w=$w '$3>=w { print $0 }' \
  | awk -v w=$w '$5>=w { print $0 }' \
  | awk -v w=$w '$7>=w { printf "%19.19s %20.13E %20.13E %20.13E\n",\
  $1,$2,$4,$6 }' > $fout ;;
  7) paste $tmp.d.1 <( awk '{ print $3 }' $file ) \
  <( awk '{ print $2,$3  }' $tmp.d.2 ) <( awk '{ print $5 }' $file ) \
  <( awk '{ print $2,$3  }' $tmp.d.3 ) <( awk '{ print $7 }' $file ) \
  | awk -v w=$w '$3>=w { print $0 }' \
  | awk -v w=$w '$6>=w { print $0 }' \
  | awk -v w=$w '$9>=w { printf "%19.19s %20.13E %20.13E %20.13E \
  %20.13E %20.13E %20.13E\n",$1,$2,$4,$5,$7,$8,$10 }' > $fout
 esac
 
 ## to only output one stations's data remove the temporary log file
 /bin/rm -f $tmp.log
 
## end loop - existing input
done
