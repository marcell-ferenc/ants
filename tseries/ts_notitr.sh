#!/bin/bash

##
## S1 & S2     T I D E S
##
## remove S1 & S2 tides
##
## model:
##
##  - y = A + B*t + C*sin(w1*t) + D*cos(w1*t) + E*sin(w2*t) + F*(w2*t)
##
##  - T = 1 / f    [period                                          unit:     day]
##  - f = 1 / T    [frequency                                       unit:   1/day]
##  - w = 2*pi*f   [angular frequency, rate of change of the phase  unit: rad/day] = 2*pi/T
##
##  S1 signal: period T    = 1   * [day] = 1   *  (1/365.25)
##
##                      frequency      f = 1  [f = 1 / 1  , one cycle per day] ==> w1=2*pi/(1/365.25)
##
##  S2 signal: period T    = 0.5 * [day] = 0.5 *  (1/365.25)
##
##                      frequency      f = 2  [f = 1 / 0.5, two cycle per day] ==> w2=4*pi/(1/365.25)

general_func=$HOME/bin/func/general.func
ts_func=$HOME/bin/func/ts.func

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}
 
## cleaning
clean()
{
 /bin/rm -rf $tmp_dir .gmt* gmt.conf gmt.history 2> /dev/null
 ## restore default value 
 trap EXIT
 exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source
source $general_func
source $ts_func

## list of options the program will accept
optstring=f:o:vh

## interpret options
while getopts $optstring opt
do
 case $opt in
  f) ts=$OPTARG ;;
  o) output=$OPTARG ;;
  v) v=yes ;;
  h) usage $0; exit ;;
 esac
done
shift "$(( $OPTIND - 1 ))"

if [ -z "$output" ]; then exit 2; fi

case $output in
 solution) fout=${ts//-solution.txt/}-solution.txt; cflag="+s" ;;
 residual) fout=${ts//-residual.txt/}-residual.txt; cflag="+r" ;;
 *) exit 2 ;;
esac

if [ ! -s $ts ]; then
 color_txt bold red "${0##*/}: error: missing file: <$ts>"; exit 2; fi
 
sta=${ts##*/}; sta=${sta:0:4}
tts=$tmp.${ts##*/}
wts=$tts.wrk
w1=$( gmtmath -Q 2 PI MUL 1 365.25 DIV DIV = ) ## S1 omega
w2=$( gmtmath -Q 4 PI MUL 1 365.25 DIV DIV = ) ## S2 omega

/bin/cp -f $ts $tts
ts_convert_date.sh -o yr $tts

info=( $( istab $tts ))

case ${info[1]} in
 2) cols=( 2 ) ;;
 3) cols=( 2 ) ;;
 4) cols=( 2 3 4 ) ;;
 7) cols=( 2 4 6 ) ;;
esac

cnt=0
for c in ${cols[@]}; do

 cnt=$(( $cnt + 1 ))

 awk -v c=$c '{ print $1,$c }' $tts > $wts

 gmt gmtmath -N7/1 -A${wts}$cflag -C0 1 ADD \
 -C2 1 COL $w1 MUL SIN ADD \
 -C3 1 COL $w1 MUL COS ADD \
 -C4 1 COL $w2 MUL SIN ADD \
 -C5 1 COL $w2 MUL COS ADD \
 -Ca LSQFIT = | awk '{ print $2 }' > $tmp.$cnt

done

case ${info[1]} in
 2) paste <( awk '{ print $1 }' $ts ) $tmp.1 ;;
 3) paste <( awk '{ print $1 }' $ts ) $tmp.1 <( awk '{ print $3 }' $ts ) ;;
 4) paste <( awk '{ print $1 }' $ts ) $tmp.1 $tmp.2 $tmp.3 ;;
 7) paste <( awk '{ print $1 }' $ts ) $tmp.1 <( awk '{ print $3 }' $ts ) \
 $tmp.2 <( awk '{ print $5 }' $ts ) $tmp.3 <( awk '{ print $7 }' $ts ) ;;
esac > $fout
