#!/bin/bash

#@
#@ USAGE  : ts_filt.sh -f <ts(s)> -t <filter_type> -w <filter_window_size> -u <time_unit> <options>
#@
#@ OPTIONS: -c <weights> - custom weights
#@             - example: -c "weight_1 weight_2 ... weight_n"
#@          -h           - help
#@          -v           - verbose mode
#@
#@ TASK   : Filter time serie(s).
#@
#@ OUTPUT :
#@          <time_serie>-<filter>-filt.txt
#@
#* default ctm filter parameters: 0.0545, 0.2442, 0.4026, 0.2442, 0.0545
#* from the paper: "Geng_etal_2012"
#* "Detecting storm surge loading deformations around the southern North Sea using subdaily GPS"
#*
#* NOTE   : The input time serie can have any number of columns
#*
#@ Available filters (GMT manual page):
#@ Convolution filters are:
#@ (b) Boxcar: All weights are equal.
#@ (c) Cosine Arch: Weights follow a cosine arch curve.
#@ (g) Gaussian: Weights are given by the Gaussian function.
#@ (f) ctm: Instead of width give name of a one-column file with your own weight coefficients.
#@ Non-convolution filters are:
#@ (m) Median: Returns median value.
#@ (p) Maximum likelihood probability (a mode estimator): Return modal value.
#@ (l) Lower: Return the minimum of all values.
#@ (L) Lower: Return minimum of all positive values only.
#@ (u) Upper: Return maximum of all values.
#@ (U) Upper: Return maximum or all negative values only.
#@ Upper case type B, C, G, M, P, F will use robust filter versions
#@
#* by     : marcell.ferenc@cnam.fr or marcell.ferenc.uni@gmail.com

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## default variable
v=no
LC_NUMERIC=C ## for decimal point separator in floating numbers
ctm=no
fu=h
w=$tmp.w

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir $w .gmt* gmt.conf gmt.history 2> /dev/null
 ## restore default value 
 trap EXIT; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
. $HOME/bin/func/general.func
. $HOME/bin/func/ts.func

## test GMT version
case $( vgmt ) in
 5) ;;
 *) color_txt bold red "${0##*/}: error: <GMT> problem: <$gmtv>"; exit 2 ;;
esac

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## list of options the program will accept
optstring=f:t:u:w:z:cdvh

## interpret options
while getopts $optstring opt; do
 case $opt in
  c) flt=custom ;;
  d) flt=dailymean ;;
  f) tss=( $OPTARG ) ;;
  t) flt=intrinsic; ft=$OPTARG ;;
  u) fu=$OPTARG ;;
  w) fs=$OPTARG ;;
  z) wn=$OPTARG ;;
  v) v=yes ;;
  h) usage $0; exit ;;
 esac
done
shift "$(( $OPTIND - 1 ))"

## set filter
case $flt in
    custom) if [[ -z "$wn" || ! -s $wn ]]; then
             printf "%f\n" 0.0545 0.2442 0.4026 0.2442 0.0545 > $w
             #printf "%f\n" 0.2987 0.4026 0.2987 > $w
            else /bin/cp -f $wn $w; fi
            ft=F; fs=$w ;;
 dailymean) ft=M; fs=30; fu=h ;;
 intrinsic) case $ft in
             b|c|g|m|p|l|u|B|C|G|M|P|L|U) ;;
                                       *) ft=M ;;
            esac
            if [ "$( isint $fs )" == "no_integer" ]; then
             fs=72; fu=h; fi
            case $fu in
             d|h|m) ;;
                 *) fu=h ;;
            esac ;;
 *) color_txt bold red \
 "${0##*/}: error: one of the option is obligatory: -c -d -t"; exit 2 ;;
esac

## test files
if [ -z "$tss" ]; then
 color_txt bold red "${0##*/}: error: missing option: -f <time_serie(s)>"
 exit 2; fi

for ts in ${tss[@]}; do
 if [ ! -s $ts ]; then
  color_txt bold red "${0##*/}: warning: missing/empty: <$ts>"; continue; fi
 info=( $( istab $ts ) )
 if [ "${info[0]}" != "is_data_table" ]; then
  color_txt bold red "${0##*/}: warning: wrong format: <$ts>"; continue; fi
 if [ "${info[2]}" -lt 2 ]; then
  color_txt bold red "${0##*/}: warning: wrong format: <$ts>"; continue; fi
 if [ "${info[3]}" -lt 2 ]; then
  color_txt bold red "${0##*/}: warning: wrong format: <$ts>"; continue; fi
 is_item+=( "$ts" )
 if [ "$v" == "yes" ]; then color_txt bold green "exist: <$ts>"; fi
done  

if [ ${#is_item[@]} -eq 0 ]; then
 color_txt bold red "${0##*/}: error: no accepted tserie" ; exit 2; fi
##------------------------------------------------------------------------------
## GMT settings
##------------------------------------------------------------------------------
#gmt set --FORMAT_FLOAT_OUT= --IO_NAN_RECORDS=
gmt set --FORMAT_DATE_IN=yyyy-mm-dd --FORMAT_CLOCK_IN=hh:mm:ss
##------------------------------------------------------------------------------

for ts in ${is_item[@]}; do

 ## date conversion and sampling information
 finf=( $( istab $ts ) )
 tinf=( $( def_sampling $ts ) )
 tsn=${ts##*/}
 tsp=${ts%/*}
 tsw=$tmp.$tsn
 /bin/cp -f $ts $tsw
 
 ts_convert_date.sh -o iso $tsw

 ## s and e time
 s=$( awk 'NR==1 { print $1 }' $tsw )
 e=$( awk 'END { print $1 }' $tsw )

 ## information
 echo "${0##*/}: $ts: $s ---> ${tinf[@]:0:2} ---> $e"

 ## information
 color_txt bold blue "${0##*/}: filter: -F$ft$fs$fu for <$ts>" 
 
 case $flt in
  dailymean) filter1d $tsw -F$ft$fs -E -N0 -T${s:0:10}T12:00:00/${e:0:10}T12:00:00/24 -f0T --TIME_UNIT=$fu > ${ts%-filt.txt}-D-filt.txt ;;
     custom) filter1d $tsw -F$ft$fs -E -N0 -f0T > ${ts%-filt.txt}-${ft}-filt.txt ;;
  intrinsic) filter1d $tsw -F$ft$fs -E -N0 -T$s/$e/${tinf[0]} -f0T --TIME_UNIT=$fu > ${ts%-filt.txt}-${ft}${fs}${fu}-filt.txt ;;
 esac
 
done
