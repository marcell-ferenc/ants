#@ This directory contains fortran program source codes that are used for date 
#@ conversions (date folder), coordinate transformation (coord folder),
#@ frequency analysis (famous folder), read merra netcdf files (merra folder)
#@ and for miscellaneous programs (misc folder).

#@ <date folder> The heart of the date conversion programs are the subroutines
#@ of Michael R. Craymer (NRC) and Dr. David G. Simpson (NASA GSFC).

#@ <coord folder> The coordinate transformation program uses the <GCONV2>
#@ subroutine of the International Earth Rotation and Reference Systems Service 
#@ (IERS) Conventions software collection.

#@ <famous folder> It is the Frequency Analysis Mapping On Unusual Sampling 
#@ (FAMOUS) library of F. Mignard.

#@ <merra folder> Zhao Li provided me the source code <readmerrapress_mod.f>
#@ that I have slightly modified to output data according to my needs from
#@ MERRA netcdf grids.

#@ <misc folder> This folder contains codes for miscellaneous programs.
#@ <corvals.f> for example generate possible correlation values with 3 digit
#@ precision from 0.000 to 1.000. I created this file for testing correlation 
#@ significant test functions.
