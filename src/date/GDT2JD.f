      SUBROUTINE GDT2JD(YR,MN,DY,HR,M,S,JD)
*+    
*  - - - - - - - - - - -
*   G D T 2 J D
*  - - - - - - - - - - -
*
*  This routine convert from Gregorian Calendar date and time to
*  Julian Date.
*     
*  Given:
*     yr                    Year
*     mn                    Month
*     dy                    Day
*     hr                    Hour
*     m                     Minute
*     s                     Second
*
*  Returned:
*     jd                    Julian Date
*
*  Test case:
*     given input: yr = 2013
*                  mn = 2
*                  dy = 2
*                  hr = 16
*                  m  = 12
*                  s  = 8
*
*     expected output: jd    =   2456326.1750926175
*                                2456326.175093
*
*  References:
*
*  Algorithm taken from the site:
*  http://scienceworld.wolfram.com/astronomy/JulianDate.html (2013/04/26)
*
*  This file was created by: Marcell Ferenc (marcell.ferenc@cnam.fr)
*-----------------------------------------------------------------------

      IMPLICIT NONE
      INTEGER                   :: YR,MN,DY,HR,M
      DOUBLE PRECISION          :: JD,UT,S

      UT = HR + (M+S/60.)/60.

      JD = 367.*YR - INT(7.*(YR+INT((MN+9.)/12.))/4.) -
     .     INT(3.*(INT((YR+(MN-9.)/7.)/100.)+1.)/4.) +
     .     INT(275.*MN/9.)+DY+1721028.5+UT/24.
      
      RETURN

      END
