c*********************************************************************** jd2gps
c 
      subroutine jd2gps (jd, gpsweek, sow, rollover, err)
c
c     Converts Julian date to GPS week, seconds of week and number of
c     1024 week rollovers. See also CAL2JD, DOY2JD, GPS2JD, JD2CAL,
c     JD2DOW, JD2DOY, JD2GPS, JD2YR, YR2JD.
c
c     Input:
c       jd - julian date
c
c     Output:
c       gpsweek  - GPS week beginning with 0 on 1980.1.6 (modulo 1024)
c       sow      - seconds of week since 0 hr, Sun
c       rollover - number of GPS week rollovers every 1024 weeks
c       err      - logical error flag (.true.=error, .false.=no error)
c
c     Externals:
c       cal2jd, jd2cal
c
c     Created:
c       25 Apr 99  Michael R. Craymer
c                  Converted from MATLAB function of same name.
c
c     Modified:
c       09 Jul 99  MRC  Corrected GPS week number to begin at 0
c
c     Copyright (c) 1999 Michael R. Craymer
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Dummy specifications
c
      logical err
      integer gpsweek, rollover
      real*8  jd, sow
c
c  Local specifications
c
      integer nweek
      real*8  jdgps
c-----------------------------------------------------------------------
c  Check for valid Julian date
c
      err = .false.
      if (jd.lt.0) then
        write(*,*) '***** ERROR: Invalid Julian date in JD2CAL'//char(7)
        write(*,*) '      Must be greater than or equal to zero'
        write(*,*) '      JD =',jd
        err = .true.
        return
      end if
c
c  Compute GPS week and seconds of week
c
      call cal2jd (1980, 1, 6.d0, jdgps, err) ! beginning of GPS week numbering
      if (err) return
      nweek = int((jd-jdgps)/7.d0)
      sow = (jd - (jdgps+nweek*7.d0)) * 3600.d0*24.d0
      rollover = int(nweek/1024.d0)   ! rollover every 1024 weeks
      gpsweek = nweek ! mod(nweek,1024)
c
c  Return to calling routine
c
      return
      end
