      PROGRAM ISODT2JD
*+    
*  - - - - - - - - - - -
*   I S O D T 2 J D
*  - - - - - - - - - - -
*
*  This file was created by: Marcell Ferenc (marcell.ferenc@cnam.fr)
*-----------------------------------------------------------------------

      IMPLICIT NONE

      INTEGER                   :: YR,MN,DY,HR,M
      DOUBLE PRECISION          :: JD,S

      DO WHILE (1.EQ.1)
 
       READ (*,*,END=200) YR,MN,DY,HR,M,S

       CALL GDT2JD(YR,MN,DY,HR,M,S,JD)
      
       WRITE (*,100) JD
100    FORMAT (f20.12)

      END DO
200   CONTINUE

      END PROGRAM ISODT2JD
