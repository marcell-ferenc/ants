c*********************************************************************** doy2jd
c
      subroutine doy2jd (yr, doy, jd, err)
c
c     Converts year and day of year to Julian date. See also CAL2JD,
c     GPS2JD, JD2CAL, JD2DOW, JD2DOY, JD2GPS, JD2YR, YR2JD.
c
c     Input:
c       yr  - year of calendar date (4 digit)
c       doy - day of year
c
c     Output:
c       jd  - julian date
c       err - logical error flag (.true.=error, .false.=no error)
c
c     Externals:
c       cal2jd
c
c     Created:
c       25 Apr 99  Michael R. Craymer
c                  Converted from MATLAB function of same name.
c
c     Modified:
c
c     Copyright (c) 1999 Michael R. Craymer
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Dummy specifications
c
      logical err
      integer yr
      real*8  doy, jd
c-----------------------------------------------------------------------
c  Compute Julian date
c
      call cal2jd (yr, 1, 0.d0, jd, err)
      if (err) return
      jd = jd + doy
c
c  Return to calling routine
c
      return
      end
