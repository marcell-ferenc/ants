      PROGRAM JULIANDATE2GPSW
*+    
*  - - - - - - - - - - -
*   J D 2 G P S W
*  - - - - - - - - - - -
*
*
*
*     
*       
*     Used subroutines:
*  
*     subroutines of Michael R. Craymer 
*
*     Copyright (c) 2004 Michael R. Craymer
*
*-----------------------------------------------------------------------

      IMPLICIT NONE

      LOGICAL                   :: err
      INTEGER                   :: iyr, gpsweek, dow, rollover
      DOUBLE PRECISION          :: jd, yr, sow

      DO WHILE (1.EQ.1)
       
       READ (*,*,END=200) jd
       
       CALL jd2yr (jd, yr, err)
       
       iyr = int(yr)

       CALL jd2gps (jd, gpsweek, sow, rollover, err)

       dow = int(mod(jd+1.5d0,7d0))

       WRITE (*,100) jd,iyr,gpsweek,dow,sow
100    FORMAT (f20.12,1x,i4,1x,i4,1x,i1,1x,f10.3)
 
      END DO
200   CONTINUE

      END PROGRAM JULIANDATE2GPSW
