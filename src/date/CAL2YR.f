      PROGRAM CAL2YEAR
*+    
*  - - - - - - - - - - -
*   J D 2 Y R
*  - - - - - - - - - - -
*
*
*
*     
*       
*     Used subroutines:
*  
*     subroutines of Michael R. Craymer 
*
*     Copyright (c) 2004 Michael R. Craymer
*
*-----------------------------------------------------------------------

      IMPLICIT NONE

      LOGICAL                   :: err
      INTEGER                   :: iyr, mn, Y, M, D, H, N, S
      DOUBLE PRECISION          :: jd, yr, dy, doy
      CHARACTER(LEN=8)          :: HHMMSS
      CHARACTER(LEN=8)          :: FORMAT_TIME

      DO WHILE (1.EQ.1)
       
       READ (*,*,END=200) Y,M,D,H,N,S
       
       CALL GDT2JD(Y,M,D,H,N,S,JD)
       CALL jd2yr (jd, yr, err)
       
       iyr = int(yr)

       CALL jd2cal (jd, iyr, mn, dy, err)

       HHMMSS = FORMAT_TIME (dy)

       WRITE (*,100) jd,yr,iyr,mn,int(dy),HHMMSS
100    FORMAT (f20.12,1x,f20.15,1x,i4,'-',i2.2,'-',i2.2,'T',a)
*100    FORMAT (f18.10,1x,f18.13,1x,i2.2,1x,f10.7,
*     +        1x,i4,'-',i2.2,'-',i2.2,'T',a)
 
      END DO
200   CONTINUE

      END PROGRAM CAL2YEAR
