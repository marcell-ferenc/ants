c***********************************************************************
c
      program dirinv
c
c     Solves the direct and inverse geodetic problems using console i/o.
c
c     Input:
c       Console - solution type (direct or inverse)
c               - reference ellipsoid (clk86, grs80, wgs84)
c               - lat,lon,h of point 1
c               - azimuth, v. angle, dist. point 1 to 2 (direct only)
c               - lat,lon,h of point 2 (inverse only)
c
c     Output:
c       Console - lat,lon,h of point 2 (direct only)
c               - azimuth, v. angle, dist. point 1 to 2 (inverse only)
c
c     Externals:
c       ct2lg, direct, dms2rad, ell2xyz, inverse, lg2ct, p2r, r2p,
c       rad2dms, refell, upcase, xyz2ell
c
c     Created:
c       18 Oct 96  Michael R. Craymer
c
c     Modified:
c       25 Oct 96  MRC  Corrected xyz2ell for negative lat & h
c       15 Jan 08  MRC  Added reverse observations for inverse problem
c
c     Copyright (c) 2008 Michael R. Craymer
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Local variables
c
      character soln, rell*5, ver*15
      integer   id, m
      real*8    a, b, e2, finv, lat1, lon1, h1, lat2, lon2, h2,
     +          az, va, d, s
      data      ver/'1.2 (08-01-15)'/
c
c  Format statements
c
 100  format(1x,a,$)
 105  format(1x,a)
 110  format(1x,a,i4,i3,f10.6)
 120  format(1x,a,f17.4)
 130  format(1x,a,i4,i3,f7.3)
 140  format(1x,a,f14.3)

c-----------------------------------------------------------------------
c  Get general input
c
      write(*,*) '-----------------------------------------------------'
      write(*,*) ' DIRINV: Direct & Inverse Geodetic Problem Solver'
      write(*,*) '         Version ',ver
      write(*,*) '-----------------------------------------------------'

 10   continue
      write(*,*)
      write(*,100) 'Enter solution type (D=direct,I=inverse,Q=quit) > '
      read(*,'(a)') soln
      if (soln.eq.' ') then
         write(*,*) 
         stop 'DIRINV finished'
      end if
      call upcase (soln)
      if (soln.ne.'D' .and. soln.ne.'I' .and. soln.ne.'Q') then
         write(*,*) '*** Error: Invalid solution type'
         goto 10
      end if

 20   continue
      write(*,100) 'Enter ref. ellipsoid (C=CLK86,G=GRS80,W=WGS84) > '
      read(*,'(a)') rell
      call upcase (rell)
      if (rell.eq.' ') then
         goto 10
      else if (rell.eq.'C') then
         rell = 'CLK86'
      else if (rell.eq.'G') then
         rell = 'GRS80'
      else if (rell.eq.'W') then
         rell = 'WGS84'
      else
         write(*,*) '*** Error: Invalid reference ellipsoid'
         goto 20
      end if
      call refell (rell, a, b, e2, finv)

      write(*,*)
      write(*,*) 'Enter point 1 coordinates:'
      write(*,100) '  Latitude (D M S)  > '
      read(*,*) id, m, s
      call dms2rad (id, m, s, lat1)
      write(*,100) '  Longitude (D M S) > '
      read(*,*) id, m, s
      call dms2rad (id, m, s, lon1)
      write(*,100) '  Ell. Height (m)   > '
      read(*,*) h1

c-----------------------------------------------------------------------
c  Direct problem
c
      if (soln.eq.'D') then
         write(*,*)
         write(*,*) 'Enter point 1 to 2 observations:'
         write(*,100) '  Azimuth (D M S)  > '
         read(*,*) id, m, s
         call dms2rad (id, m, s, az)
         write(*,100) '  V. Angle (D M S) > '
         read(*,*) id, m, s
         call dms2rad (id, m, s, va)
         write(*,100) '  Distance (m)     > '
         read(*,*) d

         call direct (a, e2, lat1, lon1, h1, az, va, d, lat2, lon2, h2)

         write(*,*)
         write(*,*) 'Point 2 coordinates:'
         call rad2dms (lat2, id, m, s)
         write(*,110) '  Latitude (D M S)  > ', id, m, s
         call rad2dms (lon2, id, m, s)
         write(*,110) '  Longitude (D M S) > ', id, m, s
         write(*,120) '  Ell. Height (m)   > ', h2

         goto 10

c-----------------------------------------------------------------------
c  Inverse problem
c
      else if (soln.eq.'I') then
         write(*,*)
         write(*,*) 'Enter point2 coordinates:'
         write(*,100) '  Latitude (D M S)  > '
         read(*,*) id, m, s
         call dms2rad (id, m, s, lat2)
         write(*,100) '  Longitude (D M S) > '
         read(*,*) id, m, s
         call dms2rad (id, m, s, lon2)
         write(*,100) '  Ell. Height (m)   > '
         read(*,*) h2

         call inverse (a, e2, lat1, lon1, h1, lat2, lon2, h2, az, va, d)

         write(*,*)
         write(*,*) 'Point 1 to 2 observations:'
         call rad2dms (az, id, m, s)
         write(*,130) '  Azimuth (rad)    > ', id, m, s
         call rad2dms (va, id, m, s)
         write(*,130) '  V. Angle (rad)   > ', id, m, s
         write(*,140) '  Distance (m)     > ', d

         call inverse (a, e2, lat2, lon2, h2, lat1, lon1, h1, az, va, d)

         write(*,*)
         write(*,*) 'Point 2 to 1 observations:'
         call rad2dms (az, id, m, s)
         write(*,130) '  Azimuth (rad)    > ', id, m, s
         call rad2dms (va, id, m, s)
         write(*,130) '  V. Angle (rad)   > ', id, m, s
         write(*,140) '  Distance (m)     > ', d

         goto 10
      end if

c-----------------------------------------------------------------------
c  Exit program
c
      write(*,*)
      stop 'DIRINV Finished'
      end
c*********************************************************************** ct2lg
c
      subroutine ct2lg (lat, lon, dxct, dyct, dzct, dxlg, dylg, dzlg)
c
c     Transforms Conventional Terrestrial (geocentric) Cartesian
c     coordinate differences between two points to local geodetic
c     (topocentric) coordinate differences. If local astronomic
c     coordinate differences desired, must use astronomic lat,lon.
c
c     Input:
c       lat  = ellipsoidal (geodetic) latitude of base point (rad)
c       lon  = ellipsoidal (geodetic) longitude of base point (rad)
c       dxct = Conventional Terrestrial x coordinate difference (m)
c       dyct = Conventional Terrestrial y coordinate difference (m)
c       dzct = Conventional Terrestrial y coordinate difference (m)
c
c     Output:
c       dxlg = local geodetic x coordiante difference (m)
c       dylg = local geodetic y coordiante difference (m)
c       dzlg = local geodetic z coordiante difference (m)
c
c     Externals:
c
c     Created:
c       14 Sep 88  Michael R. Craymer
c
c     Modified:
c       11 Feb 96  MRC  Minor cosmetic changes
c       19 Oct 96  MRC  Renamed rlat,rlon to lat,lon
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      real*8 lat, lon, dxct, dyct, dzct, dxlg, dylg, dzlg
c
c  Local specifications
c
      real*8 r11, r12, r13, r21, r22, r23, r31, r32, r33
c-----------------------------------------------------------------------
c  Form rotation matrix R
c
      r11 = -sin(lat) * cos(lon)
      r12 = -sin(lat) * sin(lon)
      r13 =  cos(lat)
      r21 = -sin(lon)
      r22 =  cos(lon)
      r23 =  0.d0
      r31 =  cos(lat) * cos(lon)
      r32 =  cos(lat) * sin(lon)
      r33 =  sin(lat)
c
c  Transform CT to LG:  Xlg = R Xct
c
      dxlg = r11*dxct + r12*dyct + r13*dzct
      dylg = r21*dxct + r22*dyct + r23*dzct
      dzlg = r31*dxct + r32*dyct + r33*dzct
c
c  Return to calling routine
c
      return
      end
c*********************************************************************** direct
c
      subroutine direct (a, e2, lat1, lon1, h1, az, va, d, lat2, lon2,
     +                  h2)
c
c     Computes direct (forward) geodetic problem. Given ellipsoidal
c     coordinates of 1st point and azimuth, vertical angle and
c     distance to 2nd point, computes ellipsoidal coordinates of 2nd
c     point. If azimuth and vertical angles are local astronomic
c     (relative to plumb line), then latitude and longitude must also
c     be astronomic. If azimuth and vertical angles are local geodetic
c     (relative to reference ellipsoid normal), then latitude and
c     longitude must be geodetic. See also subroutine INVERSE.FOR.
c
c     Input:
c       lat1 = ellipsoidal latitude of point 1 (rad)
c       lon1 = ellipsoidal longitude of point 1 (rad)
c       h1   = ellipsoidal height of point 1 (m)
c       az   = azimuth (clockwise from north) from point 1 to 2 (rad)
c       va   = vertical angle (from horizontal) from point 1 to 2 (rad)
c       d    = spatial (3D) distance from point 1 to 2 (m)
c       a    = major semi-axis of reference ellipsoid (m)
c       e2   = square of eccentricity of reference ellipsoid
c
c     Output:
c       lat2 = ellipsoidal latitude of point 2 (rad)
c       lon2 = ellipsoidal longitude of point 2 (rad)
c       h2   = ellipsoidal height of point 2 (m)
c
c     Externals:
c       ell2xyz, lg2ct, p2r, xyz2ell
c
c     Created:
c       18 Oct 96  Michael R. Craymer
c
c     Modified:
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      real*8 lat1, lon1, h1, lat2, lon2, h2, a, e2, az, va, d
c
c  Local specifications
c
      real*8 xct1, yct1, zct1, xct2, yct2, zct2, dxct, dyct, dzct,
     +       dxlg, dylg, dzlg
c-----------------------------------------------------------------------
c  Convert ellipsoidal coordinates of point 1 to Cartesian
c
      call ell2xyz (a, e2, lat1, lon1, h1, xct1, yct1, zct1)
c
c  Convert local polar coordinates (point 1 to 2) to local Cartesian
c
      call p2r (az, va, d, dxlg, dylg, dzlg)
c
c  Transform local Cartesian coordinate differences to global Cartesian
c
      call lg2ct (lat1, lon1, dxlg, dylg, dzlg, dxct, dyct, dzct)
c
c  Compute global Cartesian coordinates of point 2
c
      xct2 = xct1 + dxct
      yct2 = yct1 + dyct
      zct2 = zct1 + dzct
c
c  Convert global Cartesian coordinates to ellipsoidal
c
      call xyz2ell (a, e2, xct2, yct2, zct2, lat2, lon2, h2)
c
c  Return to calling routine
c
      return
      end
c*********************************************************************** dms2rad
c
      subroutine dms2rad (id, m, s, rad)
c
c     Transforms angle in degrees, minutes, seconds to radians.
c
c     Input:
c       id  = degrees
c       m   = minutes
c       s   = seconds
c
c     Output:
c       rad = radians
c
c     Externals:
c
c     Created:
c       05 Sep 88  Michael R. Craymer
c
c     Modified:
c       11 Feb 96  MRC  Minor cosmetic changes
c       18 Oct 96  MRC  Added support for zero degrees and minutes
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      integer id, m
      real*8  s, rad
c
c  Local specifications
c
      real*8  d, pi
      parameter (pi=3.141592653589793238462643d0)
c-----------------------------------------------------------------------
c  Convert DMS to deg then to radians
c
      d   = (dble(abs(id)) + dble(m)/60.d0 + s/3600.d0) * pi/180.d0
      if (id.ne.0) then
         rad = sign(d,dble(id))
      else
         if (m.ne.0) then
            rad = sign(d,dble(m))
         else
            rad = sign(d,s)
         end if
      end if
c
c  Return to calling program
c
      return
      end
c*********************************************************************** ell2xyz
c
      subroutine ell2xyz (a, e2, lat, lon, h, x, y, z)
c
c     Transforms curvilinear ellipsoidal (lat,lon,h) coordinates to
c     Cartesian (x,y,z) coordinates.
c
c     Input:
c       a   = major semi-axis of reference ellipsoid (m)
c       e2  = square of eccentricity of reference ellipsoid
c       lat = ellipsoidal latitude (rad)
c       lon = ellipsoidal longitude (rad)
c       h   = ellipsoidal height (m)
c
c     Output:
c       x   = Cartesian x coordinate (m)
c       y   = Cartesian y coordinate (m)
c       z   = Cartesian z coordinate (m)
c
c     Externals:
c
c     Created:
c       05 Sep 88  Michael R. Craymer
c
c     Modified:
c       11 Feb 96  MRC  Changed some working trig variable names
c                       Minor cosmetic changes
c       02 Aug 96  MRC  Corrected specification of trig variables
c       19 Oct 96  MRC  Renamed rlat,rlon to lat,lon
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      real*8 a, e2, lat, lon, h, x, y, z
c
c  Local specifications
c
      real*8 slat, clat, slon, clon, v
c-----------------------------------------------------------------------
c  Compute some working variables
c
      slat = sin(lat)
      clat = cos(lat)
      slon = sin(lon)
      clon = cos(lon)
      v = a / sqrt(1.d0-e2*slat*slat)
c
c  Compute Cartesian coordinates
c
      x = (v+h) * clat * clon
      y = (v+h) * clat * slon
      z = (v*(1.d0-e2)+h) * slat
c
c  Return to calling routine
c
      return
      end
c*********************************************************************** inverse
c
      subroutine inverse (a, e2, lat1, lon1, h1, lat2, lon2, h2,
     +                    az, va, d)
c
c     Computes inverse (backward) geodetic problem. Given ellipsoidal
c     coordinates of 1st and 2nd point, computes azimuth, vertical
c     angle and distance from point 1 to 2. If latitudes and longitudes
c     are local astronomic, then azimuth and vertical angle are
c     astronomic (relative to plumb line). If latitude and longitude
c     are local geodetic, then azimuth and vertical angle are local
c     geodetic (relative to reference ellipsoid normal). See also
c     subroutine INVERSE.FOR.
c
c     Input:
c       a    = major semi-axis of reference ellipsoid (m)
c       e2   = square of eccentricity of reference ellipsoid
c       lat1 = ellipsoidal latitude of point (rad)
c       lon1 = ellipsoidal longitude of point 1 (rad)
c       h1   = ellipsoidal height of point 1 (m)
c       lat2 = ellipsoidal latitude of point 2 (rad)
c       lon2 = ellipsoidal longitude of point 2 (rad)
c       h2   = ellipsoidal height of point 2 (m)
c
c     Output:
c       az   = azimuth (clockwise from north) from point 1 to 2 (rad)
c       va   = vertical angle (from horizontal) from point 1 to 2 (rad)
c       d    = spatial (3D) distance from point 1 to 2 (m)
c
c     Externals:
c       ell2xyz, ct2lg, r2p
c
c     Created:
c       18 Oct 96  Michael R. Craymer
c
c     Modified:
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      real*8 a, e2, lat1, lon1, h1, lat2, lon2, h2, az, va, d
c
c  Local specifications
c
      real*8 xct1, yct1, zct1, xct2, yct2, zct2, dxct, dyct, dzct,
     +       dxlg, dylg, dzlg
c-----------------------------------------------------------------------
c  Convert ellipsoidal coordinates to Cartesian
c
      call ell2xyz (a, e2, lat1, lon1, h1, xct1, yct1, zct1)
      call ell2xyz (a, e2, lat2, lon2, h2, xct2, yct2, zct2)
c
c  Compute global Cartesian coordinate differences
c
      dxct = xct2 - xct1
      dyct = yct2 - yct1
      dzct = zct2 - zct1
c
c  Transform global Cartesian coordinate differences to local Cartesian
c
      call ct2lg (lat1, lon1, dxct, dyct, dzct, dxlg, dylg, dzlg)
c
c  Convert local Cartesian (point 1 to 2) to local polar coordinates
c
      call r2p (dxlg, dylg, dzlg, az, va, d)
c
c  Return to calling routine
c
      return
      end
c*********************************************************************** lg2ct
c
      subroutine lg2ct (lat, lon, dxlg, dylg, dzlg, dxct, dyct, dzct)
c
c     Transforms local geodetic (topocentric) coordinate differences
c     between two points to Conventional Terrestrial (geocentric)
c     coordinate differences.
c
c     Input:
c       lat  = ellipsoidal (geodetic) latitude of base point (rad)
c       lon  = ellipsoidal (geodetic) longitude of base point (rad)
c       dxlg = local geodetic x coordiante difference (m)
c       dylg = local geodetic y coordiante difference (m)
c       dzlg = local geodetic z coordiante difference (m)
c
c     Output:
c       dxct = Convential Terrestrial x coordinate difference (m)
c       dyct = Convential Terrestrial y coordinate difference (m)
c       dzct = Convential Terrestrial z coordinate difference (m)
c
c     Externals:
c
c     Created:
c       14 Sep 88  Michael R. Craymer
c
c     Modified:
c       11 Feb 96  MRC  Minor cosmetic changes
c       19 Oct 96  MRC  Remaned rlat,rlon to lat,lon
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      real*8 lat, lon, dxlg, dylg, dzlg, dxct, dyct, dzct
c
c  Local specifications
c
      real*8 r11, r12, r13, r21, r22, r23, r31, r32, r33
c-----------------------------------------------------------------------
c  Form rotation matrix R
c
      r11 = -sin(lat) * cos(lon)
      r12 = -sin(lat) * sin(lon)
      r13 =  cos(lat)
      r21 = -sin(lon)
      r22 =  cos(lon)
      r23 =  0.d0
      r31 =  cos(lat) * cos(lon)
      r32 =  cos(lat) * sin(lon)
      r33 =  sin(lat)
c                              T
c  Transform CT to LG:  Xct = R  Xlg
c
      dxct = r11*dxlg + r21*dylg + r31*dzlg
      dyct = r12*dxlg + r22*dylg + r32*dzlg
      dzct = r13*dxlg + r23*dylg + r33*dzlg
c
c  Return to calling routine
c
      return
      end
c*********************************************************************** p2r
c
      subroutine p2r (az, va, d, x, y, z)
c
c     Converts polar coordinates (azimuth, vertical angle, distance) to
c     rectangular Cartesian coordinates (x, y, z).
c
c     Input:
c       az = azimuth; clockwise from north (rad)
c       va = vertical angle; up from horizon (rad)
c       d  = spatial (3D) distance (m)
c
c     Output:
c       x  = Cartesian x coordinate (m)
c       y  = Cartesian y coordinate (m)
c       z  = Cartesian z coordinate (m)
c
c     Externals:
c
c     Created:
c       18 Oct 96  Michael R. Craymer
c
c     Modified:
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      real*8 az, va, d, x, y, z
c
c  Local specifications
c
      real*8 caz, saz, cva, sva
c-----------------------------------------------------------------------
c  Compute sines and cosines
c
      caz = cos(az)
      saz = sin(az)
      cva = cos(va)
      sva = sin(va)
c
c  Compute Cartesian coordinates
c
      x = d * cva * caz
      y = d * cva * saz
      z = d * sva
c
c  Return to calling routine
c
      return
      end
c*********************************************************************** r2p
c
      subroutine r2p (x, y, z, az, va, d)
c
c     Converts rectangular Cartesian coordinates (x, y, z) to polar
c     coodinates (azimuth, vertical angle, distance).
c
c     Input:
c       x  = Cartesian x coordinate (m)
c       y  = Cartesian y coordinate (m)
c       z  = Cartesian z coordinate (m)
c
c     Output:
c       az = azimuth; clockwise from north (rad)
c       va = vertical angle; up from horizon (rad)
c       d  = spatial (3D) distance (m)
c
c     Externals:
c
c     Created:
c       18 Oct 96  Michael R. Craymer
c
c     Modified:
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      real*8 x, y, z, az, va, d
c-----------------------------------------------------------------------
c  Compute Cartesian coordinates
c
      d  = sqrt(x*x + y*y + z*z)
      va = asin(z/d)
      az = atan2(y,x)
c
c  Return to calling routine
c
      return
      end
c*********************************************************************** rad2dms
c
      subroutine rad2dms (rad, id, m, s)
c
c     Transforms angle in radians to degrees, minutes, seconds.
c
c     Input:
c       rad = radians
c
c     Output:
c       id  = degrees
c       m   = minutes
c       s   = seconds
c
c     Externals:
c
c     Created:
c       05 Sep 88  Michael R. Craymer
c
c     Modified:
c       11 Feb 96  MRC  Minor cosmetic changes
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      integer id, m
      real*8  s, rad
c
c  Local specifications
c
      real*8  d, rm, pi
      parameter (pi=3.141592653589793238462643d0)
c-----------------------------------------------------------------------
c  Convert radians to degrees then to DMS
c
      d  = rad * 180.d0/pi
      id = int(d)
      rm = abs(d-id)*60.d0
      m  = int(rm)
      s  = (rm-m)*60.d0
c
c  Correct minutes and seconds for negative angle
c
      if (d.lt.0.d0) then
         if (id.eq.0 .and. m.ne.0) m = -m
         if (id.eq.0 .and. m.eq.0) s = -s
      end if
c
c  Return to calling routine
c
      return
      end
c*********************************************************************** refell
c
      subroutine refell (name, a, b, e2, finv)
c
c     Computes reference ellipsoid parameters for given reference
c     ellipsoids.
c
c     Input:
c       name = name of reference ellipsoid (case sensitive!) - the
c              following are supported:
c              CLK86 = Clarke 1886 (NAD27)
c              ATS77 = Average Terrestrial System 1977
c              GRS80 = Geodetic Reference System 1980 (NAD83)
c              WGS72 = World Geodetic Reference System 1972 (Old GPS)
c              WGS84 = World Geodetic Reference System 1984 (New GPS)
c              INTER = International (Europe) 
c              KRASS = Krassovsky (USSR) 
c
c     Output:
c       a    = major semi-axis (m)
c       b    = minor semi-axis (m)
c       e2   = square of eccentricity
c       finv = inverse of flattening
c
c     Externals:
c
c     Created:
c       09 Jun 90  Michael R. Craymer
c
c     Modified:
c       11 Feb 96  MRC  Added many other ellipsoids
c                       Defined all ellipsoids in terms of a,finv
c       02 Aug 96  MRC  Corrected computation of finv
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      character name*5
      real*8 a, b, e2, finv
c
c  Local specifications
c
      real*8 f
c-----------------------------------------------------------------------
c  CLK86 - Clarke 1866 (NAD27)
c
      if (name.eq.'CLK86') then
         a    = 6378206.4d0
         finv = 294.9786982d0
c
c  ATS77 - Average Terrestrial System 1977
c
      else if (name.eq.'ATS77') then
         a    = 6378135.0d0
         finv = 298.257d0
c
c  GRS80 - Geodetic Reference System 1980 (NAD83)
c
      else if (name.eq.'GRS80') then
         a    = 6378137.0d0
         finv = 298.257222101d0
c
c  WGS72 - World Geodetic System 1972
c
      else if (name.eq.'WGS72') then
         a    = 6378135.0d0
         finv = 298.26d0
c
c  WGS84 - World Geodetic System 1984
c
      else if (name.eq.'WGS84') then
         a    = 6378137.0d0
         finv = 298.257223563d0
c
c INTER - International (Europe)
c
      else if (name.eq.'KRASS') then
         a    = 6378388.0d0
         finv = 297.0d0
c
c KRASS - Krassovsky (USSR)
c
      else if (name.eq.'KRASS') then
         a    = 6378245.0d0
         finv = 298.3d0
      end if
c
c Compute other ellipsoid parameters
c
      f  = 1.d0 / finv
      b  = a * (1.d0-f)
      e2 = 1.d0 - (1.d0-f)**2
c
c  Return to calling routine
c
      return
      end
c*********************************************************************** upcase
c
      subroutine upcase (string)
c
c     Converts a character string to upper case letters.
c
c     Input:
c       string = input character string
c
c     Output:
c       string = output character string converted to upper case
c                (input replaced)
c
c     Externals:
c
c     Created:
c       02 Jun 95  Michael R. Craymer
c
c     Modified:
c       11 Feb 96  MRC  Minor cosmetic changes
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      character string*(*)
c
c  Local specifications
c
      integer i, ic
c-----------------------------------------------------------------------
c  Convert to upper case
c
      do i=1,len(string)
         ic = ichar(string(i:i))
         if (ic.ge.97 .and. ic.le.122) string(i:i) = char(ic-32)
      end do
c
c  Return to calling routine
c
      return
      end
c*********************************************************************** xyz2ell
c
      subroutine xyz2ell (a, e2, x, y, z, lat, lon, h)
c
c     Transforms Cartesian (x,y,z) coordinates to curvilinear
c     ellipsoidal (lat,lon,h) coordinates. Uses iterative algorithm.
c
c     *** Note: ELAT and EHT are the convergence criteria. ELAT in
c     *** radians and eht in metres (1.d-12 rad about 1.d-5 m).
c
c     Input:
c       a   = major semi-axis of reference ellipsoid (m)
c       e2  = square of eccentricity of reference ellipsoid
c       x   = Cartesian x coordinate (m)
c       y   = Cartesian y coordinate (m)
c       z   = Cartesian z coordinate (m)
c
c     Output:
c       lat = ellipsoidal latitude (rad)
c       lon = ellipsoidal longitude (rad)
c       h   = ellipsoidal height (m)
c
c     Externals:
c
c     Created:
c       05 Sep 88  Michael R. Craymer
c
c     Modified:
c       11 Feb 96  MRC  Added test to make long positive (0-2pi)
c                       Minor cosmetic changes
c       02 Aug 96  MRC  Removed rlon0, not used
c       19 Oct 96  MRC  Renamed rlat,rlon,rlat0 to lat,lon,lon0
c                       Removed conversion to positive longitude, now
c                       -pi to +pi
c       25 Oct 96  MRC  Corrected dlat & dh (used absolute values) for
c                       negative latitudes and heights
c
c---+*---1----+----2----+----3----+----4----+----5----+----6----+----7-*
      implicit none
c
c  Global specifications
c
      real*8 a, e2, lat, lon, h, x, y, z
c
c  Local specifications
c
      real*8 pi, dlat, dh, lat0, h0, elat, eh, v, p
      data   elat/1.d-12/, eh/1.d-5/
      parameter (pi=3.141592653589793238462643d0)
c-----------------------------------------------------------------------
c  Compute some initial parameters
c
      p   = sqrt(x*x + y*y)
      lat = atan2(z, p/(1.d0-e2))
      h   = 0.d0
c
c  Iterate until height and latitude converge
c
 10   continue
         lat0 = lat
         h0   = h
         v    = a / sqrt( 1.d0-e2*sin(lat)*sin(lat) )
         h    = p / cos(lat) - v
         lat  = atan2( z, p*(1.d0-e2*v/(v+h)) )
         dlat = abs(lat - lat0)
         dh   = abs(h - h0)
      if (dlat.gt.elat .or. dh.gt.eh) goto 10
c
c  Compute longitude
c
      lon = atan2(y,x)
c     if (lon.lt.0.d0) lon = lon + 2*pi
c
c  Return to calling routine
c
      return
      end
