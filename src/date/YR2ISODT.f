      PROGRAM YR2ISODT
*+    
*  - - - - - - - - - - -
*   Y R 2 I S O D T
*  - - - - - - - - - - -
*
*
*
*     
*       
*     Used subroutines:
*  
*     subroutines of Michael R. Craymer 
*
*     Copyright (c) 2004 Michael R. Craymer
*
*-----------------------------------------------------------------------

      IMPLICIT NONE

      LOGICAL                   :: err
      INTEGER                   :: iyr, mn
      DOUBLE PRECISION          :: jd, yr, dy, doy
      CHARACTER(LEN=8)          :: HHMMSS
      CHARACTER(LEN=8)          :: FORMAT_TIME

      DO WHILE (1.EQ.1)
       
       READ (*,*,END=200) yr

       CALL yr2jd (yr, jd, err)
       CALL jd2cal (jd, iyr, mn, dy, err)
      
       HHMMSS = FORMAT_TIME (dy)

*       WRITE (*,100) jd,yr,mn,dy,iyr,mn,FLOOR(dy),HHMMSS
        WRITE (*,100) jd,yr,iyr,mn,FLOOR(dy),HHMMSS

100    FORMAT (f20.12,1x,f20.15,1x,i4,'-',i2.2,'-',i2.2,'T',a)
*100    FORMAT (f18.10,1x,f18.13,1x,i2.2,1x,f10.7,
*     +        1x,i4,'-',i2.2,'-',i2.2,'T',a)
 
      END DO
200   CONTINUE

      END PROGRAM YR2ISODT
