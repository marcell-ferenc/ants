#@------------------------------------------------------------------------------
This directory contains the original source code of The Geodetic Library, 
Version 1.8, 2014. of Mike Craymer <org/craymer>, downloaded via ftp:
<ncftpget -R ftp://ftp.geod.nrcan.gc.ca/pub/GSD/craymer/software/fortran/geodlib/*>
and the original source code of David Simpson <org/simpson>, downloaded 
via web browser (2014-07-21):
http://caps.gsfc.nasa.gov/simpson/

#@------------------------------------------------------------------------------
