      FUNCTION FORMAT_TIME (DAY) RESULT (HMS_STRING)

!***********************************************************************************************************************************
!  FORMAT_TIME
!***********************************************************************************************************************************
!***********************************************************************************************************************************
!
!                                                          J D 2 G R E G
!
!
!  Program:      JD2GREG
!
!  Programmer:   Dr. David G. Simpson
!                NASA Goddard Space Flight Center
!                Greenbelt, Maryland  20771
!
!  Date:         November 20, 2001
!
!  Language:     ANSI Standard Fortran-90
!
!  Version:      1.00b  (October 25, 2004)
!
!  Description:  This program converts a Julian day to a date on the Gregorian calendar.
!
!***********************************************************************************************************************************

      IMPLICIT NONE

      DOUBLE PRECISION, INTENT(IN) :: DAY                                           ! input: day with fraction
      CHARACTER(LEN=8) :: HMS_STRING                                                ! result: hh:mm:ss string

      DOUBLE PRECISION :: FRAC_DAY                                                  ! fractional part of input day
      INTEGER :: HRS, MIN, SEC                                                      ! hours, minutes, and seconds for frac_day


      FRAC_DAY = DAY - INT(DAY)                                                     ! find fractional day from input day
      HRS = INT(FRAC_DAY*24.0D0)                                                    ! compute hours
      MIN = INT((FRAC_DAY - HRS/24.0D0) * 1440.0D0)                                 ! compute minutes
      SEC = NINT((FRAC_DAY - HRS/24.0D0 - MIN/1440.0D0) * 86400.0D0)                ! compute seconds (rounded to nearest second)

      IF (SEC .EQ. 60) THEN                                                         ! if rounding sec rolled it to 60..
         SEC = 0                                                                    ! ..then set in back to 0..
         MIN = MIN + 1                                                              ! ..and increment min
      END IF

      IF (MIN .EQ. 60) THEN                                                         ! if min is now 60..
         MIN = 0                                                                    ! ..then set it back to 0..
         HRS = HRS + 1                                                              ! ..and increment hrs
      END IF

      WRITE (UNIT=HMS_STRING, FMT='(I2.2,1H:,I2.2,1H:,I2.2)')                       ! print hh:mm:ss to output string
     .      HRS, MIN, SEC
      END FUNCTION FORMAT_TIME
