#@------------------------------------------------------------------------------
It contains the Frequency Analysis Mapping On Unusual Sampling (FAMOUS) 
library of F. Mignard. The <org> folder contains its original source downloaded
via ftp: <ncftpget -R ftp://ftp.obs-nice.fr/pub/mignard/Famous/*>.

I have copied the necessary files into the <src/famous> directory and 
created a <Makefile> for its easier installation.
#@------------------------------------------------------------------------------
