
****************************************************
*                       FAMOUS                     *
*                       ------                     *
*  Frequency Analysis Mapping On Unusual Sampling  *
*  -         -        -       -  -       -         *
*         Program designed and written  by         *
*             F. Mignard   OCA/Cassiopee           *
*           Version 4.6 - December  2005           *
****************************************************
*    Run  made  on  05-12-2005  at  11:59:38       *
**************************************************** 


       SETTINGS OF THE PROGRAM       
       -----------------------

Data are read in the file          : hd_16141.txt                     in columns  1 and  2

Number of data points found        :    72

Results are written in the file    :  spectra_hd16141.txt          

Multiperiodic (.T.) Periodic (.F.) :   F

The number of lines searched is    :   2

Time offset: auto(.T.) or not(.T.) :   T

Freq. auto (.T.) or not (.F.)      :   T

S/N threshold for acceptance       :   2.0



        TIME SAMPLING IN THE DATA
        -------------------------

The sampling is not in  arithmetic progression

Mean stepsize in the time domain    :   0.41351E+02

Origin of time after time offset    :   0.11636E+05

Lowest  value of the time variable  :  -0.12697E+04

Largest value of the time variable  :   0.16662E+04

Smallest  two-point  interval       :   0.20000E-02

Nyquist freq. for uniform sampling  :   0.12092E-01




       REPORT OF THE FREQUENCY SEARCH
       ------------------------------

Line width in the frequency domain  :   0.60243E-03

Stepsize in the frequency domain    :   0.10040E-03

Lowest  frequency recoverable       :   0.50202E-03

Typical highest freq. recoverable   :   0.50607E+00

First (lowest) frequency analysed   :   0.50202E-03

Last (highest) frequency analysed   :   0.50604E+00

Number of frequencies sampled       :     5035



-------------------------------
REMOVAL OF THE AVERAGE or TREND      
-------------------------------

  Data RMS after removal of the trend :  0.815052E+01


-----------------------------
SOLUTION WITH     1 FREQUENCY
-----------------------------

       SOLUTION  AFTER NON LINEAR LEASTSQUARES FIT  
       -------------------------------------------  

       Frequency     Period      cos term      sine term       power   


  0   0  0.000000E+00              0.681854E-02
  1   0  0.132616E-01  0.7541E+02 -0.316347E+01 -0.104226E+02 0.108922E+02

  RMS of residuals  wrt. lin.  model  :  0.401769E+01



  Convergence code found for the line   1 :    0
  Code = 0   : good convergence, no more attempts
       = 100 : apply non-linear LSQR with good starting values
       = 200 : apply non-linear LSQR with poor starting values 




---------------------------------------  
SOLUTION  BEFORE FINAL LEASTSQUARES FIT  
---------------------------------------  

            Frequency        Period        cos term         sine term       power   

  0   0  0.00000000E+00                  2.19289841E-01
  1   0  1.32616136E-02  7.54056055E+01 -3.78089317E+00 -1.08348777E+01  1.147561E+01
  2   0  2.65799501E-02  3.76223431E+01 -2.49246361E+00 -2.68481145E-01  2.506882E+00





          FINAL SOLUTION  AFTER NON LINEAR FIT  
          ------------------------------------  


 ******************************************************* 
 * The linearized solution has properly converged      * 
 * You can trust the following solution                * 
 *  rms full     model :   0.362049E+01                *
 *  rms linear   model :   0.362049E+01                *
 ******************************************************* 

 ******************************************************* 
 * The linearized solution  is stable                  * 
 * You can trust the evaluation of the sigmas          * 
 ******************************************************* 


- Solution given as ( A*cos(omega*t) + B*sin(omega*t)  or  amp*cos(omega*t+phi)  
  for each term  A= a0 or a1*t or a2*t^2 (resp. for B ) 


- Frequencies in cycles/unit of time of the input data


- Periods in the same unit of time as the input data


- Amplitudes in the same unit as Y(t) in the input data


  ***  Origin of time in the following solution :   0.116357264E+05


              Frequency       Period        cos term        sine term    sigma(f)/f sigma(cos) sigma(sin)  amplitude    phase(deg)      S/N 

  0  t^0  0.00000000E+00                  1.55680165E-01                             4.519E-01            1.556802E-01
  1  t^0  1.32451952E-02  7.54990758E+01 -3.86810447E+00 -1.09220701E+01  1.040E-03  6.075E-01  6.710E-01 1.158680E+01  109.5018487  1.23E+01
  2  t^0  2.65706477E-02  3.76355147E+01 -2.71913533E+00 -2.64295992E-01  2.026E-03  6.445E-01  6.485E-01 2.731950E+00  174.4483730  2.96E+00


No line has been rejected from the significance test 

No line has been rejected from the spectral resolution test 


Execution time in seconds :       0.05
