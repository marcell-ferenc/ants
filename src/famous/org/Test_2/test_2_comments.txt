
--------------------------------
TEST_2 : Analysis of the results
--------------------------------
F. Mignard
13/07/05
--------------------------------


Report on the frequency analysis of a signal giving the radial velocity of a star 
due to the presence of a planet.

Data for the star HD_16141 with the observed radial velocity due to the planet orbiting the star.
                        (from Marcy et al, 2000, Ap. J. 2000, 536. File kindly provided by J. Marcy)


The test illustrates the problem of a period search on a very irregular sampling
and a low signal-to-noise ratio in the data.

The time span covers just over 3000 days and is made up of 72 data point.
So the average interval is 42 days. Were the sample uniform one should not 
be able to recover without  aliasing periodic signal with period less that
84 days (nu = 0.012 cy/day)

Having no idea of the period, the frequency search is run with the flauto = .true.
and  one lets the program determines a reasonable upper frequency for the search.
The upper  limit is found to be nu_max = 0.5 cy/day, or a period of 2 days. 

As one searches for a periodic signal, I put fmulti = .false. in the setting.
The program is then optimised to locate the fundamental and the first harmonic.
In this case the ratio of the two amplitudes is very simply related to the
orbital eccentricity.

The  period of 75.4 days is easily found (convergence code = 0). 
The final solution is as follows :



  ***  Origin of time in the following solution :   0.116357264E+05


              Frequency       Period        cos term        sine term    sigma(f)/f sigma(cos) sigma(sin)  amplitude    phase(deg)      S/N 

  0  t^0  0.00000000E+00                  1.55680165E-01                             4.519E-01            1.556802E-01
  1  t^0  1.32451952E-02  7.54990758E+01 -3.86810447E+00 -1.09220701E+01  1.040E-03  6.075E-01  6.710E-01 1.158680E+01  109.5018487  1.23E+01
  2  t^0  2.65706477E-02  3.76355147E+01 -2.71913533E+00 -2.64295992E-01  2.026E-03  6.445E-01  6.485E-01 2.731950E+00  174.4483730  2.96E+00


No line has been rejected from the significance test 

No line has been rejected from the spectral resolution test 


Execution time in seconds :       0.05


Period : 75.499+/- 0.08 days (published value 75.560 days).
Amplitude : 11.6 km/s
RMS of residuals  : 3.6 km/s

The second period of 37.6 days, is  the first harmonic but its amplitude of
2.73 km/s is marginally significant compared to the measurement noise, of the order of 3.5 km/s
per observation. Assuming it to be meaningful, this gives an eccentricity of 0.13 (published value 0.21 +/- 0.15).

The  periodogram in periodogram_01.png, shows clearly the main line at nu = 0.013 cy/day.
The program gives for this line S/N = 12, which may seem optimistic from the plots, if everyting
outside the line is considered as noise. In fact most of this signal comes from the spectral power
of the observing window with this line. So one must first remove the line and recompute the 
spetral power to assess the part of the power not linked to this line. This is done
in periodogram_01.png and this time the 'noise' is about 0.6-0.8 km/s, accounting for the 
significant S/N of the 74-day line.



