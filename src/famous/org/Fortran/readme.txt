!***********************************************************************************
!*                                                                                 *
!*                                 F A M O U S                                     *
!*                                                                                 * 
!*                  Frequency Analysis Mapping On Unusual Sampling                 * 
!*                  -         -        -       -  -       -                        * 
!*                                                                                 *
!*               SEARCH OF A SET OF FREQUENCIES IN A DISCRETE TIMES SERIES         *
!*                                                                                 *
!*                            Version 4.5 - July 2005                              * 
! **********************************************************************************
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   Version Date: 12-07-05
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!

This readme file provides the minimum needed to run the program on the test data placed
in the directory /Famous/fortran  and compare the results to the reference output.

The test illustrates the case of a nearly quasi periodic signal with a global linear
drift and a time-varying amplitude in the main periodic line.


input files :
 setting.txt          : program setting (keep as it stands for you first run so that you get
                                        exactly the results of the reference output)
                                        
 test_data.txt        : the data analysed here (plotted in plot_test_data.png)
 
 famous_driver.f90    : the main program to be run
 
 famouslib.f90        : the file with the two fortran 90 modules
 
 (simul_famous.f90 and the other drivers are not required for this test)
 
 
output file

 test_results_ref.txt : the output file with the report and the results


 Place the sources files, the data, the setting in a directory, compile, link and run.
 
 The output file (test_results.txt) should come in the same directory and should be
 identical to test_results_ref.txt.
 
 If everything goes as planned  you have been successful and here comes the point :
 
 *****  Guess what the data is all about ?   ****
 
 answer to : francois.mignard@obs-nice.fr
 
 Now you are ready to change the setting( e.g : turn the periodogram flag to true or
 to cancel the time dependance of the first periodic line to see what happens and obtain
 an alternative description of the signal)
 
 Then why not to look at the test cases in the test directories ?
 
 Good luck.
 
 