!***********************************************************************************
!*                                                                                 *
!*                                 F A M O U S                                     *
!*                                                                                 * 
!*                  Frequency Analysis Mapping On Unusual Sampling                 * 
!*                  -         -        -       -  -       -                        * 
!*                                                                                 *
!*               SEARCH OF A SET OF FREQUENCIES IN A DISCRETE TIMES SERIES         *
!*                                                                                 *
! **********************************************************************************
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   Version Date:  12-07-05
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!



What to do first and next ?

- look at the content of the associated directories, just to understand
  the overall arrangement.

- browse quickly in the famous_doc.txt, just to have an idea of the 
  global structure. No need to read carefully at the moment.

- open the \fortran\readme.txt and follow the instructions to
  check your implementation with a test file
  
- after that, have a deeper reading of the doc file and run the
  test cases in the relevant directories to experiment by yourself.
  
- finally you may be impatient to have a try on your own precious data
  that have been waiting for too long. This is where you
  should run into the first real troubles !
  
  that's all, 
  
  
  F. Mignard
  12/07/05