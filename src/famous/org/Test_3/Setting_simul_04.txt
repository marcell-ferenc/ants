1000                           number of data points in the simulation
' spectra_simul_04.txt'          ouptut file with the frequencies and power
  4                           numf : number of lines searched 
.true.                        flauto : flag for the frequency range .true. = automatic .false. : with preset frequencies
   0.0001d0                   frbeg  : lowest  frequency analysed in the periodograms (disabled if flauto =true)
   0.03d0                     frend  : largest frequency analysed in the periodograms (disabled if flauto =true)
 3.5d0                        Threshold in S/N to reject non statistically significant lines
.false.                        Flag for the output of the  periodograms and successive residuals (true = output)
 1                            iprint in freqsearch : 0 : setting and results, 1 : intermediate results, 2 : extended with correlations
 0                            iresid : = 1 residuals are printed. 0 : not printed
 .true.                       flagdeg : uniform degree for the mixed term (.true.) or not (.false.)
 0                            idm : Value of the degree in term like a +bt + ... ct^idm (used if flagdeg = .true.)
 1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  ideg(k) k=0..numf  loaded if flagdeg = .false.
 
 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30   (just to see the columns numbers to ease the editing of the above array)

