      program readmerrapressure
c     modified by Zhao Li, 22/01/2013 
     
      implicit none
      include 'netcdf.inc'
      integer nX,nY,nTime,ncount,tcount
      integer i,j,ifile,itime,iyr,jdoff,iymdjd
      integer imo,iday
      integer i100,i10,i1
c      parameter (nX = 540, nY = 361, nTime=4)
      parameter (nX = 540, nY = 361, nTime=4)
      real*4 varData(nX,nY,nTime), timeData(nTime)
      real*4 pres(nX,nY),lon,lat,offset,scale
      character varName*50,filein*100,fileout*36,finalou*19,chyr*4,fileout2*36
      character chmo*2,chday*2,chymd*8
      character dig(0:9)*1
      integer start(3), count(3), stride(3)
      
c     starting from first zonal grid, the first meridional grid, the 1st
c     depth (00 hour)
      data start /1, 1, 1/
c      data start /-5, 45, 1/
c     counts in each dimension, it is set to the largest possible count.
      data count /0, 0, 0/
c      data count /5, 5, 0/  
c     the intervals of the subsampling
      data stride /1, 1, 1/
      data dig/'0','1','2','3','4','5','6','7','8','9'/


C===  read the MERRA surface pressure data, 2D fields
c     Note: No offset has been added into MERRA pressure data
      varName = ' ' 
      ncount=0 
      
c     information files
      open(unit=99,status='unknown',file='merrarec.out')
      open(unit=98,status='unknown',file='merradat.time.rec')
      open(unit=23,status='unknown',file='datemerra.file')
c      open(unit=8,status='old',file='calendar.list')

c     user input: year,month,day e.g.: 2010 01 01
      print 130
130   format(/"Enter YEAR MONTH DAY (e.g.: 2010 1 1):")
      read(5,*) iyr,imo,iday
      i100=imo/100
      i10=(imo-100*i100)/10
      i1=(imo-100*i100-10*i10)
      
c     character month
      chmo=dig(i10)//dig(i1)
      i100=iday/100
      i10=(iday-100*i100)/10
      i1=(iday-100*i100-10*i10)
      
c     character day
      chday=dig(i10)//dig(i1)
      
c     character year month day
      write(chyr,'(i4)') iyr
      chymd=chyr//chmo//chday
      
c     print information on the screen, year month day in character
c     format
      print*,chymd
      write(23,'(a8)') chymd
      
c       jd=iymdjd(iyr,imon,iday)
c      iymdjd(2007,01,01)=2454101.5
c      jdoff=732678
c      print*,jdoff

c     path and filename of the INPUT *.nc file
c      filein='MERRA300.prod.assim.inst6_3d_ana_Nv.'
c     +   //chymd//'.SUB.nc'

      filein='MERRA301.prod.assim.inst6_3d_ana_Nv.'
     +   //chymd//'.SUB.nc'
     
c     filename of the OUTPUT
      fileout=chymd//'.merra'
      finalou='merra_'//chymd//'.dat'
      
c     print information on the screen
      print*,filein,fileout
      
c     open/create two empty file
      open(unit=20,status='unknown',file=fileout,
     +   form='unformatted')
      open(unit=22,status='unknown',file=finalou)
      
c     format for what?
100   format(a57)

c     You can trick the subroutine to read the latitude data from the netCDF file:
c     real*4 lat(224), time1(224)
c     call readnetcdf('test_2D.nc', 'lat', 1,224,1,lat,time1)

c      call readnetcdf('filein', 'lat', 1,361,1,lat,time1)
      
c     call readnetcdf soubrutine
      call readnetcdf(filein
     +    ,varName,start,count,stride,varData,timeData,tcount)

c        write the tcount variable into the "merrarec.out" file,
c        in this case it is 4 (four epoch layer)
         write(99,*) tcount
         
c      start a loop from the 1st until the last epoch layer
c      itime is the number of the time layer
       do itime=1,tcount
       
c        write the the number of the epoch layer and their
c        correcponding epoch into the "merradat.time.rec"
c        file (each 6 hours: 00,06,12,18)
         write(98,*) itime,(timeData(itime)/24.+1)
         
c        start the loop according to the zonal grid (longitude grid 1 - 540)
         do i=1,nX
           lon=-180+(i-1)*(2./3.)
c          start the loop according to the meridional grid (latitude grid 1 - 361)
           do j=1,nY
             lat=-90+(j-1)*(1./2.)
c            dataOutput -- array for the data output, pressure array
             pres(i,j)=varData(i,j,itime)
c            write results into ascii.test file             
c            pressure  =====> f9.2
c            longitude =====> f17.12
c            latitude  =====> f5.1
c             write(22,"(f17.12,1x,f5.1,1x,f9.2)") pres(i,j)
              write(22,"(f17.12,1x,f5.1,1x,f9.2,1x,i1)") 
     +        lon,lat,pres(i,j),itime
           end do
         end do
         ncount=ncount+1
         write(20) pres
      end do
      stop
      end

     
     
     
     
cb::iymdmj
c
      integer*4 function iymdjd( iyr, imon, iday )
c
c********1*********2*********3*********4*********5*********6*********7**
c
c name:       iymdmj
c version:    9703.07
c written by: M. Schenewerk
c purpose:    convert date to modified Julian date
c
c input parameters from the arguement list:
c -----------------------------------------
c iday              day
c imon              month
c iyr               year
c
c output parameters from arguement list:
c --------------------------------------
c iymdmj            modified Julian date
c
c
c local variables and constants:
c ------------------------------
c a                 temporary storage
c b                 temporary storage
c c                 temporary storage
c d                 temporary storage
c imop              temporary storage
c iyrp              temporary storage
c
c global variables and constants:
c ------------------------------
c
c
c calls:
c ------------------------------
c
c include files:
c ------------------------------
c
c common blocks:
c ------------------------------
c
c references:
c ------------------------------
c Duffett-Smith, Peter  1982, "Practical Astronomy With Your
c Calculator", 2nd edition, Cambridge University Press,
c New York, p.9
c
c comments:
c ------------------------------
c This subroutine requires the full year, i.e. 1992 rather than 92.
c It assumes any year between 0 and 78 to be 2000 + year
c
c********1*********2*********3*********4*********5*********6*********7**
c::last modification
c::8909.06, MSS, doc standard implimented
c::9004.17, MSS, change order yy mm dd
c::9703.07, MSS, changed rollback date from 2020 to 2078
c********1*********2*********3*********4*********5*********6*********7**
ce::iymdmj
c
c      implicit double precision(a-h, o-z)
c      implicit integer(i-n)
      implicit none
c
      integer       iday
      integer       imon
      integer       imop
      integer       iyr
      integer       iyrp
      integer*4     a, b, c, d
c
c........  0.0  explicit initialization
c
      if( iyr.lt.78 ) then
        iyrp= iyr + 2000
      elseif( iyr.lt.100 ) then
        iyrp= iyr + 1900
      else
        iyrp= iyr
      endif
c
      if( imon .lt. 3 ) then
        iyrp= iyrp - 1
        imop= imon + 12
      else
        imop= imon
      end if
c
c........  1.0  calculation
c
      a=  iyrp*0.01d0
      b=  2 - a + dint( a*0.25d0 )
      c=  365.25d0*iyrp
      d=  30.6001d0*(imop + 1)
      iymdjd=  b + c + d + iday-679006+2400000.5
c
      return
      end

      subroutine readnetcdf(fName,varName,
     &   start,count,stride,dataOutput,timeOutput,timecount)

C     subroutine readnetcdf(fName,varName,
C    &   start,count,stride,dataOutput,timeOutput)
C
C A single subroutine call to readnetcdf is all you need to read a real*4
C variable from a netCDF file. It greatly simplifies the process, shielding the
C user from the overly complicated netCDF interface.
C
C THE INPUT ARGUMENTS:
C--------------------
C
C fName -- Name of the netCDF file
C
C varName -- Name of the variable to be read. If is ' ', the variable will be
C set to the first variable in the netCDF file that has more than one
C dimensions (including the time dimension).
C
C start -- The starting corner of the subsample, e.g., /1,1,5,2/ is starting
C from first zonal grid, the first meridional grid, the 5th depth and the
C second time level.
C
C count -- The counts in each dimension, e.g., /360,224,46,3/. If an element of
C count is 0, it is set to the largest possible count.
C
C stride -- the intervals of the subsampling, e.g., /1,1,1,2/ for taking every 2 time levels.
C
C THE OUTPUT ARGUMENTS:
C--------------------
C
C dataOutput -- array for the data output
C
C timeOutput -- array for the time output. Here time is whatever the last
C dimension of the data.
C
C EXAMPLES:
C --------
C 
C To read the 2nd 3D field:        
C       integer start(4), count(4), stride(4)
C       data start /1,1,1,2/
C       data count /0,0,0,1/
C       data stride /1,1,1,1/
C       call readnetcdf('test_3D.nc', ' ', start,count,stride,data,time)
C
C To read the vertical profile at lon=60,lat=112, all time levels:
C       integer start(4), count(4), stride(4)
C       data start /60,112,1,1/
C       data count /1,1,0,0/
C       data stride /1,1,1,1/
C       call readnetcdf('test_3D.nc', ' ', start,count,stride,data,time)
C
C To read the time series at lon=60,lat=112, depth=5, all time levels:
C       integer start(4), count(4), stride(4)
C       data start /60,112,5,1/
C       data count /1,1,1,0/
C       data stride /1,1,1,1/
C       call readnetcdf('test_3D.nc', ' ', start,count,stride,data,time)
C
C To read the 5th (5th time level) 2D field :
C       integer start(3), count(3), stride(3)
C       data start /1,1,5/
C       data count /0,0,1/
C       data stride /1,1,1/
C       call readnetcdf('test_2D.nc', ' ', start,count,stride,data,time)
C
C You can trick the subroutine to read the latitude data from the netCDF file:
C       real*4 lat(224), time1(224)
C       call readnetcdf('test_2D.nc', 'lat', 1,224,1,lat,time1)

      implicit none
 
      include 'netcdf.inc'

C----- begin of interface
      character fName*(*)
      character varName*(*)
      integer start(*), count(*), stride(*),timecount
      character unitname*7
      real    dataOutput(*), timeOutput(*)
C----- end   of interface

C=== local variables
C--- used for NF subroutines' outputs
      character*(NF_MAX_NAME) dimName, varName1
      integer status1, ncID
      integer varID, timeID, timeID_ind 
      integer dims(NF_MAX_VAR_DIMS)
      integer nDims,nVars,NGAtts,unlimitedID
      real VRVAL(2),offset,scale
      integer varType,nDimsVar, nDimsTime
      integer dimIDs(NF_MAX_VAR_DIMS),dimIDs1(NF_MAX_VAR_DIMS),nAtts
      integer dimLen(NF_MAX_VAR_DIMS)

      integer count1(NF_MAX_VAR_DIMS)
      integer start1(NF_MAX_VAR_DIMS)

      character*(NF_MAX_NAME) timeName

      integer i, j, k, kk, maxCount

      integer  iLo, iHi
      INTEGER  IFNBLNK
      EXTERNAL IFNBLNK
      INTEGER  ILNBLNK
      EXTERNAL ILNBLNK

C=== open the netCDF file to read
      iLo = IFNBLNK(fName)
      iHi = ILNBLNK(fName)
      status1 = nf_open(fName(iLo:iHi), 0, ncID)

      write(6,*) ' '
      if (status1.ne.NF_NOERR) then 
        write(6,*) 'readnetcdf: Fail to open file:'
        write(6,*) fName(iLo:iHi)
        call flush(6)
      else
        write(6,*) 'Open file: ',
     &    fName(iLo:iHi) 
C       write(6,*) 'fileID=',ncID
        write(6,*) ' '
      endif

C=== get the dataOutput name, in case it is not given
      if (varName(1:1) .eq. ' ') then
        status1 = NF_INQ(ncID,nDims,nVars,NGAtts,unlimitedID)
        if (status1.ne.NF_NOERR) then 
          write(6,*) 'readnetcdf: Fail to get file info, for ncID:'
          write(6,*) ncID
          call flush(6)
        endif

        varID = -1
        do i=1,nVars
          status1 = NF_INQ_VAR(ncID,i,varName1,
     &             varType,nDimsVar,dimIDs,nAtts)
c         print*,nAtts
          if (status1.ne.NF_NOERR) then 
            write(6,*) 
     &      'readnetcdf: Fail to get variable info, for varID:',i
            call flush(6)
          endif

          if (nDimsVar .ge. 2) then
            iLo = IFNBLNK(varName1)
            iHi = ILNBLNK(varName1)

            write(6,*) 'readnetcdf: variable to be read: ===> ',
     &           varName1(iLo:iHi),
     &          ' <=== Please verify.'
            varID = i
C           write(6,*) 'varID = ',varID
            write(6,*) ' '
            goto 101
          endif
        enddo
  101   continue

        if (varID .eq. -1) then
          write(6,*) 
     &  'Cannot find  a multi-dimensional variable to read. Exiting.'
          return
        endif

C=== get the attributes
          status1=NF_GET_ATT_REAL(ncID,varID,'valid_range',VRVAL)
c         print*,'valid_range=',VRVAL(1),VRVAL(2)
          if (status1.ne.NF_NOERR) then 
            write(6,*) 
     &      'readnetcdf: Fail to get variable info, for varID:',i
            call flush(6)
          endif
          status1=NF_GET_ATT_REAL(ncID,varID,'add_offset',offset)
c         print*,'offset=',offset
          if (status1.ne.NF_NOERR) then 
            write(6,*) 
     &      'readnetcdf: Fail to get variable info, for varID:',i
            call flush(6)
          endif
          status1=NF_GET_ATT_REAL(ncID,varID,'scale_factor',scale)
c         print*,'scale=',scale
          if (status1.ne.NF_NOERR) then 
            write(6,*) 
     &      'readnetcdf: Fail to get variable info, for varID:',i
            call flush(6)
          endif
          status1=NF_GET_ATT_TEXT(ncID,varID,'units',unitname)
c         print*,'units=',unitname
          if (status1.ne.NF_NOERR) then 
            write(6,*) 
     &      'readnetcdf: Fail to get variable info, for varID:',i
            call flush(6)
          endif

C=== get the varID, if varName is given
      else
        varName1 = varName
        iLo = IFNBLNK(varName1)
        iHi = ILNBLNK(varName1)
        status1 = NF_INQ_VARID(ncID,varName1(iLo:iHi),varID)

        if (status1.ne.NF_NOERR) then 
          write(6,*) 'readnetcdf: Fail to get varID, for varName1:'
          write(6,*) varName1(iLo:iHi)
          call flush(6)
        else 
          write(6,*) 'readnetcdf: variable to be read: ',
     &           varName1(iLo:iHi)
C         write(6,*) 'varID = ',varID
          write(6,*) ' '
        endif
      endif

C=== get variable's properties
      status1 = NF_INQ_VAR(ncID,varID,varName1,
     &           varType,nDimsVar,dimIDs,nAtts)
C=== the default count
      if (status1.ne.NF_NOERR) then 
        write(6,*) 
     &    'readnetcdf: Fail to get variable info, for varID:',varID
        call flush(6)
      endif

      write(6,*) varName1(IFNBLNK(varName1):ILNBLNK(varName1))
     & , ' is a ', nDimsVar,'-D (including the time dimension) data'

      do i=1,nDimsVar
        status1 = NF_INQ_DIM(ncID,dimIDs(i),dimName,dimLen(i))

C       write(6,*) dimLen(i)

        if (start(i) .gt. dimLen(i)) then
          start1(i) = dimLen(i)
          count1(i) = 1
        else
          start1(i) = start(i)
          count1(i) = count(i)
        endif

        maxCount = (dimLen(i)-start1(i))/stride(i) + 1
        if (count1(i) .eq. 0 .or. count1(i) .gt. maxCount) then
          count1(i) = maxCount
        endif

        write(6,*) dimName(1:10), ':  start=', start1(i)
     &     , ';   count= ', count1(i)
     &     , ';   stride= ', stride(i)
      enddo
      write(6,*) ' '

C=== timeID
      iLo = IFNBLNK(dimName)
      iHi = ILNBLNK(dimName)
	timecount=count1(3)

      write(6,*) 'The time variable (the last dimension) to be read: ',
     &           dimName(iLo:iHi)
      write(6,*) ' '

      status1 = NF_INQ_VARID(ncID, dimName(iLo:iHi),timeID)
      if (status1.ne.NF_NOERR) then
        write(6,*) 'readnetcdf: Fail to get timeID.'
        call flush(6)
      endif

C=== read time data
      status1 = NF_GET_VARS_REAL(ncID, timeID, 
     &    start1(nDimsVar), count1(nDimsVar), stride(nDimsVar),
     &    timeOutput)
      if (status1.ne.NF_NOERR) then
        write(6,*) 'readnetcdf: Fail to read time values.'
        call flush(6)
      else
        write(6,*) 'read time values successfully.'
      endif

C=== read dataOutput
      status1 = NF_GET_VARS_REAL(ncID, varID, 
     &          start1, count1, stride,dataOutput)
      if (status1.ne.NF_NOERR) then
        write(6,*) 'readnetcdf: Fail to read data.'
        call flush(6)
      else
        write(6,*) 'read data successfully.'
        write(6,*) ' '
      endif

C=== close file
      status1 = NF_CLOSE(ncID)
      if (status1 .ne. NF_NOERR) then
        write(6,*) 'readnetcdf: Fail to close file.'
        call flush(6)
      endif

      return
      end

CStartOfInterface
      INTEGER FUNCTION IFNBLNK( string )
C     /==========================================================\
C     | FUNCTION IFNBLNK                                         |
C     | o Find first non-blank in character string.              |
C     \==========================================================/
      IMPLICIT NONE

      CHARACTER*(*) string
CEndOfInterface
      INTEGER L, LS

      LS     = LEN(string)
      IFNBLNK = 0
      DO 10 L = 1, LS
       IF ( string(L:L) .EQ. ' ' ) GOTO 10
        IFNBLNK = L
        GOTO 11
   10 CONTINUE
   11 CONTINUE

      RETURN
      END

CStartOfInterface
      INTEGER FUNCTION ILNBLNK( string )
C     /==========================================================\
C     | FUNCTION ILNBLNK                                         |
C     | o Find last non-blank in character string.               |
C     \==========================================================/
      IMPLICIT NONE

      CHARACTER*(*) string
CEndOfInterface
      INTEGER L, LS

      LS      = LEN(string)
      ILNBLNK = LS
      DO 10 L = LS, 1, -1
        IF ( string(L:L) .EQ. ' ' ) GOTO 10
         ILNBLNK = L
         GOTO 11
   10 CONTINUE
   11 CONTINUE

      RETURN
      END


