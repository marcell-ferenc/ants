      PROGRAM CORVALS
*+    
*  - - - - - - - - - - -
*   C O R V A L S
*  - - - - - - - - - - -
*
*  To generate correlation values for testing.
*
*  Created by: Marcell Ferenc (marcell.ferenc.uni@gmail.com)
*-----------------------------------------------------------------------

      IMPLICIT NONE

      INTEGER                   :: i
      DOUBLE PRECISION          :: r

      DO i = 0, 1000, 1
 
       r = i / 1000.
       WRITE(*,100) r
100    FORMAT (f5.3)

      END DO

      END PROGRAM CORVALS
