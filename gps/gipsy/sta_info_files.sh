#!/bin/bash

#@
#@ USAGE  : sta_info_files.sh -s [STATION_NAME] -d [YYYY-MM-DD] | [OPTIONS] 
#@
#@ OPTIONS:
#@          -h - help
#@
#@ TASK   : create station info files into $goa_sta_dir
#@          download station logsheet into $goa_logsheet
#@
#* by: marcell.ferenc@cnam.fr
#*

## default environment
v=no
sta_id=$goa_sta_dir/sta_id
sta_pos=$goa_sta_dir/sta_pos
sta_svec=$goa_sta_dir/sta_svec
ftps=( "ftp://igscb.jpl.nasa.gov/igscb/station/log" "ftp://rgpdata.ign.fr/pub/logsheet" )
mkdir -p $goa_logsheet
## touch sta_info files
touch $sta_id $sta_pos $sta_svec

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=tmp.$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )

## start directory
sdir=$( pwd )

## cleaning
clean()
{
 /bin/rm -rf ${tmp}* /tmp/${tmp}*
 ## restore default value 
 trap EXIT
 ## return to start directory
 cd $sdir
 exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
. $HOME/bin/func/gipsy.func
. $HOME/bin/func/general.func
. $HOME/bin/func/ts.func

## help
if [ "$#" -eq 0 ]; then clear; usage $0; exit 2; fi

## list of options the program will accept
optstring=d:s:hv

## interpret options
while getopts $optstring opt
do
 case $opt in
  d) date_str=$OPTARG ;;
  s) station=$OPTARG ;;
  v) v=yes ;;
  h) usage $0 ; exit 2 ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## date
if [ -z "$date_str" ]
then
 color_txt bold red "-b option is obligatory [date_str]"; exit 2
else
 date_inf=( $( isdate $date_str ) )
 if [ "${#date_inf[@]}" -gt 1 ]
 then
  Y4=${date_inf[1]}; MM=${date_inf[2]}; DD=${date_inf[3]}
 else
  color_txt bold red "not valid date [$date_str]"; exit 2
 fi
fi

## station
if [ -z "$station" ]
then
 color_txt bold red "-s option is obligatory [station]"; usage $0; exit 2
else
 inf=( $( isvalstatname $station ) )
 case ${inf[0]} in
  is_valid) sta_upper=${inf[1]}; sta_lower=${inf[2]} ;;
  no_valid) echo "not a valid station name [$station]"; exit 2 ;;
 esac
fi

## input for sta_info files
## the latest rinex found for the station will provide the apriori positions
crnx=$( find $goa_rnx_dir/$Y4 -type f -name ${sta_lower}\* -print | sort | tail -1 )

sta_svec_string()
{
printf " %4.4s %4.4s %4d %02d %02d %02d:%02d:00.00 %012.2f %10.10s %10.4f %10.4f %10.4f %010.4f %1.1s\n" \
"$name" "$sta" "$yr_ins" "${mn_ins#0}" "${dy_ins#0}" "${hr_ins#0}" "${mm_ins#0}" "$dur" "$ant" 0 0 0 "$arp_u" l
}

## sta_id
echo
echo $sta_id
echo

grep -i $station $sta_id &> /dev/null
if [ $? -ne 0 ]
then
 [ "$v" == "yes" ] && color_txt bold blue "$station not in $sta_id - update"
 rnx2sta_id $crnx >> $sta_id
else
 [ "$v" == "yes" ] && color_txt bold green "$station in $sta_id"
fi

## sta_pos
echo
echo $sta_pos
echo

grep -i $station $sta_pos &> /dev/null
if [ $? -ne 0 ]
then
 [ "$v" == "yes" ] && color_txt bold blue "$station not in $sta_pos - update"
 rnx2sta_pos $crnx >> $sta_pos
else
 [ "$v" == "yes" ] && color_txt bold green "$station in $sta_pos"
fi

## sta_svec
## it will be read from station log file
echo
echo $sta_svec
echo

grep -i $station $sta_svec &> /dev/null
if [ $? -ne 0 ]
then

 [ "$v" == "yes" ] && color_txt bold blue "$station not in $sta_svec - update"

 cd $goa_logsheet
 
 ## monument information
 mon_inf=$tmp.mon.inf
 ## station information
 sta_inf=$tmp.sta.inf
 ## antenna information
 ant_inf=$tmp.ant.inf
 ## receiver information
 rec_inf=$tmp.rec.inf
 
 ## station log file
 #logsheet=$( ncftpls -u anonymous -p anonymous -x "-l" ${server[0]} | awk '/'${sta_lower}'_/ { print $NF }' | tail -n1 )
 ## download station log file via ftp
 for ftp in ${ftps[@]}
 do
  echo $ftp
  ncftpls -u anonymous -p anonymous -x "-l" "$ftp/${sta_lower}*" \
  | awk '{ print $NF }' \
  | sort -r \
  | head -n 1 \
  | tee $tmp \
  | xargs -I {} ncftpget -u anonymous -p anonymous $ftp/{} &> /dev/null
  exit_status=$?
  echo $exit_status
  if [ $exit_status -eq 0 ]; then break; fi
 done

 logsheet="$( cat $tmp )"
 echo $logsheet
 ## remove special character from the station log file
 tr -d '\r' < $logsheet > $tmp

 ## information
 echo
 echo logsheet: $goa_logsheet/$logsheet
 echo
 
 ## splitted log file
 grep -A19 "1.   Site Identification of the GNSS Monument" $tmp > $mon_inf
 grep -A13 "2.   Site Location Information" $tmp > $sta_inf
 
 ## antenna
 grep -A13 -e '^[4]\.[0-9]' -e '^[4]\.[0-9][0-9]' $tmp > $ant_inf
 split -l 15 $ant_inf $ant_inf.
 
 /bin/rm -f $tmp
 
 ## site specific
 name=$( awk '/Four Character ID/ { print substr($0,33) }' $mon_inf )
 
 for ant_file in $ant_inf.*
 do
 
  ant="$( awk '/Antenna Type/ { print substr($0,33,20) }' $ant_file )"
  ant_num="$( awk '/Serial Number/ { print substr($0,33,5) }' $ant_file )"
  arp_e="$( awk '/Marker-\>ARP East Ecc\(m\)/ { print substr($0,33,8) }' $ant_file )"
  arp_n="$( awk '/Marker-\>ARP North Ecc\(m\)/ { print substr($0,33,8) }' $ant_file )"
  arp_u="$( awk '/Marker-\>ARP Up Ecc. \(m\)/ { print substr($0,33,8) }' $ant_file )"
  ant_date_ins="$( awk '/Date Installed/ { print substr($0,33,16) }' $ant_file )"
  yr_ins=${ant_date_ins:0:4}
  mn_ins=${ant_date_ins:5:2}
  dy_ins=${ant_date_ins:8:2}
  hr_ins=${ant_date_ins:11:2}
  hr_ins=${hr_ins:-00}
  mm_ins=${ant_date_ins:14:2}
  mm_ins=${mm_ins:-00}
  ant_date_ins_sec="$( date +%s -d "${yr_ins}-${mn_ins}-${dy_ins}T${hr_ins}:${mm_ins}:00" )"
  ant_date_rem="$( awk '/Date Removed/ { print substr($0,33,16) }' $ant_file )"
  yr_rem=${ant_date_rem:0:4}
  mn_rem=${ant_date_rem:5:2}
  dy_rem=${ant_date_rem:8:2}
  hr_rem=${ant_date_rem:11:2}
  hr_rem=${hr_rem:-00}
  mm_rem=${ant_date_rem:14:2}
  mm_rem=${mm_rem:-00}
  
  if [[ ( "$ant_date_rem" == "CCYY-MM-DDThh:mm" ) || ( "$ant_date_rem" == "CCYY-MM-DD      " ) ]]
  then
   rem_string=$( date --iso-8601 )
   yr_rem=${rem_string:0:4}
   mn_rem=${rem_string:5:2}
   dy_rem=${rem_string:8:2}
   hr_rem=00
   mm_rem=00
   ss_rem=00
   ant_date_rem_sec="$( date +%s -d "$rem_string" )"
  else
   ant_date_rem_sec="$( date +%s -d "${yr_rem}-${mn_rem}-${dy_rem}T${hr_rem}:${mm_rem}:00" )"
  fi

  dur=$(( $ant_date_rem_sec - $ant_date_ins_sec ))

  sta_svec_string >> $tmp

 done

 ## merge with sta_svec file
 sort -s -n -r -k 5 $tmp | sort -s -n -r -k 4 | sort -s -n -r -k 3
 sort -s -n -r -k 5 $tmp | sort -s -n -r -k 4 | sort -s -n -r -k 3 >> $sta_svec

else
 [ "$v" == "yes" ] && color_txt bold green "$station in $sta_svec"
fi
