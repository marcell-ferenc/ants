#!/bin/bash

#@
#@ USAGE  : tropo.sh -s [SSSS] -b [YYYY-MM-DD] -e [YYYY-MM-DD] [OPTIONS]
#@
#@ OPTIONS:
#@
#@          -t - troposphere model             [ STATIC, GPT or VMF1GRID   DEFAULT ==> VMF1GRID]
#@          -i - troposphere sampling interval [ in seconds                DEFAULT ==>    3600 ]
#@          -h - help
#@
#* by     : marcell.ferenc@cnam.fr

sta_pos=$goa_sta_dir/sta_pos

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=tmp.$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )

## start directory
sdir=$( pwd )

## cleaning
clean()
{
 /bin/rm -f ${tmp}*
 ## restore default value 
 trap EXIT
 ## go back to launch dir
 cd $sdir
 exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
. $HOME/bin/func/ts.func
. $HOME/bin/func/general.func

## help
if [ "$#" -eq 0 ]; then clear; usage $0; exit 2; fi

## list of options the program will accept
optstring=b:e:i:s:t:h

## interpret options
while getopts $optstring opt
do
 case $opt in
  b) beg_str=$OPTARG ;;
  e) end_str=$OPTARG ;;
  i) sampt_t=$OPTARG ;;
  s) station=$OPTARG ;;
  t) tropmodel=$OPTARG ;;
  h) clear; usage $0; exit 2 ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## begin date
if [ -z "$beg_str" ]
then
 color_txt bold red "-b option is obligatory [begin_date]"; exit 2
else
 beg_inf=( $( isdate $beg_str ) )
 if [ "${#beg_inf[@]}" -gt 1 ]
 then
  beg_YY=${beg_inf[1]}; beg_MM=${beg_inf[2]}; beg_DD=${beg_inf[3]}
 else
  color_txt bold red "not valid begin_date [$beg_str]"; exit 2
 fi
fi

## end date
if [ -z "$end_str" ]
then
 color_txt bold red "-e option is obligatory [end_date]"; exit 2
else
 end_inf=( $( isdate $end_str ) )
 if [ "${#end_inf[@]}" -gt 1 ]
 then
  end_YY=${end_inf[1]}; end_MM=${end_inf[2]}; end_DD=${end_inf[3]}
 else
  color_txt bold red "not valid end_date [$end_str]"; exit 2
 fi
fi

## start and end date in seconds
stsec=$( cal2sec $( date --date="$beg_str -1 day" "+%Y %m %d" ) 00 00 00 )
#stsec=$( cal2sec $( date --date="$beg_str" "+%Y %m %d" ) 00 00 00 )
#stsec="${stsec#"${stsec%%[![:space:]]*}"}"
#endsec=$( cal2sec $( date --date="$end_str +1 day" "+%Y %m %d" ) 24 00 00 )
endsec=$( cal2sec $( date --date="$end_str" "+%Y %m %d" ) 24 00 00 )
#endsec="${endsec#"${endsec%%[![:space:]]*}"}"
#dur=$(( ($(date --date="$end_str +1 day" +%s) - $(date --date="$beg_str -1 day" +%s) )/(60*60*24) + 1 ))
start=$( date --date="$beg_str -1 day" "+%Y-%m-%d" )
## we need +1 day too and if we would use like that it would stop at end_str and not at end_str+1 --> +2
end=$( date --date="$end_str +2 day" "+%Y-%m-%d" )



## station
if [ -z "$station" ]
then
 color_txt bold red "-s option is obligatory [station_name]"; exit 2
else
 ## station name test
 sta_inf=( $( isvalstatname $station ) ); sta_upper=${sta_inf[1]}; sta_lower=${sta_inf[2]}
 if [ "${#sta_inf[@]}" -gt 1 ]
 then
  ## test if station in sta_pos file
  grep -i $sta_upper $sta_pos &>/dev/null
  if [ $? -eq 0 ]
  then
   ## xyz to lat,lon,height
   xyz=( $( awk -v sta=$sta_upper '$0 ~ sta { print sta,$7,$8,$9 }' $sta_pos ) )
   plh=( $( echo ${xyz[@]} | xyz2flh_iers2010.bin ) )
   lat=${plh[2]}; lon=${plh[1]}; hgt=${plh[3]}
  else
   color_txt bold red "station not in sta_pos file"; exit 2
  fi
 else
  color_txt bold red "not a valid station name $station"; exit 2
 fi
fi
echo "station              ==> $sta_upper"

## tropmodel
if [ -z "$tropmodel" ]
then
 tropmodel=VMF1GRID
else
 case $tropmodel in
  GPT|VMF1GRID) tropmodel=$tropmodel ;;
  *) color_txt bold red "not a valid tropo model name $tropmodel"; exit 2 ;;
 esac
fi
echo "tropmodel            ==> $tropmodel"

## tropo sampling interval
if [ -z "$samp_t" ]
then
 samp_t=3600
else
 if [ "$( isint $samp_t )" == "no_integer" ]
 then
  color_txt bold red "not a valid sampling interval $samp_t"; usage $0; exit 2
 fi
fi
echo "troposphere sampling ==> $samp_t"
echo

## go to final location
tropo_dir=$goa_tpd_dir/$tropmodel
mkdir -p $tropo_dir
cd $tropo_dir

## remove files with same names
/bin/rm -f $sta_upper.TDP* 2> /dev/null

## calculate tropo values
if [ "$tropmodel" == "GPT" ]
then

 command="tropnominal -n $sta_upper -m $tropmodel -latdeg $lat -londeg $lon -h_m $hgt -stsec $stsec -endsec $endsec -samp $samp_t"
 
elif [ "$tropmodel" == "VMF1GRID" ]
then

 #VMF1_DIR=$tropo_dir/VMF1GRID
 VMF1_DIR=$tropo_dir
 mkdir -p $VMF1_DIR
 
 ## necessary files for VMF1/ECMWF
 
 ## orography ellipsoid file
 if [ ! -e $VMF1_DIR/orography_ell ]
 then
  #wget http://mars.hg.tuwien.ac.at/~ecmwf1/GRID/orography_ell
  wget -O $VMF1_DIR/orography_ell http://ggosatm.hg.tuwien.ac.at/DELAY/GRID/orography_ell
 fi

 while [ "$start" != "$end" ]
 do

  date_inf=( $( date --date="$start" "+%Y %y %j" ) )
  
  y4=${date_inf[0]}
  y2=${date_inf[1]}
  doy=${date_inf[2]}
  
  mkdir -p $VMF1_DIR/$y4/{ah,aw,zh,zw}

  ##color_txt bold blue "$y4 - $doy"

  #echo $VMF1_DIR/$y4/ah/ah${y2}${doy}.h
  #echo $VMF1_DIR/$y4/aw/aw${y2}${doy}.h
  
  ## all
  #wget http://ggosatm.hg.tuwien.ac.at/DELAY/GRID/STD/$y4/ah$y4.tar.gz
  #tar -xvf ah$y4.tar.gz
  #wget http://ggosatm.hg.tuwien.ac.at/DELAY/GRID/STD/$y4/aw$y4.tar.gz
  #tar -xvf aw$y4.tar.gz
  #wget http://ggosatm.hg.tuwien.ac.at/DELAY/GRID/STD/$y4/zh$y4.tar.gz
  #tar -xvf zh$y4.tar.gz  
  #wget http://ggosatm.hg.tuwien.ac.at/DELAY/GRID/STD/$y4/zw$y4.tar.gz
  #tar -xvf zw$y4.tar.gz
  
  for ep in 00 06 12 18
  do
  
   #echo $ep
   
   ## ah files
   if [ ! -e $VMF1_DIR/$y4/ah/ah${y2}${doy}.h${ep} ]
   then
    echo wget -O $VMF1_DIR/$y4/ah/ah${y2}${doy}.h${ep} http://ggosatm.hg.tuwien.ac.at/DELAY/GRID/STD/$y4/ah${y2}${doy}.h$ep >> $tmp.ah
   fi
 
   ## aw files
   if [ ! -e $VMF1_DIR/$y4/aw/aw${y2}${doy}.h${ep} ]
   then
    echo wget -O $VMF1_DIR/$y4/aw/aw${y2}${doy}.h${ep} http://ggosatm.hg.tuwien.ac.at/DELAY/GRID/STD/$y4/aw${y2}${doy}.h$ep >> $tmp.aw 
   fi

   ## zh files
   if [ ! -e $VMF1_DIR/$y4/zh/zh${y2}${doy}.h${ep} ]
   then
    echo wget -O $VMF1_DIR/$y4/zh/zh${y2}${doy}.h${ep} http://ggosatm.hg.tuwien.ac.at/DELAY/GRID/STD/$y4/zh${y2}${doy}.h$ep >> $tmp.zh
   fi

   ## zw files
   if [ ! -e $VMF1_DIR/$y4/zw/zw${y2}${doy}.h${ep} ]
   then
    echo wget -O $VMF1_DIR/$y4/zw/zw${y2}${doy}.h${ep} http://ggosatm.hg.tuwien.ac.at/DELAY/GRID/STD/$y4/zw${y2}${doy}.h$ep >> $tmp.zw
   fi

  done
  start=$( date --date="$start +1 day" "+%Y-%m-%d" )
 done

 [ -e $tmp.ah ] && sh $tmp.ah
 [ -e $tmp.aw ] && sh $tmp.aw
 [ -e $tmp.zh ] && sh $tmp.zh
 [ -e $tmp.zw ] && sh $tmp.zw

 command="tropnominal -n $sta_upper -m $tropmodel -vmf1_dir $VMF1_DIR -latdeg $lat -londeg $lon -h_m $hgt -stsec $stsec -endsec $endsec -samp $samp_t"
 
fi

echo
color_txt bold green "$tropmodel"
echo "$command"
echo

$command
