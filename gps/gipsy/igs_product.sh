#!/bin/bash

#@
#@ USAGE     : igs_product.sh -b [YYYY-MM-DD] -e [YYYY-MM-DD] [OPTIONS]
#@
#@ OPTIONS   :
#@             -t [ARG] -- data_type    -- DEFAULT: final
#@             -v       -- verbose mode -- DEFAULT: no
#@             -h       -- help
#@
#@ TASK      : this script inwoke the "igs_orbits_clks_fetch.pl & igs2flinn.pl" official GIPSY scripts with their necessary arguments
#@
#* by        : marcell.ferenc@cnam.fr
#*

server_a=ftp://igscb.jpl.nasa.gov/pub/station/general/pcv_archive
server=ftp://sideshow.jpl.nasa.gov/pub/gipsy_files/gipsy_params/antenna_cals_xmit
 
## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=tmp.$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )

## start directory
sdir=$( pwd )

## cleaning
clean()
{
 /bin/rm -rf ${tmp}* /tmp/${tmp}*
 ## restore default value 
 trap EXIT
 ## return to start directory
 cd $sdir
 exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
. $HOME/bin/func/ts.func
. $HOME/bin/func/general.func

## help
if [ "$#" -eq 0 ]; then clear; usage $0; exit; fi

## list of options the program will accept
optstring=b:e:t:h

## interpret options
while getopts $optstring opt
do
 case $opt in
  b) beg_str=$OPTARG ;;
  e) end_str=$OPTARG ;;
  t) data_type=$OPTARG ;;
  h) clear; usage $0 ; exit 2 ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## begin date
if [ -z "$beg_str" ]
then
 color_txt bold red "-b option is obligatory [begin_date]"; exit
else
 beg_inf=( $( isdate $beg_str ) )
 if [ "${#beg_inf[@]}" -eq 1 ]; then color_txt bold red "not valid begin_date $beg_str"; exit; fi
fi

## end date
if [ -z "$end_str" ]
then
 color_txt bold red "-e option is obligatory [end_date]"; exit
else
 end_inf=( $( isdate $end_str ) )
 if [ "${#end_inf[@]}" -eq 1 ]; then color_txt bold red "not valid end_date $end_str"; exit; fi
fi

## data type
if [ -z "$data_type" ]
then
 data_type=final
else
 case $data_type in
  final) ;;
  *) color_txt bold red "not valid data_type $data_type"; exit ;;
 esac
fi

orb_start=$( date --date="$beg_str day" "+%Y-%m-%d" )
orb_end=$( date --date="$end_str +1 day" "+%Y-%m-%d" )

## while - orbit exist
while [ "$orb_start" != "$orb_end" ]
do

 orb_Y4=${orb_start:0:4}
 
 [ -e $goa_igs_dir/$data_type/$orb_Y4/$orb_start.wlpb.gz ] && color_txt bold green "orbit files exist for $orb_start" \
 && orb_start=$( date --date="$orb_start +1 day" "+%Y-%m-%d" ) && continue

 ## start the work
 start=$( date --date="$beg_str -1 day" "+%Y-%m-%d" )
 ## -1 and +1 days needed for SP3 files (see igs2flinn.pl manual), because of the while loop +2 days needed
 ## which will give the desired data span
 end=$( date --date="$end_str +2 day" "+%Y-%m-%d" )

 ##
 ## igs_orbits_clks_fetch.pl
 ##

 ## while - igs_orbits_clks_fetch
 while [ "$start" != "$end" ]
 do

  ## obtain gps week of the desired date
  Y4=${start:0:4}; MM=${start:5:2}; DD=${start:8:2}
  secs=$( cal2sec $Y4 $MM $DD 00 00 00 )
  gpsw=$( sec2gpsws $secs | awk '{ print $1 }' )
  gpswd=$( sec2gpsws $secs | awk '{ print int($2/86400) }' )
  gpsw_str=$gpsw$gpswd

  ## change from ITRF2005 to ITRF2008
  sys_ch=$( cal2sec 2011 04 17 )

  ## ITRF2005 before 2011/04/17
  if [ $( gmtmath -Q $secs $sys_ch LT = ) -eq 1 ]
  then
   model=igs05
  ## ITRF2008 after 2011/04/17
  elif [ $( gmtmath -Q $secs $sys_ch GE = ) -eq 1 ]
  then
   model=igs08
  ## end fi - ITRFYY
  fi

  sp3clk_dir=$goa_igs_dir/sp3clk_$data_type/$Y4

  [ ! -d $sp3clk_dir ] && mkdir -p $sp3clk_dir

  cd $sp3clk_dir

  if [ ! -e igs$gpsw_str.sp3.Z ]
  then
   color_txt bold blue "fetch igs$gpsw_str.sp3.Z"
   igs_orbits_clks_fetch.pl -b $start -e $start -type $data_type
  else
   color_txt bold green "igs$gpsw_str.sp3.Z exist"
  fi
 
  cd $goa_atx_dir
 
  ## corresponding atx file from SP3 file | not sure it is available with tdp and xyz files so we will look for the closest one
  map_base=$( zcat $sp3clk_dir/igs$gpsw_str.sp3.Z | grep -m 1 "PCV:" | awk '{ print tolower(substr($0,8,10)) }' )
 
  ## test corresponding atx tdp xyz files
  for ext in atx tdp xyz
  do
   if [ ! -e $map_base.$ext ]
   then
    echo "ini: $map_base"
    ncftpget -u anonymous -p anonymous $server/$map_base.$ext.gz &> /dev/null
    if [ $? -ne 0 ]
    then
     max_iter=10 ## max 10 gps week later
     iter=0
     while :
     do
      iter=$(( $iter + 1 ))
      num="${map_base#?????_}"; str="${map_base%????}"
      new_gpsw=$(( $num + 1 )); new_base=$str$new_gpsw
      echo new: $new_base
      if [ ! -e $new_base.$ext ]
      then
       ncftpget -u anonymous -p anonymous $server/$new_base.$ext.gz &> /dev/null
       if [ $? -ne 0 ]
       then
        num="${new_base#?????_}"; str="${new_base%????}"
        new_gpsw=$(( $num + 1 )); new_base=$str$new_gpsw
        if [ $iter -eq $max_iter ]
        then
         color_txt bold red "$new_base.$ext error - try to look max 10 gps week back"; error=999
        fi
       else
        echo "---: $new_base.$ext ok"; break
       fi
      else
       echo "---: $new_base.$ext ok"; break
      fi
     done
     if [ ${error:-0} -eq 999 ]
     then
      max_iter=10 ## max 10 gps_week back
      iter=0
      while :
      do
       iter=$(( $iter - 1 ))
       num="${map_base#?????_}"; str="${map_base%????}"
       new_gpsw=$(( $num - 1 )); new_base=$str$new_gpsw
       echo new: $new_base
       if [ ! -e $new_base.atx ]
       then
        ncftpls -u anonymous -p anonymous $server/$new_base.atx.gz &> /dev/null
        if [ $? -ne 0 ]
        then
         num="${new_base#?????_}"; str="${new_base%????}"
         new_gpsw=$(( $num - 1 )); new_base=$str$new_gpsw
         if [ $iter -eq $max_iter ]
         then
          color_txt bold red "$new_base error"; break
         fi
        else
         echo "---: $new_base ok"; break
        fi
       else
        echo "---: $new_base ok"; break
       fi
      done
     fi
     unset error
    fi
    #antex file on server_a probably available but not the tdp and xyz files
    #if [ "$ext" == "$atx" ]
    #then
    # [ ! -e $map_base.$ext ] && ncftpget -u anonymous -p anonymous $server_a/$map_base.$ext &> /dev/null
    #fi
    [ -e $new_base.$ext.gz ] && gunzip -f $new_base.$ext.gz
   else
    color_txt bold blue "$map_base.$ext already exists"
   fi
  done
  ## increment the date
  start=$( date --date="$start +1 day" "+%Y-%m-%d" )

 ## end while - igs_orbits_clks_fetch
 done

 ##
 ## igs2flinn.pl
 ##
 
 start=$( date --date="$beg_str" "+%Y-%m-%d" )
 end=$( date --date="$end_str +1 day" "+%Y-%m-%d" )

 ## while - igs2flinn
 while [ "$start" != "$end" ]
 do

  echo "day: $start"

  mkdir -p -m 700 /tmp/${tmp}_dir && cd $_

  # 0 day
  Y4=${start:0:4}; MM=${start:5:2}; DD=${start:8:2}
  secs=$( cal2sec $Y4 $MM $DD 00 00 00 )
  gpsw=$( sec2gpsws $secs | awk '{ print $1 }' )
  gpswd=$( sec2gpsws $secs | awk '{ print int($2/86400) }' )
  gpsw_str=$gpsw$gpswd
  cp $goa_igs_dir/sp3clk_$data_type/$Y4/igs$gpsw_str.???.Z .
 
  des_dir=$goa_igs_dir/$data_type/$Y4
  mkdir -p $des_dir
 
  #-1 day
  tmp_d=$( date --date="$start -1 day" "+%Y-%m-%d" )
  tmp_Y4=${tmp_d:0:4}; tmp_MM=${tmp_d:5:2}; tmp_DD=${tmp_d:8:2}
  tmp_secs=$( cal2sec $tmp_Y4 $tmp_MM $tmp_DD 00 00 00 )
  tmp_gpsw=$( sec2gpsws $tmp_secs | awk '{ print $1 }' )
  tmp_gpswd=$( sec2gpsws $tmp_secs | awk '{ print int($2/86400) }' )
  tmp_gpsw_str=$tmp_gpsw$tmp_gpswd
  cp $goa_igs_dir/sp3clk_$data_type/$tmp_Y4/igs$tmp_gpsw_str.???.Z .
 
  #+1 day
  tmp_d=$( date --date="$start +1 day" "+%Y-%m-%d" )
  tmp_Y4=${tmp_d:0:4}; tmp_MM=${tmp_d:5:2}; tmp_DD=${tmp_d:8:2}
  tmp_secs=$( cal2sec $tmp_Y4 $tmp_MM $tmp_DD 00 00 00 )
  tmp_gpsw=$( sec2gpsws $tmp_secs | awk '{ print $1 }' )
  tmp_gpswd=$( sec2gpsws $tmp_secs | awk '{ print int($2/86400) }' )
  tmp_gpsw_str=$tmp_gpsw$tmp_gpswd
  cp $goa_igs_dir/sp3clk_$data_type/$tmp_Y4/igs$tmp_gpsw_str.???.Z .
  
  map_base=$( zcat $goa_igs_dir/sp3clk_$data_type/$Y4/igs$gpsw_str.sp3.Z | grep -m 1 "PCV:" | awk '{ print tolower(substr($0,8,10)) }' )
 
  ## test corresponding atx tdp xyz files
  if [ ! -e $goa_atx_dir/$map_base.atx ]
  then
   echo "ini: $map_base"
   ncftpls -u anonymous -p anonymous $server/$map_base.atx.gz &> /dev/null
   if [ $? -ne 0 ]
   then
    max_iter=10 ## max 10 gps_week later
    iter=0
    while :
    do
     iter=$(( $iter + 1 ))
     num="${map_base#?????_}"; str="${map_base%????}"
     new_gpsw=$(( $num + 1 )); new_base=$str$new_gpsw
     echo new: $new_base
     if [ ! -e $new_base.atx ]
     then
      ncftpls -u anonymous -p anonymous $server/$new_base.atx.gz &> /dev/null
      if [ $? -ne 0 ]
      then
       num="${new_base#?????_}"; str="${new_base%????}"
       new_gpsw=$(( $num + 1 )); new_base=$str$new_gpsw
       if [ $iter -eq $max_iter ]
       then
        color_txt bold red "$new_base error - try to look max 10 gps week back"; error=999
       fi
      else
       echo "---: $new_base ok"; break
      fi
     else
      echo "---: $new_base ok"; break
     fi
    done
    if [ ${error:-0} -eq 999 ]
    then
     max_iter=10 ## max 10 gps_week back
     iter=0
     while :
     do
      iter=$(( $iter - 1 ))
      num="${map_base#?????_}"; str="${map_base%????}"
      new_gpsw=$(( $num - 1 )); new_base=$str$new_gpsw
      echo new: $new_base
      if [ ! -e $new_base.atx ]
      then
       ncftpls -u anonymous -p anonymous $server/$new_base.atx.gz &> /dev/null
       if [ $? -ne 0 ]
       then
        num="${new_base#?????_}"; str="${new_base%????}"
        new_gpsw=$(( $num - 1 )); new_base=$str$new_gpsw
        if [ $iter -eq $max_iter ]
        then
         color_txt bold red "$new_base error"; break
        fi
       else
        echo "---: $new_base ok"; break
       fi
      else
       echo "---: $new_base ok"; break
      fi
     done
    fi
    unset error
   fi
  else
   new_base=$map_base
  fi

  ## convert SP3 files to GIPSY format
  igs2flinn.pl -b $start -e $start -flinn_dir $des_dir -map_base $new_base
 
  ## wlpb file
  cd $des_dir
  if [ ! -e $start.wlpb.gz ]
  then
   echo "---: $start.wlpb.gz"
   ncftpget ftp://sideshow.jpl.nasa.gov/pub/JPL_GPS_Products/Final/$Y4/$start.wlpb.gz 2> /dev/null
  fi
 
  ## increment the date
  start=$( date --date="$start +1 day" "+%Y-%m-%d" )
 
 ## end while - igs2flinn
 done

 ## increment the date
 orb_start=$( date --date="$orb_start +1 day" "+%Y-%m-%d" )

## end while - orbit exist
done
