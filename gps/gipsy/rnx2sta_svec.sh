#!/bin/bash

#
# ADD_STA_SVEC
#
# Script to add station site vectors to sta_info/sta_svec.
# N.E. King
# October 1993
# December 1993:  Modified to look for names in GIPSY format.
# Oct 1995 Adopted and Modified by Ken Hurst
#
#$Id: rnx2sta_svec 32041 2010-05-10 20:02:54Z jpweiss $
#                 Account for possible year and month changes in calculation of time duration.

## modified and transformed into bash
## M. Ferenc
## July 2013

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.bash}

## temporary filenames
rnd=$RANDOM
rnd_date=$( date +%Y%m%d_%H%M%S )
output=$TMPDIR/$scriptname.$rnd.$rnd_date

usage()
{
echo "                                                             "
echo "rnx2sta_svec [-a antenna_height(m) ] rnx_file1 rnx_file2...."
echo "   writes sta_svec entry to stdout                           "
echo "                                                             "
}

## interpret options
while getopts a:hH opt
do
 case $opt in
   a) ant_hgt=$OPTARG ;;
 h|H) usage; exit ;;
   *) usage; exit ;;
 esac
done

shift $(( $OPTIND - 1 ))

if [ $# -eq 0 ]
then
 usage
 exit
if

for item in "$@"
do
 ## test if rinex exists
 if [[ ( -e $item ) && ( -r $item ) ]]
 then
  ## test valid rinex name
  case $item in
    [a-z][a-z0-9][a-z0-9][a-z0-9][0-9][0-9][0-9][a-z0-9].[0-9][0-9]o) rnx_type=uncompressed ;;
  [a-z][a-z0-9][a-z0-9][a-z0-9][0-9][0-9][0-9][a-z0-9].[0-9][0-9]d.Z) rnx_type=compressed ;;
  *) echo "not a valid rinex name"; continue ;;
  esac
 else
  echo "$item does not exist or can not read"
  continue
 ## end if - rinex exists
 fi
 
 tmp=$( crz2rnx -echo $item )
 
 sta=${tmp##*/}
 sta=${sta:0:4}
 sta_min=$( echo "$sta" | tr '[:upper:]' '[:lower:]' )
 sta_cap=$( echo "$sta" | tr '[:lower:]' '[:upper:]' )
 
 if [ "$sta_cap" != "$( awk '/MARKER NAME/ { print $1 }' $item )" ]
 then
  color_text bold red "rinex and marker name does not match"
  continue
 fi 
 
 # Start time
 rnxdate=( $( rnx_time $tmp | awk '{ printf "%2.2d %2.2d %2.2d %2.2d %2.2d %2.2d\n",$1,$2,$3,$4,$5,$6 }' ) )
 
done

 year2nostrip=${rnxdate[0]}
 year2=$( echo $year2nostrip | sed 's/0*//' )
 yearst=$(( $year2 + 1900 ))
 if [ $yearst -lt 1980 ]
 then
  yearst=$(( $yearst + 100 ))
 fi

 most=$rnxdate[1]
 dayst=$rnxdate[2]
 hrst=$rnxdate[3]
 minst=$rnxdate[4]
 secst=$rnxdate[5]
 sttime="$( echo "${yearst} ${most} ${dayst} ${hrst}:${minst}:${secst}.00" )"
 
 rend=( $( rnx_time $tmp | awk '{ print $7,$8,$9,$10,$11,$12 }' ) )
 
 echo $item $rnxdate - $rend
 
 yearendnostrip=${rend[0]}
 yearend=$( echo $yearendnostrip | sed 's/0*//' )
 yeare4=$(( $yearend + 1900 ))
 if [ $yeare4 -lt 1980 ]
 then
  yeare4=$(( $yeare4 + 100 ))
 fi

 moend=${rend[1]}
 daend=${rend[2]}
 hrend=${rend[3]}
 minend=${rend[4]}
 secend=$( echo ${rend[5]} | awk '{printf "%d\n",$1}' )
 stjsec=$( cal2sec $yearst $most $dayst $hrst $minst $secst )
 stpjsec=$( cal2sec $yeare4 $moend $daend $hrend $minend $secend )
 temp=$( echo $stjsec $stpjsec | awk '{print $2-$1}' )
 valtime=$( echo $temp | awk '{printf "%11.2f\n",$1}' | sed 's/ /=/g' )

 rec="$( awk '/REC # \/ TYPE \/ VERS/ { print substr($0,21,20) }' $tmp )"
 rec_num="$( awk '/REC # \/ TYPE \/ VERS/ { print substr($0,1,20) }' $tmp )"

 ant="$( awk '/ANT # \/ TYPE/ { print substr($0,21,20) }' $tmp )"
 ant_num="$( awk '/ANT # \/ TYPE/ { print substr($0,1,20) }' $tmp )"

 ecc=0

 if [ -z "$ant_hgt" ]
 then
  ant_hgt="$( awk '/ANTENNA: DELTA H\/E\/N/ { print substr($0,0,14) }' $tmp )"
 fi
 
 flag=l  # Flag "l" or "c" - assume "l"
 datedone=$( date +'%Y %m %d' )   # Date entry created
 creator="$scriptname %I% $( whoami )"    # Creator - assume "addstasvec"

 printf " %4.4s %4.4s %4d %02d %02d %02d:%02d%02d.00\n" "$sta_cap" "$sta_cap" "${yearst}" "${most}" "${dayst}" "${hrst}" "${minst}" "${secst}" "$valtime" "$ant" "$ecc" "$ecc" "$ant_hgt" "$flag" "$datedone" "$creator" >> $output
 " PENC PENC 2010 02 05 00:00:00.00     86370.00 ROGUE_T        0.0000     0.0000     0.0000 00000.0300 l 2013 07 04 rnx2sta_svec %I% marcell"

 echo " "{$sta} $sta $sttime " "{$valtime} $rcvr "      " {$ecc}"    " {$ecc}"    " {$ecc}" $hi "$flag $datedone $creator >> $TMPDIR/addstasvec.$$

 if [ "${tmp##*/}" != "$item" ]
 then
  /bin/rm -f $tmp
 fi

mv $TMPDIR/addstasvec.$$ $TMPDIR/temp.$$
sed 's/=/ /g' $TMPDIR/temp.$$ > $TMPDIR/addstasvec.$$                        # CHANGE LEADING AND TRAILING = TO BLANKS

cat $TMPDIR/addstasvec.$$

# CLEAN UP

rm -f $TMPDIR/temp.$$ 
rm -f $TMPDIR/addstasvec.$$
