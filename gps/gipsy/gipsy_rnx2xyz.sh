#!/bin/bash

#@
#@ USAGE     : goa_rnx2xyz.sh -s <station> -d <yyyy-mm-dd> -a [ANALYZIS_CENTER] [OPTIONS]
#@
#@ OPTIONS   :
#@             -t [ARG] -- data_type    -- DEFAULT: "-a JPL -t flinnR" or "-a IGS -t final"
#@             -v       -- verbose mode -- DEFAULT: no
#@             -h       -- help
#@
#@             ANALYZIS_CENTER          -- jpl | JPL | igs | IGS
#@
#@             DATA_TYPE                -- jpl | JPL ---------------------------
#@                                                   - flinnR,flinnR_nf,
#@                                                     qlR,ultra,ql,flinn,
#@                                                     15m,flinn_nf,retro_flinn,
#@                                                     retro_flinn_nf
#@                                      -- igs | IGS ---------------------------
#@                                                   - final
#@
#@ IMPORTANT : this script uses the antex [*.atx] file specified in the ANALYZIS_CENTER's [*.ant.gz] data.
#@
#* by        : marcell.ferenc@cnam.fr
#*

if [ -z "$goa_xyz_dir" ]; then echo "$HOME/bin/gipsy/gipsy.conf is not sourced"; exit 2 ; fi

## servers
#server=ftp://sideshow.jpl.nasa.gov/pub/gipsy_files/eph/antenna_cals_xmit/
#server=ftp://igscb.jpl.nasa.gov/pub/station/general/pcv_archive/
server=ftp://sideshow.jpl.nasa.gov/pub/gipsy_files/gipsy_params/antenna_cals_xmit/

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=tmp.$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )

## cleaning
clean()
{
 /bin/rm -f ${tmp}*
 ## restore default value 
 trap EXIT
 exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## start directory
sdir=$( pwd )

## source functions
. $HOME/bin/func/general.func
. $HOME/bin/func/gipsy.func
. $HOME/bin/func/ts.func

## help
if [ "$#" -eq 0 ]; then clear; usage $0; exit 2; fi

## list of options the program will accept
optstring=a:d:s:tvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  a) ac_data=$OPTARG ;;
  d) date_str=$OPTARG ;;
  s) station=$OPTARG ;;
  t) data_type=$OPTARG ;;
  v) v=yes;;
  h) clear; usage $0 ; exit 2 ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## date
if [ -z "$date_str" ]
then
 color_txt bold red "-d option is obligatory [date_str]"; exit 2
else
 date_inf=( $( isdate $date_str ) )
 if [ "${#date_inf[@]}" -gt 1 ]
 then
  Y4=${date_inf[1]}; MM=${date_inf[2]}; DD=${date_inf[3]}
  date_inf=( $( date --date="$date_str" "+%Y %y %j" ) ); y2=${date_inf[1]}; doy=${date_inf[2]}
 else
  color_txt bold red "not valid date_str $date_str"; exit 2
 fi
fi

## station
if [ -z "$station" ]
then
 color_txt bold red "-s option is obligatory [station_name]"; exit 2
else
 ## test station name
 sta_inf=( $( isvalstatname $station ) ); sta_upper=${sta_inf[1]}; sta_lower=${sta_inf[2]}
 if [ "${#sta_inf[@]}" -eq 1 ]
 then
  color_txt bold red "not a valid station name $station"; exit 2
 fi
fi

## ac data
if [ -z "$ac_data" ]
then
 color_txt bold red "-a option is obligatory [analyzis_center]"; exit 2
else
 case $ac_data in
  jpl|JPL) ac_data=jpl ;;
  igs|IGS) ac_data=igs ;;
    *) color_txt bold red "not valid analyzis center: $ac_data"; exit 2 ;;
 esac
fi

## data type
## it make sense only in case we want to deal with other type of data and some other already exists
if [ -z "$data_type" ]
then
 ## default for analyzis center
 case $ac_data in
  jpl) data_type=flinnR ;;
  igs) data_type=final ;;
 esac
else
 case ${ac_data}${data_type} in
  jplflinnR|jplflinnR_nf|jplqlR|jplultra|jplql|jplflinn|jpl15m|jplflinn_nf|jplretro_flinn|jplretro_flinn_nf) ;;
  igsfinal) ;;
  *) color_txt bold red "not coherent use of ac_data and data_type $ac_data & $data_type"; exit 2 ;;
 esac
fi

## used directories
mkdir -p $goa_atx_dir $goa_xyz_dir

## orb clk dir
case $ac_data in
 jpl) orb_clk_dir=$goa_jpl_dir/$data_type;;
 igs) orb_clk_dir=$goa_igs_dir/$data_type;;
esac

## rinex file related to the date
crnx=$goa_rnx_dir/$Y4/${sta_lower}${doy}0.${y2}d.Z
rnx=${sta_lower}${doy}0.${y2}o
isrnx -r "${crnx##*/}"

## obtain necessary information from rinex header
if [ ! -s $rnx ]
then
 rnx=$( crz2rnx -echo $crnx )
 anttype="$( awk '/ANT # \/ TYPE/ { print substr($0,21,16)}' $rnx )"
 radcode="$( awk '/ANT # \/ TYPE/ { print substr($0,37,4)}' $rnx )"
 /bin/rm -f $rnx
else
 anttype="$( awk '/ANT # \/ TYPE/ { print substr($0,21,16)}' $rnx )"
 radcode="$( awk '/ANT # \/ TYPE/ { print substr($0,37,4)}' $rnx )"
fi

## obtain gps week of the desired date
secs=$( cal2sec $Y4 $MM $DD 00 00 00 )
gpsw=$( sec2gpsws $secs | awk '{ print $1 }' )

## change from ITRF2005 to ITRF2008
sys_ch=$( cal2sec 2011 04 17 )

## ITRF2005 before 2011/04/17
if [ $( gmtmath -Q $secs $sys_ch LT = ) -eq 1 ]
then
 model=igs05
## ITRF2008 after 2011/04/17
elif [ $( gmtmath -Q $secs $sys_ch GE = ) -eq 1 ]
then
 model=igs08
## end fi - ITRFYY
fi

cd $goa_atx_dir

[ "$v" == "yes" ] && echo "${model}_$gpsw - corresponds to date"

## use that antex which corresponds to the orbit & clock files 
if [ ! -e $orb_clk_dir/$Y4/$date_str.ant.gz ]
then
 ${ac_data}_product.sh -b $date_str -e $date_str -t $data_type
 if [ -e $orb_clk_dir/$Y4/$date_str.ant.gz ]
 then
  cd $orb_clk_dir/$Y4
  atx_file=$( zcat $date_str.ant.gz | awk '/GRN/ { print $2 }' )
  base=${atx_file%.atx}
 else
  color_txt bold red error; exit 2
 fi 
else
 cd $orb_clk_dir/$Y4
 atx_file=$( zcat $date_str.ant.gz | awk '/GRN/ { print $2 }' )
 base=${atx_file%.atx}
 #atx_file=$( zcat $date_str.ant.gz | awk '/GRN/ { print $2 }' )
 #xyz_file=$( zcat $date_str.ant.gz | awk '/SAT/ { print $2 }' )
fi

cd $goa_atx_dir
 
for ext in atx tdp xyz
do
 if [ ! -e $base.$ext ]
 then
  [ "$v" == "yes" ] && color_txt bold blue "fetch $base.$ext"
  ncftpget -u anonymous -p anonymous $server/$base.$ext.gz
  gunzip -f $base.$ext.gz
 else
  [ "$v" == "yes" ] && color_txt bold green "$base.$ext already exist"
 fi 
done

## result xyz file
xyzfile=${sta_inf[1]}.xyz

## remove xyz file if exist
/bin/rm -rf $goa_xyz_dir/$xyzfile

[ "$v" == "yes" ] && echo
## create xyz file based on provided data
if [ "$radcode" == "    " ]; then radcode=NONE; fi
anttype=$( trim "$anttype" )
radcode=$( trim "$radcode" )
[ "$v" == "yes" ] && color_txt bold green "antenne: $anttype     radome: $radcode"
[ "$v" == "yes" ] && echo
command="antex2xyz.py -antexfile $goa_atx_dir/$atx_file -xyzfile $goa_xyz_dir/$xyzfile -anttype $anttype -radcode $radcode -recname ${sta_inf[1]}"
[ "$v" == "yes" ] && echo $command
$command
[ "$v" == "yes" ] && echo

## go back to launch dir
cd $sdir

 # it was for testing
 # use the latest antex file on local machine or the latest from the server in the actual system (igs05/igs08)
 #for ext in atx tdp xyz
 #do
 # ls -1 ${model}_*.$ext &>/dev/null
 # if [ $? -eq 0 ]
 # then
 #  latest=$( ls -1 ${model}_*.$ext | sort -r | head -n 1 )
 #  latest_gpsw=${latest:6:4}
 #  echo "$latest_gpsw - latest on local"
 #  if [ $latest_gpsw -lt $gpsw ]
 #  then
 #   color_txt bold blue "download $ext file"
 #   ncftpls -u anonymous -p anonymous -x "-l" "$server/${model}_*.$ext.gz" \
 #   | awk '{ print $NF }' \
 #   | sort -r \
 #   | head -n 1 \
 #   | tee $tmp \
 #   | xargs -I {} ncftpget -u anonymous -p anonymous $server/{} &> /dev/null
 #   gunzip -f $( cat $tmp ) && /bin/rm -f $tmp
 #  else
 #   color_txt bold green "$ext file already exist"
 #  fi
 # else
 #  color_txt bold blue "download $ext file"
 #  ncftpls -u anonymous -p anonymous -x "-l" "$server/${model}_*.$ext.gz" \
 #  | awk '{ print $NF }' \
 #  | sort -r \
 #  | head -n 1 \
 #  | tee $tmp \
 #  | xargs -I {} ncftpget -u anonymous -p anonymous $server/{} &> /dev/null
 #  gunzip -f $( cat $tmp ) && /bin/rm -f $tmp   
 # fi
 # [ "$ext" == "atx" ] && atx_file=$( ls -1 ${model}_*.$ext | sort -r | head -n 1 )
 #done
