#!/bin/bash

#@
#@ USAGE    : gipsy_main.sh -w [WORK_DIR] -s [STATION_LIST] -b [YYYY-MM-DD] -e [YYYY-MM-DD] [OPTIONS]
#@
#@ REQUIRED :
#@            -w [WORK_DIR       ] --          work_dir     [$HOME/$wrk_dir         ]
#@            -s [STATION_LIST   ] --          station_list [station_list_file !!!  ]
#@            -b [YYYY-MM-DD     ] --          start_date   [YEAR-MONTH-DAY         ]
#@            -e [YYYY-MM-DD     ] --          end_date     [YEAR-MONTH-DAY         ]
#@
#@ OPTIONS  :
#@            -a [ANALYZIS_CENTER] -- DEFAULT: jpl | JPL    [opt: igs | IGS         ]
#@            -t [TROP_MAP       ] -- DEFAULT: VMF1GRID     [opt: GMF               ]
#@            -k                   -- DEFAULT: s            [s: static; k: kinematic]
#@            -h                   -- help
#@
#@ IMPORTANT:
#@            other DEFAULT values which are not possible to choose here BUT
#@            in the "run_gipsy.sh" script!
#@
#@            -ambi_res  -- DEFAULT: 1
#@            -cutoff    -- DEFAULT: 7
#@            -data_rate -- DEFAULT: 300
#@
#* by       : marcell.ferenc@cnam.fr

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=tmp.$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/${tmp#tmp.} && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/$tmp

## start directory
sdir=$( pwd )

## cleaning
clean()
{
 /bin/rm -rf ${tmp_dir}*
 ## restore default value 
 trap EXIT
 ## return to start directory
 cd $sdir
 exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
. $HOME/bin/func/gipsy.func
. $HOME/bin/func/general.func
. $HOME/bin/func/ts.func

## help
if [ "$#" -eq 0 ]; then clear; usage $0; exit 2; fi

## list of options the program will accept
optstring=a:b:e:s:t:w:kh

## interpret options
while getopts $optstring opt
do
 case $opt in
  b) beg_str=$OPTARG ;;
  e) end_str=$OPTARG ;;
  s) station_list=$OPTARG ;;
  t) trop_map=$OPTARG ;;
  w) wrk_dir=$OPTARG ;;
  h) clear; usage $0 ; exit 2 ;;
 esac
done

shift "$(( $OPTIND - 1 ))"
 
## default environment
oload_file=$goa_otl_dir/FES2004.otl
trop_map=VMF1GRID
ac=jpl

#############################################################################################################
#############################################################################################################
######### !!! AFTER THIS LINE NORMALLY YOU DON'T NEED TO CHANGE ANYTHING !!! ################################
#############################################################################################################
#############################################################################################################

## work_dir
if [ -z $wrk_dir ]; then color_txt bold red "-w option is obligatory [work_dir]"; exit 2; else wrk_dir=$goa_wrk_dir/$wrk_dir; fi
## note: in the "marcell" account due to the GINS-PC installation there is a "srcipts" direcotry
## which would conflict if the work_dir wouldbe only $HOME/work_dir
clear
echo
echo
color_txt bold blue "The directory you specified \"${wrk_dir}\" will be created with sequential numbers"
color_txt bold blue "depending on the number of sessions:"
echo
color_txt bold blue " ${wrk_dir}_1, ${wrk_dir}_2, ... ${wrk_dir}_n"
echo
echo 
while true; do
 read -p "Are you sure to start?" yn
 case $yn in
  [Yy]*) break;;
  [Nn]*) exit 2;;
      *) echo "Please answer y|yes|Y|YES| or n|no|N|NO";;
 esac
done

## begin date
if [ -z "$beg_str" ]
then
 color_txt bold red "-b option is obligatory [begin_date]"; exit 2
else
 beg_inf=( $( isdate $beg_str ) )
 if [ "${#beg_inf[@]}" -eq 1 ]; then color_txt bold red "not valid begin_date $beg_str"; exit 2; fi
fi

## end date
if [ -z "$end_str" ]
then
 color_txt bold red "-e option is obligatory [end_date]"; exit 2
else
 end_inf=( $( isdate $end_str ) )
 if [ "${#end_inf[@]}" -eq 1 ]; then color_txt bold red "not valid end_date $end_str"; exit 2; fi
fi

## trop_map
case $trop_map in
 VMF1GRID) ;;
      GMF) ;;
        *) color_txt bold red "not supported trop_map $trop_map"; exit 2;;
esac

## analyzis_center
case $ac in
 jpl|JPL) ac=jpl; orb_type=flinnR; jpl_product.sh -b $beg_str -e $end_str ;;
 igs|IGS) ac=igs; orb_type=final; igs_product.sh -b $beg_str -e $end_str ;;
       *) exit 2 ;;
esac

pwd
## station_list
if [ -z $station_list ]
then
 color_txt bold red "-s option is obligatory [station_list]"; exit 2
else
 if [ -s $station_list ]
 then
  ## make sure the $station_list file have one station per line
  for item in $( cat $station_list )
  do
   printf "%s\n" $item
  done > $tmp.list
 else
  color_txt bold red "station_list $station_list does not exist"; exit 2
 fi
fi

start=$beg_str
## due to the while loop with string test
end=$( date --date="$end_str +1 day" "+%Y-%m-%d" )

###### splitting the file into $nb_session
nb_lines_total=$( awk 'END { print NR }' $tmp.list )
nb_session=10 ## max number of sessions
nb_lines=$(( ( $nb_lines_total + $nb_session - 1 ) / $nb_session ))

## split station list in order to run calculations in up to $nb_session session
split -l $nb_lines $tmp.list $tmp.list.

## information
echo
echo "total_lines    = $nb_lines_total"
echo "lines_in_files = $nb_lines"
echo
echo "splitting results"
echo
wc -l $tmp.list.*
echo

## information log file
echo "$scriptname - start - $(date)" > $goa_log_dir/$scriptname.$( date "+%Y%m%d_%H%M%S" ).log

## remove batch files
/bin/rm -f $goa_log_dir/batch.* 2> /dev/null

## generate epoch list
## while - period
while [ "$start" != "$end" ]
do
 ##
 ## prepare part of parameter list for batch list
 ##
 echo -d $start -o $ac
 start=$( date --date="$start +1 day" "+%Y-%m-%d" )
## end while - period
done > $tmp.epoch

## for - station_block
count=0
for station_block in $tmp.list.*
do

 count=$(( $count + 1 ))
 
 ## while - station in station_block
 while IFS= read -r sta
 do

  ##
  ## prepare batch list for run_gipsy.sh
  ##
  awk -v s=$sta -v c=$count -v w="${wrk_dir#$goa_wrk_dir/}" '{ print "run_gipsy.sh -w",w "_" c,"-n",s,$0 }' $tmp.epoch

 ## end while - station in station_block
 done < $station_block >> $goa_log_dir/batch.$count

## end for - station_block
done

## start batch process
for batch_list in $goa_log_dir/batch.*
do
 count=$(( $count + 1 ))
 gnome-terminal -x bash -c \
 "sh $batch_list; \
 read -n1" &
done
