#!/bin/bash

#@
#@ USAGE   : run_gipsy_alone.sh -w [WORK_DIR] -n [STATION_NAME] -d [YYYY-MM-DD] -o [ANALYZIS_CENTER] | [OPTIONS]
#@
#@ REQUIRED:
#@           -w [WORK_DIR       ] -- $HOME/gipsy_work/$work_dir
#@           -n [STATION_NAME   ] -- station_name
#@           -d [YYYY-MM-DD     ] -- date
#@           -o [ANALYZIS_CENTER] -- jpl | JPL | igs | IGS
#@ OPTIONS : 
#@           -a [AMB_RES        ] -- DEFAULT: 1
#@           -c [CUTOFF         ] -- DEFAULT: 7
#@           -k                   -- DEFAULT: s        [s: static; k: kinematic]
#@           -s [DATA_RATE      ] -- DEFAULT: 300
#@           -t [TROP_MAP       ] -- DEFAULT: VMF1GRID [opt: GMF]
#@
#@ TASK    : run gp2p.pl in the current directory with is parameters
#@
#* by      : marcell.ferenc@cnam.fr
#*
## MISSING: verify valid date, valid station name, rinex existence after isrnx

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=tmp.$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )

## start directory
sdir=$( pwd )

## cleaning
clean()
{
 /bin/rm -rf ${tmp}* /tmp/${tmp}*
 ## restore default value 
 trap EXIT
 ## return to start directory
 cd $sdir
 exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
. $HOME/bin/func/gipsy.func
. $HOME/bin/func/general.func
. $HOME/bin/func/ts.func

## help
if [ "$#" -eq 0 ]; then clear; usage $0; exit 2; fi

## default values
amb_res=1 ## a value of 1 worked for station positioning
cutoff=7
data_rate=300
trop_map=VMF1GRID
solution=s
otl_file=$goa_otl_dir/FES2004.otl

## list of options
optstr=a:c:d:n:o:s:t:w:kh

## interpret options
while getopts $optstr opt
do
 case $opt in
  a) amb_res=$OPTARG ;;
  c) cutoff=$OPTARG ;;
  d) date=$OPTARG ;;
  k) solution=k ;;
  n) sta=$OPTARG ;;
  o) ac=$OPTARG ;;
  s) data_rate=$OPTARG ;;
  t) trop_map=$OPTARG ;;
  w) wrk_dir=$OPTARG ;;
  h) clear; usage $0 ; exit 2 ;;
 esac
done
shift "$(( $OPTIND - 1 ))"

## work_dir
if [ -z $wrk_dir ]; then color_txt bold red "-w option is obligatory [work_dir]"; exit 2; else wrk_dir=$goa_wrk_dir/$wrk_dir; fi
#/bin/rm -rf $wrk_dir 2> /dev/null
mkdir -p $wrk_dir && cd $_

## station
if [ -z $date ] || [ -z $sta ]
then
 color_txt bold red "-d and -n options are obligatory [date] and [station]"; exit 2
fi
sta_lower=$( echo $sta | tr [:upper:] [:lower:])
sta_upper=$( echo $sta | tr [:lower:] [:upper:])
date_inf=( $( date --date="$date" "+%Y %y %j" ) )
y4=${date_inf[0]}
y2=${date_inf[1]}
doy=${date_inf[2]}
base=${sta_lower}${doy}0
crnx=$base.${y2}d.Z
rnx=$base.${y2}o

## rinex
isrnx -r $crnx -u #-c

## analyzis center
if [ -z $ac ]
then
 color_txt bold red "-o option is obligatory [analyzis_center]"; exit 2
else
 case $ac in
  jpl|JPL) ac=jpl; orb_type=flinnR; orb_dir=$goa_jpl_dir/$orb_type; orb_clk_flag="flinnR $orb_dir/$y4"; jpl_product.sh -b $date -e $date ;;
  igs|IGS) ac=igs; orb_type=final; orb_dir=$goa_igs_dir/$orb_type; orb_clk_flag="flinnR $orb_dir/$y4"; igs_product.sh -b $date -e $date ;;
        *) color_txt bold red "not supported analyzis_center $ac"; exit 2 ;;
 esac
fi

## data rate in seconds
if [ $( gmtmath -Q $data_rate 300 LT = ) -eq 1 ]
then
 interp_300="-interp_300"
fi

## trop map
case $trop_map in
 VMF1GRID) trop_map_flag="VMF1GRID -vmf1dir $goa_vmf1grid_dir"; tdp_in_flag="$goa_vmf1grid_dir/$sta_upper.TDPdry"
           tropo.sh -s $sta -b $date -e $date ;;
      GMF) trop_map_flag="GMF"; tdp_in_flag="$goa_gmf_dir/$sta_upper.TDPdry"
           tropo.sh -s $sta -b $date -e $date -t GPT;;
   *) color_txt bold red "not supported trop_map $trop_map"; exit 2 ;;
esac

## sta_info files
## missing!!!!

## xyz file
rnx2xyz.sh -s $sta -d $date -a $ac

## test ocean load file for station
grep -i $sta $otl_file &> /dev/null
if [ $? -ne 0 ]
then
 color_txt bold red "$sta is not in $otl_file"; exit 2
fi
 
## type settings static/kinematic
case $solution in
 s) type_flag="";;
 k) type_flag="-kin_sta_xyz 100E-5 5.7E-7 $data_rate RANDOMWALK" ;; ## 1.0E-1 1.0E-7 30 RANDOMWALK  ##100E-5 5.7E-7 300 RANDOMWALK <-- slowly movement ex.: ice flow
esac

## work with gd2p.pl
gd2p.pl -d $date \
-n $sta \
-r $data_rate $interp_300 \
-type $solution $type_flag \
-i $rnx \
-amb_res $amb_res \
-w_elmin $cutoff \
-orb_clk "$orb_clk_flag" \
-sta_info $goa_sta_dir \
-arp -AntCal $goa_xyz_dir/$sta_upper.xyz \
-trop_map $trop_map_flag \
-tdp_in $tdp_in_flag \
-wetzgrad 5E-9 \
-trop_z_rw 5E-8 \
-add_ocnld " -c $otl_file" \
-tides WahrK1 PolTid FreqDepLove OctTid \
-post_wind 5.0E-3 5.0E-5 \
-stacov

tdp2xyz

## remove rinex
/bin/rm -f $rnx ${rnx%o}S 2> /dev/null
