#!/bin/bash

#@
#@ USAGE     : jpl_product.sh -b [YYYY-MM-DD] -e [YYYY-MM-DD] [OPTIONS]
#@
#@ OPTIONS   :
#@             -t [ARG] -- data_type    -- DEFAULT: flinnR
#@             -v       -- verbose mode -- DEFAULT: no
#@             -h       -- help
#@
#@ TASK      : this script inwoke the "goa_prod_ftp.pl" official GIPSY script with its necessary arguments
#@
#* by        : marcell.ferenc@cnam.fr
#*

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=tmp.$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )

## start directory
sdir=$( pwd )

## cleaning
clean()
{
 /bin/rm -rf ${tmp}* /tmp/${tmp}*
 ## restore default value 
 trap EXIT
 ## return to start directory
 cd $sdir
 exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
. $HOME/bin/func/ts.func
. $HOME/bin/func/general.func

## help
if [ "$#" -eq 0 ]; then clear; usage $0; exit; fi

## list of options the program will accept
optstring=b:e:t:fh

## interpret options
while getopts $optstring opt
do
 case $opt in
  b) beg_str=$OPTARG ;;
  e) end_str=$OPTARG ;;
  t) data_type=$OPTARG ;;
  f) high_rate="-hr" ;;
  h) clear; usage $0 ; exit 2 ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## begin date
if [ -z "$beg_str" ]
then
 color_txt bold red "-b option is obligatory [begin_date]"; exit
else
 beg_inf=( $( isdate $beg_str ) )
 if [ "${#beg_inf[@]}" -eq 1 ]; then color_txt bold red "not valid begin_date $beg_str"; exit; fi
fi

## end date
if [ -z "$end_str" ]
then
 color_txt bold red "-e option is obligatory [end_date]"; exit
else
 end_inf=( $( isdate $end_str ) )
 if [ "${#end_inf[@]}" -eq 1 ]; then color_txt bold red "not valid end_date $end_str"; exit; fi
fi

## data type
if [ -z "$data_type" ]
then
 data_type=flinnR
else
 case $data_type in
            flinnR|flinnR_nf) ;;
                qlR|ultra|ql) ;;
          flinn|15m|flinn_nf) ;;
  retro_flinn|retro_flinn_nf) ;;
                           *) color_txt bold red "not valid data_type $data_type"; exit ;;
 esac
fi

## start the work
start=$( date --date="$beg_str" "+%Y-%m-%d" )
end=$( date --date="$end_str +1 day" "+%Y-%m-%d" )

while [ "$start" != "$end" ]
do

 Y4=${start:0:4}
 des_dir=$goa_jpl_dir/$data_type/$Y4

 [ ! -d $des_dir ] && mkdir -p $des_dir

 cd $des_dir

 case ${high_rate}${data_type: -2} in
  -hrnf) file=${start}_nf_hr.tdp.gz;; 
   -hr*) file=${start}_hr.tdp.gz;;
     nf) file=${start}_nf.pos.gz;;
      *) file=${start}.pos.gz;;
 esac
 
 if [ ! -e $file ]
 then
  color_txt bold blue "fetch files of ${start}"
  goa_prod_ftp.pl -d $start -s $data_type $high_rate
 else
  color_txt bold green "files of ${start} already exist"
 fi

 ## increment the date
 start=$( date --date="$start +1 day" "+%Y-%m-%d" )

done
