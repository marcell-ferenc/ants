#!/bin/bash

#@
#@ USAGE     : goa_rnx2xyz.sh -s <station> -d <yyyy-mm-dd>
#@
#@ OPTIONS   :
#@             -v - verbose mode
#@             -h - help
#@
#* by        : marcell.ferenc.uni@gmail.com
#*

if [ -z "$goa_xyz_dir" ]; then
 echo "$HOME/bin/gipsy/gipsy.conf is not sourced"; exit 2 ; fi

## scriptname
scrn=${0##*/}
scrn=${scrn%.sh}

## temporary filename
tmp=$scrn.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean(){ /bin/rm -rf $tmp_dir; trap EXIT; cd; exit; }

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source
## source functions
source $HOME/bin/func/general.func
source $HOME/bin/func/gipsy.func
source $HOME/bin/func/ts.func

## help
case "$#" in 0) usage $0; exit ;; esac

## list of options the program will accept
optstring=d:s:tvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  d) dte=$OPTARG ;;
  s) sta=$OPTARG ;;
  v) v=yes;;
  h) usage $0 ; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## dte -------------------------------------------------------------------------
if [ -z "$dte" ]; then
 color_txt bold red "${0##*/}: error: missing option: -d <yyyy-mm-dd>"
 exit 2; fi
dte_inf=( $( isdate $dte ) )
case "${#dte_inf[@]}" in
 0|1) color_txt bold red "not valid dte $dte"; exit 2 ;;
esac
Y4=${dte_inf[1]}
MM=${dte_inf[2]}
DD=${dte_inf[3]}
dte_inf=( $( date --date="$dte" "+%Y %y %j" ) )
y2=${dte_inf[1]}
doy=${dte_inf[2]}
##------------------------------------------------------------------------------

## sta -------------------------------------------------------------------------
if [ -z "$sta" ]; then
 color_txt bold red "${0##*/}: error: missing option: -s <sta_name>"; exit 2; fi
sta_inf=( $( isvalstatname $sta ) )
sta_upper=${sta_inf[1]}
sta_lower=${sta_inf[2]}
case "${#sta_inf[@]}" in
 1) color_txt bold red "not a valid sta name $sta"; exit 2 ;;
esac
##------------------------------------------------------------------------------

## used directories
mkdir -p $goa_xyz_dir

rnx=${sta_lower}${doy}0.${y2}o
isrnx -r "${crnx##*/}"

## obtain necessary information from rinex header
if [ ! -s $rnx ]; then
 rnx=$( crz2rnx -echo $crnx )
 anttype="$( awk '/ANT # \/ TYPE/ { print substr($0,21,16)}' $rnx )"
 radcode="$( awk '/ANT # \/ TYPE/ { print substr($0,37,4)}' $rnx )"
 /bin/rm -f $rnx
else
 anttype="$( awk '/ANT # \/ TYPE/ { print substr($0,21,16)}' $rnx )"
 radcode="$( awk '/ANT # \/ TYPE/ { print substr($0,37,4)}' $rnx )"
fi

## obtain gps week of the desired date
secs=$( cal2sec $Y4 $MM $DD 00 00 00 )
gpsw=$( sec2gpsws $secs | awk '{ print $1 }' )

## change from ITRF2005 to ITRF2008
#sys_ch=$( cal2sec 2011 04 17 )

## ITRF2005 before 2011/04/17
#if [ $( gmtmath -Q $secs $sys_ch LT = ) -eq 1 ]; then
# model=igs05
## ITRF2008 after 2011/04/17
#elif [ $( gmtmath -Q $secs $sys_ch GE = ) -eq 1 ]; then
# model=igs08
## end fi - ITRFYY
#fi
model=igs08

cd $goa_atx_dir

[ "$v" == "yes" ] && echo "${model}_$gpsw - corresponds to date"

#atx_file=$( zcat $dte.ant.gz | awk '/GRN/ { print $2 }' )

atx_file=$( ls -1 igs08*.atx | awk -v g=$gpsw 'substr($1,7,4) >= g-10 && substr($1,7,4) <= g+10 { print substr($1,7,4) }' | head -1 )

## result xyz file
xyzfile=${sta_inf[1]}.xyz

## remove xyz file if exist
/bin/rm -rf $goa_xyz_dir/$xyzfile

## create xyz file based on provided data
case "$radcode" in "    ") radcode=NONE ;; esac
anttype=$( trim "$anttype" )
radcode=$( trim "$radcode" )

cmd="antex2xyz.py -antexfile $goa_atx_dir/$atx_file -xyzfile $goa_xyz_dir/$xyzfile -anttype $anttype -radcode $radcode -recname ${sta_inf[1]}"

case $v in
 yes) echo; color_txt bold green "antenne: $anttype     radome: $radcode"
      echo; echo $cmd; echo ;;
esac

$cmd
