#!/bin/bash

#@
#@ USAGE     : gins_fich_dir_ppp.sh -o <rinex/prairie> <options>
#@
#@ OPTIONS   :
#@             -a <an_center_data>                              [default:                    GR2]
#@             -f                        forced station info    [default: only_if_not_in_nominal]
#@             -g <tropo_gradients>                             [default:                      0]                 
#@             -h                        help                   [default:                     no]
#@             -i                        2nd order ionosphere   [default:                     no]
#@             -m <2_char_str>                                  [default:                     xx]
#@             -n                        info string            [default:              see_below]
#@             -c <cutoff>                                      [default:                     10]
#@             -p <problematic_sat_file>                        [default:   gnss_sat_problem.lst]
#@             -s <position_sampling>                           [default:                     6h]
#@             -t <tropo_sampling>                              [default:                     1h]             
#@             -v                        verbose mode           [default:                     no]
#@
#@             -a GRG | GR2 | IGS
#@
#@ TASK      : Create driver file for GINS-PC
#@
#@             output name: SSSS_DDu_AAA_JJJJJ_cXXtXXugXXionXigsYYmm
#@
#* by        : marcell.ferenc@cnam.fr
#*

## scriptname
scrn=${0##*/}
scrn=${scrn%.sh}

## temporary filename
tmp=$scrn.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean(){ /bin/rm -rf $tmp_dir; trap EXIT; cd; exit; }

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/gins.func
source $HOME/bin/func/gps.func
source $HOME/bin/func/ts.func

## help
case "$#" in 0) usage $0; exit ;; esac

## default variable
def_satprob=$HOME/gin/data/HorlogesAnom0.log # problematic satellites
prob_ep_nb=10                            # threshold of problematic epoch number
GINS_otl=otl_rgp_fes2012.txt
#load_rgp_fes2012_felix.txt
#load_rgp_fes2012.txt #gins_otl_TOULOUSE.dat #nominal
template=GINS_PPP.ref
acdata=GR2
cutoff=10
pos_dt=6h
pos_ch=00060000
tro_dt=1h
tro_ch="   24"
tro_gr=0
ion2nd=no
ionstr=ion1
f_stainfo=no
v=no
str=xxxxxxxxxx
otl_model=fes2004
cmc=no #yes
atml=no
s1s2=no
log=$scrn.log
max_path_length=52

##------------------------------------------------------------------------------
## command line long options
##------------------------------------------------------------------------------
while (( $# > 0 )); do
 case $1 in
  -help|-h|-H) usage $0; exit ;;
  -debug) set -x ;;
  -ac*) if [ ${#1} -gt 3 ]; then ac=${1#"-ac"}; else shift; ac=$1; fi ;;
  -atml*) if [ ${#1} -gt 5 ]; then atml=${1#"-atml"}; else shift; atml=$1; fi ;;
  -s1s2) s1s2=yes ;;
  -cmc) #if [ ${#1} -gt 4 ]; then cmc=${1#"-cmc"}; else shift; cmc=$1; fi ;;
        cmc=yes ;;
  -cutoff*) if [ ${#1} -gt 7 ]; then
             cutoff=${1#"-cutoff"}; else shift; cutoff=$1; fi ;;
  -tropo*) if [ ${#1} -gt 6 ]; then
            tropo=${1#"-tropo"}; else shift; tropo=$1; fi ;;
  -grad*) if [ ${#1} -gt 5 ]; then grad=${1#"-grad"}; else shift; grad=$1; fi ;;
  -ion) #if [ ${#1} -gt 4 ]; then ion=${1#"-ion"}; else shift; ion=$1; fi ;;
         ion2nd=yes; ionstr=ion2 ;;
  -ntol*) if [ ${#1} -gt 5 ]; then ntol=${1#"-ntol"}; else shift; ntol=$1; fi ;;
  -cwsl*) if [ ${#1} -gt 5 ]; then cwsl=${1#"-cwsl"}; else shift; cwsl=$1; fi ;;
  -otl*) if [ ${#1} -gt 4 ]; then otl=${1#"-otl"}; else shift; otl=$1; fi ;;
  -obs_file*) if [ ${#1} -gt 9 ]; then
               obs_file=${1#"-obs_file"}; else shift; obs_file=$1; fi ;;
  -sta_info*) #if [ ${#1} -gt 9 ]; then
              # sta_info=${1#"-sta_info"}; else shift; sta_info=$1; fi ;;
              sta_info=yes ;;
  -sat_prob*) rm_sat=yes
              if [ ${#1} -gt 9 ]; then
               satprob=${1#"-sat_prob"}
              else if [ "${2:0:1}" != "-" ]; then
               shift; satprob=$1; fi
              fi ;;
  -verbose|-v|-V) v=yes ;;
  *) color_txt bold red "not valid option [$1]"; exit 2 ;;
 esac
 shift
done
##------------------------------------------------------------------------------

## list of options the program will accept
#optstring=a:c:g:m:n:o:p:s:t:ifvh

## interpret options
#while getopts $optstring opt
#do
# case $opt in
#  a) acdata=$OPTARG ;;
#  c) cutoff=$OPTARG ;;
#  f) f_stainfo=yes ;;
#  g) tro_gr=$OPTARG ;;
#  m) str=$OPTARG ;;
#  i) ion2nd=yes; ionstr=ion2 ;;
#  n) inf_ch=$OPTARG ;;
#  p) satprob=$OPTARG ;;
#  s) pos_dt=$OPTARG ;;
#  t) tro_dt=$OPTARG ;;
#  v) v=yes ;;
#  h) usage $0 ; exit ;;
# esac
#done
echo $v
#shift "$(( $OPTIND - 1 ))"

##------------------------------------------------------------------------------
## observation file rinex or prairie <obs_file>
##------------------------------------------------------------------------------
if [ -s "$obs_file" ]; then

 ## test observation filename
 case $obs_file in
  */*) obs_path=${obs_file%/*}; obs_name="${obs_file##*/}" ;;
    *) obs_name=$obs_file; obs_file=$(pwd)/$obs_name ;;
 esac
 
 ## test observation filename path length
 if [ ${#obs_file} -gt $max_path_length ]; then
  color_txt bold red "${0##*/}: error: path is too long"; exit 2; fi
 
 ## test input file extension
 case $obs_file in
  *o) if [ "$v" == "yes" ]; then
       color_txt bold blue "${0##*/}: info: input is rinex [$obs_name]"; fi ;;
  *.pra) if [ "$v" == "yes" ]; then
          color_txt bold blue \
          "${0##*/}: info: input is prairie [$obs_name]"; fi ;;
  *) color_txt bold red \
     "${0##*/}: error: not a valid input file extension [.${obs_file##*.}]"
     exit 2 ;;
 esac

 ## get station and date information from conventional observation filename
 site_info=( $( isvalobsfile $obs_name ) )
 ## station name (upper and lowercase)
 st_upper=${site_info[1]}; st_lower=${site_info[2]}
 ## date informations
 y4=${site_info[3]}; y2=${site_info[5]}; doy=${site_info[4]}
 sjul50=$( jjul $doy $y4 | awk '/JUL50/ { print $3 }' )
 ejul50=$(( $sjul50 + 1 ))
 
 ## input observation filename path in driver file
 printf -v obs_file "%-55.55s" ".temp.${obs_file#$HOME/}"
 
else
 color_txt bold red "${0##*/}: error: file is missing [$obs_file]"; exit 2; fi
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## problematic satellites <satprob>
##------------------------------------------------------------------------------
## list all of those satellites that has more than 
## <prob_ep_nb> problematic epoch on the given date
## problematic satellites file <satprob> format:
## jjul gpsw gpsd svn problematic_epoch_nb
if [ "$rm_sat" == "yes" ]; then
 if [ -s "$satprob" ]; then
  satprob=$satprob
 else 
  satprob=$def_satprob
  if [ "$v" == "yes" ]; then 
   color_txt bold blue \
   "${0##*/}: warning: default sat_prob file is used [$satprob]"; fi
 fi
 if [ -s "$satprob" ]; then
  if [ "$v" == "yes" ]; then 
   color_txt bold blue "${0##*/}: info: sat_prob file [$satprob]"; fi
   grep "^$sjul50" $satprob | awk -v p=$prob_ep_nb '$5 >= p { print $0 }' \
   | awk '{ printf "   %2d   %2.2s   %2.2s%5d%5d%5d%5d%5d%51.51s\n",\
   -1,substr($4,4,2),substr($4,1,2),0,0,0,0,0,"HMODM" }' > $tmp.satprob
 fi
fi
##------------------------------------------------------------------------------

exit

##------------------------------------------------------------------------------
## cutoff angle
##------------------------------------------------------------------------------
case $cutoff in [1-9]|[1-9][0-9]) ;; *) cutoff=10 ;; esac
printf -v cutoff "%2d   %2d" $cutoff $cutoff
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## sampling interval <pos_dt> and its time unit <pos_tu>
##------------------------------------------------------------------------------
pos_dt=${pos_dt#0}
pos_tu=${pos_dt: -1}
case $pos_dt in
 *s) printf -v pos_dt "%02d" "${pos_dt%?}"; pos_ch=000000${pos_dt} ;; 
 *m) printf -v pos_dt "%02d" "${pos_dt%?}"; pos_ch=0000${pos_dt}00 ;;
 *h) printf -v pos_dt "%02d" "${pos_dt%?}"; pos_ch=00${pos_dt}0000 ;;
  *) color_txt bold red "not a valid position sampling interval [$pos_dt]"
     exit 2 ;;
esac
pos_dt=${pos_dt}$pos_tu
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## orbit and clock product <acdata>
##------------------------------------------------------------------------------
case $acdata in
 GRG|GR2|IGS) printf -v clk_data "%-55.55s" "horloges/$acdata/defaut"
              printf -v orb_data "%-55.55s" "orbites/$acdata/defaut" ;;
 *) color_txt bold red "not supported analyzis center [$acdata]"; exit 2 ;;
esac
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## troposphere sampling interval <tro_dt> and its time unit <tro_tu>
##------------------------------------------------------------------------------
tro_dt=${tro_dt#0}
tro_tu=${tro_dt: -1}
case $tro_dt in
  #5m) tro_dt="  288"; tro_ch=5m ;;
 #30m) tro_dt="   48"; tro_ch=30m ;;
  1h) tro_ch="   24" ; printf -v tro_dt "%02d" "${tro_dt%?}" ;;
  2h) tro_ch="   12" ; printf -v tro_dt "%02d" "${tro_dt%?}" ;;
  6h) tro_ch="    4" ; printf -v tro_dt "%02d" "${tro_dt%?}" ;;
   *) color_txt bold red "not valid troposphere sampling interval [$tro_dt]"
      exit 2 ;;
esac
tro_dt=${tro_dt}$tro_tu
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## troposphere gradient <tro_gr>
##------------------------------------------------------------------------------
case $tro_gr in
 [0-9]|[1-9][0-9]) printf -v tro_gr_nb "%2d" "$tro_gr"
                   printf -v tro_gch "%02d" "$tro_gr" ;;
 *) color_txt bold red "not valid troposphere gradient number [$tro_gr]"
    exit 2 ;;
esac
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## change of macromodels
##------------------------------------------------------------------------------
if [ $sjul50 -ge 22575 ]; then
 printf -v macromod "%-55.55s" "macromodeles/macromodeles.xml"
## before this date we used old macromodels
elif [ $sjul50 -lt 22575 ]; then
 printf -v macromod "%-55.55s" ".temp.gin/archives/LISTE_GNSS_ATTN.xml"; fi
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## change of ITRF frame (ITRF2005 --> ITRF2008)
##------------------------------------------------------------------------------
if [ $sjul50 -ge 22386 ]; then
 igsyy=igs08; itrf=2008
 printf -v ant_file "%-55.55s" "antex/igs08.atx"
 printf -v sta_file "%-55.55s" "stations/nominal"
 #printf -v otl_file "%-55.55s" "charge/ocean/$GINS_otl"
 printf -v otl_file "%-55.55s" ".temp.gin/data/charge/ocean/$GINS_otl"  
## before this date we used the ITRF2005 frame
elif [ $sjul50 -lt 22386 ]; then
 igsyy=igs05; itrf=2005
 printf -v ant_file "%-55.55s" "antex/igs05.atx"
 printf -v sta_file "%-55.55s" "stations/nominal"
 #printf -v otl_file "%-55.55s" "charge/ocean/$GINS_otl"
 printf -v otl_file "%-55.55s" ".temp.gin/data/charge/ocean/$GINS_otl"
 #printf -v otl_file "%-55.55s" ".temp.gin/data/charge/ocean/scherneck/$GINS_otl"
fi
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## REPRO2 settings
##------------------------------------------------------------------------------
case $acdata in
 GR2) igsyy=igs08; itrf=2008
      printf -v ant_file "%-55.55s" "antex/igs08.atx"
      printf -v macromod "%-55.55s" "macromodeles/nominal"
      printf -v otl_file "%-55.55s" "charge/ocean/$GINS_otl"
      printf -v otl_file "%-55.55s" ".temp.gin/data/charge/ocean/$GINS_otl"
      #printf -v otl_file "%-55.55s" ".temp.gin/data/charge/ocean/scherneck/$GINS_otl" ;;
      printf -v otl_file "%-55.55s" "charge/ocean/load_fes2012_itrf2008" ;;
esac
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## pole file
##------------------------------------------------------------------------------
#pole/eop97c04_itrf2005g_6h
#pole/eop97c04_itrf2008g_6h
#pole/eop97c04_nro_itrf2008g_6h
#pole/nominal
printf -v pol_file "%-55.55s" "pole/eop97c04_itrf${itrf}g_6h"
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## information characters for file_name
##------------------------------------------------------------------------------
if [[ -z "$inf_ch" || "${#inf_ch}" -ne 22 ]]
then
 printf -v inf_ch "%-22.22s" \
 "c${cutoff%% *}t${tro_dt}g${tro_gch}${ionstr}${igsyy}$str"
else
 printf -v inf_ch "%-22.22s" \
 "c${cutoff%% *}t${tro_dt}g${tro_gch}${ionstr}${igsyy}$str"
fi
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## path to the fichier directeur
##------------------------------------------------------------------------------
_FDP_=$HOME/gin/data/directeur
FICH_DIR_NAME=$_FDP_/${st_upper}_${pos_dt}_${acdata}_${sjul50}_${inf_ch}
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## information
##------------------------------------------------------------------------------
if [ "$v" == "yes" ]
then
 printf "observation_file   : %s\n" $obs_file
 printf "cutoff_angle       : %s\n" ${cutoff% *}
 printf "position_sampling  : %s\n" $pos_dt
 printf "an_center_data     : %s\n" $acdata
 printf "tropo_sampling     : %s\n" $tro_dt
 printf "2nd_order_ion      : %s\n" $ion2nd
 printf "fichier_director   : %s\n" $FICH_DIR_NAME
fi
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
{
 if [ -s $tmp.satprob ]; then
  sed -n '1,/.*1000.*elevations.*min.*/p' $HOME/bin/gins-ppp/templates/$template
  printf "%5d %4.4s   %2.2s%5d%5d%5d%5d%5d %50.50s\n" \
  1 ${st_upper} GL 0 0 0 0 0 HMODM
  cat $tmp.satprob
  printf "%5d%5d%5d%5d%5d%5d%5d%5d %36.36s %13.13s\n" \
  0 0 0 0 0 0 0 0 "fin correction mesures" FINME
  sed -n '/.*INTEG*/,$p' $HOME/bin/gins-ppp/templates/$template
 else cat $HOME/bin/gins-ppp/templates/$template; fi
} > $FICH_DIR_NAME
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## set for 24h for DATDF in case of 24h pos sampling
##------------------------------------------------------------------------------
change SS.SSSSSS 19.000000 $FICH_DIR_NAME &> /dev/null
case $pos_dt in
 24h) change SS.SSSEND 19.000000 $FICH_DIR_NAME &> /dev/null ;;
   *) change SS.SSSEND 18.000000 $FICH_DIR_NAME &> /dev/null ;;
esac
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## forced station file creation
##------------------------------------------------------------------------------
if [ "$f_stainfo" == "yes" ]
then
 [ "$v" == "yes" ] && echo station_in_stainfo : ${site_info[1]} - forced
 gins_station_info_rinex.sh -r $rinex -t
 sta_file=${obs_name%?}s #sta_file=${st_upper}_${sjul50}_RINEX.inf
 otl_file=$st_lower.otl
 #otl_file=$GINS_otl
 #otl_file=${st_lower}-sch.otl
 printf -v sta_file "%-55.55s" ".temp.gin/data/stations/$sta_file"
 printf -v otl_file "%-55.55s" ".temp.gin/data/charge/ocean/$otl_file"
 #printf -v otl_file "%-55.55s" ".temp.gin/data/charge/ocean/scherneck/$otl_file"
elif [ "$f_stainfo" == "no" ]
then
 grep -i ${site_info[1]} $HOME/gin/data/stations/nominal &> /dev/null
 if [ $? -ne 0 ]
 then
  [ "$v" == "yes" ] && echo station_in_stainfo : ${site_info[1]} - no
  gins_station_info_rinex.sh -r $rinex -t
  sta_file=${obs_name%?}s #sta_file=${st_upper}_${sjul50}_RINEX.inf
  otl_file=$st_lower.otl
  #otl_file=$GINS_otl
  printf -v sta_file "%-55.55s" ".temp.gin/data/stations/$sta_file"
  printf -v otl_file "%-55.55s" ".temp.gin/data/charge/ocean/$otl_file"
 else
  [ "$v" == "yes" ] && echo station_in_stainfo : ${site_info[1]} - yes
 fi
fi
##------------------------------------------------------------------------------

printf -v val_apri "%-55.55s" ".temp.bin/gins/templates/apriori_PPP.ref"
printf -v pra_opts "%-55.55s" ".temp.bin/gins/templates/options_prairie.dat"
printf -v atm_file "%-55.55s" "charge/atmosphere/defaut"
printf -v atm_s1s2 "%-55.55s" "charge/s1s2/s1_s2_def_cm.dat"

gmfgpt="10715$tro_ch"
gpvmf="10816$tro_ch"
 
printf -v tro_info "%10.10s" "$gmfgpt"
printf -v prakey "%5d" 0

case $otl_model in
 *[fesFES]2004*) otl_model=FES2004 ;;
 *[fesFES]2012*) otl_model=FES2012 ;;
esac

case $cmc in
 yes) printf -v cmc_key "%5d" 1
      printf -v cmc_file "%-55.55s" "charge/cmc/$otl_model.cmc" ;;
  no) printf -v cmc_key "%5d" 0
      printf -v cmc_file "%-55.55s" "charge/cmc/$otl_model.cmc" ;;
esac

case $atml in
 yes) printf -v atml_key "%5d" 1 ;;
  no) printf -v atml_key "%5d" 0 ;;
esac

case $s1s2 in
 yes) printf -v s1s2_key "%5d" 1 ;;
  no) printf -v s1s2_key "%5d" 0 ;;
esac

##------------------------------------------------------------------------------
## change driver file items
##------------------------------------------------------------------------------
{
 change "CHANGE_POLE_TERRESTRE                                  " "$pol_file" $FICH_DIR_NAME
 change "CHANGE_STATION_FILE                                    " "$sta_file" $FICH_DIR_NAME
 change "CHANGE_ANTENNE_FILE                                    " "$ant_file" $FICH_DIR_NAME
 change "CHANGE_MACROMODEL                                      " "$macromod" $FICH_DIR_NAME
 change "CHANGE_CLOCK                                           " "$clk_data" $FICH_DIR_NAME
 change "CHANGE_OCEAN_LOAD_FILE                                 " "$otl_file" $FICH_DIR_NAME
 change "CHANGE_OCEAN_LOAD_CMC                                  " "$cmc_file" $FICH_DIR_NAME
 change "OTLCM"                                                   "$cmc_key"  $FICH_DIR_NAME
 change "CHANGE_ATMOS_LOAD_FILE                                 " "$atm_file" $FICH_DIR_NAME
 change "ATMLK"                                                   "$atml_key" $FICH_DIR_NAME
 change "CHANGE_ATMOS_LOAD_S1S2                                 " "$atm_s1s2" $FICH_DIR_NAME
 change "S1S2K"                                                   "$s1s2_key" $FICH_DIR_NAME 
 change "CHANGE_PRAIRIE_OPTION                                  " "$pra_opts" $FICH_DIR_NAME
 change "CHANGE_A_PRIORI_VALUES                                 " "$val_apri" $FICH_DIR_NAME
 change "CHANGE_ORBIT                                           " "$orb_data" $FICH_DIR_NAME
 change "CHANGE_MEASUREMENT_FILE                                " "$obs_file" $FICH_DIR_NAME
 change "CHANGE_TRO"                                              "$tro_info" $FICH_DIR_NAME
 change "CUT_ANG"                                                 "$cutoff"   $FICH_DIR_NAME
 change "PRKEY"                                                   "$prakey"   $FICH_DIR_NAME
 change "JUL50"                                                   "$sjul50"   $FICH_DIR_NAME
 change "JUL51"                                                   "$ejul50"   $FICH_DIR_NAME
 change "JJHHMMSS"                                                "$pos_ch"   $FICH_DIR_NAME
 change "SSSS"                                                    "$st_upper" $FICH_DIR_NAME
} &> /dev/null
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## 2nd order ionospheric correction
##------------------------------------------------------------------------------
case $ion2nd in
 no) sed -i '/ionex/d' $FICH_DIR_NAME
esac
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## remove NO_???LOAD items from LFREE block
##------------------------------------------------------------------------------
sed -i '/\(NO_ATMLOAD\|NO_SOLLOAD\|NO_OCELOAD\)/d' $FICH_DIR_NAME
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## tropospheric gradient
##------------------------------------------------------------------------------
case $tro_gr in
 [1-9]|[1-9][0-9]) printf "%17.17s %68.68sLFREE\n" \
 "GRADIENTS_TROPO$tro_gr_nb" " " >> $FICH_DIR_NAME ;;
esac
##------------------------------------------------------------------------------

printf "%s\n" "${FICH_DIR_NAME#$HOME/gin/data/directeur/}"
