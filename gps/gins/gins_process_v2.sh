#!/bin/bash

#@
#@ USAGE  : gins_process_v2.sh -f <driver_file> <options>
#@
#@ OPTIONS: -w <wrk_dir> - work dirname [default: $HOME/wrk/$USER-ginspc]
#@          -s <seconds> - sleep time   [default:               1 second]
#@          -v           - verbose_mode [default:                    off]
#@          -c           - clean_up     [default:                    off]
#@          -h           - help         [default:                    off]
#@      
#@ TASK   : Test GINS's *.prepars and run exe_gins90
#@
#* by     : marcell.ferenc@cnam.fr
#*

##------------------------------------------------------------------------------
cnf=${0%/*}/gins_env.cnf
if [ ! -s $cnf ]; then
 echo "${0##*/}: error: <gins_env.cnf> file is missing"; exit 2
else
 bcp_dir=$( awk '$1=="GINS_WORK_BACKUP" { print $2; exit; }' $cnf )
 if [ -z "$bcp_dir" ]; then
  echo "${0##*/}: error: <bcp_dir> is not defined in <gins_env.cnf>"; exit 2; fi
fi
##------------------------------------------------------------------------------

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir 2> /dev/null
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/gins.func
source $HOME/bin/func/gps.func
source $HOME/bin/func/ts.func

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## default variable
version=VALIDE_13_3
exe_gins=exe_gins90_mf
cleaning=no
sleep=1
v=no
ld=$HOME/gin/batch/listing
fp=$HOME/gin/batch/fic
wrk_dir=${USER}-ginspc
log=$scriptname.log
LC_NUMERIC=C ## for decimal separator in floating numbers

##---------------------------------
## variable names
## fdp = fich_dir_path
## fdn = fich_dir_name
## ld  = listing_dir
## rf  = result_file
## gp  = gins_prepars
## fnm = fic_name
## fp  = fic_path
##
##
##
##
##
##
##
##---------------------------------

## list of options the program will accept
optstring=f:s:w:cvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  w) wrk_dir=$OPTARG; wrk_dir="${wrk_dir##*/}" ;;
  f) fich_dir=$OPTARG ;;
  s) sleep=$OPTARG ;;
  v) v=yes ;;
  h) usage $0; exit ;;
  c) cleaning=yes ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## test fichier_directory
if [ -z "$fich_dir" ]; then
 color_txt bold red "-f <driver_file> option is obligatory"; exit 2
else
 if [[ -s "$fich_dir" && ! -d "$fich_dir" ]]; then
  case $fich_dir in
   */*) fdp=${fich_dir%/*}; fdn="${fich_dir##*/}" ;;
     *) fdp=$HOME/gin/data/directeur; fdn=$fich_dir ;;
  esac
 else
  color_txt bold red "file error: [$fich_dir]"; exit 2; fi; fi

## wait time
case $sleep in
 [1-9]|[1-9][0-9]|[1-9][0-9][0-9]) sleep=$sleep ;;
 *) color_txt bold red "not a valid wait time [$sleep]"; exit 2 ;;
esac

[ "$v" == "yes" ] && echo sleep_time : $sleep

## test output result type
case $fdn in
 *GR2*|*GRG*) result=fixed; fixed=-IPPP ;;
     *IGS*|*) result=float; fixed="" ;;
esac

## wrk_dir and result_dir
wrk_dir=$HOME/wrk/${wrk_dir:0:30}
bcp_dir=$bcp_dir/${wrk_dir##*/}
log_dir=$wrk_dir/log
lst_dir=$wrk_dir/lst
raw_dir=$wrk_dir/result/raw
day_dir=$wrk_dir/result/daily
drv_dir=$wrk_dir/result/driver
  
## real time
timeout=90

## cpu time
timeout_cpu=40

## information
color_txt bold blue $fdn

## test prepars  
## if multiple *prepars file exists, then use just the most recent one
find $ld -name "$fdn.*.prepars" | egrep '.*' &> /dev/null
#ls -t $ld/$fdn.*.prepars &>/dev/null
if [ $? -ne 0 ]; then
 color_txt bold red "no_prepars           : $fdn"; exit 2
else
 #gp=$( ls -t $ld/$fdn.*.prepars 2>/dev/null | head -n1 )
 gp=$( find $ld -name "$fdn.*.prepars" | sort -n 2>/dev/null | tail -n1 ) ### exit code!!!!!!!!!!
 if [ -z "$gp" ]; then
  color_txt bold red "${0##*/}: error: no prepars file found." ; exit 2
 elif [ ! -s "$gp" ]; then
  color_txt bold red "${0##*/}: error: no prepars file found." ; exit 2; fi
 [ "$v" == "yes" ] && color_txt bold blue "prepars             : $gp"; fi
  
## fic file
fnm=${gp%.prepars}
fnm=${fnm##*/}
fic=$fp/$fnm

## test fic file
if [ ! -s $fic ]; then
 color_txt bold red "no_fic : $fnm"; exit 2
else [ "$v" == "yes" ] && color_txt bold blue "fic    : $fnm"; fi

## start time of exe_gins
stime=$( date +%s )

## GINS-PC
color_txt bold blue "$exe_gins -fic $fnm -v $version $fixed"
$exe_gins -fic $fnm -v $version $fixed

sleep 1

if [ -s $ld/$fnm.gins ]; then
 mkdir -p $wrk_dir $raw_dir $day_dir $drv_dir $log_dir $lst_dir
 mkdir -p $bcp_dir/{directeur,eqna,eqnbc,fic,graphique,horloges,listing,orbite,references,statistiques}
 cd $wrk_dir
else color_txt bold red "$ld/$fnm.gins"; exit 2; fi

color_txt bold green "$exe_gins finished"

## chck prepars listing and analyze results
extraction_listing_pour_synthese $ld/$fnm.prepars > $log_dir/${fnm}-prepars.log

echo
echo $ld/$fnm.gins
echo

## chck gins listing and analyze results
extraction_listing_pour_synthese $ld/$fnm.gins > $log_dir/${fnm}-gins.log
  
## parameters to extract
{
 echo "[SH  ddhhiiss?????????yym]"
 echo "[SP  ddhhiiss?????????yym]"
 echo "[SL  ddhhiiss?????????yym]"
 echo "[SHE ddhhiiss?????????yym]"
 echo "[SPE ddhhiiss?????????yym]"
 echo "[SLE ddhhiiss?????????yym]"
 echo "[SX  ????????????????????]"
 echo "[SY  ????????????????????]"
 echo "[SZ  ????????????????????]"
 echo "[MNA?????????????????????]"
 echo "[MNG?????????????????????]"
 echo "[MNS?????????????????????]"
 echo "[MZB??????????????????GPS]"
 echo "[MZD?????????????????????]"
} > $tmp.unknowns
  
## desired name of result file
rf=${fnm%.??????_??????}
  
## control file to extract results
{ echo $tmp.unknowns; echo 1; echo $rf; } > $tmp.exin
   
## extract parameters
extraction_parametres_sortie_gins_mod_mf \
NOZERO $ld/$fnm.gins < $tmp.exin 2> /dev/null

ls *$rf &>/dev/null
if [ $? -ne 0 ]; then
 color_txt bold red "no extracted result for $fic"; exit 2; fi

## new output file name, without unnecessary characters
for i in $( ls *$rf 2>/dev/null )
do
 nnm=${i:0:3}_$rf
    
 /bin/mv $i $day_dir/$nnm

 color_txt bold green "$nnm"
      
 ## X,Y,Z coordinates - SX_.. SY_.. SZ_.. or SP_.. SL.. SH..
 case ${nnm:0:2} in
  S[XYZPLH]) 
             color_txt bold green "coordinates"
             if [ "$result" == "fixed" ]
             then
              ## separate float and fixed results, NOTE: these few lines 
              ## copied and modified from decoupe_en_2 scripts
              nb=$( wc $day_dir/$nnm | awk '{ print $1/2 }' )
              head -n$nb $day_dir/$nnm > $day_dir/${nnm}-fl
              tail -n$nb $day_dir/$nnm > $day_dir/${nnm}-fx

              ## remove the file which contains all solutions (fix and float)
              /bin/rm -f $day_dir/$nnm
         
             elif [ "$result" == "float" ]
             then
              /bin/mv $day_dir/$nnm $day_dir/${nnm}-fl
             fi ;;
 esac
      
      ## ambiguity - MNA_...
#       if [ "${nnm:0:3}" ==  "MNA" ]
#       then
#        color_txt bold green "ambiguity"
#        if [ "$result" == "float_fixed" ]
#        then
#         ## separate float and fixed results
#         ## NOTE: this few line copied and modified from decoupe_en_2 scripts
#         nb=$( wc $day_dir/$nnm | awk '{ print $1/2 }' )
#         head -n$nb $day_dir/$nnm > $day_dir/${nnm%.dat}.fl
#         tail -n$nb $day_dir/$nnm > $day_dir/${nnm%.dat}.fx

         ## remove the result file which contains all the solutions (fix and float)
#         /bin/rm -f $day_dir/$nnm
         
#        elif [ "$result" == "float" ]
#        then
#         /bin/mv $day_dir/$nnm $day_dir/${nnm%.dat}.fl
#        fi
       ## end - MNA  
#       fi
       
       ## receiver clock - MNS_...
#       if [ "${nnm:0:3}" ==  "MNS" ]
#       then
#        color_txt bold green "receiver clock"
#        if [ "$result" == "float_fixed" ]
#        then
         ## separate float and fixed results
         ## NOTE: this few line copied and modified from decoupe_en_2 scripts
#         nb=$( wc $day_dir/$nnm | awk '{ print $1/2 }' )
#         head -n$nb $day_dir/$nnm > $day_dir/${nnm%.dat}.fl
#         tail -n$nb $day_dir/$nnm > $day_dir/${nnm%.dat}.fx

         ## remove the result file which contains all the solutions (fix and float)
#         /bin/rm -f $day_dir/$nnm 
#        elif [ "$result" == "float" ]
#        then
#         /bin/mv $day_dir/$nnm $day_dir/${nnm%.dat}.fl
#        fi
       ## end - MNS  
#       fi
       
 ## zenithal tropospheric delay - MZB_...
 if [ "${nnm:0:3}" ==  "MZB" ]
 then
  color_txt bold green "zenithal tropospheric delay"
  if [ "$result" == "fixed" ]
  then
   nb=$( wc $day_dir/$nnm | awk '{ print $1/2 }' )
   head -n$nb $day_dir/$nnm > $day_dir/${nnm}-fl
   tail -n$nb $day_dir/$nnm > $day_dir/${nnm}-fx
   /bin/rm -f $day_dir/$nnm
  elif [ "$result" == "float" ]
  then
   /bin/mv $day_dir/$nnm $day_dir/${nnm}-fl
  fi
 ## end - MZB
 fi 
done
      
jjul=${fnm:13:5}
y4=$( jjul $jjul | awk '/DATE/ {print $5}' )
y2=${y4:2:2}
d3=$( jjul $jjul | awk '/J\/AN/ {printf "%0.3d\n",$3}' )
rnx=${sta_min}${d3}0.${y2}o
echo $jjul - $rnx
     
## free up some place
#if [ "$cleaning" == "yes" ]
#then
 #/bin/rm -f $rnx_dir/$rnx
{
 echo "${0##*/}: backing up <$fic> to <$bcp_dir/fic/.>"
 /bin/mv -f $fic $bcp_dir/fic/.
 
 echo "${0##*/}: backing up <$HOME/gin/batch/eqna/$fnm.gins.000> to <$bcp_dir/eqna/.>"
 /bin/mv -f $HOME/gin/batch/eqna/$fnm.gins.000 $bcp_dir/eqna/.
 
 echo "${0##*/}: backing up <$HOME/gin/batch/statistiques/$fnm.gins.0> to <$bcp_dir/statistiques/.>"
 /bin/mv -f $HOME/gin/batch/statistiques/$fnm.gins.0 $bcp_dir/statistiques/.
 
 echo "${0##*/}: backing up <$HOME/gin/batch/statistiques/$fnm.gins.1> to <$bcp_dir/statistiques/.>"
 /bin/mv -f $HOME/gin/batch/statistiques/$fnm.gins.1 $bcp_dir/statistiques/.
 ## move results and driver file to the work directory
 
 echo "${0##*/}: backing up <$ld/$fnm.gins> to <$bcp_dir/listing/.>"
 /bin/mv -f $ld/$fnm.gins $bcp_dir/listing/.
 #/bin/mv -f $HOME/gin/batch/listing/$fnm.gins $bcp_dir/listing/.
 
 echo "${0##*/}: backing up <$fdp/$fdn> to <$bcp_dir/directeur/.>"
 /bin/mv -f $fdp/$fdn $bcp_dir/directeur/.
} #2> /dev/null
#else
# echo not yet
 #/bin/rm -f $rnx_dir/$rnx
 #/bin/mv $HOME/gin/batch/listing/$fic.gins $raw_dir/.
 #/bin/cp $HOME/gin/data/directeur/$item $drv_dir/.     
#fi

# echo "cleaning stocked files for $fic"
# {
#  rm -rf $HOME/gin/batch/eqna/$fnm.gins.000
#  rm -rf $HOME/gin/batch/statistiques/$fnm.gins.0
#  rm -rf $HOME/gin/batch/statistiques/$fnm.gins.1
#  rm -rf $HOME/gin/batch/listing/out_gins_goce
#  rm -rf $HOME/gin/batch/listing/out_gins_goce_$fnm.gins
#  rm -rf $HOME/gin/batch/listing/PROV.$fnm.gins
#  rm -rf $HOME/gin/batch/listing/$fnm.gins
# } 2>/dev/null
 
## sleep a little
sleep $sleep

## end time of exe_gins90
etime=$( date +%s )  
     
## elapsed time with sleep
dt=$(( $etime - $stime ))
ds=$(( $dt % 60 ))
dm=$(( ( $dt / 60 ) % 60 ))
dh=$(( $dt / 3600 ))

echo
echo $dh $dm $ds
echo

## make a log file with result gins file and its process time
printf "%s %10.10s %02d:%02d:%02d\n" $ld/$fnm.gins " " $dh $dm $ds
