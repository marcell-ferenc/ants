#!/bin/bash

#@
#@ USAGE  : xyz2enu.sh -f [TSERIE] | [OPTIONS]
#@
#@ OPTIONS:
#@          -m - reference_coord   [default:    MED]
#@          -v - verbose mode
#@          -h - help
#@
#@ TASK   : Convert cartesian X, Y, Z time series [m] into e, n, u residuals [mm].
#@          To determine the residuals of X, Y and Z this script uses 
#@          the median value of the X, Y and Z components:
#@
#@           dX = X - X_median
#@           dY = Y - Y_median
#@           dZ = Z - Z_median
#@
#@          The input time serie have to have 7 columns and 
#@          the coordinates have to be in meters (user's responsibility)
#@
#@          - TIME X X_sig Y Y_sig Z Z_sig [meters]
#@
#@ NOTE   : In case of multiple input files use double quotes: 
#@          "TSERIE1 ... TSERIEn"
#@
#@ OUTPUT : - *.enu
#@          TIME, EAST, EAST_sig, NORTH, NORTH_sig, UP, UP_sig values in [mm]
#@
#@          - *.res
#@          TIME, dX, X_sig, dY, Y_sig, dZ, Z_sig values in [mm]
#@ 
#@ WARNING: 
#@          This script is only valid on the GALILEE server because
#@          uses the xyz2env program (GIPSY package)
#@
#* by     : marcell.ferenc@cnam.fr
#*

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir
 ## restore default value 
 trap EXIT
 exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
source $HOME/bin/func/general.func
source $HOME/bin/func/ts.func

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## test xyz2env program
xyz2env 2>/dev/null
if [ $? -eq 127 ]
then
 xyz2env=/opt/goa-6.1/bin/Linux86/xyz2env.e
 if [ ! -s $xyz2env ]; then color_txt bold red "$xyz2env not exist"; exit 2; fi
else
 xyz2env=xyz2env
fi

## default variables
method=MED
xyz=$tmp.xyz
sig=$tmp.sig
un=1000 ## unit conversion m --> mm

## list of options the program will accept
optstring=f:m:vh

## interpret options
while getopts $optstring opt
do
 case $opt in
  f) files=( $OPTARG ) ;;
  m) method=$OPTARG ;;
  v) v=yes ;;
  h) usage $0; exit ;;
 esac
done
shift "$(( $OPTIND - 1 ))"

## test files
if [ -z "$files" ]
then
 color_txt bold red "-f [file(s)] option is required"; usage $0; exit 2
else
 for file in ${files[@]}
 do
  if [ -s $file ]
  then
   info=( $( istab $file ) )
   if [ "${info[0]}" == "is_data_table" ]
   then
    case "${info[1]}" in
     7) is_item+=( "$file" ); [ "$v" == "yes" ] && color_txt bold green "$file - exist" ;;
     *) no_item+=( "$file" ); [ "$v" == "yes" ] && color_txt bold red "$file - not in a good format" ;; 
    esac
   else
    no_item+=( "$file" ); [ "$v" == "yes" ] && color_txt bold red "$file - not a data table"
   fi
  else
   no_item+=( "$file" ); [ "$v" == "yes" ] && color_txt bold red "$file - does not exist or empty"
  fi
  if [ -z "$is_item" ]; then color_txt bold red "not accepted input: ${no_item[@]}" ; exit 2; fi
 done  
fi

## test method
case $method in
 MED|MEAN) ;;
        *) method=MED ;;
esac

[ "$v" == "yes" ] && color_txt bold blue "using: $method"

## loop - existing input
for file in ${is_item[@]}
do

 ## enu file
 enu=${file%.*}.enu
 ## dxdydz file
 res=${file%.*}.res

 ## create xyz X, Y, Z
 awk '{ print $2,$4,$6 }' $file > $xyz

 ## create sig sig_X, sig_Y, sig_Z
 awk '{ print $3,$5,$7 }' $file > $sig

 ## median of X, Y, Z
 gmtmath -T -Ca $xyz $method = $tmp.dat

 ## remove median
 gmtmath -T -Ca $xyz $tmp.dat SUB = $tmp.res

 ## construct file for transformation [6 columns]
 paste $tmp.dat $tmp.res | awk '{ print $0 }' > $tmp.xyz-inp
 paste $tmp.dat $sig | awk '{ print $0 }' > $tmp.sig-inp

 ## tranformation of X, Y, Z [6 columns]
 [ "$v" == "yes" ] && color_txt bold green "${file:0:4} - coordinates"
 awk '{ print $0 }' $tmp.xyz-inp | $xyz2env - > $tmp.xyz-out

 ## transformation of sig_X, sig_Y, sig_Z [6 columns]
 [ "$v" == "yes" ] && color_txt bold green "${file:0:4} - sigmas"
 awk '{ print $0 }' $tmp.sig-inp | $xyz2env - | gmtmath -T -Ca STDIN ABS = $tmp.sig-out

 ## creat final result file
 ## TIME, E, sig_E, N, sig_N, U, sig_U [7 columns]
 #paste $file $tmp.xyz-inp | awk -v u=$un '{ printf "%20.13E %20.13E %20.13E %20.13E %20.13E %20.13E\n",$11*u,$3*u,$12*u,$5*u,$13*u,$7*u }' > $tmp.res-out
 
 ## residuals in xyz system in [mm]
 paste $sig $tmp.xyz-inp | awk -v u=$un '{ printf "%20.13E %20.13E %20.13E %20.13E %20.13E %20.13E\n",$7*u,$1*u,$8*u,$2*u,$9*u,$3*u }' > $tmp.res-out
 #paste $file $tmp.xyz-out $tmp.sig-out | awk -v u=$un '{ printf "%20.13E %20.13E %20.13E %20.13E %20.13E %20.13E\n",$8*u,$11*u,$9*u,$12*u,$10*u,$13*u }' > $tmp.enu-out
 
 ## residuals in enu system in [mm]
 paste $tmp.xyz-out $tmp.sig-out | awk -v u=$un '{ printf "%20.13E %20.13E %20.13E %20.13E %20.13E %20.13E\n",$1*u,$4*u,$2*u,$5*u,$3*u,$6*u }' > $tmp.enu-out

 paste <( awk '{ print $1 }' $file ) $tmp.res-out > $res
 paste <( awk '{ print $1 }' $file ) $tmp.enu-out > $enu

done
