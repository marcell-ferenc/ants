#!/bin/bash

#@
#@ USAGE  : gins_process.sh -f [DIRECTOR_FILE] | [OPTIONS]
#@
#@ OPTIONS:
#@          -w nb_of_seconds   [default:   1]
#@          -v verbose_mode    [default:  no]
#@          -c clean_up        [default:  no]
#@          -h help
#@      
#@ TASK   : Test GINS's *.prepars and run exe_gins90
#@
#* by     : marcell.ferenc@cnam.fr
#*

##---------------------------------
## D E F A U L T   S E T T I N G S 
##---------------------------------
version=VALIDE_13_3
exe_gins=exe_gins90_mf
cleaning=no
sleep=1
v=no
ld=$HOME/gin/batch/listing
fp=$HOME/gin/batch/fic
wrk_dir=$USER
##---------------------------------
## variable names
## fdp = fich_dir_path
## fdn = fich_dir_name
## ld = listing_dir
## rf = result_file
## gp = gins_prepars
## fnm = fic_name
## fp = fic_path
##
##
##
##
##
##
##
##---------------------------------

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/gins.func
source $HOME/bin/func/gps.func
source $HOME/bin/func/ts.func

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## list of options the program will accept
optstring=f:p:w:cvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  p) wrk_dir=$OPTARG ;;
  f) fich_dir=$OPTARG ;;
  w) sleep=$OPTARG ;;
  v) v=yes ;;
  h) usage $0; exit ;;
  c) cleaning=yes ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## test fichier_directory
if [ -z "$fich_dir" ]
then
 color_txt bold red "-f option is obligatory [fich_dir]"; exit 2
else
 if [ -s "$fich_dir" ]
 then
  case $fich_dir in
   */*) fdp=${fich_dir%/*}; fdn="${fich_dir##*/}" ;;
     *) fdp=$HOME/gin/data/directeur; fdn=$fich_dir ;;
  esac
  ## wrk_dir and result_dir
  wrk_dir=${wrk_dir#$HOME/}
  wrk_dir=$HOME/wrk/${wrk_dir:0:10}-$( date "+%Y-%m-%d" )
  log_dir=$wrk_dir/log
  lst_dir=$wrk_dir/lst
  raw_dir=$wrk_dir/result/raw
  day_dir=$wrk_dir/result/daily
  drv_dir=$wrk_dir/result/driver
 else
  color_txt bold red "file error: [$fich_dir]"; exit 2
 fi
fi

## wait time
case $sleep in
 [1-9]|[1-9][0-9]|[1-9][0-9][0-9]) sleep=$sleep ;;
 *) color_txt bold red "not a valid wait time"; exit 2 ;;
esac

[ "$v" == "yes" ] && echo sleep_time : $sleep

## test output result type
case $fdn in
 *GR2*|*GRG*) result=fixed ;;
     *IGS*|*) result=float ;;
esac

## real time
timeout=90

## cpu time
timeout_cpu=40

## information
color_txt bold blue $fdn

## test prepars  
## if multiple *prepars file exists, then use just the most recent one
ls -t $ld/$fdn.*.prepars &>/dev/null
if [ $? -ne 0 ]
then
 color_txt bold red "no_prepars           : $fdn" exit 2
else
 gp=$( ls -t $ld/$fdn.*.prepars 2>/dev/null | head -n1 )
 [ "$v" == "yes" ] && color_txt bold blue "prepars             : $gp"
fi
  
## fic file
fnm=${gp%.prepars}
fnm=${fnm##*/}
fic=$fp/$fnm

## test fic file
if [ ! -s $fic ]
then
 color_txt bold red "no_fic : $fnm"; exit 2
else
 [ "$v" == "yes" ] && color_txt bold blue "fic    : $fnm" 
fi

## start time of exe_gins
stime=$( date +%s )

#----------------------------------------------------------------------------
#----------------------------------------------------------------------------      
#----------------------------------------------------------------------------
if [ "$result" == "fixed" ]
then
 echo "$exe_gins -fic $fnm -v $version -IPPP"
 ( $exe_gins -fic $fnm -v $version -IPPP ) & pid=$!
 ( sleep $timeout && kill -HUP $pid ) 2>/dev/null & watcher=$!
elif [ "$result" == "float" ]
then
 echo "$exe_gins -fic $fnm -v $version"
 ( $exe_gins -fic $fnm -v $version ) & pid=$!
 ( sleep $timeout && kill -HUP $pid ) 2>/dev/null & watcher=$!
fi
#----------------------------------------------------------------------------
#----------------------------------------------------------------------------
#----------------------------------------------------------------------------

if [ -s $ld/$fnm.gins ]
then
 mkdir -p $wrk_dir $raw_dir $day_dir $drv_dir $log_dir $lst_dir; cd $wrk_dir
else
 color_txt bold red "$ld/$fnm.gins"; exit 2
fi

#----------------------------------------------------------------------------
#------------ S U C C E S F U L   R U N -------------------------------------
#----------------------------------------------------------------------------
if wait $pid 2>/dev/null
then

 color_txt bold green "$exe_gins finished"
 echo $fic >> $lst_dir/exe_gins90_ok.txt
 pkill -HUP -P $watcher
 wait $watcher
   
 ## !!! $fic.prepars == $gp without its path
   
 ## chck prepars listing and analyze results
 extraction_listing_pour_synthese $ld/$fnm.prepars \
 > $log_dir/${fnm}-prepars.log

 ## test *.gins
 if [ -s $ld/$fnm.gins ]
 then
  
  echo
  echo $ld/$fnm.gins
  echo
  
  
  ## chck gins listing and analyze results
  extraction_listing_pour_synthese $ld/$fnm.gins \
  > $log_dir/${fnm}-gins.log
  
  ## parameters to extract
  {
   echo "[SH  ddhhiiss?????????yym]"
   echo "[SP  ddhhiiss?????????yym]"
   echo "[SL  ddhhiiss?????????yym]"
   echo "[SHE ddhhiiss?????????yym]"
   echo "[SPE ddhhiiss?????????yym]"
   echo "[SLE ddhhiiss?????????yym]"
   echo "[SX  ????????????????????]"
   echo "[SY  ????????????????????]"
   echo "[SZ  ????????????????????]"
   echo "[MNA?????????????????????]"
   echo "[MNG?????????????????????]"
   echo "[MNS?????????????????????]"
   echo "[MZB??????????????????GPS]"
   echo "[MZD?????????????????????]"
  } > $tmp.unknowns
  
  ## desired name of result file
  rf=${fnm%.??????_??????}
  
  ## control file to extract results
  {
   echo $tmp.unknowns
   echo 1
   echo $rf
  } > $tmp.exin
   
  ## extract parameters
  extraction_parametres_sortie_gins_mod_mf NOZERO $ld/$fnm.gins < $tmp.exin
  
  ## cleaning
  /bin/rm -rf $tmp.exin
   
  ## new output file name, without unnecessary characters
  for i in $( ls *$rf 2>/dev/null )
  do
   
   if [ -s $i ]
   then
    
    nnm=${i:0:3}_$rf
    
    /bin/mv $i $day_dir/$nnm

    color_txt bold green "$nnm"
      
    ## X,Y,Z coordinates - SX_.. SY_.. SZ_.. or SP_.. SL.. SH..
    case ${nnm:0:2} in
     S[XYZPLH]) 
               color_txt bold green "coordinates"
               if [ "$result" == "fixed" ]
               then
                ## separate float and fixed results, NOTE: these few lines 
                ## copied and modified from decoupe_en_2 scripts
                nb=$( wc $day_dir/$nnm | awk '{ print $1/2 }' )
                head -n$nb $day_dir/$nnm > $day_dir/${nnm}-fl
                tail -n$nb $day_dir/$nnm > $day_dir/${nnm}-fx

                ## remove the file which contains all solutions (fix and float)
                /bin/rm -f $day_dir/$nnm
         
                elif [ "$result" == "float" ]
                then
                 /bin/mv $day_dir/$nnm $day_dir/${nnm}-fl
                fi ;;
    esac
      
       ## ambiguity - MNA_...
#       if [ "${nnm:0:3}" ==  "MNA" ]
#       then
#        color_txt bold green "ambiguity"
#        if [ "$result" == "float_fixed" ]
#        then
#         ## separate float and fixed results
#         ## NOTE: this few line copied and modified from decoupe_en_2 scripts
#         nb=$( wc $day_dir/$nnm | awk '{ print $1/2 }' )
#         head -n$nb $day_dir/$nnm > $day_dir/${nnm%.dat}.fl
#         tail -n$nb $day_dir/$nnm > $day_dir/${nnm%.dat}.fx

         ## remove the result file which contains all the solutions (fix and float)
#         /bin/rm -f $day_dir/$nnm
         
#        elif [ "$result" == "float" ]
#        then
#         /bin/mv $day_dir/$nnm $day_dir/${nnm%.dat}.fl
#        fi
       ## end - MNA  
#       fi
       
       ## receiver clock - MNS_...
#       if [ "${nnm:0:3}" ==  "MNS" ]
#       then
#        color_txt bold green "receiver clock"
#        if [ "$result" == "float_fixed" ]
#        then
         ## separate float and fixed results
         ## NOTE: this few line copied and modified from decoupe_en_2 scripts
#         nb=$( wc $day_dir/$nnm | awk '{ print $1/2 }' )
#         head -n$nb $day_dir/$nnm > $day_dir/${nnm%.dat}.fl
#         tail -n$nb $day_dir/$nnm > $day_dir/${nnm%.dat}.fx

         ## remove the result file which contains all the solutions (fix and float)
#         /bin/rm -f $day_dir/$nnm 
#        elif [ "$result" == "float" ]
#        then
#         /bin/mv $day_dir/$nnm $day_dir/${nnm%.dat}.fl
#        fi
       ## end - MNS  
#       fi
       
    ## zenithal tropospheric delay - MZB_...
    if [ "${nnm:0:3}" ==  "MZB" ]
    then
     color_txt bold green "zenithal tropospheric delay"
     if [ "$result" == "float_fixed" ]
     then
      nb=$( wc $day_dir/$nnm | awk '{ print $1/2 }' )
      head -n$nb $day_dir/$nnm > $day_dir/${nnm}-fl
      tail -n$nb $day_dir/$nnm > $day_dir/${nnm}-fx
      /bin/rm -f $day_dir/$nnm
     elif [ "$result" == "float" ]
     then
      /bin/mv $day_dir/$nnm $day_dir/${nnm}-fl
     fi
    ## end - MZB
    fi 
    ## no extracted result fit...
   else
    color_txt bold red "no extracted result for $fic"
   fi
  done
      
  jjul=${fnm:13:5}
  y4=$( jjul $jjul | awk '/DATE/ {print $5}' )
  y2=${y4:2:2}
  d3=$( jjul $jjul | awk '/J\/AN/ {printf "%0.3d\n",$3}' )
  rnx=${sta_min}${d3}0.${y2}o
  echo $jjul - $rnx
     
  ## free up some place
  if [ "$cleaning" == "yes" ]
  then
   echo not yet
   #/bin/rm -f $rnx_dir/$rnx
   #/bin/mv $fic_dir/$fic $fic_storage/.
   #/bin/mv $eqna_dir/$fic.gins.000 $eqna_storage/.
   #/bin/mv $statistiques_dir/$fic.gins.0 $statistiques_storage/.
   #/bin/mv $statistiques_dir/$fic.gins.1 $statistiques_storage/.
   ## move results and driver file to the work directory
   #/bin/mv $HOME/gin/batch/listing/$fic.gins $raw_dir/.
   #/bin/mv $HOME/gin/data/directeur/$item $drv_dir/.
  else
   echo not yet
   #/bin/rm -f $rnx_dir/$rnx
   #/bin/mv $HOME/gin/batch/listing/$fic.gins $raw_dir/.
   #/bin/cp $HOME/gin/data/directeur/$item $drv_dir/.     
  fi
       
 ## end fi - *.gins exists
 fi
 ##------------ end - GINS RUN TEST SUCCESSFUL ------------------------------
   
#----------------------------------------------------------------------------
#---------------- P R O B L E M A T I C - R U N -----------------------------
#----------------------------------------------------------------------------
else

 color_txt bold red "$exe_gins stucked"
    
 echo $fic >> $lst_dir/exe_gins90_er.txt
 echo "cleaning stocked files for $fic"
 {
  rm -rf $HOME/gin/batch/eqna/$fnm.gins.000
  rm -rf $HOME/gin/batch/statistiques/$fnm.gins.0
  rm -rf $HOME/gin/batch/statistiques/$fnm.gins.1
  rm -rf $HOME/gin/batch/listing/out_gins_goce
  rm -rf $HOME/gin/batch/listing/out_gins_goce_$fnm.gins
  rm -rf $HOME/gin/batch/listing/PROV.$fnm.gins
  rm -rf $HOME/gin/batch/listing/$fnm.gins
 } 2>/dev/null

 nb_bad_pids=$(( $( ps -eaf \
 | grep $USER | grep ./gins90_$fnm \
 | awk -v tlimit=$timeout_cpu \
 '{ hr=substr($7,1,2); min=substr($7,4,2); sec=substr($7,7,2); print (((hr*3600)+(min*60)+sec)>tlimit) ? $2 : " " }' | wc -l ) - 1 ) )

 bad_pids=( $( ps -eaf \
 | grep $USER | grep ./gins90_$fnm \
 | awk -v tlimit=$timeout_cpu \
 '{ hr=substr($7,1,2); min=substr($7,4,2); sec=substr($7,7,2); print (((hr*3600)+(min*60)+sec)>tlimit) ? $2 : " " }' ) )
 
 ## loop in case if there are some task from previous run
 echo "------------------------------------------------------------------------" 
 echo "$nb_bad_pids --> ${bad_pids[@]}"
 echo "------------------------------------------------------------------------" 
 for bad_pid in ${bad_pids[@]}
 do
  if [ -n "$bad_pid" ]; then kill -9 $bad_pid; echo $bad_pid; fi
 done
 echo "------------------------------------------------------------------------" 
 
 ## the kill command does not print out information until a next command input
 echo
 
 sleep 2

#-------------------------------------------------------------------------------    
#----- if end - GINS RUN TEST PROBLEMATIC --------------------------------------
#-------------------------------------------------------------------------------
fi
   
## sleep a little
sleep $sleep

## end time of exe_gins90
etime=$( date +%s )  
     
## elapsed time with sleep
dt=$(( $etime - $stime ))
ds=$(( $dt % 60 ))
dm=$(( ( $dt / 60 ) % 60 ))
dh=$(( $dt / 3600 ))

echo
echo $dh $dm $ds
echo

## make a log file with result gins file and its process time
printf "%s %10.10s %02d:%02d:%02d\n" $ld/$fnm.gins " " $dh $dm $ds
