#!/bin/bash

#@
#@ USAGE  : gins_prairie.sh -r [RINEX]
#@
#* by     : marcell.ferenc@cnam.fr
#*

## source functions
. $HOME/bin/func/general.func
. $HOME/bin/func/gins.func
. $HOME/bin/func/gps.func
. $HOME/bin/func/ts.func

## test P1C1 file
if [ ! -s $P1C1 ]; then color_txt bold red "missing P1C1 file [$P1C1]"; exit 2; fi

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf ${tmp_dir}* list_prairie prairie_in MuSatRef.res.dat 2> /dev/null
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi
 
## default variable
v="no"
pra_out_opt=0

## list of options the program will accept
optstring=r:vh

## interpret options
while getopts $optstring opt
do
 case $opt in
  r) rinex_list=( $OPTARG ) ;;
  v) v=yes ;;
  h) usage $0 ; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

if [ -z "$rinex_list" ]
then
 color_txt bold red "-r option is obligatory [rinex_file(s)]"; exit 2
## rinex is an array with multiple items
else
 ## test rinex
 for rinex in ${rinex_list[@]}
 do
  if [ -s $rinex ]
  then
   [ "$v" == "yes" ] && color_txt bold blue "file: $rinex"
   info_rinex=( $( isvalrinex $rinex ) )
   case ${info_rinex[0]} in
    is_valid) is_valid+=( $rinex ) ;;
    no_valid) no_valid+=( $rinex ); [ "$v" == "yes" ] && color_txt bold red "file error: $rinex is not a rinex" ;;
   esac
  else
   [ "$v" == "yes" ] && color_txt bold red "file error: $rinex does not exist"; no_valid+=( $rinex )
  fi
 done
fi

## test rinex
if [ "${#is_valid[@]}" -eq 0 ]; then color_txt bold red "file error: no valid rinex(s)"; exit 2; fi

## Wide-lane Satellite Biases (WSB) from CNES-CLS IGS Analysis Center 
cp $HOME/gin/archives/WSBREF.res.dat MuSatRef.res.dat

for rinex in ${is_valid[@]}
do

 info_rinex=( $( isvalrinex $rinex ) ); station_uppercase=${info_rinex[1]}; station_lowercase=${info_rinex[2]}
 y4=${info_rinex[3]}; y2=${info_rinex[5]}; doy=${info_rinex[4]}; jul50=$( jjul $doy $y4 | awk '/JUL50/ {print $3}' )
 prairie_gps_opt -d $jul50 > options_prairie.dat
 date_rnx=( $( date --date="$y4-01-01 +$doy days -1 day" "+%Y %y %j %m %d" ) )
 date_bef=( $( date --date="$y4-01-01 +$doy days -1 day -1 day" "+%Y %y %j %m %d" ) )
 date_aft=( $( date --date="$y4-01-01 +$doy days -1 day +1 day" "+%Y %y %j %m %d" ) )
 rinex_day_bef=${station_lowercase}${date_bef[2]}0.${date_bef[1]}o; rinex_day_aft=${station_lowercase}${date_aft[2]}0.${date_aft[1]}o
 prairie_out=${rinex%o}p #${station_lowercase}_${jul50}.pra
 
 if [[ -s $rinex_day_bef && -s $rinex && -s $rinex_day_aft ]]
 then
  printf "%s\n" $rinex_day_bef $rinex $rinex_day_aft > list_prairie; [ "$v" == "yes" ] && color_txt bold blue "$rinex_day_bef -- $rinex -- $rinex_day_aft"
 elif [[ -s $rinex_day_bef && -s $rinex ]]
 then
  printf "%s\n" $rinex_day_bef $rinex > list_prairie; [ "$v" == "yes" ] && color_txt bold blue "$rinex_day_bef -- $rinex -- ____________"
 elif [[ -s $rinex && -s $rinex_day_aft ]]
 then
  printf "%s\n" $rinex $rinex_day_aft > list_prairie; [ "$v" == "yes" ] && color_txt bold blue "____________ -- $rinex -- $rinex_day_aft"
 else
  printf "%s\n" $rinex > list_prairie; [ "$v" == "yes" ] && color_txt bold blue "____________ -- $rinex -- ____________"
 fi
 
 ## prairie
 { printf "%s\n" list_prairie; printf "%d\n" $pra_out_opt; } > prairie_in
 prairie < prairie_in &> ${rinex%o}p.log
 
 if [ -s "sortiePDGR90" ]
 then
  /bin/mv -f sortiePDGR90 $prairie_out; prairie_list+=( $prairie_out ); rinex_to_remove+=( $rinex )
 else
  color_txt bold red "prairie error: $prairie_out (no output)"; printf "%s\n" $rinex >> no-prairie.log
 fi
 
done

printf "%s\n" "${prairie_list[@]}" > prairie.list
printf "/bin/rm -f %s\n" "${rinex_to_remove[@]}" | sh
