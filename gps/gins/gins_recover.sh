#!/bin/bash

#@
#@ USAGE     : gins_recover.sh -d <date_string> or -b <backup_number> <options>
#@
#@ OPTIONS   :
#@             -s <select_dir>          recover only the one selected directory
#@             -h                       help
#@
#@ TASK      : recover GINS-PC directories from desired backup.
#@
#* by        : marcell.ferenc@cnam.fr or marcell.ferenc.uni@gmail.com
#*

##------------------------------------------------------------------------------
cnf=${0%/*}/gins_env.cnf
if [ ! -s $cnf ]; then
 echo "${0##*/}: error: <gins_env.cnf> file is missing"; exit 2
else
 bcp_dir=$( awk '$1=="BACKUP_DIRECTORY" { print $2; exit; }' $cnf )
 if [ -z "$bcp_dir" ]; then
  echo "${0##*/}: error: <bcp_dir> is not defined in <gins_env.cnf>"; exit 2; fi
fi

##------------------------------------------------------------------------------

## source
source $HOME/bin/func/general.func

## default variable
sel=no
bcps=( $( find $bcp_dir -maxdepth 1 -print | sort -n ) )

## list of options the program will accept
optstring=b:d:s:h

## interpret options
while getopts $optstring opt
do
 case $opt in
  d) date_str=$OPTARG ;;
  b) bcp_num=$OPTARG ;;
  s) sel=yes; sel_dir=$OPTARG ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## test input
if [[ -z "$date_str" && -z "$bcp_num" ]]; then
 echo "${0##*/}: error: -d <date_string> option or" \
 "-b <backup_number> is required for bcp_id"; exit 2; fi

## test bcp_num
if [ -n "$bcp_num" ]; then
 
 ## test if bcp_num is integer
 case "$bcp_num" in
  *[!0-9]*) echo "${0##*/}: error: wrong <bcp_num> [$bcp_num]"; exit 2 ;;
         *) ;;
 esac
 
 ## define possible range for bcp_num
 cnt=0
 for bcp in ${bcps[@]:1}; do
  cnt=$(( $cnt + 1 ))
  bcp_array[$cnt]=$bcp
 done
 max_cnt=$cnt
 
 ## test if bcp_num is in range
 if [[ $bcp_num -gt 0 && $bcp_num -le $max_cnt ]]; then
  bcp_id=${bcp_array[$bcp_num]}
 else
  echo "${0##*/}: error: wrong <bcp_num>" \
  "[bcp_num:$bcp_num not in range (0 < bcp_num < $max_cnt)]"; exit 2; fi
fi

## if bcp_id not yet defined
if [ -z "$bcp_id" ]; then
 ## test date_str
 if [ -n "$date_str" ]; then
  case $date_str in
   [1-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9][0-9][0-9]) ;;
   *) echo "${0##*/}: error: wrong <date_string> value [$date_str]"; exit 2 ;;
  esac
 fi
 bcp_id=$bcp_dir/bcp-$date_str
fi

## if the desired bcp does not exist
if [ ! -d $bcp_id ]; then
 echo "${0##*/}: error: back up dir does not exist! [bcp-$date_str]"; exit 2; fi

src_dirs=( $bcp_id/eqna \
$bcp_id/eqnbc \
$bcp_id/fic \
$bcp_id/graphique \
$bcp_id/horloges \
$bcp_id/listing \
$bcp_id/orbite \
$bcp_id/references \
$bcp_id/statistiques \
$bcp_id/dynamo-listing \
$bcp_id/directeur )

if [ "$sel" == "yes" ]; then 
 case $sel_dir in
  eqna|eqnbc|fic|graphique|horloges|listing|orbite|references\
  |statistiques|dynamo-listing|directeur) dir=$bcp_id/$sel_dir ;;
  *) echo "${0##*/}: error: wrong <sel_dir> value [$sel_dir]"; exit 2 ;;
 esac
 case $dir in
  *eqna|*eqnbc|*fic|*graphique|*horloges|*/listing\
  |*orbite|*references|*statistiques) rec_path=$HOME/gin/batch/${dir##*/} ;;
                          *directeur) rec_path=$HOME/gin/data/${dir##*/} ;;
                     *dynamo-listing) rec_path=$HOME/dynamo/batch/listing ;;
 esac
 echo "${0##*/}: recovering <$dir> to <$rec_path>"
 find $dir -type f -name '*' -exec /bin/cp -f {} $rec_path \;
 exit 0
fi

for dir in ${src_dirs[@]}; do
 case $dir in
  *eqna|*eqnbc|*fic|*graphique|*horloges|*/listing\
  |*orbite|*references|*statistiques) rec_path=$HOME/gin/batch/${dir##*/} ;;
                          *directeur) rec_path=$HOME/gin/data/${dir##*/} ;;
                     *dynamo-listing) rec_path=$HOME/dynamo/batch/listing ;;
 esac
 echo "${0##*/}: recovering <$dir> to <$rec_path>"
 find $dir -type f -name '*' -exec /bin/cp -f {} $rec_path \; #2> /dev/null
done
