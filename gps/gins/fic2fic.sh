#!/bin/bash

#@
#@ USAGE  : fic2fic.sh -f [FIC] -r [RINEX] -x [ANTEX_FILE]
#@
#@ OPTIONS:
#@
#@ TASK   : construct a FIC file for a desired station and date, using the
#@          stations's RINEX and any other stations's already existing FIC file
#@          for that day to obtain the common environmental parameters.     
#@
#* by     : marcell.ferenc@cnam.fr
#*

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/gps.func
source $HOME/bin/func/gins.func
source $HOME/bin/func/ts.func

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## list of options the program will accept
optstring=f:r:vh

## interpret options
while getopts $optstring opt
do
 case $opt in
  f) fic=$OPTARG ;;
  r) rnx=$OPTARG ;;
  v) v=yes ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## test fic
if [ -z "$fic" ]
then
 color_txt bold red "$scriptname: -f option is obligatory [fic]"; exit 2
elif [[ ! -s $fic || $( grep GPS__HAUTE_FREQ $fic &> /dev/null; echo $? ) -ne 0 ]]
then
 color_txt bold red "$scriptname: fic file error [$fic]"; exit 2
fi

## test rinex
if [ -z "$rnx" ]
then 
 color_txt bold red "$scriptname: -r option is obligatory [rnx]"; exit 2
elif [ ! -s "$rnx" ]
then 
 color_txt bold red "$scriptname: rinex is missing [$rnx]"; exit 2
fi
 
rnx_info=$( ( isvalobsfile $rnx ) )
case ${rnx_info[0]} in
 is_valid) sta_upper=${rnx_info[1]}; sta_lower=${rnx_info[2]} ;;
 no_valid) color_txt bold red "$scriptname: rinex name error [$rnx]"; exit 2 ;;
esac

## _FDP_=$HOME/gin/data/directeur
## FICH_DIR_NAME=$_FDP_/${st_upper}_${pos_dt}_${acdata}_${sjul50}_${inf_ch}
## default variables

fic_path=$HOME/gin/batch/fic #${fic%/*}
old_fic="${fic##*/}"
new_fic=$fic_path/${sta_upper}_${old_fic:5}

## copy master fic files
/bin/cp -f $fic $tmp.fic

## construct station file
gins_station_info_rinex.sh -r $rnx -t

## construct new proper fic file
fic_gps -f $tmp.fic -s $sta_upper > $tmp.fic-tmp
if [ $? -ne 0 ]; then color_txt bold red "fic_gps error"; exit 2
else /bin/mv -f $tmp.fic-tmp $tmp.fic; fi

fic_sta -f $tmp.fic -r $rnx > $tmp.fic-tmp
if [ $? -ne 0 ]; then color_txt bold red "fic_sta error"; exit 2
else /bin/mv -f $tmp.fic-tmp $tmp.fic; fi

fic_oce -f $tmp.fic -r $rnx > $tmp.fic-tmp
if [ $? -ne 0 ]; then color_txt bold red "fic_gps oce"; exit 2
else /bin/mv -f $tmp.fic-tmp $tmp.fic; fi

fic_oce_cm -f $tmp.fic > $tmp.fic-tmp
if [ $? -ne 0 ]; then color_txt bold red "fic_oce_cm error"; exit 2
else /bin/mv -f $tmp.fic-tmp $tmp.fic; fi

fic_ant -f $tmp.fic -r $rnx -x $atx > $tmp.fic-tmp
if [ $? -ne 0 ]; then color_txt bold red "fic_ant error"; exit 2
else /bin/mv -f $tmp.fic-tmp $tmp.fic; fi

/bin/mv -f $tmp.fic $new_fic
