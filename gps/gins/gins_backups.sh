#!/bin/bash

#@
#@ USAGE     : gins_backups.sh
#@
#@ TASK      : list GINS-PC backups and their backup message. 
#@
#* by        : marcell.ferenc@cnam.fr or marcell.ferenc.uni@gmail.com
#*

##------------------------------------------------------------------------------
cnf=${0%/*}/gins_env.cnf
if [ ! -s $cnf ]; then
 echo "${0##*/}: error: <gins_env.cnf> file is missing"; exit 2
else
 bcp_dir=$( awk '$1=="BACKUP_DIRECTORY" { print $2; exit; }' $cnf )
 if [ -z "$bcp_dir" ]; then
  echo "${0##*/}: error: <bcp_dir> is not defined in <gins_env.cnf>"; exit 2; fi
fi
##------------------------------------------------------------------------------

bcps=( $( find $bcp_dir -maxdepth 1 -print | sort -n ) )

cnt=0

echo
for bcp in ${bcps[@]:1}; do
 cnt=$(( $cnt + 1 ))
 rdm=$bcp/README.txt
 printf "%4d - backup dir: <%s>\n" $cnt $bcp
 if [ -s $rdm ]; then printf "     * %s\n" "$(cat $rdm )"; fi
done
echo
