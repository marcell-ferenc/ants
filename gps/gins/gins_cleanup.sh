#!/bin/bash

#@
#@ USAGE     : gins_cleanup.sh <options>
#@
#@ OPTIONS   :
#@             -m <"message ...">       create a <README.txt> file during backup
#@             -h                       help
#@
#@ TASK      : clean and backup GINS-PC directories.
#@
#@ NOTE      : when -m option is not used an automatic message is generated.
#@
#* by        : marcell.ferenc@cnam.fr or marcell.ferenc.uni@gmail.com
#*

##------------------------------------------------------------------------------
cnf=${0%/*}/gins_env.cnf
if [ ! -s $cnf ]; then
 echo "${0##*/}: error: <gins_env.cnf> file is missing"; exit 2
else
 bcp_dir=$( awk '$1=="BACKUP_DIRECTORY" { print $2; exit; }' $cnf )
 if [ -z "$bcp_dir" ]; then
  echo "${0##*/}: error: <bcp_dir> is not defined in <gins_env.cnf>"; exit 2; fi
fi
##------------------------------------------------------------------------------

## source
source $HOME/bin/func/general.func

## list of options the program will accept
optstring=m:h

## interpret options
while getopts $optstring opt
do
 case $opt in
  m) msg="$OPTARG" ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## security question
echo; read -p "${0##*/}: are you sure to cleanup? " -n 1 -r; echo
case $REPLY in
 y|yes|Y|YES) ;;
           *) echo "answer: $REPLY"; exit 2 ;;
esac

date_str=$( date "+%Y%m%d_%H%M%S" )
bcp_id=$bcp_dir/bcp-$date_str
mkdir -p $bcp_id
if [ $? -ne 0 ]; then 
 echo "${0##*/}: error: no backup dir created <$bcp_id>"; exit 2; fi
rdm=$bcp_id/README.txt

## backup message
if [ -z "$msg" ]; then
 echo "backup-$date_str: user $USER: no backup message." > $rdm
else
 printf "%80.80s\n" "backup-$date_str: user $USER: $msg" > $rdm; fi

src_dirs=( $HOME/gin/batch/eqna \
$HOME/gin/batch/eqnbc \
$HOME/gin/batch/fic \
$HOME/gin/batch/graphique \
$HOME/gin/batch/horloges \
$HOME/gin/batch/listing \
$HOME/gin/batch/orbite \
$HOME/gin/batch/references \
$HOME/gin/batch/statistiques \
$HOME/dynamo/batch/listing \
$HOME/gin/data/directeur )

for dir in ${src_dirs[@]}; do
 #find $HOME/gin/batch/$dir -type f -delete #-print
 case $dir in
  *dynamo*listing) cmd=mv; bcp_path=$bcp_id/dynamo-${dir##*/} ;;
       *directeur) cmd=mv; bcp_path=$bcp_id/${dir##*/} ;;
                *) cmd=mv; bcp_path=$bcp_id/${dir##*/} ;;
 esac
 
 mkdir -p $bcp_path
 if [ ! -d "$bcp_path" ]; then
  echo "${0##*/}: error: no backup dir created <$bcp_path>"
  continue; fi
  
 echo "${0##*/}: backing up <$dir> to <$bcp_path>"
 find $dir -maxdepth 1 -type f -name '*' -exec /bin/$cmd -f {} $bcp_path \; \
 2> /dev/null
done
