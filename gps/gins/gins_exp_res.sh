#!/bin/bash

## compress xyz results
for ts in *fx*xyz; do tar -rvf gr2-storm-xyz.tar.gz $ts; done

## reject records with high sigma
ts_reject_sig.sh -x 0.05 -y 0.05 -z 0.05 -f "*fx*xyz" -v

## reject records with five times the stdev
ts_reject_std.sh -f "*cleaned*" -v

## coordinate transformation
for i in *cleaned*; do xyz2enu.sh -f $i -v; done

#ts_detrend.sh -f "*cleaned*enu" -v
for ts in *fx-cleaned-detrended.txt; do
 sta=$( echo ${file:0:4} | tr [:upper:] [:lower:] )
 /bin/mv -f $file $sta.grg
done

## remove intermediate files
/bin/rm -f *.enu *.res *.txt *.xyz

## compress results
for ts in *gr2; do tar -rvf gr2-storm.tar.gz $ts; done


