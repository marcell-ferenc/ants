#!/bin/bash

#@
#@ USAGE  : gins_plot_otl.sh -r <RINEX> <OPTIONS>
#@
#@ OPTIONS:
#@          -a            - auto_scale       [DEFAULT:                   no]
#@          -c <ref_otl>  - comparison       [DEFAULT:                   no]
#@          -d            - print_diff_val   [DEFAULT:                   no]
#@          -f            - force_rnx        [DEFAULT: use_nominal_sta_file]
#@          -o <otl>      - use_otl          [DEFAULT: use_nominal_otl_file]
#@          -t <title>    - title            [DEFAULT:        ssss_otl_plot]
#@          -h            - hep
#@          -v            - verbose_mode
#@
#@ TASK   : Plot OTL coefficients for STATION from GINS OTL file.
#@          First it will look for STATION in NOMINAL station file and in
#@          NOMINAL OTL file. If STATION is not found in NOMINAL then create
#@          both files using RINEX header data. If -s option is used then
#@          forced station file and forced otl file creation will be performed.
#@          
#@ NOTE   : Reimplemented and modified version of the script of Francois FUND
#@
#* by     : marcell.ferenc@cnam.fr
#*

## default variables
sh_sta=gins_station_info_rinex.sh
sta=$HOME/gin/data/stations/nominal
otl=$HOME/gin/data/charge/ocean/nominal
force_rnx=no
auto_scale=no
compare=no
awk=awk
diff=no
tides=( m2 s2 k1 o1 n2 p1 k2 q1 mf mm ssa ) #2n2 ) #l2 mu2 nu2 t2 )
tides=( M2 S2 K1 O1 N2 P1 K2 Q1 Mf Mm Ssa ) #2N2 ) #L2 Mu2 Nu2 T2 )
LC_NUMERIC=C

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/gps.func
source $HOME/bin/func/ts.func

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir .gmt*
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## list of options the program will accept
optstring=c:r:o:adfvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  c) ref=$OPTARG; if [ -s $ref ]; then compare=yes; fi ;;
  d) diff=yes ;;
  a) auto_scale=yes ;;
  f) force_rnx=yes ;;
  r) rnx=$OPTARG; rnx_name="${rnx##*/}" ;;
  o) otl=$OPTARG ;;
  t) title=$OPTARG; title=${title%.*}; title="${title##*/}"; title=${title// /_} ;;
  v) v=yes ;;
  h) usage $0 ; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## test rinex
if [ -z "$rnx" ]; then
 color_txt bold red "-r option is obligatory [rinex]"; exit 2
else
 rnx_inf=( $( isvalrinex $rnx ) )
 case ${rnx_inf[0]} in
  is_valid) if [ "$force_rnx" == "no" ]; then
             grep -i -m 1 ${rnx_inf[2]} $sta &> /dev/null
             if [ $? -ne 0 ]; then
              [ "$v" == "yes" ] && color_txt bold red "${rnx_inf[2]} is not in $sta"
              if [ ! -s $rnx ]; then
               color_txt bold red "rinex is missing [$rnx]"; exit 2
              else
               sta=$HOME/gin/data/stations/${rnx_name%?}s
               otl=$HOME/gin/data/charge/ocean/${rnx_name:0:4}.otl
               if [[ ! -s "$sta" || ! -s "$otl" ]]; then
                [ "$v" == "yes" ] && color_txt bold blue "create $sta and $otl"
                $sh_sta -r $rnx -t -f
               else
                [ "$v" == "yes" ] && color_txt bold green "$sta and $otl exist"; fi
              fi
             else
              [ "$v" == "yes" ] && color_txt bold green "${rnx_inf[2]} is in $sta"; fi    
            elif [ "$force_rnx" == "yes" ]; then
             if [ ! -s $rnx ]; then
              color_txt bold red "rinex is missing [$rnx]"; exit 2
             else
              sta=$HOME/gin/data/stations/${rnx_name%?}s
              otl=$HOME/gin/data/charge/ocean/${rnx_name:0:4}.otl
              [ "$v" == "yes" ] && color_txt bold blue "create $sta and $otl"
              $sh_sta -r $rnx -t -f; fi
            fi ;;
  no_valid) color_txt bold red "rinex name error [$rnx]"; exit 2 ;;
 esac
fi

## domes number
sta_inf=( $( grep -m 1 -i ${rnx_inf[2]} $sta ) )
domes=${sta_inf[0]}
if [ "${#domes}" -ne 5 ]; then color_txt bold red "domes error [$domes]"; exit 2; fi

## test otl
if [[ ! -s "$otl" || ! -s "$ref" ]]; then
 color_txt bold red "otl or ref_otl is missing [$otl or $ref]"; exit 2
else
 grep -m 1 -i $domes $otl &> /dev/null
 if [ $? -ne 0 ]; then
  color_txt bold red "${rnx_inf[2]} is not in $otl"; exit 2
 else
  [ "$v" == "yes" ] && color_txt bold green "${rnx_inf[2]} is in $otl"; fi
 if [ "$compare" == "yes" ]; then
  grep -m 1 -i $domes $ref &> /dev/null
  if [ $? -ne 0 ]; then
   color_txt bold red "${rnx_inf[2]} is not in $ref"; exit 2
  else
   [ "$v" == "yes" ] && color_txt bold green "${rnx_inf[2]} is in $ref"; fi
 fi
fi

## default settings
title=${title:-${rnx_inf[2]}_otl_plot}
ps=$title.ps

## all otl coefficients at each direction up, east and north (in place of 23 use NF)
$awk -F, -v c=1 '/'"$domes"'/ {count++;} count == c { for (i=2; i<=23; i+=2) printf (i==23) ? $i RS:$i " "; exit }' $otl > $tmp.otl-u-amp
$awk -F, -v c=2 '/'"$domes"'/ {count++;} count == c { for (i=2; i<=23; i+=2) printf (i==23) ? $i RS:$i " "; exit }' $otl > $tmp.otl-e-amp
$awk -F, -v c=3 '/'"$domes"'/ {count++;} count == c { for (i=2; i<=23; i+=2) printf (i==23) ? $i RS:$i " "; exit }' $otl > $tmp.otl-n-amp

if [ "$compare" == "yes" ]; then
 $awk -F, -v c=1 '/'"$domes"'/ {count++;} count == c { for (i=2; i<=23; i+=2) printf (i==23) ? $i RS:$i " "; exit }' $ref > $tmp.ref-u-amp
 $awk -F, -v c=2 '/'"$domes"'/ {count++;} count == c { for (i=2; i<=23; i+=2) printf (i==23) ? $i RS:$i " "; exit }' $ref > $tmp.ref-e-amp
 $awk -F, -v c=3 '/'"$domes"'/ {count++;} count == c { for (i=2; i<=23; i+=2) printf (i==23) ? $i RS:$i " "; exit }' $ref > $tmp.ref-n-amp
fi

## gmt settigns
gmt_settings()
{
 #gmtset FRAME_WIDTH 0.2c
 gmtset FRAME_PEN thin
 gmtset CHAR_ENCODING Standard+
 gmtset DOTS_PR_INCH 600
 gmtset HEADER_FONT Helvetica
 gmtset HEADER_FONT_SIZE 12
 gmtset HEADER_OFFSET -0.5c
 gmtset ANNOT_FONT_PRIMARY Helvetica
 gmtset ANNOT_FONT_SIZE_PRIMARY 10
 gmtset ANNOT_OFFSET_PRIMARY 0.1c
 gmtset ANNOT_FONT_SIZE_SECONDARY 6
 gmtset ANNOT_OFFSET_SECONDARY 0.2c
 gmtset TICK_LENGTH 0.1c
 gmtset LABEL_FONT_SIZE 10
 gmtset GRID_PEN_PRIMARY 0.05p,lightgray
 gmtset GRID_PEN_SECONDARY 0.05p,black
 gmtset INPUT_DATE_FORMAT yyyy-mm-dd
 gmtset PAGE_ORIENTATION portrait
 gmtset PAPER_MEDIA A4
}

gmt_settings

## gmt variables
JxJy_ratio=7.5 #$( gmtmath -Q 16 9 DIV = ) #
Jx=12
Jy=$( gmtmath -Q $Jx $JxJy_ratio DIV = )
J="-JX${Jx}c/${Jy}c"
Y_ini=21

if [ "$diff" == "yes" ]; then space=1.35; else space=1.2; fi
dY=$( gmtmath -Q $Jy $space MUL = )
X=-Xa2c
x_min=0
x_max=$(( ${#tides[@]} + 1 ))
y_min=0

## auto_scale
if [ "$auto_scale" == "no" ]; then
 [ "$v" == "yes" ] && color_txt bold blue "automatic scale off"
 y_max_up=6
 y_max_hz=1.5
 B=( "" -B0g0/a1g0.5Wesn -B0g0/0.5g0.25Wesn -B0g0/0.5g0.25Wesn )
elif [ "$auto_scale" == "yes" ]; then
 [ "$v" == "yes" ] && color_txt bold blue "automatic scale on"
 y_max_up=$( printf "%s\n" $( cat $tmp.otl-u-amp $tmp.ref-u-amp 2> /dev/null \
 | gmtmath STDIN -Ca -T UPPER -Sl = ) | gmtmath STDIN -Ca -T UPPER -Sl \
 1.2 MUL --D_FORMAT=%.2f = )
 y_max_hz=$( printf "%s\n" $( cat $tmp.otl-e-amp $tmp.ref-e-amp \
 $tmp.otl-n-amp $tmp.ref-n-amp 2> /dev/null | gmtmath STDIN -Ca -T UPPER -Sl \
 1.2 MUL --D_FORMAT=%.2f = ) \
 | gmtmath STDIN -Ca -T UPPER -Sl = )
 up_annot=$( gmtmath -Q $y_max_up 0.2 MUL --D_FORMAT=%.2f = )
 up_grid=$( gmtmath -Q $y_max_up 0.1 MUL --D_FORMAT=%.2f = )
 hz_annot=$( gmtmath -Q $y_max_hz 0.2 MUL --D_FORMAT=%.2f = )
 hz_grid=$( gmtmath -Q $y_max_hz 0.1 MUL --D_FORMAT=%.2f = )
 B=( "" -B0g0/a${up_annot}g${up_grid}Wesn -B0g0/a${hz_annot}g${hz_grid}Wesn -B0g0/a${hz_annot}g${hz_grid}Wesn )
fi

Y=( "" -Ya${Y_ini}c -Ya$( gmtmath -Q $Y_ini 1 $dY MUL SUB = )c -Ya$( gmtmath -Q $Y_ini 2 $dY MUL SUB = )c )
R=( "" -R$x_min/$x_max/$y_min/$y_max_up -R$x_min/$x_max/$y_min/$y_max_hz -R$x_min/$x_max/$y_min/$y_max_hz )

## initialize plot legend
echo N 2 > $tmp.legend

## plot
i=0
for comp in u e n
do

 i=$(( $i + 1 ))
 
 case $comp in
  u) psbasemap ${R[$i]} $J ${B[$i]}:"up [cm]"::."@;blue;${rnx_inf[1]}@;;": $X ${Y[$i]} -K -P --D_FORMAT=%.2f > $ps ;;
  e) psbasemap ${R[$i]} $J ${B[$i]}:"east [cm]": $X ${Y[$i]} -K -O -P --D_FORMAT=%.2f >> $ps ;;
  n) psbasemap ${R[$i]} $J ${B[$i]}:"north [cm]": $X ${Y[$i]} -K -O -P --D_FORMAT=%.2f >> $ps ;;
 esac
 
 if [[ "$compare" == "yes" && "$diff" == "yes" ]]; then
  [ "$v" == "yes" ] && color_txt bold blue "print difference for each wave"
  ## plot difference
  if [ "$comp" == "n" ]; then D=-D-0.33/-0.5; else D=-D-0.33/-0.15; fi
  {
   echo 0 0 10 0 0 TL "diff:"
   printf "%s\n" $( gmtmath $tmp.ref-${comp}-amp $tmp.otl-${comp}-amp SUB ABS -Ca -T --D_FORMAT=%.2f = ) \
   | awk '{ print NR,0,10,"0 0 TL",$1 }'
  } | pstext ${R[$i]} $J -W255 -G0 -C0.005/0.005 $D $X ${Y[$i]} -N -K -O -P >> $ps
  echo ${rnx_inf[2]} $( gmtmath $tmp.ref-${comp}-amp $tmp.otl-${comp}-amp SUB ABS -Ca -T --D_FORMAT=%.2f = ) >> sch-otl-$comp.diff
  gmtmath $tmp.ref-${comp}-amp $tmp.otl-${comp}-amp SUB ABS -Ca -T --D_FORMAT=%.2f = | awk '{ print $1,"&",$2 }' > $tmp.m2s2diff-$comp
 fi

 for (( tide=0; tide<"${#tides[@]}"; tide++ ))
 do
  #[ "$v" == "yes" ] && color_txt bold blue "$(( $tide + 1 )) - ${tides[$tide]} - $comp"

  ## plot tide values
  if [ "$compare" == "yes" ]; then
   [ "$v" == "yes" ] && color_txt bold blue "plot ${tides[$tide]} $comp amplitude from $ref and $otl"
   $awk -v c=$(( $tide + 1 )) '{ print c,$c }' $tmp.ref-${comp}-amp \
   | psxy ${R[$i]} $J -Sb0.4 -Wred -Gred $X ${Y[$i]} -D-0.125/0 -K -O -P >> $ps
   $awk -v c=$(( $tide + 1 )) '{ print c,$c }' $tmp.otl-${comp}-amp \
   | psxy ${R[$i]} $J -Sb0.4 -Wblack -Gblack $X ${Y[$i]} -D0.125/0 -K -O -P >> $ps
  else
   [ "$v" == "yes" ] && color_txt bold blue "plot ${tides[$tide]} $comp amplitude from $otl"
   $awk -v c=$(( $tide + 1 )) '{ print c,$c }' $tmp.otl-${comp}-amp \
   | psxy ${R[$i]} $J -Sb1.0 -W0 -G0 $X ${Y[$i]} -K -O -P >> $ps
  fi

  ## only annotate x axis at the bottom plot
  if [ "$comp" == "n" ]; then
   $awk -v c=$(( $tide + 1 )) -v t=${tides[$tide]} '{ print c,0,10,"0 0 TL",t }' $tmp.otl-${comp}-amp \
   | pstext ${R[$i]} $J -W255 -G0 -C0.005/0.005 -D-0.33/-0.125 $X ${Y[$i]} -N -K -O -P >> $ps; fi
   
 done
done

## complete legend
if [ "$compare" == "yes" ]; then
 X="${X#-Xa}"; Y=${Y[$i]}; Y="${Y#-Ya}"
 if [ "$diff" == "yes" ]; then Y=$( gmtmath -Q "${Y%c}" 0.75 SUB = )
 else Y=$( gmtmath -Q "${Y%c}" 0.5 SUB = ); fi
 {
  echo "S 0.15 s 0.125 red 0.25p 0.35 @;red;${ref##*/}@;;"
  echo "S 0.15 s 0.125 black 0.25p 0.35 @;black;${otl##*/}@;;"
 } >> $tmp.legend
 pslegend $tmp.legend ${R[$i]} $J -Dx$X/$Y/${Jx}c/2c/TL -K -O -P >> $ps
fi

## close the ps file at the end of the script
psxy -J -R -T -O >> $ps

## create png
ps2raster -A -E600 -Tg $ps

###################### to REMOVE

### phi: 2; lambda: 1
coords=( $( echo ${sta_inf[2]} ${sta_inf[3]} ${sta_inf[5]} ${sta_inf[7]} | xyz2flh_iers2010.bin ) )

sta_cdist=$HOME/DATA/grd/sta-cdist.txt
cdist_txt=$HOME/DATA/grd/dist2coast.txt
cdist_grd=$HOME/DATA/grd/dist2coast.grd

## create grid from table
if [ ! -s $cdist_grd ]; then xyz2grd $cdist_txt -G$cdist_grd -Rd -I25m=/25m=; fi

## sample the distance grid at the stations and use the distance [km]
echo ${coords[1]} ${coords[2]} ${coords[0]} > $tmp.xyfile
grdtrack $tmp.xyfile -G$cdist_grd -Ql > $tmp.cdist
cat $tmp.cdist >> $sta_cdist
cdist=$( awk '{ printf "%d",$4 }' $tmp.cdist )

for comp in u e n
do
 echo ${rnx_inf[1]} ${coords[1]} ${coords[2]} $( awk '{ print $1 }' $tmp.m2s2diff-$comp ) >> map_M2_diff_$comp.txt
 echo ${rnx_inf[1]} ${coords[1]} ${coords[2]} $( awk '{ print $3 }' $tmp.m2s2diff-$comp ) >> map_S2_diff_$comp.txt

 echo ${rnx_inf[1]} $cdist $( awk '{ print $1 }' $tmp.ref-${comp}-amp ) >> fig_M2_sch_$comp.txt
 echo ${rnx_inf[1]} $cdist $( awk '{ print $2 }' $tmp.ref-${comp}-amp ) >> fig_S2_sch_$comp.txt

 echo ${rnx_inf[1]} $cdist $( awk '{ print $1 }' $tmp.otl-${comp}-amp ) >> fig_M2_otl_$comp.txt
 echo ${rnx_inf[1]} $cdist $( awk '{ print $2 }' $tmp.otl-${comp}-amp ) >> fig_S2_otl_$comp.txt
done

if [ "$compare" == "yes" ]; then
 tides=( M2 S2 K1 O1 N2 P1 K2 Q1 Mf )
 last_tide="\textit{Mm} and \textit{Ssa}"
 {
  echo
  echo "\begin{figure}[H]"
  echo " \centering"
  echo " \includegraphics[width=14cm]{fig/${ps//.ps/.png}}"
  echo " \caption{Comparison of 11 tidal waves ($( printf "\\\\textit{%s}, " ${tides[@]} )$last_tide) from \textit{nominal\_FES2004} (black) and \textit{Scherneck\_FES2004} files (red) at \textit{${rnx_inf[1]}} station which is \$\approx{\ ${cdist}\ km}\$ far from the nearest coast.}"
  echo " \label{fig:${rnx_inf[2]}-otl}"
  echo "\end{figure}"
  echo
 } >> latex-figures.tex
fi

if [ $(awk '$1 > 0.2 { print $0 }' $tmp.m2s2diff-u | wc -l ) -eq 1 ]; then
 echo \\textcolor{new}{${rnx_inf[1]}} \& ${cdist:----} \& $( awk '{ print "$\\textcolor{new}{\\rm{"$1"}}$",$2,$3 }' $tmp.m2s2diff-u ) \& $( cat $tmp.m2s2diff-e ) \& $( cat $tmp.m2s2diff-n ) \\\\ >> latex-table-cells.tex
else
 echo ${rnx_inf[1]} \& ${cdist:----} \& $( cat $tmp.m2s2diff-u ) \& $( cat $tmp.m2s2diff-e ) \& $( cat $tmp.m2s2diff-n ) \\\\ >> latex-table-cells.tex; fi
