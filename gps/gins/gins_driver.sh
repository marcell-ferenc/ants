#!/bin/bash

#@
#@ USAGE     : gins_ppp_driver.sh -o <rinex/prairie> <options>
#@
#@ OPTIONS   :
#@             -d <dir_path>     - set output directory       [default: $HOME/gin/data/directeur]
#@             -c <cutoff_angle> - set cutoff angle           [default: 10]
#@             -p <prob_sat>     - set problematic satellite  [default: gnss_sat_problem.lst]
#@             -s <station_info> - set station info          
#@             -a <ac_product>   - set analysis center data   [default: GR2]
#@             -t <tropo_param>  - set troposphere parameters
#@             -i <iono_param>   -
#@             -l <load_param>   -
#@             -v                - verbose mode           [default:                     no]
#@             -h                - help                   [default:                     no]
#@
#@ TASK      : Create driver file for GINS-PC.
#@
#@             output name: SSSS_DDu_AAA_JJJJJ_cXXtXXugXXionXigsYYmm
#@
#* by        : marcell.ferenc@cnam.fr
#*

## scriptname
scrn=${0##*/}
scrn=${scrn%.sh}

## temporary filename
tmp=$scrn.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean(){ /bin/rm -rf $tmp_dir; trap EXIT; cd; exit; }

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/gins.func
source $HOME/bin/func/gps.func
source $HOME/bin/func/ts.func

## help
case "$#" in 0) usage $0; exit ;; esac

## default variable
def_satprob=$HOME/gin/data/HorlogesAnom0.log # problematic satellites
prob_ep_nb=10                            # threshold of problematic epoch number
GINS_otl=otl_rgp_fes2012.txt
#load_rgp_fes2012_felix.txt
#load_rgp_fes2012.txt #gins_otl_TOULOUSE.dat #nominal
template=GINS_PPP.ref
acdata=GR2
cutoff=10
pos_dt=6h
pos_ch=00060000
tro_dt=1h
tro_ch="   24"
tro_gr=0
ion2nd=no
ionstr=ion1
f_stainfo=no
v=no
str=xxxxxxxxxx
otl_model=fes2004
cmc=no #yes
atml=no
s1s2=no
log=$scrn.log
max_path_length=52

