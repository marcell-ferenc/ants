#!/bin/bash

#@
#@ USAGE     : fich_dir_ppp.sh -r [RINEX] or -p [PRAIRIE] | [OPTIONS]
#@
#@ OPTIONS   :
#@             -c [CUTOFF]         [                                        ==> DEFAULT:                  10]
#@             -i [SAMPLING_INT]   [1s-24h                                  ==> DEFAULT:                  6h]
#@             -a [ORB_CLK_DATA]   [GRG,IGS                                 ==> DEFAULT:                 GRG]
#@             -t [TROPO_SAMP_INT] [5m,30m,1h,2h                            ==> DEFAULT:                  1h]
#@             -g [NB_TROPO_GRAD]  [0-24                                    ==> DEFAULT:                   2]                 
#@             -n [INFO_6_CHAR]    [e.g.: abcdef                            ==> DEFAULT: t{tropo}g{gradient}]
#@             -s                  [create station info and ocean load file ==> DEFAULT:            official]
#@
#@ TASK      : Create driver file for GINS-PC and output a list of driver files
#@
#@ CREATE    : 
#@             fich_dir_PPP.list
#@
#* by        : marcell.ferenc@cnam.fr
#*

##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
## D E F A U L T   S E T T I N G S 
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
cutoff=10
samp_t=06h; samp=00060000
ac_data=GRG
tropo_int="   24"; tropo_str=1h
#tropo_int="   12"; tropo_str=2h
tropo_grad=2
info_files=no
v=no
##* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/gins.func
source $HOME/bin/func/gps.func
source $HOME/bin/func/ts.func

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## list of options the program will accept
optstring=c:i:a:t:g:n:p:r:s:hxv

## interpret options
while getopts $optstring opt
do
 case $opt in
  c) cutoff=$OPTARG ;;
  g) tropo_grad=$OPTARG ;;
  n) inf_char=$OPTARG ;;
  p) prairie=$OPTARG ;;
  s) site=$OPTARG ;;
  r) rinex=$OPTARG ;;
  a) ac_data=$OPTARG ;;
  i) samp_t=$OPTARG ;;
  t) tropo_int=$OPTARG ;;
  x) iono_2nd=yes ;;
  s) info_files=yes ;;
  v) v=yes ;;
  h) usage $0 ; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## test rinex
if [ -z $rinex ]
then
 color_txt bold red "-r option is obligatory [rinex]"; exit 2
else
 if [ -s $rinex ]
 then
  rnx_path=${rinex%/*}; rnx_name=${rinex##*/}
  case $rnx_name in
   ???????0.??o) site_info=( $( isvalrinex $rnx_name ) ); st_upper=${site_info[1]}; st=${site_info[2]}
                 y4=${site_info[3]}; y2=${site_info[5]}; doy=${site_info[4]};;
              *) color_txt bold red "not accepted rinex name [$rinex]"; exit 2 ;;
  esac
 else
  color_txt bold red "rinex not found [$rinex]"; exit 2
 fi
fi

## test options
## cutoff
case $cutoff in
 [1-9]|[1-9][0-9]) ;;
                *) color_txt bold red "not a valid cutoff value [$cutoff]"; exit 2 ;;
esac
printf -v cut_angl "%2d" "$cutoff"
[ "$v" == "yes" ] && echo "cutoff angle         ==> $cutoff"
cut_angl="$cut_angl   $cut_angl"
printf -v cut_angl "%7.7s" "$cut_angl"

## sampling_int
case $samp_t in
 *s) printf -v samp_t "%02d" "${samp_t%?}"; samp=000000${samp_t}; samp_t=${samp_t}s ;; 
 *m) printf -v samp_t "%02d" "${samp_t%?}"; samp=0000${samp_t}00; samp_t=${samp_t}m ;;
 *h) printf -v samp_t "%02d" "${samp_t%?}"; samp=00${samp_t}0000; samp_t=${samp_t}h ;;
  *) color_txt bold red "not a valid sampling interval [$samp_t]"; exit 2 ;;
esac
[ "$v" == "yes" ] && echo "sampling time        ==> $samp_t"

## orbit and clock product
case $ac_data in
 GRG|IGS|BERN) printf -v clk_data "%-55.55s" "horloges/$ac_data/defaut"
               printf -v orb_data "%-55.55s" "orbites/$ac_data/defaut" ;;
 *) color_txt bold red "not supported analyzis center [$ac_data]"; exit 2 ;;
esac
[ "$v" == "yes" ] && echo "orbit and clock data ==> $ac_data"

## tropo_sampling_int
case $tropo_int in
  5m) tropo_int="  288"; tropo_str=5m ;;  ##  5 minutes
 30m) tropo_int="   48"; tropo_str=30m ;;  ## 30 minutes
  1h) tropo_int="   24"; tropo_str=1h ;;  ##  1 hour
  2h) tropo_int="   12"; tropo_str=2h ;;  ##  2 hours
   *) color_txt bold red "not a valid troposphere sampling interval [$trop_int]"; exit 2 ;;
esac
[ "$v" == "yes" ] && echo "troposphere sampling ==> $tropo_str"

## troposphere gradient
case $tropo_grad in
 [0-9]|[1-9][0-9]) printf -v tropo_grad_nb "%2d" "$tropo_grad" ;;
 *) color_txt bold red "not a valid troposphere gradient sampling [$trop_grad]"; exit 2 ;;
esac
[ "$v" == "yes" ] && echo "troposphere gradient ==> $tropo_grad"

## information characters
if [ "$iono" == "yes" ]; then iono_str=i; else iono_str=x; fi

if [ -z "$inf_char" ]
then
 printf -v inf_char "%10.10s" "c${cutoff}t${tropo_str}g${tropo_grad}${iono_str}xxxxx"
else
 if [ "${#inf_char}" -ne 10 ]
 then
  printf -v inf_char "%10.10s" "c${cutoff}t${tropo_str}g${tropo_grad}${iono_str}xxxxx"
 fi
fi
[ "$v" == "yes" ] && echo "information string   ==> $inf_char"

## information
color_txt bold blue "$scriptname"
 
start_jul50=$( jjul $doy $y4 | awk '/JUL50/ { print $3 }' )
end_jul50=$(( $start_jul50 + 1 ))

## template file
if [ "$iono" == "Y" ]
then
 template=GINS_PPP.ref.ion
else
 template=GINS_PPP.ref
fi

## path to the fichier directeur
FICH_DIR_PATH=$HOME/gin/data/directeur
FICH_DIR_NAME=$FICH_DIR_PATH/${samp_t}_${ac_data}_PPP_${st_upper}_${start_jul50}_${inf_char}

cp $HOME/bin/gins/templates/$template $FICH_DIR_NAME
 
## information plotted
color_txt bold green "$FICH_DIR_NAME" 

printf -v obs_file "%-55.55s" ".temp.${rinex#$HOME/}" ## rinex file

date_str_ref="JUL50    19.000000 JUL51    18.000000"

case $samp_t in
 24h) date_str="JUL50    19.000000 JUL51    19.000000"
      change "$date_str_ref" "$date_str" $FICH_DIR_NAME ;;
esac

## after this date we changed to macromodels
if [ $start_jul50 -ge 22575 ]
then
 printf -v mac_file "%-55.55s" "macromodeles/macromodeles.xml"
## before this date we used old macromodels
elif [ $start_jul50 -lt 22575 ]
then
 printf -v mac_file "%-55.55s" ".temp.gin/archives/LISTE_GNSS_ATTN.xml"
fi
 
## after this date we changed to ITRF2008 frame
if [ $start_jul50 -ge 22386 ]
then
 
 printf -v ant_file "%-55.55s" "antex/igs08.atx"
 case $info_files in
  no) printf -v sta_file "%-55.55s" "stations/nominal"
      printf -v otl_file "%-55.55s" "charge/ocean/nominal" ;;
 esac
  
## before this date we used the ITRF2005 frame
elif [ $start_jul50 -lt 22386 ] #$end_jul50
then
 
 printf -v ant_file "%-55.55s" "antex/igs05.atx"
 case $info_files in
  no) printf -v sta_file "%-55.55s" "stations/nominal"
      printf -v otl_file "%-55.55s" "charge/ocean/load_fes2004_itrf2005" ;;
 esac
    
fi 

case $info_files in
 yes) ## create station file if necessary
      sta_file=${st_upper}_${start_jul50}_RINEX.inf
      otl_file=$st.otl
 
      if [ ! -e $HOME/gin/data/stations/$sta_file ]
      then
       cd $rnx_dir
       create_station_file.bash $rinex
       cd $tmp_pwd
      fi 
            
      ## create ocean load file if necessary
      if [[ ( -e $HOME/gin/data/stations/$sta_file ) && ( ! -e $HOME/gin/charge/ocean/$otl_file ) ]]
      then
          
       {
        echo $HOME/gin/data/stations/$sta_file
        echo $HOME/gin/data/charge/ocean/$otl_file
       } > $tmp
            
       exe_loadoce < $tmp #&>/dev/null
            
      fi

      printf -v sta_file "%-55.55s" ".temp.gin/data/stations/$sta_file"
      printf -v otl_file "%-55.55s" ".temp.gin/data/charge/ocean/$otl_file" ;;
esac

printf -v val_apri "%-55.55s" ".temp.bin/gins/templates/apriori_PPP.ref"
printf -v pra_opts "%-55.55s" ".temp.bin/gins/templates/options_prairie.dat"
printf -v atm_file "%-55.55s" "charge/atmosphere/defaut"

## ****** TROPO INFO ****** ##

gmfgpt="10715$tropo_int"
 
printf -v tro_info "%10.10s" "$gmfgpt"
 
## **** TROPO INFO END **** ##
{
 change "CHANGE_STATION_FILE                                    " "$sta_file" $FICH_DIR_NAME
 change "CHANGE_ANTENNE_FILE                                    " "$ant_file" $FICH_DIR_NAME
 change "CHANGE_MACROMODEL                                      " "$mac_file" $FICH_DIR_NAME
 change "CHANGE_CLOCK                                           " "$clk_data" $FICH_DIR_NAME
 change "CHANGE_OCEAN_LOAD_FILE                                 " "$otl_file" $FICH_DIR_NAME
 change "CHANGE_ATMOS_LOAD_FILE                                 " "$atm_file" $FICH_DIR_NAME
 change "CHANGE_PRAIRIE_OPTION                                  " "$pra_opts" $FICH_DIR_NAME
 change "CHANGE_A_PRIORI_VALUES                                 " "$val_apri" $FICH_DIR_NAME
 change "CHANGE_ORBIT                                           " "$orb_data" $FICH_DIR_NAME
 change "CHANGE_MEASUREMENT_FILE                                " "$obs_file" $FICH_DIR_NAME
 change "CHANGE_TRO"                                              "$tro_info" $FICH_DIR_NAME
 change "CUT_ANG"                                                 "$cut_angl" $FICH_DIR_NAME
 change "JUL50"                                                   "$start_jul50" $FICH_DIR_NAME
 change "JUL51"                                                   "$end_jul50" $FICH_DIR_NAME
 change "JJHHMMSS"                                                "$samp" $FICH_DIR_NAME
 change "SSSS"                                                    "$st_upper" $FICH_DIR_NAME
} 2>> /dev/null

## gradient at LFREE
case $tropo_grad in
  [1-9]|[1-9][0-9]) printf "%17.17s %68.68sLFREE\n" "GRADIENTS_TROPO$tropo_grad_nb" " " >> $FICH_DIR_NAME ;;
esac

#printf "%s\n" "${FICH_DIR_NAME#$HOME/gin/data/directeur/}"

done
