#!/bin/bash

#@
#@ USAGE  : gps_data.sh -w [WRK_DIR] -s [STATION] -b [YYYY-MM-DD] -e [YYYY-MM-DD]
#@                                     or
#@          gps_data.sh              -s [STATION] -b [YYYY-MM-DD] -e [YYYY-MM-DD]
#@
#@ OPTIONS: -h - help
#@          -v - verbose mode
#@          -d - download only
#@
#@ TASK   : Download daily RINEX observation files and uncompress them.
#@          Then run cc2noncc program for necessary p1-c1 code correction.
#@          Files will be in $HOME/[WORK_DIR]/rinex directory.
#@
#@ NOTE   : If "-w" option is not used the rinex files will only be downloaded to the storage place
#@
#@ CREATE : 
#@          directory: 
#@                     wrk_dir=$HOME/[WRK_DIR]
#@                     rnx_dir=$wrk_dir/rinex
#@                     tmp_dir=$wrk_dir/tmp
#@                     lst_dir=$wrk_dir/lst
#@                     log_dir=$wrk_dir/log
#@                     dam_dir=$wrk_dir/damaged_rinex
#@
#@          information files: 
#@                     rnx_lst=$tmp_dir/rinex.list
#@                     sta_lst=$tmp_dir/station.list
#@
#* by     : marcell.ferenc@cnam.fr
#*

## TO BE VERIFIED START
## p1c1 bias file for P1-C1 code correction for receivers which need it
P1C1=$HOME/gin/archives/p1c1bias.2000p
## TO BE VERIFIED END

## source functions
. $HOME/bin/func/general.func
. $HOME/bin/func/gps.func
. $HOME/bin/func/ts.func

## start from $HOME, built wrk_dir here later on
cd

## test P1C1 file
if [ ! -s $P1C1 ]; then color_txt bold red "missing P1C1 file [$P1C1]"; exit 2; fi

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf ${tmp_dir}*
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi
 
## default variable
download_only="no"
v="no"
bar="===================="
bar=$bar$bar$bar

## list of options the program will accept
optstring=b:e:w:s:dvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  d) download_only="yes" ;;
  b) beg_str=$OPTARG ;;
  e) end_str=$OPTARG ;;
  w) wrk_dir=$OPTARG ;;
  s) stations=( $OPTARG ) ;;
  v) v=yes ;;
  h) usage $0 ; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## set values
## wrk_dir
if [[ -z "$wrk_dir" && "$download_only" == "yes" ]]
then
 [ "$v" == "yes" ] && echo "download only: $download_only"
elif [[ -n "$wrk_dir" && "$download_only" == "yes" ]]
then
 [ "$v" == "yes" ] && echo "download only: $download_only"
elif [[ -z "$wrk_dir" && "$download_only" == "no" ]]
then
 download_only=yes; [ "$v" == "yes" ] && echo "download only: $download_only"
elif [[ -n "$wrk_dir" && "$download_only" == "no" ]]
then
 [ "$v" == "yes" ] && echo "download only: $download_only"
 wrk_dir=${wrk_dir#$HOME/}
 wrk_dir=$HOME/wrk/${wrk_dir:0:10}-$( date "+%Y%m%d" )
 wrk_dir=${wrk_dir:0:40}
 rnx_dir=$wrk_dir/rinex
 lst_dir=$wrk_dir/lst
 log_dir=$wrk_dir/log
 dam_dir=$wrk_dir/damaged_rinex
 slp_time=1s
fi

## test stations
if [ -z "$stations" ]
then
 color_txt bold red "-s option is obligatory [station_name or a station_list_file]"; exit 2
elif [ "${#stations}" -eq 1 ]
then
 ## stations is a file
 if [ -s $stations ]
 then
  [ "$v" == "yes" ] && color_txt bold blue "station_list_file is provided: $stations"
  for station in $( cat $stations )
  do
   info_station=( $( isvalstatname $station ) )
   case ${info_station[0]} in
    is_valid) station_list_uppercase+=( ${info_station[1]} ); station_list_lowercase+=( ${info_station[2]} ) ;;
    no_valid) no_valid+=( $station ) ;;
   esac
  done
 ## stations is a station_name (not a file)
 else
  info_station=( $( isvalstatname $stations ) )
  case ${info_station[0]} in
   is_valid) station_list_uppercase+=( ${info_station[1]} ); station_list_lowercase+=( ${info_station[2]} ) ;;
   no_valid) no_valid+=( $stations ) ;;
  esac
 fi
## stations is an array with multiple items
elif [ "${#stations}" -gt 1 ]
then
 ## test stations_item
 for stations_item in ${stations[@]}
 do
  ## stations_item is a file
  if [ -s $stations_item ]
  then
   [ "$v" == "yes" ] && color_txt bold blue "station_list_file is provided: $stations_item"
   for station in $( cat $stations_item )
    do
    info_station=( $( isvalstatname $station ) )
    case ${info_station[0]} in
     is_valid) station_list_uppercase+=( ${info_station[1]} ); station_list_lowercase+=( ${info_station[2]} ) ;;
     no_valid) no_valid+=( $station ) ;;
    esac
   done
  ## stations_item is a station_name (not a file)
  else
   info_station=( $( isvalstatname $stations_item ) )
   case ${info_station[0]} in
    is_valid) station_list_uppercase+=( ${info_station[1]} ); station_list_lowercase+=( ${info_station[2]} ) ;;
    no_valid) no_valid+=( $stations_item ) ;;
   esac
  fi
 done
fi

## no accepted station
if [ "${#station_list_uppercase[@]}" -eq 0 ]; then color_txt bold red "there is no accepted station name!"; exit 2; fi

## sort station names alphabetically if they were not like that on the stdin
station_list_uppercase=( $( printf "%s\n" ${station_list_uppercase[@]} | sort ) )
station_list_lowercase=( $( printf "%s\n" ${station_list_lowercase[@]} | sort ) )

## test begin_date_str and end_date_str
if [[ -z "$beg_str" || -z "$end_str" ]]
then
 color_txt bold red "-b and -e options are obligatory [begin_date and end_date]"; exit 2
else
 info_begin=( $( isdate $beg_str ) ); info_end=( $( isdate $end_str ) )
 if [[ "${info_begin[0]}" != "is_date_str" || "${info_end[0]}" != "is_date_str" ]]
 then
  color_txt bold red "not valid date [begin_date: $beg_str or end_date: $end_str]"; exit 2
 fi
fi

if [[ -n "$wrk_dir" && "$download_only" == "no" ]]
then
 [ "$v" == "yes" ] && echo "creating work directory and subdirectories: $wrk_dir"
 ## create the work environment
 mkdir -p $wrk_dir $rnx_dir $lst_dir $log_dir $dam_dir

 ## we will work in the rinex directory inside our work directory
 cd $rnx_dir

 rnx_lst=$lst_dir/rinex.list
 status_ref=$lst_dir/status.ref
 station_list=$lst_dir/station.list

 ## delete information files before start
 /bin/rm -f $rnx_lst $mis_rnx_lst $status_ref 2>/dev/null
 /bin/rm -f $ccy_lst $ccn_list 2>/dev/null
 
 ## get_rinx function options (in case of uncompress "-u" and cc2noncc "-c")
 get_rnx_options="-u -c"
fi

start=$beg_str
end=$( date --date="$end_str +1 day" "+%Y-%m-%d" ) ## +1 day because of the while loop
ref_epoch_count=0

## initial count of desired number of epoch
while [ "$start" != "$end" ]
do
 ref_epoch_count=$(( $ref_epoch_count + 1 ))
 start=$( date --date="$start +1 day" "+%Y-%m-%d" )
done

## number of desired stations (accepted station names)
ref_station_count="${#station_list_lowercase[@]}"

## desired number of rinex
ref_rinex_count=$(( $ref_epoch_count * $ref_station_count ))

[ "$v" == "yes" ] && echo $ref_epoch_count x $ref_station_count = $ref_rinex_count

all_rinex_count=0
all_rinex_percentage=0
all_missing_rinex_count=0

rinex_count=0
rinex_percentage=0
missing_rinex_count=0

## do what we want: download, uncompress, cc2noncc
for (( i=0; i<"${#station_list_uppercase[@]}"; i++ ))
do
 echo ${station_list_lowercase[$i]} >> $tmp.station.list
 start=$beg_str
 while [ "$start" != "$end" ]
 do
  date_string=( $( date --date="$start" "+%y %j" ) )
  get_rnx -s ${station_list_lowercase[$i]} -d $start $get_rnx_options &> /dev/null
  get_rnx_exit_code=$( echo $? )
  if [ $get_rnx_exit_code -eq 0 ]
  then
   all_rinex_count=$(( $all_rinex_count + 1 ))
   rinex_count=$(( $rinex_count + 1 ))
   if [[ -n "$wrk_dir" && "$download_only" == "no" ]]
   then
    echo $rnx_dir/${station_list_lowercase[$i]}${date_string[1]}0.${date_string[0]}o >> $tmp.rinex.list
   else
    echo ${station_list_lowercase[$i]}${date_string[1]}0.${date_string[0]}o >> $tmp.rinex.list
   fi
  else
   all_missing_rinex_count=$(( $all_missing_rinex_count + 1 ))
   missing_rinex_count=$(( $missing_rinex_count + 1 ))
  fi  
  start=$( date --date="$start +1 day" "+%Y-%m-%d" )
 done
 printf -v rinex_percentage "%6.2f" "$( echo $rinex_count $ref_epoch_count | awk '{ print ($1/$2)*100 }' )"
 printf "%3d %4.4s %18d %6.2f%s\n" $(( $i + 1 )) ${station_list_lowercase[$i]} $rinex_count  $rinex_percentage % >> $tmp.status.ref
 rinex_count=0; missing_rinex_count=0
done

printf -v all_rinex_percentage "%6.2f" "$( echo $all_rinex_count $ref_rinex_count | awk '{ print ($1/$2)*100 }' )"
[ "$v" == "yes" ] && color_txt bold blue "$all_rinex_count of $ref_rinex_count data ---> ${all_rinex_percentage}%"

## information
{
 printf "%35.35s\n" "$bar"
 printf "%-18.18s%9d %6.2f%s\n" "TOTAL" "$all_rinex_count" $all_rinex_percentage %
 printf "%-18.18s%9d\n" "stations  (wanted)" "$ref_station_count" 
 printf "%-18.18s%9d\n" "duration  (# days)" "$ref_epoch_count" 
 printf "%-18.18s%9d\n" "reference (ideal)" "$ref_rinex_count" 
 printf "%-18.18s%9d\n" "exist     (# rnx)" "$all_rinex_count"
 printf "%-18.18s%9d\n" "missing   (# rnx)" "$all_missing_rinex_count"
} >> $tmp.status.ref

if [[ -n "$wrk_dir" && "$download_only" == "no" ]]
then
 /bin/mv -f $tmp.status.ref $status_ref
 /bin/mv -f $tmp.station.list $station_list
 /bin/mv -f $tmp.rinex.list $rnx_lst
 echo; cat $status_ref; echo
else
 /bin/mv -f $tmp.status.ref $HOME/status.ref
 /bin/mv -f $tmp.station.list $HOME/station.list
 /bin/mv -f $tmp.rinex.list $HOME/rinex.list
 echo; cat $HOME/status.ref; echo
fi
