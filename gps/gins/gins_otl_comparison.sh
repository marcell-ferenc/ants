#!/bin/bash

#@
#@ USAGE  : gins_plot_otl.sh -r [OTL_REF_FILE] -o [OTL_FILE]
#@
#@ TASK   : Compare two OTL files that are in GINS format
#@
#* by     : marcell.ferenc@cnam.fr
#*

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/ts.func

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## list of options the program will accept
optstring=o:r:vh

## interpret options
while getopts $optstring opt
do
 case $opt in
  r) otl_ref=$OPTARG ;;
  o) otl_inp=$OPTARG ;;
  v) v=yes ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

tr -d '\r' < $otl_ref | awk 'NR > 3 { print $0 }' | sed 's/,/ /g' > $tmp.ref
tr -d '\r' < $otl_inp | awk 'NR > 3 { print $0 }' | sed 's/,/ /g' > $tmp.otl

if [ $( wc -l < $tmp.ref ) -ne $( wc -l < $tmp.otl ) ]
then color_txt bold red "dimension error" exit 2; fi

awk '{ print $1 }' $tmp.ref >  $tmp.ref.1
awk '{ print $1 }' $tmp.otl >  $tmp.otl.1

if [ $( diff $tmp.ref.1 $tmp.otl.1 &> /dev/null; echo $? ) -ne 0 ]
then color_txt bold red "site_name error"; exit 2; fi

split_file_row.sh -f $tmp.ref -p 3 -o 2> /dev/null
split_file_row.sh -f $tmp.otl -p 3 -o 2> /dev/null

## splitted reference and otl file according to up, east and north components
ru=$tmp.ref.00001; ou=$tmp.otl.00001
re=$tmp.ref.00002; oe=$tmp.otl.00002
rn=$tmp.ref.00003; on=$tmp.otl.00003

## temp data, print all columns but the first
awk '{ print $1 }' $ru > $ru.1
awk '{ print $1 }' $re > $re.1
awk '{ print $1 }' $rn > $rn.1
awk '{ $1=""; sub(" ",""); print }' $ru > $tmp.d; /bin/mv -f $tmp.d $ru
awk '{ $1=""; sub(" ",""); print }' $re > $tmp.d; /bin/mv -f $tmp.d $re
awk '{ $1=""; sub(" ",""); print }' $rn > $tmp.d; /bin/mv -f $tmp.d $rn
awk '{ print $1 }' $ou > $ou.1
awk '{ print $1 }' $oe > $oe.1
awk '{ print $1 }' $on > $on.1
awk '{ $1=""; sub(" ",""); print }' $ou > $tmp.d; /bin/mv -f $tmp.d $ou
awk '{ $1=""; sub(" ",""); print }' $oe > $tmp.d; /bin/mv -f $tmp.d $oe
awk '{ $1=""; sub(" ",""); print }' $on > $tmp.d; /bin/mv -f $tmp.d $on

## relative
paste $ru.1 <( gmtmath $ou $ru SUB $ou $ru ADD 2 DIV DIV 100 MUL ABS -Ca -T = \
| awk '{ printf "%7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f \
%7.2f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > u_otl_diff_perc.dat
paste $re.1 <( gmtmath $oe $re SUB $oe $re ADD 2 DIV DIV 100 MUL ABS -Ca -T = \
| awk '{ printf "%7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f \
%7.2f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > e_otl_diff_perc.dat
paste $rn.1 <( gmtmath $on $rn SUB $on $rn ADD 2 DIV DIV 100 MUL ABS -Ca -T = \
| awk '{ printf "%7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f \
%7.2f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > n_otl_diff_perc.dat

## absolute
paste $ru.1 <( gmtmath $ou $ru SUB ABS -Ca -T = \
| awk '{ printf "%5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f \
%5.3f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > u_otl_diff_cm.dat
paste $re.1 <( gmtmath $oe $re SUB ABS -Ca -T = \
| awk '{ printf "%5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f \
%5.3f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > e_otl_diff_cm.dat
paste $rn.1 <( gmtmath $on $rn SUB ABS -Ca -T = \
| awk '{ printf "%5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f \
%5.3f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > n_otl_diff_cm.dat

exit
## relative
paste $ru.1 <( gmtmath $ou $ru SUB $ru DIV 100 MUL ABS -Ca -T = \
| awk '{ printf "%7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f \
%7.2f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > u_otl_diff_perc.dat
paste $re.1 <( gmtmath $oe $re SUB $re DIV 100 MUL ABS -Ca -T = \
| awk '{ printf "%7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f \
%7.2f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > e_otl_diff_perc.dat
paste $rn.1 <( gmtmath $on $rn SUB $rn DIV 100 MUL ABS -Ca -T = \
| awk '{ printf "%7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f %7.2f \
%7.2f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > n_otl_diff_perc.dat

## absolute
paste $ru.1 <( gmtmath $ou $ru SUB ABS -Ca -T = \
| awk '{ printf "%5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f \
%5.3f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > u_otl_diff_cm.dat
paste $re.1 <( gmtmath $oe $re SUB ABS -Ca -T = \
| awk '{ printf "%5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f \
%5.3f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > e_otl_diff_cm.dat
paste $rn.1 <( gmtmath $on $rn SUB ABS -Ca -T = \
| awk '{ printf "%5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f \
%5.3f\n",$1,$3,$5,$7,$9,$11,$13,$15,$17,$19,$21 }' ) > n_otl_diff_cm.dat
