#!/bin/bash

#@
#@ USAGE    : gins_station_info_rinex.sh -r [RINEX] [OPTIONS]
#@
#@ OPTIONS  :
#@            -f - forced otl file creation
#@            -t - add_tectonic_velocity
#@            -v - verbose mode
#@            -h - help
#@
#@ TASK     : create GINS station info file into $HOME/gin/data/stations from
#@            RINEX file and create ocean loading file into 
#@            $HOME/gin/data/charge/ocean based on this new station file
#@            create SITE.otl ocean loading file if it does not exist already
#@            download and complete *.atx file with missing antenna calibration
#@
#* by       : marcell.ferenc.uni@gmail.com
#*

#* original : create_station_file.ksh ( received on 18th of February 2013 )
#*
#* modified : 2014-06-01 by marcell.ferenc.uni@gmail.com
#*            - complete station file with tectonic velocity
#*
#* modified : 2014-05-09 by marcell.ferenc.uni@gmail.com
#*            - test of antenna existence in igsYY.atx file
#*            - google function to find and complete missing atx information
#*            - ocean loading file generation based on created station file
#*            - more verbose text added for debugging [in case of -v option]
#* modified : 2013-09-02 by marcell.ferenc.uni@gmail.com
#*            - modified sturcture
#*            - new functions sourced: usage, ...
#* modified : 2013-06-11 by marcell.ferenc.uni@gmail.com
#*            - all external fortran programs were omitted
#*            - new functions: xyz2flh_awk, xyz2enu_awk
#* modified : 2013-02-19 by marcell.ferenc.uni@gmail.com
#*            - the script and the used external fortran code was modified

## default variables
nominal=$HOME/gin/data/stations/nominal
if [ -s "$nominal" ]; then
echo
 ellipsoid=GRS80
 inf=( $( awk -F, 'NR==1 { print $2,$3 }' $nominal ) )
 R=${inf[1]}
 f=${inf[3]}
else
 ellipsoid=WGS84
 R=6378137.000
 f=298.257223563
fi
tecto=no
forced_otl=no

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/gps.func
source $HOME/bin/func/ts.func

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## list of options the program will accept
optstring=r:ftvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  r) rinex=$OPTARG ;;
  t) tecto=yes ;;
  v) v=yes ;;
  f) forced_otl=yes ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## GINS station info header
header()
{
 printf "%-37.37s,  R=%11.2f,   f=%11.6f\n" "$ellipsoid" "$R" "$f"
 echo " station type site                    X (m)   sig.        Y (m)   sig.        Z(m)    sig.    Xp (m/an)      Yp (m/an)      Zp (m/an)    date  plaque source "
 echo "   -9999 M000 Earth c. of mass        0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                              010100               "
 echo "   -1998 EGB  Bias       (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                              010198 311298        "
 echo "    -199 EGB  Bias       (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                              010199 310199        "
 echo "     -61 EGC1 1/year Cos (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                                            dy ecc "
 echo "     -51 EGS1 1/year Sin (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                                            dy ecc "
 echo "     -62 EGC2 2/year Cos (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                                            dy ecc "
 echo "     -52 EGS2 2/year Sin (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                                            dy ecc "
 echo "                                                                                                                                                             "
}

## GINS station info body
body()
{
 printf "%3.3s%5.5s %4.4s %4.4s %11.11s%13.3f ~%5.3f%13.3f ~%5.3f%13.3f ~%5.3f %7.4f %6.6s %7.4f %6.6s %7.4f %6.6s %-20.20s\n" \
 " " "$domes_a" "$domes_b" "$name" " " "$pos_x" "$sig_x" "$pos_y" "$sig_y" "$pos_z" "$sig_z" "$vel_x" "$vel_x_sig" "$vel_y" "$vel_y_sig" "$vel_z" "$vel_z_sig" "$info_string"
 printf " %5.5s%02d %4.4s %4.4s %-11.11s%13.3f%7.3f%13.3f%7.3f%13.3f%7.3f   %-16.16s%4.4s %-5.5s%17.17s%6.6s %6.6s %-5.5s\n" \
 "$domes_a" "$occup" "$char" "$name" "$rec" "${dxdydz[1]}" "$sig_dx" "${dxdydz[3]}" "$sig_dy" "${dxdydz[5]}" "$sig_dz" "$ant" "$rad" "$ant_num" " " "$date_ins" "$date_rem" "$data_src"
} 

## test rinex
if [ -z "$rinex" ]
then
 color_txt bold red "-r option is obligatory [rinex]"; exit 2
elif [ ! -s "$rinex" ]
then
 color_txt bold red "rinex does not exist [$rinex]"; exit 2
else
 rnx_name=${rinex##*/}
 case $rnx_name in
  ???????0.??d.Z) rnx=${rnx_name//d.Z/o}; CRZ2RNX $rinex; tr -d '\r' < $rnx > $tmp; /bin/mv -f $tmp $rnx ;;
    ???????0.??o) rnx=$rinex; tr -d '\r' < $rnx > $tmp; /bin/mv -f $tmp $rnx ;;
               *) color_txt bold red "not accepted rinex name [$rinex]"; exit 2 ;;
 esac
fi

[ "$v" == "yes" ] && color_txt bold blue "input              : $rinex"

## iformation from rinex name
st=${rnx_name:0:4}; d3=${rnx_name:4:3}; ss=${rnx_name:7:1}; yy=${rnx_name:9:2}

## for GNSS data after 2000
year=$(( 2000 + $yy ))
jul50=$( jjul $d3 $year | awk '/JUL50/ { print $3 }' )
st_upper=$( echo $st | tr '[:lower:]' '[:upper:]' )
output=${rnx_name%o}s #${st_upper}_${jul50}_RINEX.inf

## station name
name=$( awk '/MARKER NAME/ { print $1 }' $rnx )
if [ -z "$name" ]; then color_txt bold red "site name error"; exit 2; fi
name=$( trim "$name" ); name=${name:0:4}
[ "$v" == "yes" ] && color_txt bold blue "site               : $name"

## domes
domes=$( awk '/MARKER NUMBER/ { print $1 }' $rnx )
if [ -z "$domes" ]; then color_txt bold red "domes error"; exit 2; fi
domes=$( trim "$domes" ); domes_a=${domes:0:5}; domes_b=${domes:5}

[ "$v" == "yes" ] && color_txt bold blue "domes              : ${domes_a}$domes_b"

## arbitrary occupation number
occup=99
 
## antenna XYZ
pos_x=$( awk '/APPROX POSITION/ { print $1 }' $rnx ); pos_x=${pos_x//[!-0-9.]/}
if [ -z "$pos_x" ]; then color_txt bold red "pos_x error"; exit 2; fi
pos_y=$( awk '/APPROX POSITION/ { print $2 }' $rnx ); pos_y=${pos_y//[!-0-9.]/}
if [ -z "$pos_y" ]; then color_txt bold red "pos_y error"; exit 2; fi
pos_z=$( awk '/APPROX POSITION/ { print $3 }' $rnx ); pos_z=${pos_z//[!-0-9.]/}
if [ -z "$pos_z" ]; then color_txt bold red "pos_z error"; exit 2; fi
 
## antenna ARP
arp_e=$( awk '/ANTENNA: DELTA H\/E\/N/ { print $2 }' $rnx ); arp_e=${arp_e//[a-zA-Z]/}
if [ -z "$arp_e" ]; then color_txt bold red "arp_e error"; exit 2; fi
arp_n=$( awk '/ANTENNA: DELTA H\/E\/N/ { print $3 }' $rnx ); arp_n=${arp_n//[a-zA-Z]/}
if [ -z "$arp_n" ]; then color_txt bold red "arp_n error"; exit 2; fi
arp_u=$( awk '/ANTENNA: DELTA H\/E\/N/ { print $1 }' $rnx ); arp_u=${arp_u//[a-zA-Z]/}
if [ -z "$arp_u" ]; then color_txt bold red "arp_u error"; exit 2; fi
[ "$v" == "yes" ] && color_txt bold blue "arp_e              : $arp_e"
[ "$v" == "yes" ] && color_txt bold blue "arp_n              : $arp_n"
[ "$v" == "yes" ] && color_txt bold blue "arp_u              : $arp_u"

## antenna XYZ sigma
pos_x_sig=0.001; pos_y_sig=0.001; pos_z_sig=0.001

## tectonic velocity, it will be replaced by tecto_xyz
vel_x=0; vel_y=0; vel_z=0

## tectonic velocity sigma, it will be replaced by tecto_xyz
vel_x_sig="~.0001"; vel_y_sig="~.0001"; vel_z_sig="~.0001"

date_inf=                 # 010100 should be reviewed      
plaque_inf=               # EURA   should be reviewed 
source_inf=               # i05i05 should be reviewed
info_string="010100   EURA i05i05"

char="GRG3"

## ****************************************** XYZENU ******************************************************** ##
## xyz2enu_awk function transform the dH,dE,dN antenna eccentricity values from the RINEX file to dX,dY,dZ    ##
## ****************************************** XYZENU ******************************************************** ##

plh=( $( func_xyz2flh -x $pos_x -y $pos_y -z $pos_z ) )
dxdydz=( $( func_xyz2enu -t xyz -p ${plh[1]} -l ${plh[3]} -x $arp_e -y $arp_n -z $arp_u | grep dX: ) )

## ****************************************** XYZENU END ************************************* ##

[ "$v" == "yes" ] && color_txt bold blue "arp_dx             : ${dxdydz[1]}"
[ "$v" == "yes" ] && color_txt bold blue "arp_dy             : ${dxdydz[3]}"
[ "$v" == "yes" ] && color_txt bold blue "arp_dz             : ${dxdydz[5]}"

## antenna dx dy dz sigma
sig_dx=0.000; sig_dy=0.000; sig_dz=0.000

## GPS receiver
rec=$( trim "$( awk '/REC # \/ TYPE \/ VERS/ { print substr($0,21,20) }' $rnx )" )
if [ -z "$rec" ]; then rec="rrrrrrrrrrrrrrrrrrrr" ; fi

## change acronyms of GPS reveivers
rec=${rec//ASHTECH/AS}
rec=${rec//TRIMBLE/TR}
rec=${rec//ROGUE/RG}
res=${rec//TURBO-ROGUE/TG}
res=${rec//MINI-ROGUE/MG}

## GPS receiver serial number
rec_num=$( trim "$( awk '/REC # \/ TYPE \/ VERS/ { print substr($0,1,20) }' $rnx )" )
if [ -z "$rec_num" ]; then rec_num="               "; fi

## antenna name
ant=$( trim "$( awk '/ANT # \/ TYPE/ { print substr($0,21,15) }' $rnx )" ) #21,16
if [ -z "$ant" ]; then exit 2; fi
[ "$v" == "yes" ] && color_txt bold blue "antenna            : $ant" 

## antenna radome
rad=$( trim "$( awk '/ANT # \/ TYPE/ { print substr($0,36,5) }' $rnx )" ) #37,4
if [[ -z $rad || "$rad" == "    " ]]; then rad=NONE; elif [ "${#rad}" -ne 4 ]; then rad=NONE ; fi
[ "$v" == "yes" ] && color_txt bold blue "radome             : $rad"

itrf_change_date=20110417
rnx_date=$( date --date="$year-01-01 +$d3 days -1 day" "+%Y%m%d" )
rnx_ins=$( date --date="$year-01-01 +$d3 days -1 day" "+%d%m%y" )

## choose adequate atx file
if [ $rnx_date -lt $itrf_change_date ]; then igsyy=igs05.atx; else igsyy=igs08.atx; fi

## verify antenna and radome in antex file
grep -m 1 "${ant}[ ]*${rad}.*TYPE.*" $HOME/atx/$igsyy &> /dev/null
if [ $? -ne 0 ]
then
 [ "$v" == "yes" ] && color_txt bold green "atx-info           : $ant $rad is NOT in $igsyy"
 cd $HOME/atx
 atx_file=$( google $ant | grep -i ".*ATX" | head -n 1 ); wget $atx_file &> /dev/null
 { cat $igsyy; sed '1,/END OF HEADER/d' "${atx_file##*/}"; } > $tmp.atx; /bin/mv -f $tmp.atx $igsyy
 cd - 1> /dev/null
 grep -m 1 "${ant}[ ]*${rad}.*TYPE.*" $HOME/atx/$igsyy &> /dev/null
 if [ $? -ne 0 ]
 then
  color_txt bold red "atx-info           : $ant $rad is NOT in $igsyy"
 else
  [ "$v" == "yes" ] && color_txt bold blue "atx-info           : $ant $rad is in $igsyy now"
 fi
else
 [ "$v" == "yes" ] && color_txt bold blue "atx-info           : $ant $rad is in $igsyy"
fi

## antenna serial number
ant_num="$( awk '/ANT # \/ TYPE/ { print substr($0,1,20) }' $rnx )"; 
if [ -z "$ant_num" ]; then ant_num="nnnnnnnnnnnnnnnnnnnn"; fi; ant_num=$( trim "$ant_num" )

data_src=rinex

date_ins=010100 #"$rnx_ins"
date_rem="      "

## remove rnx file in case if it was uncompressed by this script
case $rnx_name in
 ???????0.??d.Z) /bin/rm -f $rnx ;;
esac

## create station file
[ "$v" == "yes" ] && color_txt bold green "sta-file creation  : $HOME/gin/data/stations/$output"
{ header; body; } > $HOME/gin/data/stations/$output

if [ "$tecto" == "yes" ]
then
 ## tectonic information to station file
 [ "$v" == "yes" ] && color_txt bold blue "tectonic correction: ${output}_tecto"
 cd $HOME/gin/data/stations
 { echo $output; echo 3; echo 0; echo 1; } > $tmp.tecto_xyz-in
 tecto_xyz < $tmp.tecto_xyz-in &> /dev/null
 #earth_old=$( grep "Earth c. of mass" $output )
 old=$( grep "$domes_a $domes_b $site" $output )
 new=$( grep "$domes_a $domes_b $site" ${output}_tecto )
 new=${new//~.0000 /~.0001 }; new=${new//~0.000 /~0.001 }
 #earth_new=$( grep "Earth c. of mass" ${output}_tecto )
 #{ change "$old" "$new" $output; change "$earth_old" "$earth_new" $output; } &> /dev/null
 change "$old" "$new" $output &> /dev/null
 /bin/rm -f ${output}_tecto
 cd - 1> /dev/null
fi

## create ocean loading file
[ "$v" == "yes" ] && color_txt bold blue "ocean loading      : $st.otl"
otl_file=$st.otl
if [ ! -s $HOME/gin/data/charge/ocean/$otl_file ]
then
 { echo $HOME/gin/data/stations/$output; echo $HOME/gin/data/charge/ocean/$otl_file; } > $tmp.otl; exe_loadoce < $tmp.otl 1> /dev/null
elif [ "$forced_otl" == "yes" ]
then
 { echo $HOME/gin/data/stations/$output; echo $HOME/gin/data/charge/ocean/$otl_file; } > $tmp.otl; exe_loadoce < $tmp.otl 1> /dev/null
fi
