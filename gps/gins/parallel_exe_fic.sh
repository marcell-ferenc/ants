#!/bin/bash

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir 2> /dev/null
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## input
fic_dir=$1
out_dir=$2
par_dir=$HOME/wrk/parallel
nbs=15

## test fic directory
if [[ -z "$fic_dir" || ! -d "$fic_dir" ]]; then
 echo "${0##*/}: error: missing directory: fic_dir: <$fic_dir>"; exit 2; fi

if [ -z "$out_dir" ]; then
 echo "${0##*/}: error: missing directory: out_dir: <$out_dir>"; exit 2; fi

## test output directory
case $out_dir in
 */*) echo "${0##*/}: error: wrong directory: <$out_dir>"; exit 2 ;;
   *) ;;
esac

## create individual commands
find $fic_dir -maxdepth 1 -type f -print \
| sort -n \
| xargs -I {} echo gins_exe_fic.sh -f {} -w ${out_dir} -v > $tmp.se

## split commands into <$nbs> sessions
split_file_row.sh -f $tmp.se -p $nbs

## create directory for parallel sessions
mkdir -p $par_dir

## parallel id
par_id=$USER.$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )

for se in $tmp.se.*; do
 echo "${0##*/}: session file: <$par_dir/$par_id.${se#$tmp.}>"
 cp $se $par_dir/$par_id.${se#$tmp.}
done

cnt=0
## run <$nbs> sessions parallel
for se in $par_dir/$par_id.*
do
 echo "${0##*/}: session file: <$se>"
 cnt=$(( $cnt + 1 ))
 gnome-terminal -x bash -c \
 "sh $se; \
 read -n1" &
done
