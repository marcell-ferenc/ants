#!/bin/bash

#@
#@ USAGE     : gins_driver.sh -d <dir_path> -o <rinex/prairie> <options>
#@
#@ OPTIONS   :
#@             -f                        forced station info    [default: only_if_not_in_nominal]
#@             -g <tropo_gradients>                             [default:                      0]                 
#@             -h                        help                   [default:                     no]
#@             -c <cutoff>                                      [default:                     10]
#@             -p <problematic_sat_file>                        [default:   gnss_sat_problem.lst]
#@             -s <position_sampling>                           [default:                     6h]
#@             -t <tropo_sampling>                              [default:                     1h]             
#@             -v                        verbose mode           [default:                     no]
#@
#@ TASK      : Create driver file for GINS-PC
#@
#@             output name: SSSS_DDu_AAA_JJJJJ_cXXtXXugXXionXigsYYmm
#@
#* by        : marcell.ferenc@cnam.fr
#*

## scriptname
scrn=${0##*/}
scrn=${scrn%.sh}

## temporary filename
tmp=$scrn.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean(){ /bin/rm -rf $tmp_dir; trap EXIT; cd; exit; }

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/gins.func
source $HOME/bin/func/gps.func
source $HOME/bin/func/ts.func

## help
case "$#" in 0) usage $0; exit ;; esac

## default variable
def_satprob=$HOME/gin/data/HorlogesAnom0.log # problematic satellites
prob_ep_nb=10                            # threshold of problematic epoch number
GINS_otl=otl_rgp_fes2012.txt
#load_rgp_fes2012_felix.txt
#load_rgp_fes2012.txt #gins_otl_TOULOUSE.dat #nominal
template=GINS_PPP.ref
acdata=GR2
cutoff=10
pos_dt=6h
pos_ch=00060000
tro_dt=1h
tro_ch="   24"
tro_gr=0
ion2nd=no
ionstr=ion1
f_stainfo=no
v=no
str=xxxxxxxxxx
otl_model=fes2004
cmc=no #yes
atml=no
s1s2=no
log=$scrn.log
max_path_length=52

## command line long options ---------------------------------------------------
while (( $# > 0 )); do
 case $1 in
  -help|-HELP|-h|-H) usage $0; exit ;;
  -debug) set -x ;;
  -atml*) if [ ${#1} -gt 5 ]; then atml=${1#"-atml"}; else shift; atml=$1; fi ;;
  -s1s2) s1s2=yes ;;
  -cmc) #if [ ${#1} -gt 4 ]; then cmc=${1#"-cmc"}; else shift; cmc=$1; fi ;;
        cmc=yes ;;
  -cutoff*) if [ ${#1} -gt 7 ]; then
             cutoff=${1#"-cutoff"}; else shift; cutoff=$1; fi ;;
  -tropo_grad*) if [ ${#1} -gt 11 ]; then tropo_grad=${1#"-tropo_grad"}; else shift; tropo_grad=$1; fi ;;
  -tropo*) if [ ${#1} -gt 6 ]; then
            tropo=${1#"-tropo"}; else shift; tropo=$1; fi ;;
  -ntol*) if [ ${#1} -gt 5 ]; then ntol=${1#"-ntol"}; else shift; ntol=$1; fi ;;
  -cwsl*) if [ ${#1} -gt 5 ]; then cwsl=${1#"-cwsl"}; else shift; cwsl=$1; fi ;;
  -otl*) if [ ${#1} -gt 4 ]; then otl=${1#"-otl"}; else shift; otl=$1; fi ;;
  -obs_file*) if [ ${#1} -gt 9 ]; then
               obs_file=${1#"-obs_file"}; else shift; obs_file=$1; fi ;;
  -sta_info*) #if [ ${#1} -gt 9 ]; then
              # sta_info=${1#"-sta_info"}; else shift; sta_info=$1; fi ;;
              sta_info=yes ;;
  -sat_prob*) rm_sat=yes
              if [ ${#1} -gt 9 ]; then
               sat_prob=${1#"-sat_prob"}
              else if [ "${2:0:1}" != "-" ]; then
               shift; sat_prob=$1; fi
              fi ;;
  -verbose|-v|-V) v=yes ;;
  *) color_txt bold red "${0##*/}: error: not valid option: <$1>"; exit 2 ;;
 esac
 shift
done
##------------------------------------------------------------------------------

echo $atml
echo $s1s2
echo $cmc
echo $cutoff
echo $tropo
echo $tropo_grad
echo $ntol
echo $cwsl
echo $otl
echo $obs_file
echo $sta_info
echo $sat_prob
echo $v
