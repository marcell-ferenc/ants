#!/bin/bash

#@
#@ USAGE  : gins_results_xyz.sh -d <work_dir> <options>
#@
#@ TASK   : Catenate GINS-PC results
#@
#@ OPTIONS: -v - verbose mode
#@          -h - help
#@
#* by     : marcell.ferenc@cnam.fr
#*

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir .gmt* gmt.conf gmt.history 2> /dev/null
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/ts.func

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## default variable
v=no
LC_NUMERIC=C ## for decimal separator in floating numbers

## list of options the program will accept
optstring=d:vh

## interpret options
while getopts $optstring opt
do
 case $opt in
  d) wrk_dir=$OPTARG; wrk_dir=${wrk_dir%/result*} ;;
  h) usage $0; exit ;;
  v) v=yes ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## set values
## project, orbit and clock data
if [ -z "$wrk_dir" ]; then
 color_txt bold red "${0##*/}: error: -d <work_dir> option missing"; exit 2; fi

ts_dir=$wrk_dir/result/tserie
rdir=$wrk_dir/result/daily

if [ ! -d "$rdir" ]; then
 color_txt bold red "${0##*/}: error: missing directory: <$rdir>"; exit 2; fi

## test if necessary input files exist
find $rdir -maxdepth 1 -type f -name "S[XYZ]__*-f[lx]" | egrep '.*' &> /dev/null

## text exit code, if there are any result files
if [ $? -ne 0 ]; then
 color_txt bold red "${0##*/}: error: empty directory: <$rdir>"; exit 2
else cd $rdir; fi

## remove and rebuild the final result directory
/bin/rm -rf $ts_dir 2> /dev/null; mkdir -p $ts_dir

stations=( $( printf "%s\n" * \
| awk '{ print substr($1,5,4) }' > $tmp.sta; remdupnoncons $tmp.sta ) )

rfile=$( printf "%s\n" * | head -n 1 )

if [[ -z "$rfile" || ! -s "$rfile" ]]; then
 color_txt bold red "${0##*/}: error: no result file <$rfile>"; exit 2; fi

## loop - station
for sta in ${stations[@]}; do

 sta_cap=$( echo $sta | tr '[:lower:]' '[:upper:]' ) ## uppercase station name
 sta_min=$( echo $sta | tr '[:upper:]' '[:lower:]' ) ## lowercase station name
 
 if [ "$v" == "yes" ]; then color_txt bold green "${0##*/}: station: $sta"; fi
 
 ## test output result type
 case $rfile in
  *GR2*|*GRG*) result_type=( fl fx ) ;;
      *IGS*|*) result_type=( fl ) ;;
 esac
 
 ## loop - solution
 for sln in ${result_type[@]}; do

  xyz=$( printf "%s\n" SX__*${sta}*$sln | head -n1 )
  xyz=${xyz:4:12}_${xyz:23:22}-$sln.xyz
  
  if [ "$v" == "yes" ]; then color_txt bold green "${0##*/}: solution: $sln"; fi
  if [ "$v" == "yes" ]; then color_txt bold green "${0##*/}: output: <$xyz>"; fi
  
  ## TIME, X, sig_X, Y, sig_Y, Z, sig_Z [7 columns]
  paste <( cat SX__*${sta}*$sln ) \
  <( cat SY__*${sta}*$sln ) \
  <( cat SZ__*${sta}*$sln ) \
  | awk '{ if (substr($24,20,1)==1) mn=01; else if (substr($24,20,1)==2) mn=02;
  else if (substr($24,20,1)==3) mn=03; else if (substr($24,20,1)==4) mn=04;
  else if (substr($24,20,1)==5) mn=05; else if (substr($24,20,1)==6) mn=06;
  else if (substr($24,20,1)==7) mn=07; else if (substr($24,20,1)==8) mn=08;
  else if (substr($24,20,1)==9) mn=09; else if (substr($24,20,1)=="O") mn=10;
  else if (substr($24,20,1)=="N") mn=11; else if (substr($24,20,1)=="D") mn=12; 
  printf "%4d-%02d-%02dT%02d:%02d:%02d %20.13E %20.13E %20.13E %20.13E %20.13E \
  %20.13E\n",substr($24,18,2)+2000,mn,substr($24,1,2),substr($24,3,2),\
  substr($24,5,2),substr($24,7,2),$5,$6,$13,$14,$21,$22 }' > $ts_dir/$xyz
 
 done
 ## end loop - solution
done
## end loop - station
