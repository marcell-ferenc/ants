#!/bin/bash

mkdir -p $HOME/OTL_COMPARISON
cd $HOME/OTL_COMPARISON


files=( $( find /data/marcell/gin/data/charge/ocean -maxdepth 1 -type f -name "*.otl" -printf "%f\n" ) )


for station in "${files[@]//.otl/}"
do
 echo $station
 gins_plot_otl.sh -r 
done
