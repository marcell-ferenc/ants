#!/bin/bash

#@
#@ USAGE  : gins_otl_sch2g.sh -s [SCHERNECK_FILE] | [OPTIONS]
#@
#@ OPTIONS:
#@          -h - hep
#@          -v - verbose_mode
#@          -o - output_name
#@
#@ TASK   : convert SCHERNECK-OTL file into GINS-PC format
#@
#@ NOTE   : station log file needed (to lookup DOMES number)
#@          
#@ TODO   : An IGS-station_name-DOMES, RGP-station_name-DOMES or 
#@          network_name-station_name-DOMES database file
#@
#* by     : marcell.ferenc@cnam.fr
#*

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/ts.func

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## list of options the program will accept
optstring=s:vh

## interpret options
while getopts $optstring opt
do
 case $opt in
  s) sch_file=$OPTARG ;;
  v) v=yes ;;
  h) usage $0 ; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## order of tides in "nominal" file gins_tides
gint=( m2 s2 k1 o1 n2 p1 k2 q1 mf mm ssa 2n2 l2 mu2 nu2 t2 )

## order of tides in Scherneck file scherneck_tides
scht=( M2 S2 N2 K2 K1 O1 P1 Q1 MF MM SSA )

header()
{
 printf "ocean tidal loading coefficients sparsed from %s\n" $sch_file
 printf "station, amplitude (cm) and phase (deg) per tidal wave\n"
 printf "%s\n" "            $( printf "%-12.12s" ${gint[@]} )"
}

## print everything after the "END HEADER" without this pattern
## and remove the line "END TABLE"
## sed -e '1,/END HEADER/d' $sch_file | sed '/END TABLE/d' \
##> $HOME/scherneck_dir/sch.txt
## split the Scherneck file according to stations
## each stations 11 lines with comments
## split_file_row.sh -f $HOME/scherneck_dir/sch.txt -s 11

## remove carriage return chars and also all comment lines form Scherneck file
tr -d '\r' < $sch_file | sed '/^\$\$/d' > $tmp.scherneck

## split the Scherneck file according to stations
## each stations have 7 lines without comments
split_file_row.sh -f $tmp.scherneck -s 7 &> /dev/null

## create destination directory
mkdir -p $HOME/gin/data/charge/ocean/scherneck

c=0
for sta_otl in $tmp.scherneck.*
do
 c=$(( $c + 1 )); sta=$( awk 'NR == 1 { print $1 }' $sta_otl )
 sta_min=$( echo $sta | tr '[:upper:]' '[:lower:]' )
 ## latest logsheet for the given site
 logsheet=$( ls -1 $HOME/logsheet/${sta_min}*log | tail -n1 )
 ## remove carriage return if exist
 ## in order to awk, grep, sed work properly with the file
 tr -d '\r' < $logsheet > $tmp.logsheet
 domes=$( grep "IERS DOMES Number" $tmp.logsheet | awk '{ print substr($0,33,5) }' )
 [ "$v" == "yes" ] && { echo file_number: $c; echo station: $sta; }
 {
 header
 TIDE=( $( awk 'NR > 1 { print $0 }' $sta_otl ) )
 for d in u e n
 do
  case $d in
   u) l=1 ;; e) l=2 ;; n) l=3 ;;
  esac
  m2="${TIDE[$( tabindex $l 1 6 ${#scht[@]} )]} ${TIDE[$( tabindex $(( $l + 3 )) 1 6 ${#scht[@]} )]}"
  s2="${TIDE[$( tabindex $l 2 6 ${#scht[@]} )]} ${TIDE[$( tabindex $(( $l + 3 )) 2 6 ${#scht[@]} )]}"
  n2="${TIDE[$( tabindex $l 3 6 ${#scht[@]} )]} ${TIDE[$( tabindex $(( $l + 3 )) 3 6 ${#scht[@]} )]}"
  k2="${TIDE[$( tabindex $l 4 6 ${#scht[@]} )]} ${TIDE[$( tabindex $(( $l + 3 )) 4 6 ${#scht[@]} )]}"
  k1="${TIDE[$( tabindex $l 5 6 ${#scht[@]} )]} ${TIDE[$( tabindex $(( $l + 3 )) 5 6 ${#scht[@]} )]}"
  o1="${TIDE[$( tabindex $l 6 6 ${#scht[@]} )]} ${TIDE[$( tabindex $(( $l + 3 )) 6 6 ${#scht[@]} )]}"
  p1="${TIDE[$( tabindex $l 7 6 ${#scht[@]} )]} ${TIDE[$( tabindex $(( $l + 3 )) 7 6 ${#scht[@]} )]}"
  q1="${TIDE[$( tabindex $l 8 6 ${#scht[@]} )]} ${TIDE[$( tabindex $(( $l + 3 )) 8 6 ${#scht[@]} )]}"
  mf="${TIDE[$( tabindex $l 9 6 ${#scht[@]} )]} ${TIDE[$( tabindex $(( $l + 3 )) 9 6 ${#scht[@]} )]}"
  mm="${TIDE[$( tabindex $l 10 6 ${#scht[@]} )]} ${TIDE[$( tabindex $(( $l + 3 )) 10 6 ${#scht[@]} )]}"
  ssa="${TIDE[$( tabindex $l 11 6 ${#scht[@]} )]} ${TIDE[$( tabindex $(( $l + 3 )) 11 6 ${#scht[@]} )]}"
  echo $m2 $s2 $k1 $o1 $n2 $p1 $k2 $q1 $mf $mm $ssa \
  | awk -v u=100 -v d=$domes 'function pha(x){return ((x < 0.0) ? x+360 : x)} { printf "%8.8s,%5.3f,%5.1f,%5.3f,%5.1f,%5.3f,%5.1f,%5.3f,%5.1f,%5.3f,%5.1f,%5.3f,%5.1f,%5.3f,%5.1f,%5.3f,%5.1f,%5.3f,%5.1f,%5.3f,%5.1f,%5.3f,%5.1f,\n",d,$1*u,pha($2),$3*u,pha($4),$5*u,pha($6),$7*u,pha($8),$9*u,pha($10),$11*u,pha($12),$13*u,pha($14),$15*u,pha($16),$17*u,pha($18),$19*u,pha($20),$21*u,pha($22) }'
 done
 } > $HOME/gin/data/charge/ocean/scherneck/${sta_min}-sch.otl
done
