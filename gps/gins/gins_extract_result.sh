#!/bin/bash

#@
#@ USAGE: gins_results_xyz.bash -p WORK_DIR
#@
#@ OBLIGATORY:
#@ -p - PROJECT                        [$WORK_DIRECTORY]
#@
#@ TASK: catenate daily GINS results
#@

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/ts.func

## cleaning
clean()
{
 /bin/rm -rf ${tmp_dir}* .gmt* gmt.conf gmt.history 2> /dev/null
 ## restore default value 
 trap EXIT
 exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## list of options the program will accept
optstring=p:h

## interpret options
while getopts $optstring opt
do
 case $opt in
  p) project=$OPTARG ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## set values
## project, orbit and clock data
if [ -z "$project" ]
then
 color_txt bold red "-p option is obligatory"; exit 2
else
 ## if full path provided with the project directory remove the HOME part
 project=${project#$HOME/}
 if [ ! -e $HOME/$project ]
 then
  color_txt bold red "$HOME/$project directory does not exist"; exit 2
 fi
 ## work environment (created by the gps_data.bash script)
 wrk_dir=$HOME/$project
 rnx_dir=$wrk_dir/rinex
 tmp_dir=$wrk_dir/tmp
 lst_dir=$wrk_dir/lst
 log_dir=$wrk_dir/log
 ## information files
 pre_err_lst=$lst_dir/prepars_PPP_error.list
 fic_lst=$lst_dir/fic_PPP.list
 fic_err_lst=$lst_dir/fic_PPP_error.list
 drv_lst=$lst_dir/fich_dir_PPP.list
 exe_run_log=$log_dir/exe_gins90_runtime.log
 exe_gins90_ok=$lst_dir/exe_gins90_ok.list
 exe_gins90_er=$lst_dir/exe_gins90_er.list
 if [ ! -e $drv_lst ]
 then
  color_txt bold red "$drv_lst does not exist, please run fich_dir_ppp.bash"; exit 2
 fi
 if [ ! -e $lst_dir/inf.dat ]
 then
  color_txt bold red "$lst_dir/inf.dat does not exist, please run fich_dir_ppp.bash"; exit 2
 else
  ac_data=$( grep ac_data $lst_dir/inf.dat | awk '{ print $2 }' )
  samp_t=$( grep samp_t $lst_dir/inf.dat | awk '{ print $2 }' )
  inf_char=$( grep inf_char $lst_dir/inf.dat | awk '{ print $2 }' )
 fi
fi

## process
if [ "$ac_data" == "IGS" ]
then
 process=float; sltn="fl"
elif [ "$ac_data" == "GRG" ]
then
 process=float_fixed; sltn="fl fx"
fi

## information
color_txt bold blue "$scriptname"

## loop - station
for sta in $( cat $lst_dir/station.list )
do

 sta_cap=$( echo $sta | tr '[:lower:]' '[:upper:]' )
 sta_min=$( echo $sta | tr '[:upper:]' '[:lower:]' )

 ## directories for the results
 wrk_dir=$HOME/$project
 day_dir=$wrk_dir/$sta_min/daily
 dat_dir=$wrk_dir/$sta_min/dat
 res_dir=$wrk_dir/result_$inf_char
 /bin/rm -rf $dat_dir
 mkdir -p $dat_dir $res_dir
 
 color_txt bold green "$sta"
 
 ## loop - solution
 for s in $sltn
 do

  color_txt bold green "     $s"

  ls -1 $day_dir/SP__${samp_t}_${ac_data}_PPP_${sta_cap}_?????_$inf_char.$s &>/dev/null

  if [ $? -ne 0 ]
  then
   color_txt bold red "no results for $sta"; usage $0; continue
  fi

  X_file=$dat_dir/SP__${samp_t}_${ac_data}_PPP_${sta_cap}_$inf_char.$s
  Y_file=$dat_dir/SL__${samp_t}_${ac_data}_PPP_${sta_cap}_$inf_char.$s
  Z_file=$dat_dir/SH__${samp_t}_${ac_data}_PPP_${sta_cap}_$inf_char.$s
  #xyz_file=$dat_dir/${samp_t}_${ac_data}_PPP_${sta_cap}_$inf_char.$s.xyz
  xyz_file=$dat_dir/${sta_min}_ginspc_${ac_data}_${samp_t}_$inf_char.$s.xyz

  cat $day_dir/SP__${samp_t}_${ac_data}_PPP_${sta_cap}_?????_$inf_char.$s > $X_file
  cat $day_dir/SL__${samp_t}_${ac_data}_PPP_${sta_cap}_?????_$inf_char.$s > $Y_file
  cat $day_dir/SH__${samp_t}_${ac_data}_PPP_${sta_cap}_?????_$inf_char.$s > $Z_file

  ## TIME, X, sig_X, Y, sig_Y, Z, sig_Z [7 columns]
  paste $X_file $Y_file $Z_file | \
  awk '{ if (substr($24,20,1)==1) mn=01; else if (substr($24,20,1)==2) mn=02;
  else if (substr($24,20,1)==3) mn=03; else if (substr($24,20,1)==4) mn=04;
  else if (substr($24,20,1)==5) mn=05; else if (substr($24,20,1)==6) mn=06;
  else if (substr($24,20,1)==7) mn=07; else if (substr($24,20,1)==8) mn=08;
  else if (substr($24,20,1)==9) mn=09; else if (substr($24,20,1)=="O") mn=10;
  else if (substr($24,20,1)=="N") mn=11; else if (substr($24,20,1)=="D") mn=12; 
  printf "%4d-%02d-%02dT%02d:%02d:%02d %20.13E %20.13E %20.13E %20.13E %20.13E %20.13E\n",substr($24,18,2)+2000,mn,substr($24,1,2),substr($24,3,2),substr($24,5,2),substr($24,7,2),$5,$6,$13,$14,$21,$22 }' > $xyz_file 

  ## copy result to result directory
  cp $xyz_file $res_dir/.

  ## cleaning
  /bin/rm -f $X_file $Y_file $Z_file 2>/dev/null
 
 done
 ## end loop - solution
done
## end loop - station
