#!/bin/bash

#@
#@ USAGE    : gins_station_info_stationlog.sh -l [STATION_LOG_DIR]
#@
#@ TASK     : print GINS station info file onto STDOUT
#@            information from station log files
#@
#@ IMPORTANT:
#@            - with some logfile I have experienced some character problem 
#@              due to non-printing "cariage return" character
#@              using the "tr" program I clean off these characters and 
#@              create a "new" cleaned station log file
#@              tr -d '\r' < stationlog > stationlog_clean
#@
#@            - may the pos_x pos_y pos_z values are better in the RINEX header
#@              than in the station log file e.g.: POTS station log file's 
#@              coordinates are around 10 meters far from the official ITRF 
#@              station file's coordinates while the coordinates in the 
#@              RINEX header are less then 1 meters far from them.
#@
#* by       : marcell.ferenc@cnam.fr
#*

nominal=$HOME/gin/data/stations/nominal
if [ -s "$nominal" ]; then
echo
 ellipsoid=GRS80
 inf=( $( awk -F, 'NR==1 { print $2,$3 }' $nominal ) )
 R=${inf[1]}
 f=${inf[3]}
else
 ellipsoid=WGS84
 R=6378137.000
 f=298.257223563
fi

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/gps.func
source $HOME/bin/func/ts.func

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir
 ## restore default value 
 trap EXIT; cd; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## list of options the program will accept
optstring=l:vh

## interpret options
while getopts $optstring opt
do
 case $opt in
  l) log_dir=$OPTARG ;;
  v) v=yes ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## test log_dir
if [ -z "$log_dir" ]
then
 color_txt bold red "-l option is obligatory [station_log_dir]"; exit 2
elif [ ! -d "$log_dir" ]
then
 color_txt bold red "station_log_dir does not exist [$log_dir]"; exit 2
else
 if [ $( find $log_dir -prune -empty -type d ) ]; then color_txt bold red "empty station_log_dir [$log_dir]"; exit 2; fi
fi

## test station log file
cd $log_dir; exit_code=$( ls ????_????????.log &> /dev/null; echo $? )
if [ "$exit_code" -ne 0 ]; then color_txt bold red "no station_log_file in station_log_dir [$log_dir]"; exit 2; fi

## default variables
output=STATIONLOG.inf
station_list=$tmp.station-list
station_logs=$tmp.station-logs
char="GRG3"
data_src=stlog

## antenna xyz sigma
pos_x_sig=0.001; pos_y_sig=0.001; pos_z_sig=0.001

## tectonic velocity, it will be replaced by tecto_xyz
vel_x=0; vel_y=0; vel_z=0

## tectonic velocity sigma, it will be replaced by tecto_xyz
vel_x_sig="~.0000"; vel_y_sig="~.0000"; vel_z_sig="~.0000"

## GINS station info header
header()
{
 printf "%-37.37s,  R=%11.2f,   f=%11.6f\n" "$ellipsoid" "$R" "$f"
 echo " station type site                    X (m)   sig.        Y (m)   sig.        Z(m)    sig.    Xp (m/an)      Yp (m/an)      Zp (m/an)    date  plaque source "
 echo "   -9999 M000 Earth c. of mass        0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                              010100               "
 echo "   -1998 EGB  Bias       (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                              010198 311298        "
 echo "    -199 EGB  Bias       (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                              010199 310199        "
 echo "     -61 EGC1 1/year Cos (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                                            dy ecc "
 echo "     -51 EGS1 1/year Sin (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                                            dy ecc "
 echo "     -62 EGC2 2/year Cos (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                                            dy ecc "
 echo "     -52 EGS2 2/year Sin (mm)         0.000 ~0.000        0.000 ~0.000        0.000 ~0.000                                                            dy ecc "
 echo "                                                                                                                                                             "
}

## GINS station info body_1
body_1()
{
 local domes_a=$1
 local domes_b=$2
 local site=$3
 local pos_x=$4
 local pos_x_sig=$5
 local pos_y=$6
 local pos_y_sig=$7
 local pos_z=$8
 local pos_z_sig=$9
 local vel_x=${10}
 local vel_x_sig=${11}
 local vel_y=${12}
 local vel_y_sig=${13}
 local vel_z=${14}
 local vel_z_sig=${15}
 local date_inf=${16}
 local tec_inf=${17}
 local src_inf=${18}
 printf "%3.3s%5.5s %4.4s %4.4s %11.11s%13.3f ~%5.3f%13.3f ~%5.3f%13.3f ~%5.3f %7.4f %6.6s %7.4f %6.6s %7.4f %6.6s %6.6s   %4.4s %6.6s\n" \
 " " "$domes_a" "$domes_b" "$site" " " "$pos_x" "$pos_x_sig" "$pos_y" "$pos_y_sig" "$pos_z" "$pos_z_sig" "$vel_x" "$vel_x_sig" "$vel_y" \
 "$vel_y_sig" "$vel_z" "$vel_z_sig" "$date_inf" "$tec_inf" "$src_inf"
}

## GINS station info body_2
body_2()
{
 local domes=$1
 local nb=$2
 local char=$3
 local site=$4
 local rec=$5
 local dx=$6
 local dy=$7
 local dz=$8
 local dx_sig=$9
 local dy_sig=${10}
 local dz_sig=${11}
 local ant=${12}
 local rad=${13}
 local ant_num=${14}
 local date_ins=${15}
 local date_rem=${16}
 local data_src=${17}
 printf " %5.5s%02d %4.4s %4.4s %-11.11s%13.3f%7.3f%13.3f%7.3f%13.3f%7.3f   %-16.16s%4.4s %-5.5s%17.17s%6.6s %6.6s %-5.5s\n" "$domes" "$nb" \
 "$char" "$site" "$rec" "$dx" "$dx_sig" "$dy" "$dy_sig" "$dz" "$dz_sig" "$ant" "$rad" "$ant_num" " " "$date_ins" "$date_rem" "$data_src"
}

## station list
for station_log in ????_????????.log; do echo ${station_log:0:4}; done | sort -u > $station_list

{
## print station file header
header
 
## latest station logs
## loop - station
while IFS= read -r line
do

 station=${line:0:4}
 
 station_log=$( printf "%s\n" ${station}_????????.log | sort | tail -1 )
 logfile=$tmp.$station_log         ## cleaned logfile (temporary)
 
 ## temporary files
 mon_inf=$tmp.mon-inf-$station_log ## monument information
 sta_inf=$tmp.sta-inf-$station_log ## station information
 ant_inf=$tmp.ant-inf-$station_log ## antenna information
 rec_inf=$tmp.rec-inf-$station_log ## receiver information
 
 ## cleaning of station log file from "cariage return" characters
 tr -d '\r' < $station_log > $logfile 2> /dev/null
 
 ## from here we work with the cleaned logfile(s) *************************************
 
 ## splitted log file
 # grep -A19 "1.   Site Identification of the GNSS Monument" $logfile > $mon_inf
 # grep -A13 "2.   Site Location Information" $logfile > $sta_inf
 grep -A19 "Site Identification of the GNSS Monument" $logfile > $mon_inf
 grep -A13 "Site Location Information" $logfile > $sta_inf
 
 ## receiver
 grep -A8 -e '^[3]\.[0-9]' -e '^[3]\.[0-9][0-9]' $logfile > $rec_inf
 split -l 10 $rec_inf $rec_inf.
 
 ## antenna
 grep -A13 -e '^[4]\.[0-9]' -e '^[4]\.[0-9][0-9]' $logfile > $ant_inf
 split -l 15 $ant_inf $ant_inf.
 
 ## site specific
 name="$( awk '/Four Character ID/ { print substr($0,33) }' $mon_inf )"; name="${name:0:4}"; if [ -z "$name" ]; then continue; fi
 site=$( echo "$name" | tr '[:lower:]' '[:upper:]' ); if [ -z "$site" ]; then continue; fi
 domes="$( awk '/IERS DOMES Number/ { print substr($0,33) }' $mon_inf )"
 if [ -z "$domes" ]; then domes="dddddddddd" ; else domes="$( trim $domes )"; fi; domes_a="${domes:0:5}"; domes_b="${domes:5}"
 if [ "$site" == "FLGY" ]; then domes="19673M001"; domes_a="${domes:0:5}"; domes_b="${domes:5}"; fi

 ## coordinates
 pos_x=$( awk '/X coordinate \(m\)/ { printf "%13.3f",substr($0,33,13) }' $sta_inf )
 if [ -z "$pos_x" ]; then continue; else pos_x="$( trim $pos_x )"; fi
 pos_y=$( awk '/Y coordinate \(m\)/ { printf "%13.3f",substr($0,33,13) }' $sta_inf )
 if [ -z "$pos_y" ]; then continue; else pos_y="$( trim $pos_y )"; fi
 pos_z=$( awk '/Z coordinate \(m\)/ { printf "%13.3f",substr($0,33,13) }' $sta_inf )
 if [ -z "$pos_z" ]; then continue; else pos_z="$( trim $pos_z )"; fi
 
 plh=( $( func_xyz2flh -x "$pos_x" -y "$pos_y" -z "$pos_z" ) )

 ## tectonic plate
 tec_p=$( awk '/Tectonic Plate/ { print toupper(substr($0,33)) }' $sta_inf )
 if [ -z "$tec_p" ]; then tec_p=" "; else tec_p="$( trim $tec_p )"; fi
 date_inf=010100         # 010100 should be reviewed      
 tec_inf="${tec_p:0:4}"  #        should be reviewed 
 src_inf=i05i05          # i05i05 should be reviewed

 occup=0

 ## to express date in seconds since 00:00:00 1970-01-01 UTC
 ## when we see reveiver data too, to see 
 ## overlaps of validity times of receivers and antennas
 #date +%s -d $date_ins

 body_1 "${domes_a:- }" "${domes_b:- }" "$site" "$pos_x" "$pos_x_sig" \
 "$pos_y" "$pos_y_sig" "$pos_z" "$pos_z_sig" "$vel_x" "$vel_x_sig" \
 "$vel_y" "$vel_y_sig" "$vel_z" "$vel_z_sig" "$date_inf" "$tec_inf" "$src_inf"

 ## initial loop over antenna files, counting antenna files for the given site
 ## to know later whether there is another antenna file after the concerned one
 ## reason: there is some stations i.e.: BAKO, ISBA where there is no antenna removal date
 ## however those antenna do not belong to the last occupation
 a=0; for ant_file in $ant_inf.*; do a=$(( $a + 1 )); done
 
 ## loop - antenna file
 aa=0
 for ant_file in $ant_inf.*
 do
  
  aa=$(( $aa + 1 ))
  
  ## occupation number
  occup="$(( $occup + 1 ))"
  
  ## antenna type
  #ant="$( awk '/Antenna Type/ { print substr($0,33,16) }' $ant_file )"
  ant="$( awk 'NR == 1 { print substr($0,33,16) }' $ant_file )"
  if [ -z "$ant" ]; then ant="aaaaaaaaaaaaaaaa"; else ant=$( trim $ant ); fi
  
  ## antenna radome type
  #rad="$( awk '/Antenna Type/ { print substr($0,49,4) }' $ant_file )"
  rad="$( awk 'NR == 1 { print substr($0,49,4) }' $ant_file )"
  if [ -z "$rad" ]; then rad=NONE; elif [ "${#rad}" -ne 4 ]; then rad=NONE; else rad="$( trim $rad )"; fi
  
  ## antenna serial number
  #ant_num="$( awk '/Serial Number/ { print substr($0,33,5) }' $ant_file )"
  ant_num="$( awk 'NR == 2 { print substr($0,33,5) }' $ant_file )"
  if [ -z "$ant_num" ]; then ant_num=" "; else ant_num="$( trim $ant_num )"; fi
  
  ## antenna eccentricity east
  #arp_e="$( awk '/Marker-\>ARP East Ecc\(m\)/ { print substr($0,33,8) }' $ant_file )"
  arp_e="$( awk '/ARP East/ { print substr($0,33,8) }' $ant_file )"
  #arp_e="$( awk 'NR == 6 { print substr($0,33,8) }' $ant_file )"
  ## this test added because of extracted values of (F8.4), same for east, north and up
  ## values in the logsheet are sometimes with unit: x.xxxm or x.xxx m
  arp_e="${arp_e//[mM]/}"
  arp_e="${arp_e//(F8.4)/}"
  if [ -z "$arp_e" ]; then arp_e=0; else arp_e="$( trim $arp_e )"; fi

  #case $arp_e in
  # \(*\)*|\(*|*\(*|*\(*\)*) arp_e=0 ;;
  #esac
  
  ## antenna eccentricity north  
  #arp_n="$( awk '/Marker-\>ARP North Ecc\(m\)/ { print substr($0,33,8) }' $ant_file )"
  arp_n="$( awk '/ARP North/ { print substr($0,33,8) }' $ant_file )"
  #arp_n="$( awk 'NR == 5 { print substr($0,33,8) }' $ant_file )"
  arp_n="${arp_n//[mM]/}"
  arp_n="${arp_n//(F8.4)/}"
  if [ -z "$arp_n" ]; then arp_n=0; else arp_n="$( trim $arp_n )"; fi
  
  ## antenna eccentricity up
  #arp_u="$( awk '/Marker-\>ARP Up Ecc. \(m\)/ { print substr($0,33,8) }' $ant_file )"
  arp_u="$( awk '/ARP Up/ { print substr($0,33,8) }' $ant_file )"
  #arp_u="$( awk 'NR == 4 { print substr($0,33,8) }' $ant_file )"
  arp_u="${arp_u//[mM]/}"
  arp_u="${arp_u//(F8.4)/}"
  if [ -z "$arp_u" ]; then arp_u=0; else arp_u="$( trim $arp_u )"; fi
  
  ant_date_ins="$( awk '/Date Installed/ { print substr($0,33,10) }' $ant_file )"
  if [ -z "$ant_date_ins" ]; then ant_date_ins="------"; else ant_date_ins="$( trim $ant_date_ins )"; fi
  #ant_date_ins_sec="$( date +%s -d $ant_date_ins )"
  yr_ins="${ant_date_ins:2:2}"
  mn_ins="${ant_date_ins:5:2}"
  dy_ins="${ant_date_ins:8:2}"
  date_ins="${dy_ins}${mn_ins}${yr_ins}"
  
  ant_date_rem="$( awk '/Date Removed/ { print substr($0,33,10) }' $ant_file )"; ant_date_rem=$( trim "${ant_date_rem//(/}" )

  case $ant_date_rem in
   [0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]) yr_rem="${ant_date_rem:2:2}"; mn_rem="${ant_date_rem:5:2}"
                                               dy_rem="${ant_date_rem:8:2}"; date_rem="${dy_rem}${mn_rem}${yr_rem}" ;;
                  *CCYY-MM-DD*|*[\ ][\ ][\ ]*) ant_date_rem_sec=$( date +%s -d $( date --iso-8601 ) ); date_rem="      " ;;
  esac

  if [[ -z "$date_rem" && $aa -lt $a ]]; then date_rem="------"; fi

  dxdydz=( $( func_xyz2enu -t xyz -p "${plh[1]}" -l "${plh[3]}" -x "$arp_e" -y "$arp_n" -z "$arp_u" | grep dX: ) )
  dx="${dxdydz[1]}"; dx_sig=0
  dy="${dxdydz[3]}"; dy_sig=0
  dz="${dxdydz[5]}"; dz_sig=0

  body_2 "${domes:- }" "$occup" "$char" "$site" "${rec:- }" "$dx" "$dy" "$dz" \
  "$dx_sig" "$dy_sig" "$dz_sig" "$ant" "${rad:-NONE}" "${ant_num:- }" \
  "$date_ins" "$date_rem" "$data_src"
  
  ## remove logsheet antenna part.x
  /bin/rm -f $ant_inf
  
  unset date_ins date_rem ant_date_rem rec ant rad ant_num dx dy dz arp_e arp_n arp_u
 ## end loop - antenna file
 done
 
 ## insert blanks between each station
 echo
 
## end loop - station 
done < $station_list
} > $HOME/gin/data/stations/$output

## timing problem

cd $HOME/gin/data/stations
{ echo $output; echo 3; echo 0; echo 1; } > $tmp.tecto_xyz-in
tecto_xyz < $tmp.tecto_xyz-in #&> /dev/null
cd - 1> /dev/null
