#!/bin/bash

lst=$1

if [ -z "$lst" ]; then echo "no list <$lst>"; exit 2; fi
if [ ! -s "$lst" ]; then echo "no list <$lst>"; exit 2; fi

#for sta in $( cat $lst ); do
#for sta in zimm; do #tlse vill 
for sta in $( cat $lst ); do
 sta_uppercase=$( echo $sta | tr [:lower:] [:upper:] )
 dir_out=ref33-grg
 ## commented out because find does not sort
 #find $HOME/gin/data/directeur -maxdepth 1 -type f -name "${sta_uppercase}*" -print -exec gins_process_v2.sh -f {} -w ${dir_out} -v \;
 find $HOME/gin/data/directeur -maxdepth 1 -type f -name "${sta_uppercase}*" -print | sort -n | xargs -I {} gins_process_v2.sh -f {} -w ${dir_out} -s 10 -v
done
