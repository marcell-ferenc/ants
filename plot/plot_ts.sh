#!/data/marcell/softwares/bash-4.2/bin/bash

#@
#@ USAGE  : gmt5ts.sh -f <tserie_file(s)> <options>
#@
#@ OPTIONS: 
#@          -c <component>             - plot desired component           [default:               all]
#@             - east|north|up|all
#@             - example: -c up
#@          -h                         - help
#@          -l <line_width/line_style> - modify line size and style       [default:    thinnest/solid]
#@             - line_width: faint|thinnest|thinner|thin|thick|thicker|
#@                           thickest|fat|fatter|fattest|obese
#@             - line_style: dot|dashed|dotdotdashed|solid
#@             - example: -l thin/dashed
#@          -p <point_size>            - modify point size                [default:             0.125]
#@             - point_size: 1-10
#@          -s                         - plot sigma                       [default:                no]
#@          -t <title>                 - modify title                     [default: name of 1st input]
#@             - example: -t "This is the plot title"
#@          -v                         - verbose_mode                     [default:                no]
#@          -w                         - window_type; wide if it is used  [default:            narrow]
#@          -y <ymin/ymax>             - modify y axis limits             [default:            -25/25]
#@          -z <tmin/tmax>             - zoom on time axis                [default:                no]
#@             - expected tmin or tmax format: 
#@               YYYY|YYYY-MM-DD|YYYY-MM-DDThh:mm:ss
#@             - example: -z 2010-02-01T00:00:00/2010-04-01T23:59:59
#@
#@ INPUT  : expected input format: TIME, EAST, EAST_sig, NORTH, NORTH_sig, UP, UP_sig
#@
#@ NOTE   : - use double quotes to plot multiple time series -f "tserie1 tserie2 ... tserieN"
#@          - in case of typo error of any option, the default values will be used
#@
#* by     : marcell.ferenc@cnam.fr or marcell.ferenc.uni@gmail.com
#*

## scriptname
scrn=${0##*/}
scrn=${scrn%.sh}

## temporary filename
tmp=$scrn.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir gmt.conf gmt.history .gmt* 2> /dev/null
 ## restore default value 
 trap EXIT; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source
source $HOME/bin/func/general.func
source $HOME/bin/func/ts.func

## help
case "$#" in 0) usage $0; exit ;; esac

## function for color generation
hue()
{
 local id=$1
 local grt=$( gmtmath -Q 1 5 SQRT ADD 2 DIV = )
 gmtmath -Q $id $grt MUL $id $grt MUL FLOOR SUB 256 MUL FLOOR =
}

## 20 well separated colors
#rgb=( $( makecpt -Cwysiwyg | sed '/[BFN\#]/d' | awk '{ print $2 }' ) )
rgb=( "" red blue green black darkred darkblue darkgreen maroon )

## default variable
y0=-25
yn=25
cmax_ini=4
window=narrow
sigma=no
v=no
lwidth=thinnest
df_lwidth=$lwidth
lstyle=solid
df_lstyle=$lstyle  ## default_line_size
psize=( "" 0.0125 0.05 0.125 0.2 0.25 0.3 0.5 0.75 1 2 )
ss=${psize[3]}
mkdir -p $HOME/log
id=$( date "+%Y%m%d_%H%M%S" )
log=$HOME/log/${scrn}-${USER}-$id.log
## colors
#c=( "" red blue green black maroon pink purple darkgreen yellow darkgray brown )
## symbols
sym_pat="c s d t a c s d t a"
s=( "" c s d t a c s d t a c s d t a c s d t a c s d t a c s d t a c s d t a c \
s d t a c s d t a c s d t a c s d t a c s d t a c s d t a c s d t a c s d t a c \
s d t a c s d t a c s d t a )
cols="1 3 5"
comp=( "" East "" North "" Up )
#comp=( "" pressure "" pressure "" pressure )
un=mm

## gmt5 related settings
fmt="--FORMAT_FLOAT_OUT"

## list of options the program will accept
optstring=c:f:l:p:y:t:z:u:dsvwh

## interpret options
while getopts $optstring opt
do
 case $opt in
  c) case $OPTARG in
        all) ;;
       east) cols=1 ;;
      north) cols=3 ;;
         up) cols=5 ;;
     esac ;;
  z) zoom=$OPTARG
     case $zoom in
      */*) t0=${zoom%/*}; tn=${zoom#*/} 
           t0_inf=$( isdate $t0 &> /dev/null; echo $? )
           tn_inf=$( isdate $tn &> /dev/null; echo $? )
           if [[ $t0_inf -ne 0 && $tn_inf -ne 0 ]]
           then
            color_txt bold red "${0##*/}: error: zoom: <$t0> or <$tn>"; exit 2
           fi ;;
        *) color_txt bold red "${0##*/}: error: zoom: <$zoom> instead of <t0/tn>"; exit 2 ;;
     esac ;;
  f) tseries=( $OPTARG ) ;;
  l) line=$OPTARG; lwidth="${line%%/*}"; lstyle="${line##*/}"
     case $lwidth in
      faint|thinnest|thinner|thin|thick) ;;
      thicker|thickest|fat|fatter|fattest|obese) ;;
      *) lwidth=$df_lwidth ;;
     esac
     case $lstyle in
      dot) lstyle=.       ;; dashed) lstyle=- ;; dotdotdashed) lstyle=..- ;;
      solid) lstyle=solid ;; *) lstyle=$df_lstyle ;;
     esac ;;
  h) usage $0; exit ;;
  w) window=wide ;;
  p) case $OPTARG in
      [1-9]) ss=${psize[$OPTARG]} ;;
          *) ss=$ss ;;
     esac ;;
  s) sigma=yes ;;
  v) v=yes ;;
  u) un=$OPTARG ;;
  y) y=$OPTARG
     case $y in
      */*) y0=${y%/*}; yn=${y#*/} 
           y0_inf=$( isnum $y0 &> /dev/null; echo $? )
           yn_inf=$( isnum $yn &> /dev/null; echo $? )
           if [[ $y0_inf -ne 0 && $yn_inf -ne 0 ]]
           then
            color_txt bold red "${0##*/}: error: y: <$y0> or <$yn>"; exit 2
           fi ;;
        *) color_txt bold red "${0##*/}: error: <$zoom> instead of <y0/yn>"; exit 2 ;;
     esac ;;
  t) title=$OPTARG; op=${title// /_}; op=${op,,} ;;
  d) debug=yes; set -x ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## test files
if [ -z "$tseries" ]; then
 color_txt bold red "${0##*/}: error: -f <tserie(s)> option missing"; exit 2; fi

for tserie in ${tseries[@]}; do
 if [ -s $tserie ]; then
  tsp=${tserie%/*}     ## time serie path
  tsn=${tserie##*/}    ## time serie name
  tsw=$tmp.$tsn        ## time serie work version
  info=( $( istab $tserie ) )
  if [ "${info[0]}" == "is_data_table" ]; then
   case "${info[1]}" in
    7) /bin/cp -f $tserie $tsw
       is_item+=( "$tsw" )
       ts_conv_dt.sh -f $tsw -o iso
       awk '{ print $1 }' $tsw | gmtinfo -C -f0T >> $tmp.t
       if [ "$v" == "yes" ]; then
        color_txt bold green "${0##*/}: exist: <$tserie>"; fi
       ;;
    2|3|4) set_col $tserie > $tsw
           is_item+=( "$tsw" )
           ts_conv_dt.sh -f $tsw -o iso 
           awk '{ print $1 }' $tsw | gmtinfo -C -f0T >> $tmp.t
           if [ "$v" == "yes" ]; then
            color_txt bold green "${0##*/}: exist: <$tserie>"; fi
           ;;
    *) no_item+=( "$tserie" )
       if [ "$v" == "yes" ]; then
        color_txt bold red "${0##*/}: error: wrong format: <$tserie>"
       fi ;; 
   esac
  else
   no_item+=( "$tserie" )
   if [ "$v" == "yes" ]; then
    color_txt bold red "${0##*/}: error: wrong format: <$tserie>"; fi
  fi
 else
  no_item+=( "$tserie" )
  if [ "$v" == "yes" ]; then
   color_txt bold red "${0##*/}: error: missing file: <$tserie>"; fi
 fi
done

if [ "${#is_item[@]}" -eq 0 ]; then
 color_txt bold red "${0##*/}: error: not accepted input files"; exit 2; fi

## test output
if [ -z "$op" ]; then op=${is_item[0]}; op=${op#$tmp.}; op=${op%.*}; title=$op; fi

## ps_name
ext=_plot.ps; ps=${op}$ext

## plot size
JxJy_ratio=7.5 #$( gmtmath -Q 16 9 DIV = ) #
Jx=20
Jy=$( gmtmath -Q $Jx $JxJy_ratio DIV = )
J="-JX${Jx}c/${Jy}c"
dY=$( gmtmath -Q $Jy 1.05 MUL = )
X="-Xa3c"
Y_ini=10
Y=( "0" -Ya${Y_ini}c "2" -Ya$( gmtmath -Q $Y_ini 1 $dY MUL SUB = )c "4" -Ya$( gmtmath -Q $Y_ini 2 $dY MUL SUB = )c )

## legend width
leg_width=$( gmtmath -Q $Jx $cmax_ini DIV CEIL = )
cmax=${#is_item[@]}
cmax=$( { echo $cmax; echo $cmax_ini; } | gmtmath STDIN LOWER -Sl = )

## tinf contains: min_of_mints max_of_mints min_of_maxts max_of_maxts
tinf=( $( awk '{ print $1 }' $tmp.t | gmtinfo -C -f0T ) \
$( awk '{ print $2 }' $tmp.t | gmtinfo -C -f0T ) )

## plot boundaries
case $window in
 narrow) t0=${t0:-${tinf[1]}}; tn=${tn:-${tinf[2]}} ;;
   wide) t0=${t0:-${tinf[0]}}; tn=${tn:-${tinf[3]}} ;;
esac

## plot boundaries
R="-R$t0/$tn/$y0/$yn"

## test x axis values
for val in "$t0" "$tn"; do
 if [ $( isdate $t0 &> /dev/null; echo $? ) -ne 0 ]; then
  color_txt bold red "${0##*/}: error: wrong x axis value <$val>"; exit 2; fi
done

## test y axis values
for val in "$y0" "$yn"; do
 if [ $( isnum $val &> /dev/null; echo $? ) -ne 0 ]; then
  color_txt bold red "${0##*/}: error: wrong y axis value <$val>"; exit 2; fi
done

## y axis
yrange=$( gmtmath -Q $yn $y0 SUB CEIL = )

## information for verbose mode
if [ "$v" == "yes" ]; then
 color_txt bold blue "${0##*/}: min_of_min_T: ${tinf[0]}"
 color_txt bold blue "${0##*/}: max_of_min_T: ${tinf[1]}"
 color_txt bold blue "${0##*/}: min_of_max_T: ${tinf[2]}"
 color_txt bold blue "${0##*/}: max_of_max_T: ${tinf[3]}"
 color_txt bold blue "${0##*/}: title       : $title"
 color_txt bold blue "${0##*/}: zoom        : $t0 - $tn"
 color_txt bold blue "${0##*/}: window      : $R"
 color_txt bold blue "${0##*/}: line_style  : $lstyle"
 color_txt bold blue "${0##*/}: line_widht  : $lwidth"
 color_txt bold blue "${0##*/}: point_size  : $ss"
fi

## gmt settings
{
 gmt set FORMAT_TIME_PRIMARY_MAP abbreviated
 #gmt set MAP_TITLE_OFFSET 1
 #gmt set FONT_LABEL +14p,Helvetica-Bold
 gmt set FONT_LABEL +12p,Helvetica
 #gmt set FONT_TITLE +14p,Helvetica-Bold
 gmt set FONT_TITLE +12p,Helvetica
 #gmt set FONT_ANNOT_PRIMARY +11p,Helvetica-Bold
 gmt set FONT_ANNOT_PRIMARY 6p,Helvetica
 #gmt set FONT_ANNOT_SECONDARY +12p,Helvetica-Bold
 gmt set FONT_ANNOT_SECONDARY 6p,Helvetica
 gmt set PS_MEDIA A4
 # gmt set MAP_FRAME_PEN thickest,black
 gmt set MAP_FRAME_PEN thin,black
 #gmt set MAP_TICK_LENGTH_PRIMARY 0.05
 gmt set MAP_TICK_LENGTH_PRIMARY 0.1
 #gmt set MAP_TICK_LENGTH_SECONDARY 0.1
 gmt set MAP_TICK_LENGTH_SECONDARY 0.2
 # gmt set MAP_GRID_PEN_PRIMARY thick,grey
 gmt set MAP_GRID_PEN_PRIMARY thin,grey
 # gmt set MAP_GRID_PEN_SECONDARY thick,grey
 gmt set MAP_GRID_PEN_SECONDARY thin,grey
}

## test output
if [ -z "$op" ]; then op=${is_item[0]}; op=${op##*/}; op=${op%.*}; title=$op; fi

## initialize plot legend
echo N $cmax > $tmp.legend

## initialize post script
gmt psxy $R $J -T -K > $ps

## automated x axis scale
awk -v b=$t0 -v e=$tn '$1 >= b && $1 <= e { print $0 }' ${is_item[0]} > $tmp.ts-zoom
ts_conv_dt.sh -f $tmp.ts-zoom -o yr
x_axis=( $( def_gmt_scale $tmp.ts-zoom ) )

ya=a$( gmtmath -Q $yrange 0.2 MUL ${fmt}=%.1f = )
yg=g$( gmtmath -Q $yrange 0.2 MUL ${fmt}=%.1f = )
yf=f$( gmtmath -Q $yrange 0.1 MUL ${fmt}=%.1f = )

if [ "${#cols}" -eq 5 ]; then
 for col in $cols; do
  ## zoom
  if [ -n "$zoom" ]; then
   #gmt psbasemap $R $J -Bpxa1dg1df1d -Bpya4f1g4 -Bsxa3D -Bsy+l"Up [mm]" \
   case $col in
    1) gmt psbasemap $R $J ${x_axis[@]:0:2} -Bpy${ya}${yf}${yg} -Bsy+l"${comp[$col]} [$un]" -BWsne+t"time serie: @;blue;${title^^}@;;" -K -O $X ${Y[$col]} ${x_axis[@]:2} ${fmt}=%.1f >> $ps ;;
    3) gmt psbasemap $R $J ${x_axis[@]:0:2} -Bpy${ya}${yf}${yg} -Bsy+l"${comp[$col]} [$un]" -BWsne -K -O $X ${Y[$col]} ${x_axis[@]:2} ${fmt}=%.1f >> $ps ;;
    5) gmt psbasemap $R $J ${x_axis[@]:0:2} -Bpy${ya}${yf}${yg} -Bsy+l"${comp[$col]} [$un]" -BWSne -K -O $X ${Y[$col]} ${x_axis[@]:2} ${fmt}=%.1f >> $ps ;;
   esac
  else
   #gmt psbasemap $R $J -Bpxa7Rg7Rf1d -Bpya4f1g4 -Bsxa1O -Bsy+l"Up [mm]" \
   case $col in
    1) gmt psbasemap $R $J ${x_axis[@]:0:2} -Bpy${ya}${yf}${yg} -Bsy+l"${comp[$col]} [$un]" -BWsne+t"time serie: @;blue;${title^^}@;;" -K -O $X ${Y[$col]} ${x_axis[@]:2} ${fmt}=%.1f >> $ps ;;
    3) gmt psbasemap $R $J ${x_axis[@]:0:2} -Bpy${ya}${yf}${yg} -Bsy+l"${comp[$col]} [$un]" -BWsne -K -O $X ${Y[$col]} ${x_axis[@]:2} ${fmt}=%.1f >> $ps ;;
    5) gmt psbasemap $R $J ${x_axis[@]:0:2} -Bpy${ya}${yf}${yg} -Bsy+l"${comp[$col]} [$un]" -BWSne -K -O $X ${Y[$col]} ${x_axis[@]:2} ${fmt}=%.1f >> $ps ;;
   esac
  fi
 done
else
 for col in $cols; do
  ## zoom
  if [ -n "$zoom" ]; then
   #gmt psbasemap $R $J -Bpxa1dg1df1d -Bpya4f1g4 -Bsxa3D -Bsy+l"Up [mm]" \ 
   gmt psbasemap $R $J ${x_axis[@]:0:2} -Bpy${ya}${yf}${yg} -Bsy+l"${comp[$col]} [$un]" -BWSne+t"time serie: @;blue;${title^^}@;;" -K -O $X ${Y[$col]} ${x_axis[@]:2} ${fmt}=%.1f >> $ps
  else
   #gmt psbasemap $R $J -Bpxa7Rg7Rf1d -Bpya4f1g4 -Bsxa1O -Bsy+l"Up [mm]" \ 
   gmt psbasemap $R $J ${x_axis[@]:0:2} -Bpy${ya}${yf}${yg} -Bsy+l"${comp[$col]} [$un]" -BWSne+t"time serie: @;blue;${title^^}@;;" -K -O $X ${Y[$col]} ${x_axis[@]:2} ${fmt}=%.1f >> $ps
  fi
 done
fi

## plot tseries
i=0
for tserie in ${is_item[@]}; do
 ## ${c[$i]} to $c
 i=$(( $i + 1 )); name=${tserie#$tmp.} #; name=${name:0:12}
 case $i in
  [1-8]) c="${rgb[$i]}" ;;
      *) c="$( hue $i )-0.5-0.90" ;;
 esac
 gap=( $( def_sampling $tserie ) )
 for col in $cols
 do
  gmt psxy $tserie $R $J -W$lwidth,$c,$lstyle -K -O -i0,$col $X ${Y[$col]} -gx$(( ${gap[0]} + 1 )) --TIME_UNIT=${gap[2]} >> $ps
  case $sigma in
   yes) gmt psxy $tserie $R $J -G$c -S${s[$i]}$ss -Ey -K -O -i0,$col,$(( $col + 1 )) $X ${Y[$col]} >> $ps 2>> $log ;;
    no) gmt psxy $tserie $R $J -G$c -S${s[$i]}$ss -K -O -i0,$col $X ${Y[$col]} >> $ps ;;
  esac
 done
 #$ss
 echo "S 0.15 ${s[$i]} 0.25 $c 0.3p,$c 0.35 @;${c};${name^^}@;;" \
>> $tmp.legend 
done

## finalize legend
gmt pslegend $tmp.legend $R $J -D$t0/$y0/$(( $cmax * $leg_width ))/LT/0/1.5 -K -O $X ${Y[$col]} >> $ps

## close the ps file at the end of the script
gmt psxy -R -J -T -O >> $ps

## convert into png
gmt ps2raster -A -E300 -Tg $ps
