#@ The <ants> acronym stands for ANalyse Time Series. This repository contains 
#@ scripts what I use for GNSS data processing and analyses.

#@ Some of them are experimental or still in development. If you don't see any
#@ warning or note about the given script that means it is operational. Scripts
#@ that are not in their final version have an <-dev.sh> ending. 

#@ Each subdirectory contains a <README.txt> file what explains
#@ the origin and the purpose of the programs, scripts found in that directory.

#@------------------------------------------------------------------------------
#@ To make (almost) everything to work type the following:

cd
git clone https://marcell-ferenc@bitbucket.org/marcell-ferenc/ants.git
cd ants
./setup_ants.sh

#@ and add to the PATH in the <$HOME/.bashrc> file the content of the
#@ <$HOME/ants/add_to_path.txt> file, example:

PATH=$PATH:\
$HOME/ants/ts:\
...

#@ By now, you should be able to use all what this repository offers.

#@ Best wishes,
#@ Marcell Ferenc
#@ marcell dot ferenc dot uni at gmail dot com
