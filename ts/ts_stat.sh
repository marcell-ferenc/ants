#!/bin/bash

#@
#@ USAGE  : ts_stat.sh -f <tserie(s)> <options>
#@
#@ OPTIONS: -d - debug
#@          -v - verbose mode
#@          -h - help
#@
#@ TASK   : Calculate statistics of time serie(s) using GMT.
#@
#@ NOTE   : Tserie(s) could be multiple filenames separated by spaces 
#@          and use double quotes "tserie_1 tserie_2 ... tserie_n".
#@          The input time serie(s) have to have the
#@          same number of columns: 2,3,4 or 7.
#@
#@ OUTPUT : <ts_stat.log>
#@          Results are catenated to this file in the current directory
#@
#* by     : marcell dot ferenc dot uni at gmail dot com

## root_dir
root_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

## source ts.conf
if [ ! -s $root_dir/ts.conf ]; then exit 2; else source $root_dir/ts.conf; fi

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
case "$#" in 0) usage $0 ;; esac

## test programs
test_prog gmtmath ts_conv_dt.sh paste awk grep
test_gmt5

## default variable
v=no
LC_NUMERIC=C ## for decimal separator in floating numbers
log=$scrn.log

## list of options the program will accept
optstring=f:dvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  f) tss=( $OPTARG ) ;;
  d) set -x; debug=-d ;;
  v) v=yes; verbose=-v ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

##------------------------------------------------------------------------------
## test time series
##------------------------------------------------------------------------------
if [ ${#tss[@]} -eq 0 ]; then
 color_txt bold red "${0##*/}: error: -f <time_serie(s)> missing"; exit 2
else
 for ts in ${tss[@]}; do
  ## test ts
  if [ ! -s $ts ]; then
   color_txt bold red "${0##*/}: error: time serie missing: <$ts>"; continue; fi
  info=( $( istab $ts ) )
  ## test ts format
  if [[ "${info[0]}" != "is_data_table" || "${info[3]}" -lt 2 ]]; then
   color_txt bold red "${0##*/}: error: wrong format: <$ts>"; continue; fi
  ## populate ts list
  tspath=${ts%/*}      # ts path
  tsname=${ts##*/}     # ts name
  tswrkv=$tmp.$tsname  # ts work version
  set_col $ts > $tswrkv
  ts_convert_date.sh -o iso $tswrkv
  is_item+=( "$tswrkv" )
  if [ "$v" == "yes" ]; then color_txt bold green "${0##*/}: exist: <$ts>"; fi
 done  
fi

if [ ${#is_item[@]} -eq 0 ]; then
 color_txt bold red "${0##*/}: error: number of good serie(s): <${#is_item[@]}>"
 exit 2; fi
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## GMT 5 version dependent settings
##------------------------------------------------------------------------------
fmt="--FORMAT_FLOAT_OUT"
fmt_dte_in="--FORMAT_DATE_IN"
fmt_clk_in="--FORMAT_CLOCK_IN"
nan="--IO_NAN_RECORDS" ;;
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## calculate statistics
##------------------------------------------------------------------------------
for ts in ${is_item[@]}; do
 sta=${ts#$tmp.}
 sta=${sta:0:4}
 smp=( $( def_sampling $ts ) )
 smp="${smp[0]}-${smp[1]}"
 echo "${0##*/} ${ts#$tmp.} $smp $sta MIN $( gmtmath $ts LOWER MEAN -Sf -f0T ${fmt}=%8.2f = )"
 echo "${0##*/} ${ts#$tmp.} $smp $sta MAX $( gmtmath $ts UPPER MEAN -Sl -f0T ${fmt}=%8.2f = )"
 echo "${0##*/} ${ts#$tmp.} $smp $sta RNG $( gmtmath -Ca $ts UPPER MEAN -Sl $ts LOWER MEAN -Sf SUB -f0T -Sl ${fmt}=%8.2f ${fmt_dte_in}=yyyy-mm-dd ${fmt_clk_in}=hh:mm:ss --TIME_EPOCH=2000-01-01T00:00:00 = )"
 echo "${0##*/} ${ts#$tmp.} $smp $sta AVE $( gmtmath -Ca $ts MEAN -Sl -f0T ${fmt}=%8.2f ${fmt_dte_in}=yyyy-mm-dd ${fmt_clk_in}=hh:mm:ss = )"
 echo "${0##*/} ${ts#$tmp.} $smp $sta MED $( gmtmath -Ca $ts MED -Sl -f0T ${fmt}=%8.2f ${fmt_dte_in}=yyyy-mm-dd ${fmt_clk_in}=hh:mm:ss = )"
 echo "${0##*/} ${ts#$tmp.} $smp $sta STD $( gmtmath $ts STD -Sl -f0T ${fmt}=%8.2f ${fmt_dte_in}=yyyy-mm-dd ${fmt_clk_in}=hh:mm:ss = )"
 echo "${0##*/} ${ts#$tmp.} $smp $sta RMS $( gmtmath $ts SQR MEAN SQRT -Sl -f0T ${fmt}=%8.2f ${fmt_dte_in}=yyyy-mm-dd ${fmt_clk_in}=hh:mm:ss = )"
 echo "#"
done | awk --posix '{ printf "%s: %-40.40s %13.13s %4.4s %3.3s %19.19s %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f\n",$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12 }' | sed 's/nan/NaN/g;s/#.*/#/g' >> $log
##------------------------------------------------------------------------------
