#!/bin/bash

#@
#@ USAGE  : ts_conv_dt.sh -f <ts> <options>
#@
#@ OPTIONS: 
#@          -o <fmt_out> - output date format [iso, yr, jd, mjd]
#@          -v           - verbose mode
#@          -d           - debug
#@          -h           - help
#@
#@ TASK   : Covert date and time format of the time series.
#@
#@ NOTE   : The default output date format is iso.
#@
#* by     : marcell dot ferenc dot uni at gmail dot com

## root_dir
root_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

## source ts.conf
if [ ! -s $root_dir/ts.conf ]; then exit 2; else source $root_dir/ts.conf; fi

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
case "$#" in 0) usage $0 ;; esac

## test programs
test_prog yr2cal.bin cal2jul.bin jul2yr.bin paste awk grep

## list of options the program will accept
opt_str=f:o:dvh

## interpret options
while getopts $opt_str opt; do
 case $opt in
  f) ts=$OPTARG ;;
  o) fmt_out=$OPTARG ;;
  v) v=yes ;;
  d) set -x ;;
  h) usage $0 ;;
 esac
done
shift "$(( $OPTIND - 1 ))"

## test output format ----------------------------------------------------------
fmt_out=${fmt_out:-iso}
case $fmt_out in
 iso|yr|jd|mjd) ;;
 *) color_txt bold red "$errm wrong option: <$fmt_out>"; exit 2 ;;
esac
##------------------------------------------------------------------------------

## test ts ---------------------------------------------------------------------
if [ -z $ts ]; then color_txt bold red "$errm missing option: -f <ts>"; exit 2; fi
if [[ ! -s $ts || ! -r $ts || -d $ts ]]; then color_txt bold red "$errm missing file: <$ts>"; exit 2; fi
##------------------------------------------------------------------------------

## ts work version
tsw=$tmp.t
tr -d '\r' < $ts > $tsw

## verbose information
case $v in yes) echo "$infm input: $ts"; echo "$infm temp: $tsw" ;; esac

## test fmt_dte ----------------------------------------------------------------
grep -m 1 -Ew "^[0-9]{4}\-[0-9]{2}\-[0-9]{2}T.*" $tsw &>/dev/null
case $? in 0) fmt_dte=iso; dc=2 ;; esac

grep -m 1 -Ew '^[0-9]{4}\.[0-9]{1,}.*' $tsw &>/dev/null
case $? in 0) fmt_dte=yr; dc=2 ;; esac

grep -m 1 -Ew '^[0-9]{7}\.[0-9]{1,}.*' $tsw &>/dev/null
case $? in 0) fmt_dte=jd; dc=2 ;; esac

grep -m 1 -Ew '^[0-9]{5}\.[0-9]{1,}.*' $tsw &>/dev/null
case $? in 0) fmt_dte=mjd; dc=2 ;; esac

#grep -m 1 -Ew \
#'^[0-9]{4}[[:space:]][0-9]{1,3}[[:space:]][0-9]{1,2}[[:space:]].*' $ts &>/dev/null
#case $? in 0) fmt_dte=ydoyep; dc=4 ;; esac

if [ -z "$fmt_dte" ]; then color_txt bold red "$errm unknown time format: <$ts>"; exit 2; fi
##------------------------------------------------------------------------------

## print all data column
awk -v d=$dc '{for(i=d;i<=NF;i++) printf("%s%s",$i,(i==NF)?"\n":OFS)}' $tsw > $tmp.d
if [[ ! -s $tmp.d || $(wc -l < $tmp.d) -eq 0 ]]; then /bin/rm -f $tmp.d ;fi

## date time converion ---------------------------------------------------------
case $fmt_dte in
 
 ## input format: decimal year -------------------------------------------------
 yr) case $fmt_out in
      iso) awk '{ print $1 }' $tsw | yr2cal.bin | awk '{ print $3 }' > $tmp ;;
       yr) case $v in yes) echo "$infm <$ts> already in <$fmt_out>" ;; esac; exit 0 ;;
       jd) awk '{ print $1 }' $tsw | yr2cal.bin | awk '{ print $1 }' > $tmp ;;
      mjd) awk '{ print $1 }' $tsw | yr2cal.bin | awk '{ printf "%.10f\n",$1-2400000.5 }' > $tmp ;;
     esac ;;
          
 ## input format: isodate ------------------------------------------------------
 iso) case $fmt_out in
       iso) case $v in yes) echo "$infm <$ts> already in <$fmt_out>";; esac; exit 0 ;;
        yr) awk '{ print substr($1,1,4),substr($1,6,2),substr($1,9,2),substr($1,12,2),substr($1,15,2),substr($1,18,2) }' $tsw | cal2jul.bin | jul2yr.bin | awk '{ print $2 }' > $tmp ;;
        jd) awk '{ print substr($1,1,4),substr($1,6,2),substr($1,9,2),substr($1,12,2),substr($1,15,2),substr($1,18,2) }' $tsw | cal2jul.bin > $tmp ;;
       mjd) awk '{ print substr($1,1,4),substr($1,6,2),substr($1,9,2),substr($1,12,2),substr($1,15,2),substr($1,18,2) }' $tsw | cal2jul.bin | awk '{ printf "%.10f\n",$1-2400000.5 }' > $tmp ;;
      esac ;;
 
 ## input format: julian date --------------------------------------------------
 jd) case $fmt_out in
      iso) awk '{ print $1 }' $tsw | jul2yr.bin | awk '{ print $3 }' > $tmp ;;
       yr) awk '{ print $1 }' $tsw | jul2yr.bin | awk '{ print $2 }' > $tmp ;;
       jd) case $v in yes) echo "$infm <$ts> already in <$fmt_out>" ;; esac; exit 0 ;;
      mjd) awk '{ print $1 }' $tsw | jul2yr.bin | awk '{ printf "%.10f\n",$1-2400000.5 }' > $tmp ;;
     esac ;;

 ## input format: modified julian date -----------------------------------------
 mjd) case $fmt_out in
       iso) awk '{ printf "%.10f\n",$1+2400000.5 }' $tsw | jul2yr.bin | awk '{ print $3 }' > $tmp ;;
        yr) awk '{ printf "%.10f\n",$1+2400000.5 }' $tsw | jul2yr.bin | awk '{ print $2 }' > $tmp ;;
        jd) awk '{ printf "%.10f\n",$1+2400000.5 }' $tsw > $tmp ;;
       mjd) case $v in yes) echo "$infm <$ts> already in <$fmt_out>" ;; esac; exit 0 ;;
      esac ;;

esac
##------------------------------------------------------------------------------ 

## date conversion information -------------------------------------------------
case $v in yes) echo "$infm convert: <$fmt_dte> to <$fmt_out> in <$ts>" ;; esac
#if [[ -s $tmp && -r $tmp ]]; then if [[ -s $tmp.d && -r $tmp.d ]]; then paste $tmp $tmp.d > $ts; fi; fi
if [ -s $tmp.d ]; then paste $tmp $tmp.d > $ts; else /bin/cp -f $tmp $ts; fi
##------------------------------------------------------------------------------