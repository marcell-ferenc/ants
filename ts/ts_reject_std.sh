#!/bin/bash

#@
#@ USAGE  : ts_reject_std.bash -f <tserie> <options>
#@
#@ OPTIONS: -n <n_std>  - <n_std> * STDEV   [default: 5 * STDEV]
#@          -d          - debug             [default:       off]
#@          -h          - help              [default:       off]
#@          -v          - verbose mode      [default:       off]
#@
#@ TASK   : Remove records which are above <n_std> times the standard deviation
#@          Do the cleaning only if 80 percent of the records would rest
#@
#@ NOTE   : - <tserie> can have any number of columns
#@          - <tserie> could be multiple filenames separated by spaces 
#@            and use double quotes "tserie_1 tserie_2 ... tserie_n"
#@
#@ OUTPUT : - <tserie>-cleaned.txt
#@          - ts_reject_std.log
#@            this is a logfile which contains how many record was removed
#@
#* by     : marcell.ferenc@cnam.fr
#*

## test GMT installation
type gmtmath &> /dev/null
if [ $? -ne 0 ]; then echo "GMT is needed for $0 script"; exit 2; fi

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir .gmt* gmt.conf gmt.history 2> /dev/null
 ## restore default value 
 trap EXIT; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
. $HOME/bin/func/general.func
. $HOME/bin/func/ts.func

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## default variable
log=$scriptname.log
v=no
LC_NUMERIC=C ## for decimal separator in floating numbers
n_std=5

## list of options the program will accept
optstring=f:n:dvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  d) set -x ;;
  f) files=( $OPTARG ) ;;
  n) n_std=$OPTARG ;;
  v) v=yes ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## test n_std
case $n_std in
 1|2|3|4|5) n_std="$n_std MUL";;
         *) color_txt bold red "not accepted <n_std> value [$n_std]"; exit 2 ;;
esac

## test files
if [ -z "$files" ]; then
 color_txt bold red "-f <tserie> option is required"; exit 2
else
 for file in ${files[@]}
 do
  if [ -s $file ]; then
   info=( $( istab $file ) )
   if [ "${info[0]}" == "is_data_table" ]; then
    is_item+=( "$file" )
    [ "$v" == "yes" ] && color_txt bold green "$file - exist"
   else
    no_item+=( "$file" )
    [ "$v" == "yes" ] && color_txt bold red "$file - not a data table"
   fi
  else
   no_item+=( "$file" )
   [ "$v" == "yes" ] && color_txt bold red "$file - does not exist or empty"
  fi
  if [ -z "$is_item" ]; then
   color_txt bold red "not accepted input: ${no_item[@]}" ; exit 2; fi
 done  
fi

## remove logfile if exists due to previous run
/bin/rm -f $log 2> /dev/null

gmt_vers=$( sample1d &> $tmp.gmt-version; \
awk 'NR==1 { print substr($2,1,1) }' $tmp.gmt-version )
case $gmt_vers in
 4) gmt_format=--D_FORMAT=%.5f ;;
 5) gmt_format=--FORMAT_FLOAT_OUT=%.5f ;;
esac

## loop - existing input
for file in ${is_item[@]}
do

 ## info of output data
 info_i=( $( istab $file ) )
 if [ $? -eq 2 ]; then continue; fi
 min_data=$( gmtmath -Q ${info_i[3]} 0.8 MUL FLOOR = )

 ## tresholds
 gmtmath $file MED -f0T = \
 | gmtmath STDIN $file STD $n_std ADD -f0T = $tmp.std_p
 wc -l $tmp.std_p
 gmtmath $file MED -f0T = \
 | gmtmath STDIN $file STD $n_std SUB -f0T = $tmp.std_n
 wc -l $tmp.std_n
 gmtmath $file $tmp.std_n $tmp.std_p INRANGE -f0T = $tmp.inrange
 wc -l $tmp.inrange

 ## inrange decision
 gmtmath $file $tmp.inrange MUL -f0T = $tmp
 
 ## remove records where any of the values are zero [0 or -0] ==> not_in_range
 for (( col=1; col<=${info_i[1]}; col++ )); do
  awk -v c=$col '$c!=0 { print $0 }' $tmp > $tmp.cl; /bin/mv -f $tmp.cl $tmp
 done

 ## information
 info_o=( $( istab $tmp ) )
 if [ $? -eq 2 ]; then
  color_txt bold red "$file - no cleaning (no remaining data)"
  info_o=( 0 0 0 0 ); fi
 removed=$(( ${info_i[3]} - ${info_o[3]} ))

 ## result file name
 out=${file//-cleaned*/}-cleaned.txt

 ## test number of data after removal
 if [[ ${info_i[3]} -ge $min_data && ${info_o[3]} -ge $min_data ]]
 then
  ## final output
  /bin/mv -f $tmp $out
  [ "$v" == "yes" ] && color_txt bold green "$file - cleaned"
 else
  [ "$v" == "yes" ] && color_txt bold red "$file - not cleaned"
  txt="(not cleaned)"
 fi
 
 ## format numbers for plotting
 printf -v rm_data "%5d" $removed; printf -v in_data "%5d" ${info_i[3]}

 ## information into a logfile
 echo "${file:0:4} [${n_std% *} x STDEV = MEDIAN +- \
 $( gmtmath $file STD $n_std $gmt_format -Sl -f0T = ) ] \
 $rm_data from $in_data removed (${info_o[3]} rest) $txt" >> $log
 unset txt
 
## end loop - existing input
done
