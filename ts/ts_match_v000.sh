#!/bin/bash

#@
#@ USAGE  : ts_match.sh -m <master> -s <slave(s)> <options>
#@
#@ OPTIONS: -i <interpolant>
#@          -d - debug
#@          -h - help
#@          -v - verbose mode
#@
#@ TASK   : Match the epoch of slave time series to the master serie. 
#@
#@ NOTE   : - <slave(s)> could be multiple filenames separated by spaces 
#@            and use double quotes "slave_1 slave_2 ... slave_n".
#@          - The input slave time serie(s) have to have 
#@            number of columns: 2,3,4 or 7.
#@
#@ OUTPUT :
#@          - <filename>-matched.txt
#@            Is the matched output file, where <filename> is the 
#@            input filename without its extension.
#@
#* by     : marcell dot ferenc dot uni at gmail dot com

## root_dir
root_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

## source ts.conf
if [ ! -s $root_dir/ts.conf ]; then exit 2; else source $root_dir/ts.conf; fi

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
case "$#" in 0) usage $0 ;; esac

## test programs
test_prog gmtmath ts_conv_dt.sh paste awk grep
test_gmt5

## default variables
v=no
LC_NUMERIC=C ## for decimal separator in floating numbers
intp=c
intp_txt="cubic spline"
perc=0.8    ## common epoch limit: 80 percent of master records

## list of options the program will accept
optstring=i:m:p:s:w:dvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  m) master=$OPTARG ;;
  s) slaves=( $OPTARG ) ;;
  i) intp=$OPTARG ;;
  w) wdw=$OPTARG ;;
  d) set -x ;;
  v) v=yes ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

## test master --------------------------------------------------------------------------------------------------------
if [ -z "$master" ]; then color_txt bold red "$errm missing option: -m <master>"; exit 2; fi
if [[ ! -s $master || -d $master ]]; then color_txt bold red "$errm missing file: <$master>"; exit 2; fi
m_info=( $( is_tab $master ) )
case "${m_info[0]}" in 0) color_txt bold red "$errm wrong format: <$master>"; exit 2 ;; esac
case "${m_info[1]}" in 1|2|3|4|7) ;; *) color_txt bold red "$errm wrong format: <$master>"; exit 2 ;; esac
##---------------------------------------------------------------------------------------------------------------------

## test slaves --------------------------------------------------------------------------------------------------------
if [ -z "$slaves" ]; then color_txt bold red "$errm missing option: -s <slave(s)>"; exit 2; fi
for slave in ${slaves[@]}; do
 if [[ ! -s $slave || -d $slave ]]; then color_txt bold red "$errm missing file: <$slave>"; continue; fi
 s_info=( $( is_tab $slave ) )
 case "${s_info[0]}" in 0) color_txt bold red "$errm wrong format: <$slave>"; continue ;; esac
 case "${s_info[1]}" in 2|3|4|7) SLV=( "$slave" ) ;; *) color_txt bold red "$errm wrong format: <$slave>"; continue ;; esac
done
##---------------------------------------------------------------------------------------------------------------------

## get overlap period boundaries (narrowest time window) and truncate master according to this window -----------------
if [ ${#SLV[@]} -eq 0 ]; then color_txt bold red "$errm no valid input"; exit 2; fi
mp=${master%/*}; mn=${master##*/}; mw=$tmp.$mn
case $wdw in 
 narrow) bnd=( $(get_n_pd_bnd $master ${SLV[@]}) ); get_ts_pd $master ${bnd[0]} ${bnd[1]} > $mw ;;
 ## wide is the same as the master option for the moment. later on I will add an extended time reference according to these wide bounds
 ## in this case master need to be verified
 wide) bnd=( $(get_w_pd_bnd $master ${SLV[@]}) ); get_ts_pd $master ${bnd[0]} ${bnd[1]} > $mw; extrap="--GMT_EXTRAPOLATE_VAL=extrap" ;;
 master|*) /bin/cp -f $master $mw; extrap="--GMT_EXTRAPOLATE_VAL=extrap" ;;
esac
## set parameters according to master ---------------------------------------------------------------------------------
## collect time information of master file: start and end in iso and yr format, increment in iso format
ts_conv_dt.sh -f $mw -o yr
m_beg_yr=$( awk 'NR == 1 { print $1 }' $mw )
m_end_yr=$( awk 'END { print $1 }' $mw )
awk '{ if (NR > 1) print $1 - last; last=$1; }' $mw | gmtmath STDIN LOWER -Sl = $tmp.inc_yr
ts_conv_dt.sh -f $mw -o iso
m_beg_iso=$( awk 'NR == 1 { print $1 }' $mw )
m_end_iso=$( awk 'END { print $1 }' $mw )
info=( ${m_info[@]} )
m_info=( $( def_sampling $mw ) )
inc=${m_info[0]}
u_str=${m_info[1]}
u_chr=${m_info[2]}
m_match=${mn//-matched*/}-matched.txt
m_match=$mp/$m_match
min_comep=$( gmtmath -Q $perc ${info[3]} MUL FLOOR = )
case "$v" in yes) echo "$infm master: <$master>" ;; esac
awk '{ print $1 }' $mw > $mw.t
## master knotfile
/bin/cp -f $mw.t $tmp.knotfile
#IFS=$'\n' read -d '' -r -a _TMP_MASTER_TIME_ < <( awk '{ print $1 }' $mw )
##---------------------------------------------------------------------------------------------------------------------

## test interpolant ---------------------------------------------------------------------------------------------------
case $intp in
  a|akima) intp=a; intp_txt="akima spline" ;;
  c|cubic) intp=c; intp_txt="cubic spline" ;;
 l|linear) intp=l; intp_txt="linear" ;;
        *) intp=c; intp_txt="cubic spline";;
esac
##----------------------------------------------------------------------------------------------------------------------

## information
color_txt bold blue "$infm master: $inc $u_str in <$master>"
if [ "$v" == "yes" ]; then echo "$infm filter, interpolate, truncate"; fi

## count input files
cnt=0

fmt_dte_in="--FORMAT_DATE_IN=yyyy-mm-dd"
fmt_clk_in="--FORMAT_CLOCK_IN=hh:mm:ss"
time_unit="--TIME_UNIT=$u_chr"
##------------------------------------------------------------------------------

## loop - slave
for slave in ${SLV[@]}; do

 spath=${slave%/*}    # slave path
 sname=${slave##*/}   # slave name
 swrkv=$tmp.$sname    # slave work version
 #set_col $slave > $swrkv
 /bin/cp -f $slave $swrkv
 ts_conv_dt.sh -f $swrkv -o iso -v
 smatch=${slave//-matched*/}-matched.txt
 sinfo=( $( def_sampling $swrkv ) )
 color_txt bold blue "${0##*/}: slave info: ${sinfo[0]} ${sinfo[1]}  in <$slave>"
 s_tab_info=( $( is_tab $swrkv ) )

 ## if dt_slave is greater or equal to dt_master: an interpolation is applied
 if [[ ${sinfo[0]} -eq ${m_info[0]} && "${sinfo[1]}" == "${m_info[1]}" ]]; then
  color_txt bold blue "$infm same sampling: dt_slave = dt_master [S:${sinfo[0]} = M:${m_info[0]}]"
  color_txt bold blue "$infm compare epoch: t_slave vs. t_master"
  comm -12 $mw.t <(  awk '{ print $1 }' $swrkv ) > $tmp.comep
  if [ $( wc -l < $tmp.comep ) -ge $min_comep ]; then
   color_txt bold green "${0##*/}: already matched: at least <$min_comep>"
   awk -v dte="$m_beg_iso" '$1>=dte { print $0 }' $swrkv | awk -v dte="$m_end_iso" '$1<=dte { print $0 }' > $swrkv.slave
   #grep -f $mw.t $swrkv > $swrkv.slave
   awk '{ print $1 }' $swrkv.slave > $swrkv.t
   comeps+=( $swrkv.t )
   fmatch+=( $swrkv.slave )
   unset method; continue
  fi
  color_txt bold blue "${0##*/}: intpol slave: epoch are shifted"; method="s"
 elif [ ${sinfo[0]} -ge ${m_info[0]} ]; then color_txt bold blue "$infm intpol slave: dt_slave >= dt_master [S:${sinfo[0]} > M:${m_info[0]}]"; method="s"
 ## if dt_slave is less than dt_master: a filter is applied
 elif [ ${sinfo[0]} -lt ${m_info[0]} ]; then color_txt bold blue "$infm filter slave: dt_slave < dt_master [S:${sinfo[0]} < M:${m_info[0]}]"; method="f"
 else color_txt bold red "${0##*/}: error: dt_slave ? dt_master [S:${sinfo[0]} ? M:${m_info[0]}]"; unset method; continue; fi

 ## +/- 3days to be sure we use the maximum from the overlap for the slave
 awk -v dte=$( date --date="$m_beg_iso 3 days ago" "+%Y-%m-%dT%H:%M:%S" ) '$1>=dte { print $0 }' $swrkv | awk -v dte=$( date --date="$m_end_iso 3 days" "+%Y-%m-%dT%H:%M:%S" ) '$1<=dte { print $0 }' > $swrkv.slave
 
 ## slave have less data during the common period due to its sampling frequency
 if [ $( wc -l < $swrkv.slave ) -lt 2 ]; then color_txt bold red "$errm intpol: not enough data"; continue; fi

 cnt=$(( $cnt + 1 ))
 
 if [ "$v" == "yes" ]; then echo "${0##*/}: $cnt - $sname"; fi

 ## test difference between master and slave epoch
 diff_info=( $( isdiff $mw $swrkv.slave ) )
  
 ## there is no difference: continue
 if [ "${diff_info[0]}" == "no_diff" ]; then          
  ## final match: match all time series (common epoch to all)
  awk '{ print $1 }' $swrkv.slave > $swrkv.t
  comeps+=( $swrkv.t )
  fmatch+=( $swrkv.slave )
  unset $method
  continue
 fi 
 
 ##------------------------------------------------------------------------------
 ## there is difference: interpolation will be applied
 ##------------------------------------------------------------------------------
 if [ "$method" == "s" ]; then

  color_txt bold blue "${0##*/}: <$slave>: $intp_txt interpolation: t_slave !=  t_master"
  
  sample1d $swrkv.slave -F$intp$inc -N$tmp.knotfile -f0T $fmt_dte_in $fmt_clk_in $time_unit $extrap > $swrkv.slave.out
   
  ## test difference between master and slave epoch after interpolation
  samp_diff_info=( $( isdiff $mw $swrkv.slave.out ) )
   
  awk '{ print $1 }' $swrkv.slave.out > $swrkv.t

  ## there is no difference after interpolation
  if [ "${samp_diff_info[0]}" == "no_diff" ]; then
   ## final match: match all time series (common epoch to all)
   comeps+=( $swrkv.t )
   fmatch+=( $swrkv.slave.out )
  elif [ "${samp_diff_info[0]}" == "is_diff" ]; then

   ## see common epoch
   comm $swrkv.t $tmp.knotfile -1 -2 > $swrkv.comep
   if [ $( wc -l < $swrkv.comep ) -ge $min_comep ]; then
     
    ## common epoch files
    comeps+=( $swrkv.comep )
    fmatch+=( $swrkv.slave.out )
     
   ## there are not enough common parts of the two files
   else
    /bin/rm -f $swrkv.comep $swrkv.t $swrkv.slave.out
    color_txt bold red "${0##*/}: error: intpol failed: <$swrkv>"
   fi
  fi
  
 ##------------------------------------------------------------------------------
 ## there is difference: filtering will be applied
 ##------------------------------------------------------------------------------
 elif [ "$method" == "f" ]; then

  color_txt bold blue "${0##*/}: filter: t_slave != t_master"
  
  filter1d $swrkv.slave -FM$inc -E -T$m_beg_iso/$m_end_iso/$inc -N0 -f0T $fmt_dte_in $fmt_clk_in $time_unit $extrap | sample1d -Fc$inc -N$tmp.knotfile -f0T $fmt_dte_in $fmt_clk_in $time_unit $extrap > $swrkv.slave.out

   ## test difference between master and slave epoch after filtering
   filt_diff_info=( $( isdiff $master $swrkv.slave.out ) )
   awk '{ print $1 }' $swrkv.slave.out > $swrkv.t
   
   ## there is no difference after filtering
   if [ "${filt_diff_info[0]}" == "no_diff" ]; then
    #color_txt bold green "${0##*/}: <$smatch>: t_slave == t_master"
    comeps+=( $swrkv.t )
    fmatch+=( $swrkv.slave.out )
   elif [ "${filt_diff_info[0]}" == "is_diff" ]; then
    ## see common epoch
    comm $swrkv.t $tmp.knotfile -1 -2 > $swrkv.comep
    if [ $( wc -l < $swrkv.comep ) -ge $min_comep ]; then
     ## common epoch files
     comeps+=( $swrkv.comep )
     fmatch+=( $swrkv.slave.out )
    ## there are not enough common parts of the two files
    else
     /bin/rm -f $swrkv.comep $swrkv.t $swrkv.slave.out
     if [ "$v" == "yes" ]; then
      color_txt bold red "${0##*/}: error: filter failed"
     fi
    fi
   fi 
 fi
## end loop - slave
done

##------------------------------------------------------------------------------
## final match: match all time series (common epoch to all)
##------------------------------------------------------------------------------
## exit if there is no serie to match
if [[ ${#comeps[@]} -eq 0 || ${#fmatch[@]} -eq 0 ]]; then
 color_txt bold red "${0##*/}: error: no series to match"; exit 2; fi

## print filename that has the fewest line 
comep=$( awk 'NR == 1 || $1 < min {line = $2; min = $1}END{print line}' <( wc -l ${comeps[@]} ) )
for strunc in ${fmatch[@]}; do
 sout=${strunc#$tmp.}
 sout=${sout%.slave*}
 sout=$mp/${sout//-matched*/}-matched.txt
 ovlisof $comep $strunc $sout
 color_txt bold green "${0##*/}: matched S: <$sout>: t_slave == t_master"
done
ovlisof $comep $mw $mmatch
color_txt bold green "${0##*/}: matched M: <$mmatch>: t_slave == t_master"
##------------------------------------------------------------------------------
