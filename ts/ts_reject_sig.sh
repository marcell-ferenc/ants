#!/bin/bash

#@
#@ USAGE  : ts_reject_sig.sh -x <xsig> -y <ysig> -z <zsig> -f <tserie> <options>
#@           or
#@          ts_reject_sig.sh -l                            -f <tserie> <options>
#@
#@ OPTIONS: -l - autolimit on: 2 x median_of_sig  [default: off]
#@          -d - debug                            [default: off]
#@          -h - help                             [default: off]
#@          -v - verbose mode                     [default: off]
#@
#@ TASK   : Remove records with higher formal errors than the provided tresholds
#@          Do the cleaning only if 80 percent of the records would rest 
#@
#@ NOTE   : - <tserie> serie(s) strictly have to have 7 columns:
#@            [TIME] [X] [x_sig] [Y] [y_sig] [Z] [z_sig]
#@          - <tserie> could be multiple filenames separated by spaces 
#@            and use double quotes "tserie_1 tserie_2 ... tserie_n"
#@
#@ OUTPUT : - <tserie>-cleaned.txt
#@          - ts_reject_sig.log
#@            this is a logfile which contains how many record was removed
#@
#* by     : marcell.ferenc@cnam.fr
#*

## test GMT installation
type gmtmath &> /dev/null
if [ $? -ne 0 ]; then echo "GMT is needed for $0 script"; exit 2; fi

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## temporary filename
tmp=$scriptname.$$.$RANDOM.$( date "+%Y%m%d_%H%M%S" )
tmp_dir=/tmp/$tmp && mkdir -p -m 700 $tmp_dir
tmp=$tmp_dir/${tmp%%.*}

## cleaning
clean()
{
 /bin/rm -rf $tmp_dir .gmt* gmt.conf gmt.history 2> /dev/null
 ## restore default value 
 trap EXIT; exit
}

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
. $HOME/bin/func/general.func
. $HOME/bin/func/ts.func

## help
if [ "$#" -eq 0 ]; then usage $0; exit; fi

## default variable
log=$scriptname.log
v=no
LC_NUMERIC=C ## for decimal separator in floating numbers
autolimit=no

## list of options the program will accept
optstring=f:x:y:z:ldvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  d) set -x ;;
  x) xs=$OPTARG ;;
  y) ys=$OPTARG ;;
  z) zs=$OPTARG ;;
  l) autolimit=yes ;;
  f) files=( $OPTARG ) ;;
  v) v=yes ;;
  h) usage $0; exit ;;
 esac
done
shift "$(( $OPTIND - 1 ))"

## test limits
if [ "$autolimit" == "yes" ]; then :
elif [[ -z "$xs" || -z "$ys" || -z "$zs" ]]; then
 color_txt bold red "-x <xs> -y <ys> -z <zs> or -l options are required"; exit 2
elif [[ "$( isnum $xs )" == "no_number" \
|| "$( isnum $ys )" == "no_number" \
|| "$( isnum $zs )" == "no_number" ]]; then
 color_txt bold red "sigma value error"; exit 2; fi

## test files
if [ -z "$files" ]; then
 color_txt bold red "-f [file(s)] option is required"; exit 2
else
 for file in ${files[@]}; do
  if [ -s $file ]; then
   info=( $( istab $file ) )
   if [ "${info[0]}" == "is_data_table" ]; then
    case "${info[1]}" in
     7) is_item+=( "$file" )
        [ "$v" == "yes" ] && color_txt bold green "$file - exist" ;;
     *) no_item+=( "$file" )
        [ "$v" == "yes" ] && color_txt bold red "$file - format error" ;; 
    esac
   else
    no_item+=( "$file" )
    [ "$v" == "yes" ] && color_txt bold red "$file - not a data table"; fi
  else
   no_item+=( "$file" )
   [ "$v" == "yes" ] && color_txt bold red "$file - does not exist or empty"; fi
  if [ -z "$is_item" ]; then
  color_txt bold red "not accepted input: ${no_item[@]}" ; exit 2; fi
 done  
fi

## remove logfile if exists due to previous run
/bin/rm -f $log 2> /dev/null

gmt_vers=$( sample1d &> $tmp.gmt-version; \
awk 'NR==1 { print substr($2,1,1) }' $tmp.gmt-version )
case $gmt_vers in
 4) gmt_format=--D_FORMAT=%.10f ;;
 5) gmt_format=--FORMAT_FLOAT_OUT=%.10f ;;
esac

## loop - existing input
for file in ${is_item[@]}
do

 ## info of output data
 info_i=( $( istab $file ) ); if [ $? -eq 2 ]; then continue; fi
 min_data=$( gmtmath -Q ${info_i[3]} 0.8 MUL FLOOR = )

 ## set autolimit
 if [ "$autolimit" == "yes" ]; then
  xs=$( awk '{ print $3 }' $file | gmtmath STDIN MED 2 MUL -Sl $gmt_format = )
  ys=$( awk '{ print $5 }' $file | gmtmath STDIN MED 2 MUL -Sl $gmt_format = )
  zs=$( awk '{ print $7 }' $file | gmtmath STDIN MED 2 MUL -Sl $gmt_format = )
 fi
 
 ## cleaning according to formal error
 awk -v t="$xs" '$3<=t { print $0 }' $file \
 | awk -v t="$ys" '$5<=t { print $0 }' \
 | awk -v t="$zs" '$7<=t { print $0 }' > $tmp
 
 ## info of output data
 info_o=( $( istab $tmp ) )
 if [ $? -eq 2 ]; then
  color_txt bold red "$file - no cleaning (no remaining data)"
  info_o=( 0 0 0 0 ); fi
 removed=$(( ${info_i[3]} - ${info_o[3]} ))

 ## result file name
 out=${file//-cleaned*/}-cleaned.txt

 ## test number of data after removal
 if [[ ${info_i[3]} -ge $min_data && ${info_o[3]} -ge $min_data ]]
 then
  ## final output
  /bin/mv -f $tmp $out
  [ "$v" == "yes" ] && color_txt bold green "$file - cleaned"
 else
  [ "$v" == "yes" ] && color_txt bold red "$file - not cleaned"
  txt="(not cleaned)"
 fi

 ## format numbers for plotting
 printf -v rm_data "%5d" $removed; printf -v in_data "%5d" ${info_i[3]}
 
 ## information logfile
 echo "${file:0:4} [x: $xs y: $ys z: $zs] \
 $rm_data from $in_data removed (${info_o[3]} rest) $txt" >> $log
 unset txt
 
## end loop - existing input
done
