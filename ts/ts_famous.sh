#!/bin/bash

#@
#@ USAGE  : ts_famous.sh -f <tserie> -x <x_col> -y <y_col> <options>
#@
#@ OPTIONS:
#@          -p <period>   - print out amplitude of fixed periods [an, sa or an/sa]
#@          -o            - only search for these periods
#@          -n <num_freq> - number of frequency lines
#@          -v            - verbose
#@          -h            - help
#@
#@ TASK   : Generate "setting.txt" driver file and run FAMOUS.
#@          
#@ [1] - INPUT_FILE
#@ [2] - column number of TIME
#@ [3] - column number of MEASUREMENT
#@ [4] - numfreq: number of lines to project the signal on
#@ [5] - flmulti: true  - search for a signal representation on numfreq different spectral lines with unrelated frequencies
#@                false - search for a periodic signature, numfreq=2
#@ [6] - flauto : true  - the program determines the lowest and highest frequency recoverable
#@                false - the user provides the lowest and highest frequency for the range of the frequency search
#@ [7] - fltime : true  - the program centers the time data about its mean value
#@                false - the time offset is provided by the user
#@
#@ IF flauto or fltime = false a famous_inf.dat file have to exist in the current directory
#@ which contains the following values
#@                                       
#@       frbeg - lower frequency     e.g.:    frbeg 1
#@       frend - highest frequency   e.g.:    frend 100
#@       tzero - time offset         e.g.:    tzero 2010.000000
#@
#@ NOTE   :
#@
#* by     : marcell.ferenc@cnam.fr or marcell.ferenc.uni@gmail.com
#*

## root_dir
root_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

## source ts.conf
if [ ! -s $root_dir/ts.conf ]; then exit 2; else source $root_dir/ts.conf; fi

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## source functions
. $HOME/bin/func/general.func
. $HOME/bin/func/ts.func

## help
case "$#" in 0) usage $0; exit ;; esac

## default variable
nf=3
fout=$scrn.txt

## list of options the program will accept
optstring=f:p:x:y:n:ovh

## interpret options
while getopts $optstring opt
do
 case $opt in
  f) ts=$OPTARG ;;
  p) pr=$OPTARG ;;
  o) op=$pr ;;
  x) xc=$OPTARG ;;
  y) yc=$OPTARG ;;
  n) nf=$OPTARG ;;
  v) v=yes; vflag=-v ;;
  h) usage $0; exit ;;
 esac
done
shift "$(( $OPTIND - 1 ))"

## test time serie -------------------------------------------------------------
if [ -z "$ts" ]; then
 color_txt bold red "${0##*/}: error: missing option: -f <ts>"; exit 2; fi

if [[ ! -s $ts || ! -r $ts || -d $ts ]]; then
 color_txt bold red "${0##*/}: error: missing input: <$ts>"; exit 2; fi
 
info=( $( istab $ts ) )
if [ "${info[0]}" != "is_data_table" ]; then
 color_txt bold red "${0##*/}: error: wrong format: <$ts>"; exit 2
elif [[ ${info[1]} -eq 1 || ${info[3]} -eq 1 ]]; then
 color_txt bold red "${0##*/}: error: wrong format: <$ts>"; exit 2; fi
##------------------------------------------------------------------------------

## test x and y columns --------------------------------------------------------
if [ -z "$xc" ]; then
 color_txt bold red "${0##*/}: error: missing option: -x <x_col>"; exit 2; fi

if [ -z "$yc" ]; then
 color_txt bold red "${0##*/}: error: missing option: -y <y_col>"; exit 2; fi

if [ $( isint $xc &> /dev/null; echo $? ) -ne 0 ]; then
 color_txt bold red "${0##*/}: warning: x_col <$xc> set to default: 1"
 xc=1; fi
 
if [ $( isint $yc &> /dev/null; echo $? ) -ne 0 ]; then
 color_txt bold red "${0##*/}: warning: y_col <$yc> set to default: 2"
 yc=2
fi

if [[ $xc -ge $yc || $yc -gt ${info[2]} ]]; then
 color_txt bold red \
 "${0##*/}: warning: x_col: <$xc> y_col: <$yc> set to default: x:1, y:2"
 xc=1
 yc=2
fi 
##------------------------------------------------------------------------------

## create working version of input file
tsn=${ts##*/}     ## tserie name
tsp=${ts%/*}      ## tserie path
tsw=$tmp_dir/$tsn ## tserie work version

## this step is necessary in the case the time serie has a NaN column
## e.g. time series after <ts_match.sh> script. without this step FAMOUS would
## fail even we define the x and y column if the y column is after a NaN one. 
awk -v x=$xc -v y=$yc '{ print $x,$y }' $ts > $tsw

## convert into decimal year format for FAMOUS
ts_conv_dt.sh -f $tsw -o yr $vflag
tzero=$( awk 'NR == 1 { print $1 }' $tsw )

if [ $( isint $nf &> /dev/null; echo $? ) -ne 0 ]; then
 color_txt bold red "${0##*/}: warning: num_freq is not integer <$nf>, set to 3"
 nf=3; fi

numfreq=$nf
flmulti=".true."
flauto=".true."
fltime=".true."
frbeg=0
frend=1
treshold_sn=3.5
periodogram=".true."
iprint=1
iresid=1
flagdeg=".false."
idm=0

## test period -----------------------------------------------------------------
case $op in
 an/sa) flauto=".false."; frbeg=0.9; frend=2.2 ;;
    an) flauto=".false."; frbeg=0.9; frend=1.2 ;;
    sa) flauto=".false."; frbeg=1.9; frend=2.2 ;;
     *) ;;
esac
##------------------------------------------------------------------------------

cd $tmp_dir
{
printf "%-30.30s\n" \'$tsn\'
printf "%3d\n" 1
printf "%3d\n" 2
printf "%-30.30s\n" \'$fout\'
printf "%d\n" $numfreq
printf "%s\n" $flmulti
printf "%s\n" $flauto
printf "%f\n" $frbeg
printf "%f\n" $frend
printf "%s\n" $fltime
printf "%f\n" $tzero
printf "%f\n" $treshold_sn
printf "%s\n" $periodogram
printf "%d\n" $iprint
printf "%d\n" $iresid
printf "%s\n" $flagdeg
printf "%d\n" $idm
echo " 1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0\
  0  0  0  0  0  0  0  ideg(k) k=0..numf  loaded if flagdeg = .false."
} > setting.txt

## run FAMOUS
famous.bin

## copy results to start directory
mv power* resid* $fout $sdir/.

cd $sdir
get_famous_res $fout | tee $tmp.res
case $pr in
 an/sa) echo an $( awk '$3 >= 0.9 && $3 <= 1.1 { printf "%7.1f\n",$10 }' $tmp.res )
        echo sa $( awk '$3 >= 1.9 && $3 <= 2.2 { printf "%7.1f\n",$10 }' $tmp.res ) ;;
    an) echo an $( awk '$3 >= 0.9 && $3 <= 1.1 { printf "%7.1f\n",$10 }' $tmp.res ) ;;
    sa) echo sa $( awk '$3 >= 1.9 && $3 <= 2.2 { printf "%7.1f\n",$10 }' $tmp.res ) ;;
     *) ;;
esac
