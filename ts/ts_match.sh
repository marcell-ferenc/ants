#!/bin/bash

#@
#@ USAGE  : ts_match.sh -m <master> -s <slave(s)> <options>
#@
#@ OPTIONS: -i <interpolant>
#@          -p <path_out>
#@          -d - debug
#@          -h - help
#@          -v - verbose mode
#@
#@ TASK   : Match the epoch of slave time series to the master serie. 
#@
#@ NOTE   : - <slave(s)> could be multiple filenames separated by spaces 
#@            and use double quotes "slave_1 slave_2 ... slave_n".
#@          - The input time serie(s) have to have 
#@            number of columns: 2,3,4 or 7.
#@
#@ OUTPUT :
#@          - <filename>-matched.txt
#@            Is the matched output file, where <filename> is the 
#@            input filename without its extension.
#@
#* by     : marcell.ferenc@cnam.fr or marcell.ferenc.uni@gmail.com

## root_dir
root_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

## source ts.conf
if [ ! -s $root_dir/ts.conf ]; then exit 2; else source $root_dir/ts.conf; fi

## set trap for signals
trap clean EXIT HUP INT QUIT TERM

## help
case "$#" in 0) usage $0 ;; esac

## test programs
test_prog gmtmath ts_conv_dt.sh paste awk grep
test_gmt5

## default variables
v=no
LC_NUMERIC=C ## for decimal separator in floating numbers
intp=c
intp_txt="cubic spline"
perc=0.8    ## common epoch limit: 80 percent of master records

## list of options the program will accept
optstring=i:m:p:s:dvh

## interpret options
while getopts $optstring opt
do
 case $opt in
  m) master=$OPTARG ;;
  s) slaves=( $OPTARG ) ;;
  i) intp=$OPTARG ;;
  d) set -x ;;
  v) v=yes ;;
  h) usage $0; exit ;;
 esac
done

shift "$(( $OPTIND - 1 ))"

##------------------------------------------------------------------------------
## test path out
##------------------------------------------------------------------------------
if [ -n "$path_out" ]; then
 if [ ! -d "$path_out" ]; then
  echo "${0##*/}: error: missing dir: <$path_out>"; exit 2; fi
 path_out=${path_out%/}
fi
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## test master
##------------------------------------------------------------------------------
if [ -z "$master" ]; then
 color_txt bold red "-m <master> option is required"; exit 2; fi

if [ ! -s $master ]; then
 color_txt bold red "${0##*/}: error: missing master <$master>"; exit 2; fi
 
info=( $( is_tab $master ) )
case "${info[0]}" in 0) color_txt bold red "${0##*/}: error: wrong master format <$master>"; exit 2;; esac
   
case "${info[1]}" in
 2|3|4|7) mpath=${master%/*}   # master path
          if [ ! -d "$mpath" ]; then mpath=$( pwd ); fi
          path_out=${path_out:-$mpath}
          mname=${master##*/}  # master name
          mwrkv=$tmp.$mname    # master work version
          set_col $master > $mwrkv
          is_item+=( "$mwrkv" )
          ## collect time information of master file
          ## start and end in iso and yr format, increment in iso format
          ts_conv_dt.sh -f $mwrkv -o yr
          m_beg_yr=$( awk 'NR == 1 { print $1 }' $mwrkv )
          m_end_yr=$( awk 'END { print $1 }' $mwrkv )
          awk '{ if (NR > 1) print $1 - last; last=$1; }' $mwrkv \
          | gmtmath STDIN LOWER -Sl = $tmp.inc_yr
          ts_conv_dt.sh -f $mwrkv -o iso
          m_beg_iso=$( awk 'NR == 1 { print $1 }' $mwrkv )
          m_end_iso=$( awk 'END { print $1 }' $mwrkv )
          minfo=( $( def_sampling $mwrkv ) )
          inc=${minfo[0]}
          ustr=${minfo[1]}
          uchr=${minfo[2]}
          mmatch=${mname//-matched*/}-matched.txt
          mmatch=$path_out/$mmatch
          min_comep=$( gmtmath -Q $perc ${info[3]} MUL FLOOR = )
          if [ "$v" == "yes" ]; then
           color_txt bold green "${0##*/}: master exists <$master>"; fi ;;
       1) mpath=${master%/*}   # master path
          if [ ! -d "$mpath" ]; then mpath=$( pwd ); fi
          path_out=${path_out:-$mpath}
          mname=${master##*/}  # master name
          mwrkv=$tmp.$mname    # master work version
          /bin/cp -f $master $mwrkv
          is_item+=( "$mwrkv" )
          ## collect time information of master file
          ## start and end in iso and yr format, increment in iso format
          ts_conv_dt.sh -f $mwrkv -o yr
          m_beg_yr=$( awk 'NR == 1 { print $1 }' $mwrkv )
          m_end_yr=$( awk 'END { print $1 }' $mwrkv )
          awk '{ if (NR > 1) print $1 - last; last=$1; }' $mwrkv \
          | gmtmath STDIN LOWER -Sl = $tmp.inc_yr
          ts_conv_dt.sh -f $mwrkv -o iso
          m_beg_iso=$( awk 'NR == 1 { print $1 }' $mwrkv )
          m_end_iso=$( awk 'END { print $1 }' $mwrkv )
          minfo=( $( def_sampling $mwrkv ) )
          inc=${minfo[0]}
          ustr=${minfo[1]}
          uchr=${minfo[2]}
          mmatch=${mname//-matched*/}-matched.txt
          mmatch=$path_out/$mmatch
          min_comep=$( gmtmath -Q $perc ${info[3]} MUL FLOOR = )
          if [ "$v" == "yes" ]; then
           color_txt bold green "${0##*/}: master exists <$master>"; fi ;;
       *) color_txt bold red "${0##*/}: error: wrong master format <$master>"
          exit 2 ;; 
esac
awk '{ print $1 }' $mwrkv > $mwrkv.t
#IFS=$'\n' read -d '' -r -a _TMP_MASTER_TIME_ < <( awk '{ print $1 }' $mwrkv )
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## test slaves
##------------------------------------------------------------------------------
if [ -z "$slaves" ]; then
 color_txt bold red "${0##*/}: error: missing option: -s <slave(s)>"
 exit 2; fi

for slave in ${slaves[@]}; do
 if [ ! -s $slave ]; then
  color_txt bold red "${0##*/}: error: missing slave: <$slave>"; continue; fi
 
 info=( $( is_tab $slave ) )
 case "${info[0]}" in 0) color_txt bold red "${0##*/}: error: wrong format: <$slave>"; continue; fi;; esac
   
 case "${info[1]}" in
  2|3|4|7) is_item+=( "$slave" )
           if [ "$v" == "yes" ]; then
            color_txt bold green "${0##*/}: slave exists: <$slave>"; fi ;;
        *) color_txt bold red "${0##*/}: error: wrong format: <$slave>"
           continue ;; 
 esac
done

if [ ${#is_item[@]} -lt 2 ]; then
 color_txt bold red "${0##*/}: error: not enough input <$is_item[@]>"
 exit 2; fi
##------------------------------------------------------------------------------

##------------------------------------------------------------------------------
## test interpolant
##------------------------------------------------------------------------------
case $intp in
  a|akima) intp=a; intp_txt="akima spline" ;;
  c|cubic) intp=c; intp_txt="cubic spline" ;;
 l|linear) intp=l; intp_txt="linear" ;;
        *) intp=c; intp_txt="cubic spline";;
esac
##------------------------------------------------------------------------------

## create master knotfile
awk '{ print $1 }' $master > $tmp.knotfile

## information
color_txt bold blue "${0##*/}: master info: $inc $ustr in <$master>"
if [ "$v" == "yes" ]; then
 color_txt bold blue "${0##*/}: filter, interpolate, truncate"; fi

## count input files
cnt=0

##------------------------------------------------------------------------------
## GMT version
##------------------------------------------------------------------------------
gmt_vers=$( sample1d 2> $tmp.gmt-version; \
awk 'NR==1 { print substr($2,1,1) }' $tmp.gmt-version )
case $gmt_vers in
 4) fmt_dte_in=INPUT_DATE_FORMAT
    fmt_clk_in=INPUT_CLOCK_FORMAT
    u=$uchr ;;
 5) fmt_dte_in=FORMAT_DATE_IN
    fmt_clk_in=FORMAT_CLOCK_IN
    u= ;;
 *) exit 2 ;;
esac
##------------------------------------------------------------------------------

## loop - slave
for slave in ${is_item[@]:1}; do

 spath=${slave%/*}    # slave path
 sname=${slave##*/}   # slave name
 swrkv=$tmp.$sname    # slave work version
 set_col $slave > $swrkv
 ts_conv_dt.sh -f $swrkv -o iso
 smatch=${slave//-matched*/}-matched.txt
 sinfo=( $( def_sampling $swrkv ) )
 color_txt bold blue \
 "${0##*/}: slave info: ${sinfo[0]} ${sinfo[1]}  in <$slave>"
 s_tab_info=( $( is_tab $swrkv ) )

 ## if dt_slave is greater or equal to dt_master: an interpolation is applied
 if [[ ${sinfo[0]} -eq ${minfo[0]} && "${sinfo[1]}" == "${minfo[1]}" ]]; then
  color_txt bold blue \
  "${0##*/}: same sampling: dt_slave = dt_master [S:${sinfo[0]} = M:${minfo[0]}]"
  color_txt bold blue \
  "${0##*/}: compare epoch: t_slave vs. t_master"
  comm -12 $mwrkv.t <(  awk '{ print $1 }' $swrkv ) \
  > $tmp.comep
  if [ $( wc -l < $tmp.comep ) -ge $min_comep ]; then
   color_txt bold green \
  "${0##*/}: already matched: at least <$min_comep>"
   awk -v dte="$m_beg_iso" '$1>=dte { print $0 }' $swrkv \
   | awk -v dte="$m_end_iso" '$1<=dte { print $0 }' > $swrkv.slave
   #grep -f $mwrkv.t $swrkv > $swrkv.slave
   awk '{ print $1 }' $swrkv.slave > $swrkv.t
   comeps+=( $swrkv.t )
   fmatch+=( $swrkv.slave )
   unset method; continue
  fi
  color_txt bold blue \
  "${0##*/}: intpol slave: epoch are shifted"
  method="s"
 elif [ ${sinfo[0]} -ge ${minfo[0]} ]; then
  color_txt bold blue \
  "${0##*/}: intpol slave: dt_slave >= dt_master [S:${sinfo[0]} > M:${minfo[0]}]"
  method="s"
 elif [ ${sinfo[0]} -lt ${minfo[0]} ]; then
  ## if dt_slave is less than dt_master: a filter is applied
  color_txt bold blue \
  "${0##*/}: filter slave: dt_slave < dt_master [S:${sinfo[0]} < M:${minfo[0]}]"
  method="f"
 else
  color_txt bold red \
  "${0##*/}: error: dt_slave ? dt_master [S:${sinfo[0]} ? M:${minfo[0]}]"
  unset method; continue
 fi

 ## +/- 3days to be sure we use the maximum from the overlap for the slave
 awk -v dte=$( date --date="$m_beg_iso 3 days ago" "+%Y-%m-%dT%H:%M:%S" ) '$1>=dte { print $0 }' $swrkv \
 | awk -v dte=$( date --date="$m_end_iso 3 days" "+%Y-%m-%dT%H:%M:%S" ) '$1<=dte { print $0 }' > $swrkv.slave
 
 ## slave have less data during the common period due to its sampling frequency
 if [ $( wc -l < $swrkv.slave ) -lt 2 ]; then
  color_txt bold red \
  "${0##*/}: error: intpol: not enough data"; continue; fi

 cnt=$(( $cnt + 1 ))
 
 if [ "$v" == "yes" ]; then echo "${0##*/}: $cnt - $sname"; fi

 ## test difference between master and slave epoch
 diff_info=( $( isdiff $mwrkv $swrkv.slave ) )
  
 ## there is no difference: continue
 if [ "${diff_info[0]}" == "no_diff" ]; then          
  ## final match: match all time series (common epoch to all)
  awk '{ print $1 }' $swrkv.slave > $swrkv.t
  comeps+=( $swrkv.t )
  fmatch+=( $swrkv.slave )
  unset $method
  continue
 fi 
 
 ##------------------------------------------------------------------------------
 ## there is difference: interpolation will be applied
 ##------------------------------------------------------------------------------
 if [ "$method" == "s" ]; then

  color_txt bold blue \
  "${0##*/}: <$slave>: $intp_txt interpolation: t_slave !=  t_master"
  
  sample1d $swrkv.slave -F$intp${inc}$u -N$tmp.knotfile -f0T \
  --${fmt_dte_in}=yyyy-mm-dd \
  --${fmt_clk_in}=hh:mm:ss \
  --TIME_UNIT=$uchr > $swrkv.slave.out
   
  ## test difference between master and slave epoch after interpolation
  samp_diff_info=( $( isdiff $mwrkv $swrkv.slave.out ) )
   
  awk '{ print $1 }' $swrkv.slave.out > $swrkv.t

  ## there is no difference after interpolation
  if [ "${samp_diff_info[0]}" == "no_diff" ]; then
   ## final match: match all time series (common epoch to all)
   comeps+=( $swrkv.t )
   fmatch+=( $swrkv.slave.out )
  elif [ "${samp_diff_info[0]}" == "is_diff" ]; then

   ## see common epoch
   comm $swrkv.t $tmp.knotfile -1 -2 > $swrkv.comep
   if [ $( wc -l < $swrkv.comep ) -ge $min_comep ]; then
     
    ## common epoch files
    comeps+=( $swrkv.comep )
    fmatch+=( $swrkv.slave.out )
     
   ## there are not enough common parts of the two files
   else
    /bin/rm -f $swrkv.comep $swrkv.t $swrkv.slave.out
    color_txt bold red "${0##*/}: error: intpol failed: <$swrkv>"
   fi
  fi
  
 ##------------------------------------------------------------------------------
 ## there is difference: filtering will be applied
 ##------------------------------------------------------------------------------
 elif [ "$method" == "f" ]; then

  color_txt bold blue \
  "${0##*/}: filter: t_slave != t_master"
  
  case $gmt_vers in
   4) filter1d $swrkv.slave -FM${inc}$u -E -T$m_beg_iso/$m_end_iso/${inc}$u \
   -N${s_tab_info[2]}/0 -f0T \
   --${fmt_dte_in}=yyyy-mm-dd \
   --${fmt_clk_in}=hh:mm:ss \
   --TIME_UNIT=$uchr \
   | sample1d -Fc${inc}$u -N$tmp.knotfile -f0T \
   --${fmt_dte_in}=yyyy-mm-dd \
   --${fmt_clk_in}=hh:mm:ss \
   --TIME_UNIT=$uchr > $swrkv.slave.out ;;
    5) filter1d $swrkv.slave -FM${inc}$u -E \
    -T$m_beg_iso/$m_end_iso/${inc}$u -N0 -f0T \
    --${fmt_dte_in}=yyyy-mm-dd \
    --${fmt_clk_in}=hh:mm:ss \
    --TIME_UNIT=$uchr \
    | sample1d -Fc${inc}$u -N$tmp.knotfile -f0T \
    --${fmt_dte_in}=yyyy-mm-dd \
    --${fmt_clk_in}=hh:mm:ss \
    --TIME_UNIT=$uchr > $swrkv.slave.out ;;
   esac

   ## test difference between master and slave epoch after filtering
   filt_diff_info=( $( isdiff $master $swrkv.slave.out ) )
   awk '{ print $1 }' $swrkv.slave.out > $swrkv.t
   
   ## there is no difference after filtering
   if [ "${filt_diff_info[0]}" == "no_diff" ]; then
    #color_txt bold green "${0##*/}: <$smatch>: t_slave == t_master"
    comeps+=( $swrkv.t )
    fmatch+=( $swrkv.slave.out )
   elif [ "${filt_diff_info[0]}" == "is_diff" ]; then
    ## see common epoch
    comm $swrkv.t $tmp.knotfile -1 -2 > $swrkv.comep
    if [ $( wc -l < $swrkv.comep ) -ge $min_comep ]; then
     ## common epoch files
     comeps+=( $swrkv.comep )
     fmatch+=( $swrkv.slave.out )
    ## there are not enough common parts of the two files
    else
     /bin/rm -f $swrkv.comep $swrkv.t $swrkv.slave.out
     if [ "$v" == "yes" ]; then
      color_txt bold red "${0##*/}: error: filter failed"
     fi
    fi
   fi 
 fi
## end loop - slave
done

##------------------------------------------------------------------------------
## final match: match all time series (common epoch to all)
##------------------------------------------------------------------------------
## exit if there is no serie to match
if [[ ${#comeps[@]} -eq 0 || ${#fmatch[@]} -eq 0 ]]; then
 color_txt bold red "${0##*/}: error: no series to match"; exit 2; fi

## print filename that has the fewest line 
comep=$( awk 'NR == 1 || $1 < min {line = $2; min = $1}END{print line}' \
<( wc -l ${comeps[@]} ) )
for strunc in ${fmatch[@]}; do
 sout=${strunc#$tmp.}
 sout=${sout%.slave*}
 sout=$path_out/${sout//-matched*/}-matched.txt
 ovlisof $comep $strunc $sout
 color_txt bold green "${0##*/}: matched S: <$sout>: t_slave == t_master"
done
ovlisof $comep $mwrkv $mmatch
color_txt bold green "${0##*/}: matched M: <$mmatch>: t_slave == t_master"
##------------------------------------------------------------------------------
