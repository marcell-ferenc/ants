#!/bin/bash

## by: marcell.ferenc@cnam.fr or marcell.ferenc.uni@gmail.com

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## default variables
fncp=${0%/*}
fncs=( $( echo $fncp/*.func) )

## function name
fnc=$1

## test function name
if [ -z "$fnc" ]; then
 echo "${0##*/}: error: missing function name"; exit 2; fi

## iterate through the function files
for fncf in ${fncs[@]}; do
 grep "^${fnc}()" $fncf &> /dev/null
 if [ $? -ne 0 ]; then msg="unknown function: <$fnc>"; continue; fi
 grep -A30 "^${fnc}()" $fncf | sed '/^}/q' | grep TASK &> /dev/null
 if [ $? -ne 0 ]; then msg="no described task: <$fnc>"; break; fi
 #echo;
 echo TASK: $( grep -A30 "^${fnc}()" $fncf \
 | sed '/^}/q' | grep TASK | sed 's/^.*\ ://' )
 #echo;
 exit
done
## print out error message
if [ -n "$msg" ]; then echo "${0##*/}: error: $msg"; echo; exit 2; fi
