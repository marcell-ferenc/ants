#@ This directory contains some shell functions that are used by the 
#@ various scripts in this repository and three shell scripts which helps to
#@ explore them. They are namely: <func.sh>, <help.sh> and <task.sh>.

#@ <func.sh>: List all the available functions over the Function files.
#@ <help.sh>: Provide help for an individual function. 
#@ <task.sh>: Describe the task of an individual function.

#@ Function files:
#@ <general.func>
#@ <gnss.func>
#@ <gins.func>
#@ <gipsy.func>
#@ <ts.func>
