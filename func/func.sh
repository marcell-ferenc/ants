#!/bin/bash

## by: marcell.ferenc@cnam.fr or marcell.ferenc.uni@gmail.com

## scriptname
scriptname=${0##*/}
scriptname=${scriptname%.sh}

## default variables
fncp=${0%/*}
fncs=( $( echo $fncp/*.func) )

## source functions
. $fncp/general.func

## for separation on stdout
echo

## iterate through the function files
tcnt=0
for fncf in ${fncs[@]}; do
 cnt=$( grep -c \#func\# $fncf )
 color_txt bold red "in <${fncf##*/}> $cnt functions"
 tcnt=$(( $tcnt + $cnt ))
 f=( $( sed -n '/#func#/{n;p;}' ${fncf} | sed 's/()//g' ) )
 #for tsk in $( sed -n '/#func#/{n;p;}' ${fncf} | sed 's/()//g' ); do
 # task.sh $tsk
 #done
 printf "%s\n" ${f[@]}
done
echo
color_txt bold blue "Total: $tcnt functions"; echo
