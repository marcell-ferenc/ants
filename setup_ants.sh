#!/bin/bash

#* by: marcell.ferenc.uni@gmail.com

## WARNING: The merra codes need to be verified for the good netcdf library path
## WARNING: The merra and catref codes need to be manually compiled!
## WARNING: Simply go to the corresponding directory and type <make> and try to
## WARNING: solve the appearing problems manually.

## get the full directory name of the script
## no matter where it is being called from
root_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

## source directories
src_dirs=( \
src/date \
src/coord \
src/famous \
src/merra \
src/misc \
func \
ts \
gins \
gipsy \
catref/catref-esgt-xc \
catref/catref-scripts \
catref/time-series-tools/R \
catref/time-series-tools/matlab \
catref/time-series-tools/matlab/tsview \
catref/time-series-tools/sigfreq \
)

## install programs and append to path
for src_dir in ${src_dirs[@]}; do
 cd $root_dir/$src_dir
 
 ## generate path 
 case $src_dir in
  src/*) ;;
      *) path+=( $root_dir/$src_dir ) ;;
 esac
 
 ## install or skip
 case $src_dir in
  *catref*) continue ;;
   *merra*) continue ;;
         *) if [[ -s Makefile || -s makefile ]]; then make; fi ;;
 esac
done

## create a list to be added to the $PATH in <.bashrc>
## simply copy and paste the content of the <path.txt> file into <.bashrc>
cd $root_dir
printf "%s:\ \n" "export PATH=\$PATH" \
"$root_dir/bin" "${path[@]}" | tee path.txt

## remove the last unnecessary ":\" characters from the last line in path.txt
sed -i '$s/...$//' path.txt
